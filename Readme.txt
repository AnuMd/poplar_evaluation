1) Program execution:
move to the Model directory then:

$ python MainLogic.py
 
Insert input directory: test

PS. Other folders with test floorplans can be found in the folder Other_tests

2) Provided output
The textual output is saved in OUTPUT
one new subfolder is created in OUTPUT at each execution. The latter directory contains the outputs of the current execution.

the textual output has extension .txt 
with the following output

Example:
-----------------
1490;1902;
WALLS
1776.0;71.0;1803.0;71.0;1803.0;731.0;1776.0;731.0
...
1048.0;71.0;1176.0;71.0;1176.0;99.0;1048.0;99.0
DOORS
1670.0;254.0;1776.0;254.0;1776.0;268.0;1670.0;268.0
...
635.0;590.0;649.0;590.0;649.0;690.0;635.0;690.0
WINDOWS
1666.0;731.0;1692.0;731.0;1692.0;792.0;1666.0;792.0
...
1019.0;705.0;1075.0;705.0;1075.0;731.0;1019.0;731.0
-----------------
where:
1490;1902;
are the height and width of the image

1776.0;71.0;1803.0;71.0;1803.0;731.0;1776.0;731.0
are the four corners of the rectangular bounding box of the DOOR, WINDOW, or WALL (x1;y1;x2;y2;x3;y3;x4;y4)
PS. Stairs are still to be saved.

the graphical output is in the same subfolder with *.png extension 

RED are walls, YELLOW, windows, and GREEN the doors

OUTPUT_DEBUG contains again one subfolder for each execution and contains several debug files (perhaps not needed for integration)

3) Evaluation of results

In order to evaluate results of one execution a ground truth file must exist for each input floorplan. THe file has extension *.grt
and must be saved in the same folder of input images. For instance, the test folder contains groundtruth files for two images.

To measure the results we must execute the program Model/precisionAndRecall.py with two command line parameters: the folder containing
the input images and groundtruth files and the number of experiment performed on these data. 
For instance:

$ python precisionAndRecall.py test 1

At the end of execution one file is created with the results in the folder RESULTS_TEST 
the file contains Precision and Recall values for wall, window, and door identification according to the recognition and to the
groudtruth information 



