__author__ = 'stefano'

import numpy as np
import time
import random
import matplotlib.pyplot as plt
from scipy import ndimage
from franges import drange
from connectedcomponent import CC, bins1, bins2
from rectangle_in_cc import Rectangle
from operator import itemgetter
from itertools import *
import os
import cv2
import cv2.cv as cv
import re
import tesseract
import math
from graph import Graph
# from node import Node
from edge import Edge
from text_recognition import text_recogntion_class
from bs4 import BeautifulSoup
from Levenshtein import distance
from PIL import Image
import PIL.ImageOps
from graphCC import GraphCC
import imageOperations as imop
from room import Room
from line_optimization import line_optimization_class
from copy import deepcopy
import mathMethods as mm
import unicodedata
import shutil
from pre_process import pre_process_class
from iterative_functions import iterative_functions_class
from natsort import natsorted
from open_plan import planClass

#from joblib import Parallel, delayed
#import multiprocessing


# def processInput(i,labels):
#         #return i+1
#         return np.argwhere(labels == i)

class Image_class():
    def __init__(self,current_directory,f,path_output,path_debug,img,image_color):
        self.current_directory = current_directory
        self.list_cc = []
        self.width = 0
        self.height = 0
        self.file_name = f
        self.original_image = img
        #self.color_image = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        self.color_image = image_color
        self.path_debug = path_debug 
        self.path_output = path_output
        self.labels = None
        self.numlabels = 0
        self.list_cc = []
        self.list_cc_text = []
        self.list_rect = []
        self.image_rect = None
        self.max_tink_wall = 0
        self.image_run_pixel=None
        #self.list_rect_wall = []
        self.graph_rect = Graph()
        self.graph_wall = Graph()
        self.graph_image_output = Graph()
        self.numberDoors = 0
        self.list_door = []
        ###
        self.list_door_2 = []
        ###
        self.numberWindows = 0
        self.list_windows = []
        self.list_wwd=[]
        self.list_scale=[]
        self.dic_scale={}
        self.length_door = 0
        self.image_remove_text=np.array(img)
        h,w = img.shape
        self.final_image=np.zeros((h,w,3),dtype=np.uint8)
       # self.image_clean=None

        #Pierpaolo's:
        self.graph_cc = GraphCC()
        self.avg_width = 0
        self.img_walls_and_doors = None
        self.img_walls_and_doors_color = None
        self.rooms = []
        self.room_index = {}
        self.num_cc_inside = 0
        self.text_coordinates = []
       # self.image_clean=None


    def contatempo(fn):
        def interna(*args,**kw):
            start=time.time()
            result=fn(*args,**kw)
            tempo=time.time()-start
            # print "Function: %s - Execution time: %4.2f" % (fn.__name__,tempo)
            return result
        return interna

    def test(self):
        n=9


        # weights = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
        start = 0.0
        stop = 0.9
        step = 0.2
        weights = []
        while start<stop:

            start = start+step
            # print start
        for w1 in drange(0.7,1.0,0.1,6):
            print w1






    def find_connected_componet(self):
        # here convert in gray-scale becouse the function  ndimage.label
        img=self.original_image
        self.height,self.width=img.shape
        s1 = [[0, 1, 0], [1,1,1], [0,1,0]] # elemento strutturato
        s2 = [[1, 1, 1], [1,1,1], [1,1,1]]
        # plt.imshow(self.labels_background*10, cmap = 'gray', interpolation = 'bicubic')
        # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
        # plt.show()
        self.labels, self.numlabels = ndimage.label(img<127,s2)
        # print 'number of connected components: '+str(self.numlabels)
        s=self.file_name[0:-4]
        for i in range(1,self.numlabels+1):
            cc=CC(i)
            point= np.argwhere(self.labels == i)
            # find x_max, x_min, y_max, y_min
            cc.y_min=point[:,0].min()
            cc.x_min=point[:,1].min()
            cc.y_max=point[:,0].max()
            cc.x_max=point[:,1].max()
            cc.height= cc.y_max-cc.y_min+1
            cc.width= cc.x_max-cc.x_min+1
            # I create image of the connected compis an open image with black background and white lines, i want as output an image with white background and black linesonent
            cc.image=(np.where(self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)

            cc.numPixel=(cc.image.sum()/255)
            # add padding to cc.image
            h,w=cc.image.shape
            z1=np.zeros((1,w))
            z2=np.zeros((h+2,1))
            cc.image = np.concatenate((z1,cc.image),axis=0)
            cc.image = np.concatenate((cc.image,z1),axis=0)
            cc.image = np.concatenate((z2,cc.image),axis=1)
            cc.image = np.concatenate((cc.image,z2),axis=1)

            cc.image_dt_1 = ndimage.morphology.distance_transform_cdt(cc.image)
            cc.image_dt_2 = np.array(cc.image_dt_1)
            #cv2.imwrite(self.path_debug+'preprocessing/'+s+'/'+str(i)+'.png', np.abs(cc.image-255))
            # I add the connected component to the list
            self.list_cc.append(cc)


    def find_connected_componet_no_text(self):
        # lo aggiungo in quanto devo resettare le liste una volta estratto il testo
        del self.list_cc[:]
        img=self.image_remove_text
        self.height,self.width=img.shape
        s1 = [[0, 1, 0], [1,1,1], [0,1,0]] # elemento strutturato
        s2 = [[1, 1, 1], [1,1,1], [1,1,1]]
        # plt.imshow(self.labels_background*10, cmap = 'gray', interpolation = 'bicubic')
        # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
        # plt.show()
        self.labels, self.numlabels =ndimage.label(img<127,s2)
        s=self.file_name[0:-4]
        for i in range(1,self.numlabels+1):
            cc=CC(i)
            point= np.argwhere(self.labels == i)
            # find x_max, x_min, y_max, y_min
            cc.y_min=point[:,0].min()
            cc.x_min=point[:,1].min()
            cc.y_max=point[:,0].max()
            cc.x_max=point[:,1].max()
            cc.height= cc.y_max-cc.y_min+1
            cc.width= cc.x_max-cc.x_min+1
            #I create image of the connected compis an open image with black background and white lines, i want as output an image with white background and black linesonent
            cc.image=(np.where(self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
            cc.numPixel=(cc.image.sum()/255)
            #add padding to cc.image
            h,w=cc.image.shape
            z1=np.zeros((1,w))
            z2=np.zeros((h+2,1))
            cc.image=np.concatenate((z1,cc.image),axis=0)
            cc.image=np.concatenate((cc.image,z1),axis=0)
            cc.image=np.concatenate((z2,cc.image),axis=1)
            cc.image=np.concatenate((cc.image,z2),axis=1)
            cc.image_dist_tran=ndimage.morphology.distance_transform_cdt(cc.image)

            cc.image_dt_1 = ndimage.morphology.distance_transform_cdt(cc.image)
            cc.image_dt_2 = np.array(cc.image_dt_1)
            #cv2.imwrite(self.path_debug+'preprocessing/'+s+'/'+str(i)+'.png', np.abs(cc.image-255))
            # I add the connected component to the list
            self.list_cc.append(cc)



    def find_connected_componet_2(self):
        # add padding  original image
        self.height,self.width=self.original_image.shape
        img=self.original_image
        s1 = [[0, 1, 0], [1,1,1], [0,1,0]] # elemento strutturato
        s2 = [[1, 1, 1], [1,1,1], [1,1,1]]
        self.labels, self.numlabels =ndimage.label(img<127,s2)
        for i in range(1,self.numlabels+1):
            cc=CC(i)
            point= np.argwhere(self.labels == i)
            if len(point)>0:
                cc.y_min=point[:,0].min()
                cc.x_min=point[:,1].min()
                cc.y_max=point[:,0].max()
                cc.x_max=point[:,1].max()
                cc.height= cc.y_max-cc.y_min+1
                cc.width= cc.x_max-cc.x_min+1
                # I create image of the connected compis an open image with black background and white lines, i want as output an image with white background and black linesonent
                cc.image=(np.where(self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
                cc.numPixel=(cc.image.sum()/255)
                #add padding to cc.image
                h,w=cc.image.shape
                z1=np.zeros((1,w))
                z2=np.zeros((h+2,1))
                cc.image=np.concatenate((z1,cc.image),axis=0)
                cc.image=np.concatenate((cc.image,z1),axis=0)
                cc.image=np.concatenate((z2,cc.image),axis=1)
                cc.image=np.concatenate((cc.image,z2),axis=1)
                cc.image_dist_tran = ndimage.morphology.distance_transform_cdt(cc.image)
                cc.image_dt_1 = ndimage.morphology.distance_transform_cdt(cc.image)
                cc.image_dt_2 = np.array(cc.image_dt_1)
                # I add the connected component to the list
                self.list_cc.append(cc)
        z1=np.zeros((1,self.width),dtype=np.uint8)
        z2=np.zeros((self.height+2,1),dtype=np.uint8)
        self.original_image=np.concatenate((z1,self.original_image),axis=0)
        self.original_image=np.concatenate((self.original_image,z1),axis=0)
        self.original_image=np.concatenate((z2,self.original_image),axis=1)
        self.original_image=np.concatenate((self.original_image,z2),axis=1)
        self.height,self.width=self.original_image.shape

    # these methods are used for extract text  in image
    def clean_image(self,i):
        original_image = np.array(self.original_image)
        cv2.imwrite(self.path_debug+'Extract_text/clean_image/Canny/1_'+str(self.file_name), i)
        kernel_1=cv2.getStructuringElement(cv2.MORPH_RECT,(3,3),(0,2))
        closing_2 = cv2.morphologyEx(i, cv2.MORPH_CLOSE, kernel_1)
        c2_copy = np.uint8(closing_2)
        edges = cv2.Canny(c2_copy,5,500,apertureSize = 3)
        cv2.imwrite(self.path_debug+'Extract_text/clean_image/Canny/2_'+str(self.file_name), edges)
        minLineLength = 10
        maxLineGap = 5
        self.lines = cv2.HoughLinesP(edges,1,np.pi/360,100,minLineLength,maxLineGap)
        hough_I= np.array(self.color_image)
        for x1,y1,x2,y2 in self.lines[0]:
            cv2.line(hough_I,(x1,y1),(x2,y2),(0,255,0),2)
        cv2.imwrite(self.path_debug+'/Extract_text/clean_image/Hough/'+str(self.file_name),hough_I)
        #considering the line in the form y=mx+q , i find m and q
        for x1,y1,x2,y2 in self.lines[0]:
            x=float(min(x1,x2))
            y=float(min(y1,y2))

            if(x1!=x2):
                m=float(y2-y1)/float(x2-x1)
                q=m*(-x1)+y1

                while(((m==0) and (x!=max(x1,x2))) ):
                    x=x+1
                    # print 'x2: '+str(x2)+' x: '+str(x)
                    #I find the pixels that are on the perpendicular line, far more maximum of 3
                    #       p3
                    #       p2
                    #       p1
                    # p  p  P  p  p  p...
                    #       p4
                    #       p5
                    #       p6
                    if(  np.sum(closing_2[y-3:y+1,x])==0 ):
                        closing_2[y:y+4,x]=0

                    elif( np.sum(closing_2[y:y+4,x])==0 ):
                        closing_2[y-3:y+1,x]=0
                        #I find the pixels that are on the perpendicular line, far more maximum of 3
                        #                   p3
                        #                p2
                        #             p1
                        # p  p  p  P  p  p  p
                        #       p4
                        #    p5
                        # p6
                        #
                if(m!=0):

                    while( x!=max(x1,x2) and y!=max(y1,y2) ):
                        if(abs(m)<=1):
                            #print 'ciao'
                            x=x+1
                            y=int(m*x+q)

                            closing_2[y,x-3:x+4]=0

                        else:
                            # da rivedere nel caso y<q e m negativo
                            y=y+1

                            x=abs(int((y-q)/m))
                            closing_2[y-1,x-3:x+4]=0
            else:
                while(y!=max(y1,y2)):
                    y=y+1
                    #I find the pixels that are on the perpendicular line, far more maximum of 3
                    #            p
                    #            p
                    #            p
                    #  p1 p2 p3  P p4 p5 p6
                    #            p
                    #            p
                    #            p
                    #

                    if(  np.sum( closing_2[y,x-3:x+1])==0 ):
                        closing_2[y,x:x+4]=0

                    elif( np.sum( closing_2[y,x:x+4])==0  ):
                        closing_2[y,x-3:x+1]=0



            #morfologic open

        kernel_2=cv2.getStructuringElement(cv2.MORPH_RECT,(3,3),(0,2))
        open=cv2.morphologyEx(i, cv2.MORPH_OPEN, kernel_2)
        # open is image with black background and white lines, i want as output an image with white background and black lines
        bw=np.ones((open.shape))*255
        output= np.where(open == 0, 510,open)-bw
        cv2.imwrite(self.path_debug+'Extract_text/clean_image/Open/'+str(self.file_name), open)
        return output


    def Methods_Text_Recognition_Based_Analysis_cc(self,path_gravvitas, current_directory, input_org, debug_mode):

        #This path is used to save the output images for each step of the analysis
        path_extract_text= self.path_debug+'Extract_text/'
# #         #------uncomment later
#         list_cc_text_1=[]
#         list_cc_no_text_1=[]
#         # selected cc for area dimension
#         list_area=[]
#         for cc_a in self.list_cc:
#             list_area.append(cc_a.getArea())
#         media=sum(list_area)/len(list_area)
#
#         for cc_a2 in self.list_cc:
#             if(cc_a2.getArea()<float(media)):# and cc.height*cc.width>float(media)*0.1 ):
#                 list_cc_text_1.append(cc_a2)
#             else:
#                 list_cc_no_text_1.append(cc_a2)
#
#
#         img_no_text=np.zeros((self.height,self.width))
#         img_ad=np.zeros((self.height,self.width))
#
#         for cc in list_cc_text_1:
#              img_ad[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=img_ad[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+cc.image[1:cc.height+1,1:cc.width+1]
#         # if debug_mode:
#         #     cv2.imwrite(path_extract_text+'area_dim/'+str(self.file_name), img_ad)
#
#         # I  clean image, and extract the text that is overlapping on the lines
#
#         for cc in list_cc_no_text_1:
#              img_no_text[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=img_no_text[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+cc.image[1:cc.height+1,1:cc.width+1]
#         new_image= self.clean_image(img_no_text)
#
#         ni = new_image.astype(np.uint8)
#         #find cc in new image
#         n_i=Image_class(self.current_directory,self.file_name,self.path_output, self.path_debug, ni,ni)
#         n_i.find_connected_componet()
#         #I repeat selection for area dimension
#
#         list_area_2=[]
#         for cc_a in n_i.list_cc:
#             list_area_2.append(cc_a.getArea())
#         media=sum(list_area_2)/len(list_area_2)
#
#         # list_cc_no_text_11 =[]
#         for cc_a2 in n_i.list_cc:
#             if(cc_a2.getArea()<float(media)*0.1):# and cc.height*cc.width>float(media)*0.1 ):
#                 list_cc_text_1.append(cc_a2)
#             else:
#                 list_cc_no_text_1.append(cc_a2)
#                 # list_cc_no_text_11.append(cc_a2)
#
#         # reconstruct image with the connected components obtained after analyzing area dimension
#         img_ad_2=np.zeros((self.height,self.width))
#         for cc in list_cc_text_1:
#              img_ad_2[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=img_ad_2[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+cc.image[1:cc.height+1,1:cc.width+1]
#         # if debug_mode:
#         #     cv2.imwrite(path_extract_text+'area_dim_2/'+str(self.file_name), img_ad_2)
#
#         #select cc for aspect ratio
#         list_cc_text_2=[]
#         for cc in list_cc_text_1:
#             if((cc.getAspectRatio()>0.1) and (cc.getAspectRatio()<2.5)):
#                 list_cc_text_2.append(cc)
#             # else:
#             #     list_cc_no_text_11.append(cc)
#         # reconstruct image with the connected components obtained after analyzing aspect ratio
#         img_ar=np.zeros((self.height,self.width))
#         for cc in list_cc_text_2:
#              img_ar[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=cc.image[1:cc.height+1,1:cc.width+1]
#         # if debug_mode:
#         #     cv2.imwrite(path_extract_text+'asp_ratio/'+str(self.file_name), img_ar)
#         # img_ar=np.zeros((self.height,self.width))
#         # for cc in list_cc_no_text_11:
#         #      img_ar[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=cc.image[1:cc.height+1,1:cc.width+1]
#         # cv2.imwrite(path_extract_text+'asp_ratio/NO TEXT'+str(self.file_name), img_ar)
#
#         # select cc for density
#         list_cc_text_3=[]
#         for cc in list_cc_text_2:
#             if( cc.getDensity()> 0.2 ):# and cc.height*cc.width>float(media)*0.1 ):
#                 list_cc_text_3.append(cc)
#             # else:
#             #     list_cc_no_text_11.append(cc)
#
#
#         # reconstruct image with the connected components obtained after analyzing density
#         img_dy=np.zeros((self.height,self.width))
#         for cc in list_cc_text_3:
#              img_dy[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=cc.image[1:cc.height+1,1:cc.width+1]
#         cv2.imwrite(path_extract_text+'density/'+str(self.file_name), img_dy)
#         # img_dy=np.zeros((self.height,self.width))
#         # for cc in list_cc_no_text_11:
#         #      img_dy[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=cc.image[1:cc.height+1,1:cc.width+1]
#         # cv2.imwrite(path_extract_text+'density/NO TEXT'+str(self.file_name), img_dy)
#
# #------text improve attempt march 31st
# #----- invert image colors
#         image_density = Image.open(path_extract_text+'density/'+str(self.file_name))
#         inverted_image = PIL.ImageOps.invert(image_density)
#         inverted_image.save(path_extract_text+'density/'+'inverted_'+str(self.file_name))
#         filename = path_extract_text+'density/'+'inverted_'+str(self.file_name)
#         # print path_extract_text+'density/'+'inverted_'+str(self.file_name)
#         # print filename
#         # print path_extract_text+'density/'+'improved_'+str(self.file_name)
#         textcleaner_file = 'text_clean_library/textcleaner'
#         os.system(textcleaner_file+' -g -e normalize -f 15 -o 10 -u -s 1 ' + filename + ' '+path_extract_text+'density/'+'improved_'+str(self.file_name))
#         image=cv.LoadImage(path_extract_text+'density/'+'improved_'+str(self.file_name), cv.CV_LOAD_IMAGE_GRAYSCALE)
#         orginal_image_path= path_extract_text+'density/'+'improved_'+str(self.file_name)
#         text_img_height, text_img_width = self.height,self.width

#--word recognition testing start
        orginal_image_path = current_directory + '/input_fps/POPLAR/improved_' + str(self.file_name)
        # print orginal_image_path
        image = cv.LoadImage(orginal_image_path, cv.CV_LOAD_IMAGE_GRAYSCALE)
        text_image = cv2.imread(orginal_image_path, cv2.IMREAD_GRAYSCALE)
        text_img_height, text_img_width = text_image.shape
#---testing end

        #--call tesseract identification method
        text_cordinates=[]
        text_cordinates,image_name_without_extension,img,orginal_image,no_text_image = self.find_text_tesseract(input_org,image,path_extract_text,orginal_image_path,text_cordinates,text_img_height, text_img_width,debug_mode)
        # if debug_mode:
        #     cv2.imwrite(path_extract_text+'text_identification/'+str(image_name_without_extension)+'_text.png', img)

        cv2.imwrite(path_extract_text+'density/'+str(image_name_without_extension)+'_iteration1.png', orginal_image)

        # print '@@@@@@@@@@@@@-iteration 2--------------------------'
        #-- call the same method after rotating the image
        img_to_rotate = Image.open(path_extract_text+'density/'+str(image_name_without_extension)+'_iteration1.png')
        rotated_img = img_to_rotate.rotate(270)
        rotated_img.save(path_extract_text+'density/'+'rotated_'+str(self.file_name))
        image2=cv.LoadImage(path_extract_text+'density/'+'rotated_'+str(self.file_name), cv.CV_LOAD_IMAGE_GRAYSCALE)
        current_image_path = path_extract_text+'density/'+'rotated_'+str(self.file_name)
        text_cordinates,image_name_without_extension2,img2,edited_image,no_text_image = self.find_text_tesseract(input_org,image2,path_extract_text,current_image_path,text_cordinates,text_img_height, text_img_width,debug_mode)
        # if debug_mode:
        #     cv2.imwrite(path_extract_text+'text_identification/'+str(image_name_without_extension2)+'_text_ROTATED.png', img2)

        #--anu work
        self.image_remove_text = no_text_image
        # these methods are used for extract wall in image
        # if debug_mode:
        #     cv2.imwrite(self.path_debug+'merge_all/'+self.file_name[0:-4]+'_Notext.png',self.image_remove_text )
        name = str(self.file_name[0:len(self.file_name)-4])
        path = str(self.path_output+name+'/')
        if not(os.path.exists(path)):
            os.mkdir(path)
        cv2.imwrite(path+'no_text.png', self.image_remove_text)
        # cv2.imwrite(reiterate_path+'no_text.png', self.image_remove_text)
        self.text_coordinates = text_cordinates
        self.save_text(self.path_output+self.file_name[0:-4]+'/')
        #--anu method
        # self.erase_all_text(text_cordinates,input_org_image_path)

        #--write text to gravvitas
        text_details_file = open(path_gravvitas+image_name_without_extension+'_6_text_details.txt', 'w')
        for text_row in text_cordinates:
            tx,ty = text_row[1]
            #--cordinates
            text_and_cordinate = str(text_row[0])+' : ['+str(tx)+','+ str(ty)+'] : '
            #---bb_box
            x1,y1,x2,y2 = text_row[2]
            text_bounding_box = '['+str(x1)+','+ str(y1)+','+ str(x2)+','+ str(y2)+'] : '
            if len(text_row[5])==0:
                text_row[5] ='None'
            confidence_and_others = 'confidence,dict,original_word,difference : ['+str(text_row[3])+','+str(text_row[4])+','+str(text_row[5])+','+str(text_row[6])+']'
            text_details_file.write(text_and_cordinate+text_bounding_box+confidence_and_others+'\n')

            # if str(text_row[4])=='dict':
            #     print image_name_without_extension,str(text_row[0]),text_row[5],text_row[3],len(text_row[0]),text_row[6]


        if os.stat(path_gravvitas+image_name_without_extension+'_6_text_details.txt').st_size == 0:
            text_details_file.write('No Text Detected'+'\n')
        text_details_file.close()

        new_text_cordinates = []
        for t_row in text_cordinates:
            # text = t_row[0].upper()
            # new_text_cordinates.append([text,t_row[1],t_row[2]])
            new_text_cordinates.append([t_row[0],t_row[1],t_row[2]])
        return new_text_cordinates


    def find_text_tesseract(self,input_org,image,path_extract_text,current_image_path,text_cordinates,text_img_height, text_img_width,debug_mode):
        no_text_image = input_org
        #load input images to color
        orginal_img = cv2.imread(current_image_path,cv2.IMREAD_COLOR)
        img = orginal_img.copy()
        image_name_without_extension=self.file_name[0:-4]


        api = tesseract.TessBaseAPI()
        api.Init(".","fra",tesseract.OEM_DEFAULT)
        # api.SetPageSegMode(tesseract.PSM_AUTO)
        tesseract.SetCvImage(image,api)
        tesseract_text=api.GetUTF8Text()
        textposition=api.GetHOCRText(1)
        # mean_confidence = api.MeanTextConf()
        word_confidence = api.AllWordConfidences()

        #--get word confidences
        confidence_list = []
        for tess_text in tesseract_text:
            for tess_confidence in word_confidence:
                confidence_list.append([tess_text,tess_confidence])

        # for test in tesseract_text:
        #     print test

        f = open("tesseract_output", "a")
        # f.write("------------------\n"+filename+"\n"+tesseract_text+"\n")


        tesseract_words = []
        soup = BeautifulSoup(textposition, "lxml")
        for span in soup.findAll("span", {"class": "ocrx_word"}):
            # no_text_image = self.my_function_remove_text(span,image)
            #--check if hocr file has a string or not '<span...> </span> against <span>abc</span>
            if len(span)>0:
                bbox_data = (span.get('title'))
                only_numbers =  re.findall(r'\d+',bbox_data)
                x1 = int(only_numbers[0])
                y1 = int(only_numbers[1])
                x2 = int(only_numbers[2])
                y2 = int(only_numbers[3])
                word_confidence = only_numbers[4]
                bounding_box = [x1,y1,x2,y2]
                x_difference,y_difference = abs(x1-x2),abs(y1-y2)
                if (x_difference- y_difference)>=0 and x_difference>20 and span.string!=None:
                    cv2.rectangle(orginal_img, (x1, y1), (x2, y2), (255, 255, 255), -1)
                    #--remove symbols from text
                    text_name = unicodedata.normalize('NFKD', span.string).encode('ascii','ignore')
                    if len(text_name) == 0:
                        continue
                    #-- count number of occurences per character to delete 'OO' like false identification
                    #-- this also deletes single chracter words
                    exit_condition_1 = self.calculate_character_occurences(text_name)
                    if exit_condition_1:
                        continue

                    #---check each character and take them only if is letter or a '/'
                    final_word = ''
                    for character in text_name:
                        new_char = ''
                        # --replace 1 with 'i' since both are similar
                        if character == '1':
                            new_char = 'i'
                        else:
                            new_char = character
                        if new_char.isalpha() or new_char=='/':
                            #-- add character to word after converting to lower case
                            final_word = final_word+new_char.lower()
                    #--check if final word is more than one character
                    if len(final_word)>1:
                        tesseract_words.append([final_word,bounding_box,word_confidence])
                        # --remove tex-piers method: reused by anu since pycharm said it has no usages in project
                        no_text_image = self.anu_function_remove_text(no_text_image, x1, y1, x2, y2)
                        continue


        # print '-----STEP 01:tesseract_words------'
        # for row in tesseract_words:
        #     print row
        # print '----------new array------------------'
        #----combine words too close to each other
        for w_row1, word_1 in enumerate(tesseract_words):
            before_x1,before_y1,before_x2,before_y2 = word_1[1]
            word_confidence1 = int(word_1[2])
            for w_row2,word_2 in enumerate(tesseract_words):
                if w_row2 > w_row1:
                    current_x1,current_y1,current_x2,current_y2 = word_2[1]
                    word_confidence2 = int(word_2[2])
                    # print word_1,word_2
                    # print 'first',before_x1,before_y1,before_x2,before_y2
                    # print '2nd',current_x1,current_y1,current_x2,current_y2
                    # print 'horizontal - abs(current_x1-before_x2)',abs(current_x1-before_x2),'abs(current_y1-before_y1)',abs(current_y1-before_y1)
                    # print 'vertical - abs(current_y1-before_y2)',abs(current_y1-before_y2),'abs(current_x1-before_x1)',abs(current_x1-before_x1)
                    # print '-------'
                    #---horizontal word combination (distance, same lineish horizontally)
                    if abs(current_x1-before_x2)<35 and abs(current_y1-before_y1)<5:
                        new_word = word_1[0]+' '+word_2[0]
                        new_cordinates = before_x1,before_y1,current_x2,current_y2
                        new_word_confidence = (word_confidence1+word_confidence2)/2
                    #--vertical word combination (distance,same columnish vertically)
                    elif abs(current_y1-before_y2)<25 and abs(current_x1-before_x1)<65:
                        new_word = word_1[0]+' '+word_2[0]
                        new_cordinates = before_x1,before_y1,current_x2,current_y2
                        new_word_confidence = (word_confidence1+word_confidence2)/2
                    else:
                        new_word = ''
                        new_cordinates = ''
                        new_word_confidence = 0

                    if new_word != '':
                        #----check word orientation
                        x_difference,y_difference = abs(new_cordinates[0]-new_cordinates[2]),abs(new_cordinates[1]-new_cordinates[3])
                        if (x_difference- y_difference)>2 and x_difference>20:
                            tesseract_words[w_row1] = [new_word,new_cordinates,new_word_confidence]
                            del tesseract_words [w_row2]
                            break


        # print '------tesseract_words------'
        # for row in tesseract_words:
        #     print row



        text_cordinates,tesseract_words,img,orginal_img = self.dictionary_matching(f,path_extract_text,tesseract_words,text_cordinates,text_img_height,image_name_without_extension,img,orginal_img,debug_mode)
        # print '2nd time----'
        # for row in tesseract_words:
        #     print row

        return text_cordinates,image_name_without_extension,img,orginal_img,no_text_image

    def dictionary_matching(self,f,path_extract_text,tesseract_words,text_cordinates,text_img_height,image_name_without_extension,img,orginal_img,debug_mode):
        text_object = text_recogntion_class(f, img)
        # if debug_mode:
        #     txt_labels_file = open(path_extract_text+"text_identification/"+str(image_name_without_extension)+"_Text Labels.txt", "w")
        for word in tesseract_words:
            x,y,xx,yy = text_object.hocr_text(word, img,False)

        #----use my own dictionary
        dictionary_content = ''
        with open(self.current_directory+'/Model/dict_french_POPLAR.txt') as f:
            dictionary_content = f.read().splitlines()

        finalized_words =[]
        for row_num,row in enumerate(tesseract_words):
            orginal_word = row[0]
            cordinates = row[1]
            confidence = row[2]

            #----check for slash and if exists seperate two words and check each with dictionary
            slash_seperated_words = []
            image_word = orginal_word
            slash_position = image_word.find('/')
            if slash_position != -1:
                slash_seperated_words.append(image_word[0:slash_position])
                slash_seperated_words.append(image_word[slash_position+1:])
                add_to_final_words = []
                for each_word in slash_seperated_words:
                    add_to_final_words,word_matched = self.find_matching_word(text_object,dictionary_content,each_word,add_to_final_words,orginal_word,cordinates,confidence)
                #---gather results from dictionary and combine them with slash
                recognized_word = ''
                edit_distance_value = -1
                for t,temp_row in enumerate(add_to_final_words):
                    if len(temp_row[4])==0:
                        recognized_word = ''
                        edit_distance_value = 0
                    else:
                        temp_word = temp_row[4]
                        temp_edit_distance = temp_row[5]
                        if t==0:
                            recognized_word = temp_word+'/'
                            edit_distance_value = temp_edit_distance
                        elif t==len(add_to_final_words)-1:
                            recognized_word = recognized_word+temp_word
                            edit_distance_value = edit_distance_value+temp_edit_distance
                        else:
                            recognized_word = recognized_word+temp_word+'/'
                            edit_distance_value = edit_distance_value+temp_edit_distance
                if len(recognized_word)>0:
                    finalized_words.append([orginal_word,cordinates,confidence,'D',recognized_word,edit_distance_value])
            #-----if no slash check word with dictionary
            else:
                #--first remove all spaces and check
                word_to_check = "".join(c for c in orginal_word if c not in (' '))
                finalized_words,word_matched = self.find_matching_word(text_object,dictionary_content,word_to_check,finalized_words,orginal_word,cordinates,confidence)




        #---append identified words to text_coordinates []
        if len(text_cordinates)==0:
            iteration = 1
        else:
            iteration = 2
        for each_row in finalized_words:
            confidence = each_row[2]
            dict_check = each_row[3]
            edit_distance_value = each_row[5]
            detected_word = ''
            if len(each_row[4])==0:
                correct_word = each_row[0]
            else:
                correct_word = each_row[4]
                detected_word = each_row[0]
            x1,y1,x2,y2 = text_object.hocr_text(each_row, img,True)
            x_centre = x1 + ((x2-x1)/2)
            y_centre = y1 + ((y2-y1)/2)
            if iteration==1:
                text_cordinates.append([correct_word,[x_centre,y_centre],[x1,y1,x2,y2],confidence,dict_check,detected_word,edit_distance_value])
            else:
                insert_x = y_centre
                insert_y = text_img_height-x_centre
                insert_x1 = y1
                insert_y1 = text_img_height-x1
                insert_x2 = y2
                insert_y2 = text_img_height-x2
                text_cordinates.append([correct_word,[insert_x,insert_y],[insert_x1,insert_y1,insert_x2,insert_y2],confidence,dict_check,detected_word,edit_distance_value])

        return text_cordinates,tesseract_words,img,orginal_img


    def calculate_character_occurences(self, text_name):
        count_letters = []
        for L in text_name:
            chracter_exists= False
            if len(count_letters)>0:
                for cur_row,C in enumerate(count_letters):
                    if C[0]==L:
                        chracter_exists = True
                        break
            if chracter_exists==False:
                count_letters.append([L,text_name.count(L)])
            else:
                continue
        exit_condition_1 = False

        #calculate if same letter is repeated or after removing '/' such, still only one letter is repeated
        alpha_character_count = 0
        for character_count in count_letters:
            if len(count_letters)==1:
                if character_count[1]==len(text_name):
                    exit_condition_1 = True
            else:
                if character_count[0].isalpha():
                    alpha_character_count+=1
        if alpha_character_count<2:
            exit_condition_1 = True

        return exit_condition_1

    def find_matching_word(self,text_object,dictionary_content,current_word,finalized_words,orginal_word,cordinates,confidence):
        word_exists, word_matched  = False, False
        suggestion,word_exists,dict_existing_word = text_object.check_dictionary(dictionary_content,current_word,word_exists)
        if word_exists:
            #--to seperate unneccasry space in cases like 'bu rea'
            word_to_be_used = text_object.check_length_of_words_seperated_by_space(orginal_word,dict_existing_word)
            finalized_words.append([word_to_be_used,cordinates,confidence,'org','',0])
            word_matched = True
        elif len(suggestion) is not 0:
            if len(current_word)>1:
                suggestion.sort(key=itemgetter(1))
                suggested_word = ''
                edit_distance_value = -1
                temp_list = []
                for suggest in suggestion:
                    if suggest[1]==1:
                        temp_list.append([suggest[0],suggest[1]])
                    else:
                        suggest_word = suggest[0]
                        if current_word[0]== suggest_word[0]:
                            temp_list.append([suggest_word,suggest[1]])
                if len(temp_list)>1:
                    original_word_length = len(current_word)
                    min_distance = 100
                    for temp_word in temp_list:
                        if original_word_length-len(temp_word[0])<min_distance:
                            suggested_word = temp_word[0]
                            edit_distance_value = temp_word[1]
                            min_distance = original_word_length-len(temp_word)
                else:
                    for temp_word in temp_list:
                        suggested_word = temp_word[0]
                        edit_distance_value = temp_word[1]
                if suggested_word != '':
                    finalized_words.append([current_word,cordinates,confidence,'dict',suggested_word,edit_distance_value])
                    word_matched = True
                    # print 'current_word,cordinates,suggested_word',current_word,cordinates,suggested_word
        # print '-----finalized_words----'
        # for row in finalized_words:
        #     print row

        return finalized_words,word_matched


    def erase_all_text(self,text_cordinates,input_org_image_path):
        input_org_image = cv2.imread(input_org_image_path,0)
        for text_row in text_cordinates:
            x1,y1,x2,y2 = text_row[2]
            cv2.rectangle(input_org_image,(x1-2,y1-2),(x2+2,y2+2),(255,255,255),-1)
        cv2.imwrite(self.path_output+self.file_name[0:-4]+'textless.png',input_org_image)



    def find_door_width(self,img):
        # img = cv2.imread(orginal_image_path,cv2.IMREAD_COLOR)
        dst = cv2.inRange(img, (0,255,0), (0,255,0))
        labels, numlabels = ndimage.label(dst>127)
        avg_width = 0
        count = 0
        for i in range(1,numlabels+1):
            cc = CC(i)
            point= np.argwhere(labels == i)
            cc.y_min=point[:,0].min()
            cc.x_min=point[:,1].min()
            cc.y_max=point[:,0].max()
            cc.x_max=point[:,1].max()
            cc.height= cc.y_max-cc.y_min+1
            cc.width= cc.x_max-cc.x_min+1
            cc.center_x = cc.y_min+(cc.height/2)
            cc.center_y = cc.x_min+(cc.width/2)
            cc.image=(np.where(labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
            cc.numPixel=(cc.image.sum()/255)
            cc.image = self.add_pad_single_pixel(cc.image)
            cc.set_coordinates()
            cc.find_contour()
            max_dist = cc.get_max_distance()
            if max_dist>20:
                avg_width+=max_dist
                count+=1
        if count > 0:
            avg_width = 2*avg_width/float(count)
        else:
            dst = cv2.inRange(img, (0,255,255),(0,255,255))
            labels, numlabels = ndimage.label(dst>127)
            avg_width = 0
            count = 0
            for i in range(1,numlabels+1):
                cc = CC(i)
                point= np.argwhere(labels == i)
                cc.y_min=point[:,0].min()
                cc.x_min=point[:,1].min()
                cc.y_max=point[:,0].max()
                cc.x_max=point[:,1].max()
                cc.height= cc.y_max-cc.y_min+1
                cc.width= cc.x_max-cc.x_min+1
                cc.center_x = cc.y_min+(cc.height/2)
                cc.center_y = cc.x_min+(cc.width/2)
                cc.image=(np.where(labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
                cc.numPixel=(cc.image.sum()/255)
                cc.image = self.add_pad_single_pixel(cc.image)
                cc.set_coordinates()
                cc.find_contour()
                max_dist = cc.get_max_distance()
                if max_dist>20:
                    avg_width+=max_dist
                    count+=1
            if count != 0:
                if count > 2:
                    avg_width = avg_width/float(count) #a 0.50 penalty because windows are tipcally larger than doors
                else:
                    avg_width = 2*avg_width/float(count)
        if avg_width < 40:
            avg_width = 40
        return avg_width
    # STRAINS
    def find_stairs(self,path_gravvitas,only_image_name):

        # the first part of the code I initialize the images to view the step of function
        h,w=self.original_image.shape
        img =  np.array(self.original_image)
        #img2 =  np.array(self.color_image)
        img2 = np.zeros((h,w,3))
        img3 =  np.zeros((h,w,3))
        img4 = np.zeros((h,w,3))
        img5 =  np.zeros((h,w,3))
        img6 =  np.zeros((h,w,3))
        img7 =  np.zeros((h,w),dtype=np.uint8)
        img8 =  np.zeros((h,w),dtype=np.uint8)

        # I find connected component
        s1 = [[0, 1, 0], [1,1,1], [0,1,0]]
        s2 = [[1, 1, 1], [1,1,1], [1,1,1]]
        labels,numlabels =ndimage.label(img>127,s1)
        list_initial_cc=[]
        for i in range(1,numlabels+1):
            point = np.argwhere(labels == i)
            cc = CC(i)
            if len(point)>0:
                cc.y_min=point[:,0].min()
                cc.x_min=point[:,1].min()
                cc.y_max=point[:,0].max()
                cc.x_max=point[:,1].max()
                cc.height= cc.y_max-cc.y_min+1
                cc.width= cc.x_max-cc.x_min+1
                cc.image=(np.where(labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
                cc.numPixel=(cc.image.sum()/255)
                list_initial_cc.append(cc)
        # I save the original image
        cv2.imwrite(self.path_debug+'scale/'+str(self.file_name)+'_1.png', self.color_image )


        # I save the image whit colored cc
        for l in list_initial_cc:
            r=random.randint(0, 255)
            g=random.randint(0, 255)
            b=random.randint(0, 255)
            cc_rgb = cv2.cvtColor(l.image.astype(np.uint8),cv2.COLOR_GRAY2RGB)/255
            img2[l.y_min:l.y_max+1,l.x_min:l.x_max+1] = img2[l.y_min:l.y_max+1,l.x_min:l.x_max+1]+(cc_rgb*(r,g,b))

        cv2.imwrite(self.path_debug+'scale/'+str(self.file_name)+'_2.png', img2 )

        # I create a dictionary
        #the key element of a dictionary is the cc area
        d_area = {}
        for cc in list_initial_cc:
            #area = ((cc.y_max - cc.y_min) + 1)*((cc.y_max - cc.y_min) + 1)
            area = cc.height*cc.width
            if area *0.6 < cc.numPixel:
                keys = d_area.keys()
                if len(keys) == 0:
                    d_area[area] = []
                    d_area[area].append(cc)
                else:
                    check_d = False
                    for k in keys:
                        if area>(k*0.80) and area<(k*1.2):
                            d_area[k].append(cc)
                            check_d = True
                            break
                    if not(check_d):
                        d_area[area] = []
                        d_area[area].append(cc)
        for k in d_area.keys():
            if len(d_area[k])>=3:
                r=random.randint(0, 255)
                g=random.randint(0, 255)
                b=random.randint(0, 255)
                for cc in d_area[k]:
                    cc_rgb = cv2.cvtColor(cc.image.astype(np.uint8),cv2.COLOR_GRAY2RGB)/255
                    img3[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] = img3[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+(cc_rgb*(r,g,b))
        cv2.imwrite(self.path_debug+'scale/'+str(self.file_name)+'_3.png', img3 )

        # in this case i select the list cc that has cc near
        d2={}
        for k in d_area.keys():
            l_cc = []
            if len(d_area[k])>=3:
                for cc in d_area[k]:
                    for cc2 in d_area[k]:
                        if cc != cc2:
                            if (cc.x_min > cc2.x_min-20 and cc.x_min < cc2.x_min + 5) and (cc.x_max > cc2.x_max-5 and cc.x_max < cc2.x_max + 5) and ( np.abs(cc.y_max -cc2.y_min)<=5 or np.abs(cc.y_min -cc2.y_max)<=5   ):
                                l_cc.append(cc)
                                break

                            elif (cc.y_min > cc2.y_min-20 and cc.y_min < cc2.y_min + 5) and (cc.y_max > cc2.y_max-5 and cc.y_max < cc2.y_max + 5) and (  np.abs(cc.x_max -cc2.x_min)<=5 or np.abs(cc.x_min -cc2.x_max)<=5  ):
                                l_cc.append(cc)
                                break
                d2[k] = l_cc
                r=random.randint(0, 255)
                g=random.randint(0, 255)
                b=random.randint(0, 255)
                for cc in l_cc:
                    cc_rgb = cv2.cvtColor(cc.image.astype(np.uint8),cv2.COLOR_GRAY2RGB)/255
                    img4[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] = img4[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+(cc_rgb*(r,g,b))
        cv2.imwrite(self.path_debug+'scale/'+str(self.file_name)+'_4.png', img4 )

        #  in una lista ci possono essere cc con la stessa area  ma che non sono tra loro vicine,
        #  per cui vado a dividere ogni lista del dizionario in liste di cc che sono vicine tra loro
        d_near = {}
        n = 0
        for k in d2.keys():
            ll = d2[k][:]
            while(len(ll)>0):
                cc = ll.pop()
                n = n + 1
                d_near[n] = [cc]
                stop = True
                while(stop):
                    stop = False
                    for cc in ll:
                        x,y = cc.getCentroid()
                        h = cc.getLengthSide()
                        for cc2 in d_near[n]:
                            x2,y2 = cc2.getCentroid()
                            if cc != cc2:
                                dist = math.sqrt((x-x2)**2 + (y-y2)**2)
                                if dist<h*2:
                                    if not( cc in d_near[n]):
                                        d_near[n].append(cc)
                                        stop=True
                for cc in d_near[n]:
                    if cc in ll:
                        ll.remove(cc)
        list_scale=[]
        d_final = {}
        for k in d_near.keys():
            if len(d_near[k])>2:
                d_final[k] = d_near[k]
                r=random.randint(0, 255)
                g=random.randint(0, 255)
                b=random.randint(0, 255)
                list_scale.extend(d_near[k])
                for cc in d_near[k]:
                    cc_rgb = cv2.cvtColor(cc.image.astype(np.uint8),cv2.COLOR_GRAY2RGB)/255
                    img5[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] = img5[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+(cc_rgb*(r,g,b))
        cv2.imwrite(self.path_debug+'scale/'+str(self.file_name)+'_5.png', img5 )

        # a questo punto ho individuato le cc che formano gli scalini di una scala
        # in piu ci possono essere gruppi di cc piu piccole oppure gruppi di componenti cc che sono vicine a gruppi di due

        # 1) il primo passo e vedere quante cc compongono la mia lista, se son maggiori di 50 sicuramente ci saranno dei
        # gruppi di cc piccole, poi mi calcolo la area media e la lunghezza media delle cc che mi restano

        #devo andare a ritrovare le cc a loro vicine e che hanno le seguenti caratteristiche
        # vicinanza rispetto ad una cc
        # area non superiore a 10 volte la cc
        # area non inferiore al meta della cc
        # devono avere un lato in comune(sara dura)

        if len(list_scale)>0:
            v_a = []
            v_l = []
            for k in d_final.keys():
                mean_area = 0
                min_dist = max(cc.width,cc.height)
                for s in d_final[k]:
                    mean_area = mean_area + (s.width*s.height)
                    sx,sy=s.getCentroid()
                    for s2 in d_final[k]:
                        if s.id != s2.id:
                            s2x,s2y=s2.getCentroid()
                            dist = math.sqrt((sx-s2x)**2 + (sy-s2y)**2)
                            if dist < min_dist:
                                min_dist = dist

                v_a.append(mean_area/len(d_final[k]))
                v_l.append(min_dist)
            mean_area = sum(v_a)/len(v_a)

            keys = d_final.keys()
            for k in keys:
                kk = d_final.keys()
                i = kk.index(k)
                if v_a[i]< mean_area*0.5 or v_a[i]<20:
                    del(d_final[k])
                    del(v_a[i])
                    del(v_l[i])




            keys = d_final.keys()
            stop = True
            if len(keys)>0:
                while(stop):
                    stop=False
                    for cc in list_initial_cc:
                        isBreak=False
                       # if (cc.width*cc.height)<(mean_area*10) and (cc.width*cc.height)>mean_area*0.2:
                        for i in range(0,len(keys)):
                            if (cc.width*cc.height)<(v_a[i]*10) and (cc.width*cc.height)>v_a[i]*0.5:
                                ccx,ccy=cc.getCentroid()
                                for s in d_final[keys[i]]:
                                    if cc.id!=s.id:
                                        sx,sy=s.getCentroid()
                                        dist = math.sqrt((ccx-sx)**2 + (ccy-sy)**2)
                                        # leng = max(max(cc.width,cc.height),max(s.width,s.height))

                                        if dist<v_l[i]*4:
                                            #list_scale2.append(cc)
                                            list_initial_cc.remove(cc)
                                            d_final[keys[i]].append(cc)

                                            isBreak=True
                                            stop=True
                                            break
                                if isBreak:
                                    break
                        if isBreak:
                            break



            for k in d_final.keys():
                self.dic_scale[k]= d_final[k]
                for cc in d_final[k]:
                    r=random.randint(0, 255)
                    g=random.randint(0, 255)
                    b=random.randint(0, 255)
                    cc_rgb = cv2.cvtColor(cc.image.astype(np.uint8),cv2.COLOR_GRAY2RGB)/255
                    img6[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] = img6[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+(cc_rgb*(r,g,b))
                cv2.imwrite(self.path_debug+'scale/'+str(self.file_name)+'_6.png', img6 )


        for k in d_final.keys():
            self.dic_scale[k]= d_final[k]
            for cc in d_final[k]:
                img7[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] = img7[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+cc.image.astype(np.uint8)
        kernel = np.ones((5,5),np.uint8)
        closing = cv2.morphologyEx(img7, cv2.MORPH_CLOSE, kernel)
        cv2.imwrite(self.path_debug+'scale/'+str(self.file_name)+'_7.png', closing )


        cnt,hierarchy = cv2.findContours(closing, 1, 2)
        max_area = 0
        for c in cnt:
            area = cv2.contourArea(c)
            if max_area < area:
                max_area = area
                scala = c
        if max_area>0:
            epsilon = 0.01*cv2.arcLength(scala,True)
            approx = cv2.approxPolyDP(scala,epsilon,True)
            self.list_scale.append(approx)
            cv2.drawContours(img8, [approx], -1, 255, -1)
            cv2.imwrite(self.path_debug+'scale/'+str(self.file_name)+'_8.png', img8 )

        stairs_path = self.path_debug+'scale/'
        stairs_image_list = os.listdir(stairs_path)
        last_image_name = natsorted(stairs_image_list)[-1]

        stairs_details_file = open(path_gravvitas+only_image_name+'_3_stairs_details.txt', 'w')

        # gray_room = cv2.imread(stairs_path+last_image_name,cv2.IMREAD_GRAYSCALE)
        # openPlan = planClass(self.path_output, 'bogus_path', gray_room,'random_name',False)
        # colored_image = cv2.imread(self.path_output+only_image_name+'/'+'Stefano_output.png')
        # avg_door_width = self.find_door_width(colored_image)
        # new_contour_lines = openPlan.improve_outer_contour(stairs_path+last_image_name,avg_door_width)
        # print new_contour_lines
        # print len(new_contour_lines)

        gray_room = cv2.imread(stairs_path+last_image_name,cv2.IMREAD_GRAYSCALE)
        ret,thresh = cv2.threshold(gray_room,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)
        for count, cnt in enumerate(contours):
            if count != len(contours)-1:
                contour_data = ''
                for c,cont_point in enumerate(cnt):
                    if c==0:
                        contour_data  = contour_data+'['+str(cont_point[0][0])+' '+str(cont_point[0][1])+']'
                    else:
                        contour_data  = contour_data+',['+str(cont_point[0][0])+' '+str(cont_point[0][1])+']'
                stairs_details_file.write('Stairs : '+contour_data + '\n')
        stairs_details_file.close()

    # WALLS
    def find_adjacent_rectangles2(self):
        list_of_serch=self.list_rect[:]# create new list
        #print len(self.list_rect)

        while(len(list_of_serch)>0):

            rect_1=list_of_serch.pop()


            merge=False
            for rect_2 in list_of_serch:
                # adjacent l1,l3
                # if rect_2.get_area>10:
                # adjacent l1,l3es che ritorna una copia della lista di tutti i
                # node_2 = Node(rect_2.id)
                #    if rect_1 is not rect_2:
                # qui potrei mettere il primo membro |y2-y1|<3 per includere anche quelle vicini ma non attaccati anche se probabilmente fanno parte di un altra cc
                # if ((rect_2.list_sides[2].y2==rect_1.list_sides[0].y1-1) and (rect_2.list_sides[2].x2<=rect_1.list_sides[0].x2) and (rect_2.list_sides[2].x1>=rect_1.list_sides[0].x1)) :
                if ((rect_2.y4==rect_1.y1-1) and (rect_2.x4<=rect_1.x2) and (rect_2.x3>=rect_1.x1)) :
                    a=rect_1.x2-rect_1.x1
                    b=rect_2.x2-rect_2.x1
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)


                    if(percent_adjacency<0.7):# questo mi basta per capire se i due rettangoli sono allineati

                        thinckness_r1=min( (a) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor

                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 1, 3] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 1, 3])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 3, 1])
                        else:
                            if ['ADIACENT',rect_2, 1, 3] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT',rect_2, 1, 3])
                                rect_2.list_adjacent_rect.append(['ADIACENT',rect_1, 3, 1])


                    else:

                        if np.abs((rect_1.x2-rect_2.x2))<=1 and  np.abs(rect_1.x1-rect_2.x1)<=1:
                            r=Rectangle(rect_1.id)
                            r.x2=max(rect_1.x2,rect_2.x2)
                            r.x1=min(rect_1.x1,rect_2.x1)
                            r.y1=rect_2.y1
                            r.y2=rect_2.y2
                            r.x3=max(rect_1.x2,rect_2.x2)
                            r.x4=min(rect_1.x1,rect_2.x1)
                            r.y3=rect_1.y3
                            r.y4=rect_1.y4
                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                            # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0: #and len(rect_1.list_adjacent_rect)>0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                        #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente
                                        a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0: #and len(rect_2.list_adjacent_rect)>0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                       # a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))
                                         a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])
                            elif len(rect_1.list_adjacent_rect)>0: # and len(rect_2.list_adjacent_rect)>0:

                                for a in rect_1.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])


                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            #print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)
                            merge=True
                        else:
                            if ['ALIGNED',rect_2, 1, 3] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 1, 3])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 3, 1])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]

                                #list_new.append(rect_1)

                                # if merge:
                                #     break

                                # adjacent l3,l1
                                #    if((rect_2.list_sides[0].y2==rect_1.list_sides[2].y1+1) and (rect_2.list_sides[0].x1<=rect_1.list_sides[2].x1) and (rect_2.list_sides[0].x2>=rect_1.list_sides[2].x2) ):
                elif((rect_2.y2==rect_1.y3+1) and (rect_2.x1<=rect_1.x3) and (rect_2.x2>=rect_1.x4) ):
                    a=rect_1.x2-rect_1.x1
                    b=rect_2.x2-rect_2.x1
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)
                    #percent_adjacency=min(float(rect_1.x2-rect_1.x1),float(rect_2.x2-rect_2.x1))/(max(float(rect_1.x2-rect_1.x1),float(rect_2.x2-rect_2.x1))+1)


                    if(percent_adjacency<0.7):
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor



                        # if(rect_1.x2-rect_1.x1>rect_2.x2-rect_1.x2):
                        #  if( (min( (rect_2.x2-rect_2.x1) , (rect_2.y4-rect_2.y1) ) == (rect_2.x2-rect_2.x1)) and ( min( (rect_1.x3-rect_1.x4) , (rect_1.y4-rect_1.y1) ) == (rect_1.x3-rect_1.x4)) ):
                        #      rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 3, 1])
                        #      rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 1, 3])

                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 3, 1] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 3, 1])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 1, 3])

                        else:
                            if ['ADIACENT',rect_2, 3, 1] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT',rect_2, 3, 1])
                                rect_2.list_adjacent_rect.append(['ADIACENT',rect_1, 1, 3])

                    else:
                        if np.abs((rect_1.x2-rect_2.x2))<=1 and  np.abs(rect_1.x1-rect_2.x1)<=1:
                            r=Rectangle(rect_1.id)
                            r.x2=max(rect_1.x2,rect_2.x2)
                            r.x1=min(rect_1.x1,rect_2.x1)
                            r.y1=rect_1.y1
                            r.y2=rect_1.y2
                            r.x3=max(rect_1.x2,rect_2.x2)
                            r.x4=min(rect_1.x1,rect_2.x1)
                            r.y3=rect_2.y3
                            r.y4=rect_2.y4

                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                           # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                        #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente
                                        a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                        #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))
                                        a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            else:

                                for a in rect_1.list_adjacent_rect:
                                    #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            #print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)
                            merge=True
                        else:
                            if ['ALIGNED',rect_2, 3, 1] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 3, 1])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 1, 3])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]

                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]

                                #list_new.append(rect_1)
                                # if merge:
                                #     break

                                # adjacent l2,l4
                                #    if((rect_2.list_sides[3].x1==rect_1.list_sides[1].x2+1) and (rect_2.list_sides[3].y1>=rect_1.list_sides[1].y1) and (rect_2.list_sides[3].y2<=rect_1.list_sides[1].y2) ):

                elif((rect_2.x4==rect_1.x3+1) and (rect_2.y4>=rect_1.y2) and (rect_2.y1<=rect_1.y3) ):
                    a=rect_1.y3-rect_1.y2
                    b=rect_2.y3-rect_2.y2
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)


                    if(percent_adjacency<0.7):
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor
                        # if( (min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) ) == (rect_1.y4-rect_1.y1)) and ( min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) ) == (rect_2.y4-rect_2.y1)) ):
                        #     rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 2, 4])
                        #     rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 4, 2])

                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 2, 4] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 2, 4])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 4, 2])

                        else:
                            if ['ADIACENT', rect_2, 2, 4] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT', rect_2, 2, 4])
                                rect_2.list_adjacent_rect.append(['ADIACENT', rect_1, 4, 2])
                    else:
                        if np.abs((rect_1.y1-rect_2.y1))<=1 and  np.abs(rect_1.y4-rect_2.y4)<=1:
                            r=Rectangle(rect_1.id)
                            r.y4=max(rect_1.y4,rect_2.y4)
                            r.y1=min(rect_1.y1,rect_2.y1)
                            r.x1=rect_1.x1
                            r.x4=rect_1.x4
                            r.y3=max(rect_1.y4,rect_2.y4)
                            r.y2=min(rect_1.y1,rect_2.y1)
                            r.x3=rect_2.x3
                            r.x2=rect_2.x2
                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                           # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                       # a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente
                                         a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                        a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            else:

                                for a in rect_1.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            merge=True
                            # print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)

                        else:
                            if ['ALIGNED',rect_2, 2, 4] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 2, 4])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 4, 2])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]
                                #list_new.append(rect_1)
                                # if merge:
                                #     break
                                # adjacent l4,l2
                                #    if((rect_2.list_sides[1].x1==rect_1.list_sides[3].x2-1) and (rect_2.list_sides[1].y1<=rect_1.list_sides[3].y1) and (rect_2.list_sides[1].y2>=rect_1.list_sides[3].y2) ):

                elif((rect_2.x2==rect_1.x1-1) and (rect_2.y2<=rect_1.y4) and (rect_2.y3>=rect_1.y1) ):
                    a=rect_1.y3-rect_1.y2
                    b=rect_2.y3-rect_2.y2
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)
                    #percent_adjacency=min(float(rect_1.y3-rect_1.y2),float(rect_2.y3-rect_2.y2))/(max(float(rect_1.y3-rect_1.y2),float(rect_2.y3-rect_2.y3))+1)


                    if(percent_adjacency<0.7):
                        # if( (min( (rect_2.x2-rect_2.x1) , (rect_2.y4-rect_2.y1) ) == (rect_2.y4-rect_2.y1)) and ( min( (rect_1.x3-rect_1.x4) , (rect_1.y4-rect_1.y1) ) == (rect_1.y4-rect_1.y1)) ):
                        #     rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 4, 2])
                        #     rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 2, 4])
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor
                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 4, 2] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 4, 2])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 2, 4])

                        else:
                            if ['ADIACENT', rect_2, 4, 2] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT', rect_2, 4, 2])
                                rect_2.list_adjacent_rect.append(['ADIACENT', rect_1, 2, 4])


                    else:
                        if np.abs((rect_1.y1-rect_2.y1))<=1 and  np.abs(rect_1.y4-rect_2.y4)<=1:
                            r=Rectangle(rect_1.id)
                            r.y4=max(rect_1.y4,rect_2.y4)
                            r.y1=min(rect_1.y1,rect_2.y1)
                            r.x1=rect_2.x1
                            r.x4=rect_2.x4
                            r.y3=max(rect_1.y4,rect_2.y4)
                            r.y2=min(rect_1.y1,rect_2.y1)
                            r.x3=rect_1.x3
                            r.x2=rect_1.x2
                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                           # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                        a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                        a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            else:

                                for a in rect_1.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            #  print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)
                            merge=True
                        else:
                            if ['ALIGNED',rect_2, 4, 2] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 4, 2])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 2, 4])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]
                                #list_new.append(rect_1)
                                # else:
                                #     rect_2.num_set_of_rectangle=0

                    # if merge:
                    #     break            # if len(list_of_serch)==0 and len(rect_1.list_adjacent_rect)>0:
                #     self.list_new.append(rect_1)
                if merge:
                    break

    def find_adjacent_rectangles(self):
        list_of_serch=self.list_rect[:]# create new list
        #print len(self.list_rect)
        while(len(list_of_serch)>0):

            rect_1=list_of_serch.pop()


            merge=False
            for rect_2 in list_of_serch:
                # adjacent l1,l3
                # if rect_2.get_area>10:
                # adjacent l1,l3es che ritorna una copia della lista di tutti i
                # node_2 = Node(rect_2.id)
                #    if rect_1 is not rect_2:
                # qui potrei mettere il primo membro |y2-y1|<3 per includere anche quelle vicini ma non attaccati anche se probabilmente fanno parte di un altra cc
                # if ((rect_2.list_sides[2].y2==rect_1.list_sides[0].y1-1) and (rect_2.list_sides[2].x2<=rect_1.list_sides[0].x2) and (rect_2.list_sides[2].x1>=rect_1.list_sides[0].x1)) :
                if (np.abs(rect_2.y4-rect_1.y1)<4 and (rect_2.x4<=rect_1.x2+3) and (rect_2.x3>=rect_1.x1-3)) :
                    a=np.abs(rect_1.x2-rect_1.x1)
                    b=np.abs(rect_2.x2-rect_2.x1)
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)


                    if(percent_adjacency<0.7):# questo mi basta per capire se i due rettangoli sono allineati

                        thinckness_r1=min( (a) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor

                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 1, 3] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 1, 3])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 3, 1])
                        else:
                            if ['ADIACENT',rect_2, 1, 3] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT',rect_2, 1, 3])
                                rect_2.list_adjacent_rect.append(['ADIACENT',rect_1, 3, 1])


                    else:

                        if np.abs((rect_1.x2-rect_2.x2))<=1 and  np.abs(rect_1.x1-rect_2.x1)<=1:
                            r=Rectangle(rect_1.id)
                            r.x2=max(rect_1.x2,rect_2.x2)
                            r.x1=min(rect_1.x1,rect_2.x1)
                            r.y1=rect_2.y1
                            r.y2=rect_2.y2
                            r.x3=max(rect_1.x2,rect_2.x2)
                            r.x4=min(rect_1.x1,rect_2.x1)
                            r.y3=rect_1.y3
                            r.y4=rect_1.y4
                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                            # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0: #and len(rect_1.list_adjacent_rect)>0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                        #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente
                                        a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0: #and len(rect_2.list_adjacent_rect)>0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                       # a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))
                                         a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])
                            elif len(rect_1.list_adjacent_rect)>0: # and len(rect_2.list_adjacent_rect)>0:

                                for a in rect_1.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])


                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            #print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)
                            merge=True
                        else:
                            if ['ALIGNED',rect_2, 1, 3] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 1, 3])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 3, 1])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]

                                #list_new.append(rect_1)

                                # if merge:
                                #     break

                                # adjacent l3,l1
                                #    if((rect_2.list_sides[0].y2==rect_1.list_sides[2].y1+1) and (rect_2.list_sides[0].x1<=rect_1.list_sides[2].x1) and (rect_2.list_sides[0].x2>=rect_1.list_sides[2].x2) ):
                elif(np.abs(rect_2.y2-rect_1.y3)<4 and (rect_2.x1<=rect_1.x3+3) and (rect_2.x2>=rect_1.x4-3)):
                    a=np.abs(rect_1.x2-rect_1.x1)
                    b=np.abs(rect_2.x2-rect_2.x1)
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)
                    #percent_adjacency=min(float(rect_1.x2-rect_1.x1),float(rect_2.x2-rect_2.x1))/(max(float(rect_1.x2-rect_1.x1),float(rect_2.x2-rect_2.x1))+1)


                    if(percent_adjacency<0.7):
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor

                        # if(rect_1.x2-rect_1.x1>rect_2.x2-rect_1.x2):
                        #  if( (min( (rect_2.x2-rect_2.x1) , (rect_2.y4-rect_2.y1) ) == (rect_2.x2-rect_2.x1)) and ( min( (rect_1.x3-rect_1.x4) , (rect_1.y4-rect_1.y1) ) == (rect_1.x3-rect_1.x4)) ):
                        #      rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 3, 1])
                        #      rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 1, 3])

                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 3, 1] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 3, 1])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 1, 3])

                        else:
                            if ['ADIACENT',rect_2, 3, 1] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT',rect_2, 3, 1])
                                rect_2.list_adjacent_rect.append(['ADIACENT',rect_1, 1, 3])

                    else:
                        if np.abs((rect_1.x2-rect_2.x2))<=1 and  np.abs(rect_1.x1-rect_2.x1)<=1:
                            r=Rectangle(rect_1.id)
                            r.x2=max(rect_1.x2,rect_2.x2)
                            r.x1=min(rect_1.x1,rect_2.x1)
                            r.y1=rect_1.y1
                            r.y2=rect_1.y2
                            r.x3=max(rect_1.x2,rect_2.x2)
                            r.x4=min(rect_1.x1,rect_2.x1)
                            r.y3=rect_2.y3
                            r.y4=rect_2.y4

                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                           # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                        #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente
                                        a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                        #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))
                                        a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            else:

                                for a in rect_1.list_adjacent_rect:
                                    #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            #print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)
                            merge=True
                        else:
                            if ['ALIGNED',rect_2, 3, 1] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 3, 1])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 1, 3])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]

                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]

                                #list_new.append(rect_1)
                                # if merge:
                                #     break

                                # adjacent l2,l4
                                #    if((rect_2.list_sides[3].x1==rect_1.list_sides[1].x2+1) and (rect_2.list_sides[3].y1>=rect_1.list_sides[1].y1) and (rect_2.list_sides[3].y2<=rect_1.list_sides[1].y2) ):

                elif(np.abs(rect_2.x4-rect_1.x3)<4 and (rect_2.y4>=rect_1.y2-3) and (rect_2.y1<=rect_1.y3+3) ):
                    a=np.abs(rect_1.y3-rect_1.y2)
                    b=np.abs(rect_2.y3-rect_2.y2)
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)


                    if(percent_adjacency<0.7):
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor
                        # if( (min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) ) == (rect_1.y4-rect_1.y1)) and ( min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) ) == (rect_2.y4-rect_2.y1)) ):
                        #     rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 2, 4])
                        #     rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 4, 2])

                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 2, 4] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 2, 4])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 4, 2])

                        else:
                            if ['ADIACENT', rect_2, 2, 4] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT', rect_2, 2, 4])
                                rect_2.list_adjacent_rect.append(['ADIACENT', rect_1, 4, 2])
                    else:
                        if np.abs((rect_1.y1-rect_2.y1))<=1 and  np.abs(rect_1.y4-rect_2.y4)<=1:
                            r=Rectangle(rect_1.id)
                            r.y4=max(rect_1.y4,rect_2.y4)
                            r.y1=min(rect_1.y1,rect_2.y1)
                            r.x1=rect_1.x1
                            r.x4=rect_1.x4
                            r.y3=max(rect_1.y4,rect_2.y4)
                            r.y2=min(rect_1.y1,rect_2.y1)
                            r.x3=rect_2.x3
                            r.x2=rect_2.x2
                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                           # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                       # a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente
                                         a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                        a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            else:

                                for a in rect_1.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            merge=True
                            # print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)

                        else:
                            if ['ALIGNED',rect_2, 2, 4] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 2, 4])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 4, 2])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]
                                #list_new.append(rect_1)
                                # if merge:
                                #     break
                                # adjacent l4,l2
                                #    if((rect_2.list_sides[1].x1==rect_1.list_sides[3].x2-1) and (rect_2.list_sides[1].y1<=rect_1.list_sides[3].y1) and (rect_2.list_sides[1].y2>=rect_1.list_sides[3].y2) ):

                elif(np.abs(rect_2.x2-rect_1.x1)<4 and (rect_2.y2<=rect_1.y4+3) and (rect_2.y3>=rect_1.y1-3) ):
                    a=np.abs(rect_1.y3-rect_1.y2)
                    b=np.abs(rect_2.y3-rect_2.y2)
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)
                    #percent_adjacency=min(float(rect_1.y3-rect_1.y2),float(rect_2.y3-rect_2.y2))/(max(float(rect_1.y3-rect_1.y2),float(rect_2.y3-rect_2.y3))+1)


                    if(percent_adjacency<0.7):
                        # if( (min( (rect_2.x2-rect_2.x1) , (rect_2.y4-rect_2.y1) ) == (rect_2.y4-rect_2.y1)) and ( min( (rect_1.x3-rect_1.x4) , (rect_1.y4-rect_1.y1) ) == (rect_1.y4-rect_1.y1)) ):
                        #     rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 4, 2])
                        #     rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 2, 4])
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor
                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 4, 2] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 4, 2])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 2, 4])

                        else:
                            if ['ADIACENT', rect_2, 4, 2] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT', rect_2, 4, 2])
                                rect_2.list_adjacent_rect.append(['ADIACENT', rect_1, 2, 4])


                    else:
                        if np.abs((rect_1.y1-rect_2.y1))<=1 and  np.abs(rect_1.y4-rect_2.y4)<=1:
                            r=Rectangle(rect_1.id)
                            r.y4=max(rect_1.y4,rect_2.y4)
                            r.y1=min(rect_1.y1,rect_2.y1)
                            r.x1=rect_2.x1
                            r.x4=rect_2.x4
                            r.y3=max(rect_1.y4,rect_2.y4)
                            r.y2=min(rect_1.y1,rect_2.y1)
                            r.x3=rect_1.x3
                            r.x2=rect_1.x2
                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                           # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                        a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                        a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            else:

                                for a in rect_1.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            #  print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)
                            merge=True
                        else:
                            if ['ALIGNED',rect_2, 4, 2] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 4, 2])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 2, 4])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]
                                #list_new.append(rect_1)
                                # else:
                                #     rect_2.num_set_of_rectangle=0

                    # if merge:
                    #     break            # if len(list_of_serch)==0 and len(rect_1.list_adjacent_rect)>0:
                #     self.list_new.append(rect_1)
                if merge:
                    break

    ####################################################################################################################

    def find_adjacent_rectangles3(self):
        list_of_serch=self.list_rect[:]# create new list
        #print len(self.list_rect)
        while(len(list_of_serch)>0):

            rect_1=list_of_serch.pop()


            merge=False
            for rect_2 in list_of_serch:
                # adjacent l1,l3
                # if rect_2.get_area>10:
                # adjacent l1,l3es che ritorna una copia della lista di tutti i
                # node_2 = Node(rect_2.id)
                #    if rect_1 is not rect_2:
                # qui potrei mettere il primo membro |y2-y1|<3 per includere anche quelle vicini ma non attaccati anche se probabilmente fanno parte di un altra cc
                # if ((rect_2.list_sides[2].y2==rect_1.list_sides[0].y1-1) and (rect_2.list_sides[2].x2<=rect_1.list_sides[0].x2) and (rect_2.list_sides[2].x1>=rect_1.list_sides[0].x1)) :
                if (np.abs(rect_2.y4-rect_1.y1)<4 and (rect_2.x4<=rect_1.x2+3) and (rect_2.x3>=rect_1.x1-3)) :
                    a=np.abs(rect_1.x2-rect_1.x1)
                    b=np.abs(rect_2.x2-rect_2.x1)
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)


                    if(percent_adjacency<0.7):# questo mi basta per capire se i due rettangoli sono allineati

                        thinckness_r1=min( (a) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor

                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 1, 3] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 1, 3])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 3, 1])
                        else:
                            if ['ADIACENT',rect_2, 1, 3] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT',rect_2, 1, 3])
                                rect_2.list_adjacent_rect.append(['ADIACENT',rect_1, 3, 1])


                    else:

                        if np.abs((rect_1.x2-rect_2.x2))<=1 and  np.abs(rect_1.x1-rect_2.x1)<=1:
                            r=Rectangle(rect_1.id)
                            r.x2=max(rect_1.x2,rect_2.x2)
                            r.x1=min(rect_1.x1,rect_2.x1)
                            r.y1=rect_2.y1
                            r.y2=rect_2.y2
                            r.x3=max(rect_1.x2,rect_2.x2)
                            r.x4=min(rect_1.x1,rect_2.x1)
                            r.y3=rect_1.y3
                            r.y4=rect_1.y4
                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                            # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0: #and len(rect_1.list_adjacent_rect)>0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                        #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente
                                        a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0: #and len(rect_2.list_adjacent_rect)>0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                       # a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))
                                         a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])
                            elif len(rect_1.list_adjacent_rect)>0: # and len(rect_2.list_adjacent_rect)>0:

                                for a in rect_1.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])


                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            #print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)
                            merge=True
                        else:
                            if ['ALIGNED',rect_2, 1, 3] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 1, 3])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 3, 1])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]

                                #list_new.append(rect_1)

                                # if merge:
                                #     break

                                # adjacent l3,l1
                                #    if((rect_2.list_sides[0].y2==rect_1.list_sides[2].y1+1) and (rect_2.list_sides[0].x1<=rect_1.list_sides[2].x1) and (rect_2.list_sides[0].x2>=rect_1.list_sides[2].x2) ):
                elif(np.abs(rect_2.y2-rect_1.y3)<4 and (rect_2.x1<=rect_1.x3+3) and (rect_2.x2>=rect_1.x4-3)):
                    a=np.abs(rect_1.x2-rect_1.x1)
                    b=np.abs(rect_2.x2-rect_2.x1)
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)
                    #percent_adjacency=min(float(rect_1.x2-rect_1.x1),float(rect_2.x2-rect_2.x1))/(max(float(rect_1.x2-rect_1.x1),float(rect_2.x2-rect_2.x1))+1)


                    if(percent_adjacency<0.7):
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor

                        # if(rect_1.x2-rect_1.x1>rect_2.x2-rect_1.x2):
                        #  if( (min( (rect_2.x2-rect_2.x1) , (rect_2.y4-rect_2.y1) ) == (rect_2.x2-rect_2.x1)) and ( min( (rect_1.x3-rect_1.x4) , (rect_1.y4-rect_1.y1) ) == (rect_1.x3-rect_1.x4)) ):
                        #      rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 3, 1])
                        #      rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 1, 3])

                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 3, 1] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 3, 1])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 1, 3])

                        else:
                            if ['ADIACENT',rect_2, 3, 1] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT',rect_2, 3, 1])
                                rect_2.list_adjacent_rect.append(['ADIACENT',rect_1, 1, 3])

                    else:
                        if np.abs((rect_1.x2-rect_2.x2))<=1 and  np.abs(rect_1.x1-rect_2.x1)<=1:
                            r=Rectangle(rect_1.id)
                            r.x2=max(rect_1.x2,rect_2.x2)
                            r.x1=min(rect_1.x1,rect_2.x1)
                            r.y1=rect_1.y1
                            r.y2=rect_1.y2
                            r.x3=max(rect_1.x2,rect_2.x2)
                            r.x4=min(rect_1.x1,rect_2.x1)
                            r.y3=rect_2.y3
                            r.y4=rect_2.y4

                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                           # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                        #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente
                                        a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                        #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))
                                        a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            else:

                                for a in rect_1.list_adjacent_rect:
                                    #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            #print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)
                            merge=True
                        else:
                            if ['ALIGNED',rect_2, 3, 1] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 3, 1])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 1, 3])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]

                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]

                                #list_new.append(rect_1)
                                # if merge:
                                #     break

                                # adjacent l2,l4
                                #    if((rect_2.list_sides[3].x1==rect_1.list_sides[1].x2+1) and (rect_2.list_sides[3].y1>=rect_1.list_sides[1].y1) and (rect_2.list_sides[3].y2<=rect_1.list_sides[1].y2) ):

                elif(np.abs(rect_2.x4-rect_1.x3)<4 and (rect_2.y4>=rect_1.y2-3) and (rect_2.y1<=rect_1.y3+3) ):
                    a=np.abs(rect_1.y3-rect_1.y2)
                    b=np.abs(rect_2.y3-rect_2.y2)
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)


                    if(percent_adjacency<0.7):
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor
                        # if( (min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) ) == (rect_1.y4-rect_1.y1)) and ( min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) ) == (rect_2.y4-rect_2.y1)) ):
                        #     rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 2, 4])
                        #     rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 4, 2])

                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 2, 4] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 2, 4])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 4, 2])

                        else:
                            if ['ADIACENT', rect_2, 2, 4] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT', rect_2, 2, 4])
                                rect_2.list_adjacent_rect.append(['ADIACENT', rect_1, 4, 2])
                    else:
                        if np.abs((rect_1.y1-rect_2.y1))<=1 and  np.abs(rect_1.y4-rect_2.y4)<=1:
                            r=Rectangle(rect_1.id)
                            r.y4=max(rect_1.y4,rect_2.y4)
                            r.y1=min(rect_1.y1,rect_2.y1)
                            r.x1=rect_1.x1
                            r.x4=rect_1.x4
                            r.y3=max(rect_1.y4,rect_2.y4)
                            r.y2=min(rect_1.y1,rect_2.y1)
                            r.x3=rect_2.x3
                            r.x2=rect_2.x2
                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                           # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                       # a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente
                                         a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                        a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            else:

                                for a in rect_1.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            merge=True
                            # print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)

                        else:
                            if ['ALIGNED',rect_2, 2, 4] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 2, 4])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 4, 2])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]
                                #list_new.append(rect_1)
                                # if merge:
                                #     break
                                # adjacent l4,l2
                                #    if((rect_2.list_sides[1].x1==rect_1.list_sides[3].x2-1) and (rect_2.list_sides[1].y1<=rect_1.list_sides[3].y1) and (rect_2.list_sides[1].y2>=rect_1.list_sides[3].y2) ):

                elif(np.abs(rect_2.x2-rect_1.x1)<4 and (rect_2.y2<=rect_1.y4+3) and (rect_2.y3>=rect_1.y1-3) ):
                    a=np.abs(rect_1.y3-rect_1.y2)
                    b=np.abs(rect_2.y3-rect_2.y2)
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)
                    #percent_adjacency=min(float(rect_1.y3-rect_1.y2),float(rect_2.y3-rect_2.y2))/(max(float(rect_1.y3-rect_1.y2),float(rect_2.y3-rect_2.y3))+1)


                    if(percent_adjacency<0.7):
                        # if( (min( (rect_2.x2-rect_2.x1) , (rect_2.y4-rect_2.y1) ) == (rect_2.y4-rect_2.y1)) and ( min( (rect_1.x3-rect_1.x4) , (rect_1.y4-rect_1.y1) ) == (rect_1.y4-rect_1.y1)) ):
                        #     rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 4, 2])
                        #     rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 2, 4])
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor
                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 4, 2] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 4, 2])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 2, 4])

                        else:
                            if ['ADIACENT', rect_2, 4, 2] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT', rect_2, 4, 2])
                                rect_2.list_adjacent_rect.append(['ADIACENT', rect_1, 2, 4])


                    else:
                        if np.abs((rect_1.y1-rect_2.y1))<=1 and  np.abs(rect_1.y4-rect_2.y4)<=1:
                            r=Rectangle(rect_1.id)
                            r.y4=max(rect_1.y4,rect_2.y4)
                            r.y1=min(rect_1.y1,rect_2.y1)
                            r.x1=rect_2.x1
                            r.x4=rect_2.x4
                            r.y3=max(rect_1.y4,rect_2.y4)
                            r.y2=min(rect_1.y1,rect_2.y1)
                            r.x3=rect_1.x3
                            r.x2=rect_1.x2
                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                           # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                        a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                        a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            else:

                                for a in rect_1.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            #  print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)
                            merge=True
                        else:
                            if ['ALIGNED',rect_2, 4, 2] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 4, 2])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 2, 4])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]
                                #list_new.append(rect_1)
                                # else:
                                #     rect_2.num_set_of_rectangle=0

                    # if merge:
                    #     break            # if len(list_of_serch)==0 and len(rect_1.list_adjacent_rect)>0:
                #     self.list_new.append(rect_1)
                #vado a vedere se i due scheletri si incontrano
                elif (self.skeleton_intersect(rect_1,rect_2)):
                    a = np.abs(rect_1.get_min_len())
                    b = np.abs(rect_2.get_min_len())
                    thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                    thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                    divisor = float(max(thinckness_r1,thinckness_r2))
                    if divisor == 0:
                        ratio_of_thicknesses=0
                    else:
                        ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor
                    if (ratio_of_thicknesses<0.5):
                        if ['TANGENT',rect_2, 0, 0] not in rect_1.list_adjacent_rect:
                            rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 0, 0])
                            rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 0, 0])

                    else:
                        if ['ADIACENT', rect_2, 0, 0] not in rect_1.list_adjacent_rect:
                            rect_1.list_adjacent_rect.append(['ADIACENT', rect_2, 0, 0])
                            rect_2.list_adjacent_rect.append(['ADIACENT', rect_1, 0, 0])

                if merge:
                    break

    def skeleton_intersect(self,r1,r2):
        # lll1 = r1.get_max_len()
        # lll2 = r2.get_max_len()
        # ll1 = r1.get_min_len()
        # ll2 = r2.get_min_len()

        x11 = r1.sk_x1
        y11 = r1.sk_y1
        x12 = r1.sk_x2
        y12 = r1.sk_y2

        x21 = r2.sk_x1
        y21 = r2.sk_y1
        x22 = r2.sk_x2
        y22 = r2.sk_y2
        # x21==613 and y21==1300 and x22==916
        # x21==613 and y21==1300 and x22==916
        a1 = self.ccw(x11, y11, x12, y12, x21, y21)
        a2 = self.ccw(x11, y11, x12, y12, x22, y22)
        a3 = self.ccw(x21, y21, x22, y22, x11, y11)
        a4 = self.ccw(x21, y21, x22, y22, x12, y12)
        # print r1.skeleton
        # print r2.skeleton
        # print '----------------------------------'
        if (a1*a2) <=0 and (a3*a4) <=0:
            return True
        else:
            return False

    def ccw(self,x1,y1,x2,y2,x3,y3):
        dx1 = x2-x1
        dy1 = y2-y1
        dx2 = x3-x1
        dy2 = y3-y1

        if (dx1*dy2 > dy1*dx2):
            return 1
        elif (dx1*dy2 < dy1*dx2):
            return -1
        elif (dx1*dx2 < 0 or dy1*dy2 < 0):
            return -1
        elif ((dx1*dx1 + dy1*dy1) < (dx2*dx2 + dy2*dy2)):
            return 1
        else:
            return 0

    ####################################################################################################################

    @contatempo
    def classification_walls(self,debug_mode):
        # cv2.imwrite(self.path_debug+'wall/'+str(self.file_name[0:len(self.file_name)-4])+'_0_original.png', self.original_image)
        # cv2.imwrite(self.path_debug+'wall/'+str(self.file_name[0:len(self.file_name)-4])+'_0_noText.png', self.image_remove_text)
        self.classification_external_walls(debug_mode)
        self.recostructed_image_whit_rectangle_oblique(debug_mode)
        self.classification_interior_walls(debug_mode)


        # self.recostructed_image_whit_rectangle()
        #
        # self.classification_oblique_walls()
        # self.recostructed_image_whit_rectangle_wall()

    def classification_external_walls(self,debug_mode):
        index_inc = 0
        for cc in self.list_cc:
             self.list_rect.extend(cc.extract_rectangle_in_cc_3(index_inc))
             index_inc = len(self.list_rect)
        self.find_run_pixel(debug_mode)
        self.find_adjacent_rectangles3()
        self.create_graph()
        self.find_external_wall(debug_mode)


        # self.recostructed_image_whit_rectangle()
        # self.find_external_wall()

    def create_graph(self):
        for r in self.list_rect:
            # nodo=Node(r.id)
            # nodo.set_coordinate(r.x1,r.y1,r.x3,r.y3);
            # l = np.argwhere(self.original_image[r.y1:r.y3+1,r.x1:r.x3+1])
            # nodo.set_list_point(l)

            self.graph_rect.add_node(r)

        # print 'len list: '+str(len(self.list_rect))
        # print 'len graph: '+ str(len(self.graph_rect.nodes()))

        id_edge=0;
        for r in self.list_rect:
            lll = r.get_max_len()
            n = self.graph_rect.get_node(Rectangle(r.id))
            if len(r.list_adjacent_rect)>0:
                for a in r.list_adjacent_rect:
                    n2 = self.graph_rect.get_node(Rectangle(a[1].id))
                    id_edge = str(max(n.id,n2.id)) + str(min(n.id,n2.id)) + str(min(n.id,n2.id)) + str(max(n.id,n2.id))
                    a1 = Edge(id_edge,n2,a[0])
                    a2 = Edge(id_edge,n,a[0])
                    n.add_edge(a1)
                    n2.add_edge(a2)


        # for r in self.list_rect:
        #      print 'ad: '+str(len(r.list_adjacent_rect))
        #      n = self.graph_rect.get_node(Rectangle(r.id))
        #      print 'archi: '+str(len(n.edges()))

    def find_run_pixel(self, debug_mode):
        # path_hist='/home/stefano/Scrivania/TestTesi/last_test/RunPixel/histogram/'
        # path_io='/home/stefano/Scrivania/TestTesi/last_test/RunPixel/image_original/'
        # path_ic='/home/stefano/Scrivania/TestTesi/last_test/RunPixel/image_color/'
        #cv2.imwrite(path_io+self.file_name+'.png', self.original_image)# save image original

        l=[cc for cc in self.list_cc if cc.getArea()>(self.height*self.width)/10]
        run_hist=[]
        i=0
        limit=max(self.width,self.height)
        m=0

        if(len(l)==0):
            for cc in self.list_cc:
                if cc.getArea()>m:
                    maxi=cc
            l.append(maxi)



        #find run pixel
        for cc in l:
            i=i+1
            #cv2.imwrite(path+self.file_name+'_CC_num_'+str(i)+'.png', cc.image)
            run=[]
            a=[]
            h,w=cc.image.shape

            for i in range(0,h):
                row=cc.image[i,:]
                a.extend([(len(list(group)),name) for name, group in groupby(row)])
            run.extend([item for item in a if item[1] == 255])

            b=[]
            for i in range(0,w):
                col=cc.image[:,i].transpose()
                b.extend([(len(list(group)),name) for name, group in groupby(col)])

            run.extend([item for item in b if item[1] == 255])

            run_np = np.asarray(run)
            run_hist.extend(run_np[:,0])



            if limit>min(h,w):
                limit = min(h,w)


        #plot histogram
        hist, bins =np.histogram(run_hist,bins= range(1,int(max(run_hist))+2), range=(int(min(run_hist)), int(max(run_hist))))

        max_first_wall=1
        max_tink_wall=1
        first=1
        for i in range(0,len(hist)):

            if hist[i] >= (limit*0.8):
                if i==0:
                    if hist[0]>hist[1]:
                        max_first_wall=1
                        max_tink_wall=1
                        first=0
                    else:
                        max_tink_wall=2
                        max_first_wall=2
                        first=0
                else:
                    if hist[i]>hist[i+1] and hist[i]>hist[i-1]:
                        max_tink_wall=i+1
                        if first:
                            max_first_wall=i+1
                            first=0
        list_first_wall=[]
        list_tink_wall=[]
        self.max_tink_wall=max_tink_wall
        if (max_tink_wall-max_first_wall)>4:
            if max_first_wall==1:
                list_first_wall.append(1)
                list_first_wall.append(2)
            else:
                lim=int(max_first_wall*1.5+1)-int(max_first_wall*0.9)
                for j in range(0,lim+1):
                    list_first_wall.append(int(max_first_wall*0.9)+j)

            if max_tink_wall==1:
                list_tink_wall.append(1)
                list_tink_wall.append(2)
            else:
                lim=int(max_tink_wall*1.5+1)-int(max_tink_wall*0.9)
                for j in range(0,lim+1):
                    list_tink_wall.append(int(max_tink_wall*0.9)+j)

        elif (max_tink_wall-max_first_wall)<=4 and (max_tink_wall-max_first_wall)>=2:

            if max_first_wall==1:
                list_first_wall.append(1)
                list_first_wall.append(2)
            else:
                list_first_wall.append(max_first_wall-1)
                list_first_wall.append(max_first_wall)
                list_first_wall.append(max_first_wall+1)

            if max_tink_wall==1:
                list_tink_wall.append(1)
                list_tink_wall.append(2)
            else:
                list_tink_wall.append(max_tink_wall-1)
                list_tink_wall.append(max_tink_wall)
                list_tink_wall.append(max_tink_wall+1)


        else:
            list_tink_wall.append(max_tink_wall)
            list_first_wall.append(max_first_wall)

        self.max_tink_wall = max_tink_wall
       #  width =  (bins[1] - bins[0])
       #  center = (bins[:-1] + bins[1:]) / 2
       #  barlist=plt.bar(range(1,61), hist[0:60], align='center', width=width)
       #  if max_tink_wall< 60:
       #      for item in list_tink_wall:
       #          barlist[item-1].set_color('r')
       #      for item in list_first_wall:
       #          barlist[item-1].set_color('g')
       #          #
       # # plt.savefig(self.path_debug+self.file_name+'_hist_runpixel.png')
       #  print hist
       #  plt.show()



        # plot image color
        image = np.zeros((self.height,self.width,3),dtype=np.uint8)
        self.image_run_pixel =  np.zeros((self.height,self.width,3),dtype=np.uint8)
        img_or = self.original_image[:]
        img_or.astype(np.float32)
        for i in range(0,self.height):
            a2=[]

            row=np.abs(img_or[i,:]-255.0)
            a2.extend([(len(list(group)),name) for name, group in groupby(row)])

            index=0
            for p in a2:
                if (p[1]==255):
                    #255,0,0 rosso
                    #0,255,0 verde
                    #if (p[0]==max_tink_wall or p[0]==check_mtw_dx or p[0]==check_mtw_sx):
                    if (p[0] in list_tink_wall ):

                        image[i,index:index+p[0]]=(0,0,255)
                        self.image_run_pixel[i,index:index+p[0]]=(0,0,255)

                    # elif (p[0]==max_freq_wall  or p[0]==check_mfw_dx or p[0]==check_mfw_sx):
                    elif (p[0] in list_first_wall):
                        # if not((0,0,255) in image[i,index:index+p[0]]):
                        image[i,index:index+p[0]]=(0,255,0)
                    else:

                        if ( np.sum(image[i,index:index+p[0]])==0):
                            image[i,index:index+p[0]]=(255,255,255)
                index=index+p[0]


        for i in range(0,self.width):
            b2=[]
            col=np.abs(img_or[:,i].transpose()-255.0)
            b2.extend([(len(list(group)),name) for name, group in groupby(col)])
            index=0
            for p in b2:
                if (p[1]==255):
                    #255,0,0 rosso
                    #0,255,0 verde
                    if(p[0] in list_tink_wall ):
                        # if(p[0]==max_tink_wall  or p[0]==check_mtw_dx or p[0]==check_mtw_sx):

                        image[index:index+p[0],i]=(0,0,255)
                        self.image_run_pixel[index:index+p[0],i]=(0,0,255)
                    #elif(p[0]==max_freq_wall or p[0]==check_mfw_dx or p[0]==check_mfw_sx):
                    elif(p[0] in list_first_wall):
                        #  if not( (0,0,255) in image[index:index+p[0],i]):
                        image[index:index+p[0],i]=(0,255,0)

                    else:

                        if(np.sum(image[index:index+p[0],i])==0):

                            image[index:index+p[0],i]=(255,255,255)
                index=index+p[0]
        # if debug_mode:
        #     cv2.imwrite(self.path_debug+'wall/'+str(self.file_name[0:len(self.file_name)-4])+'_1_RunPixel.png', image)


    def find_external_wall(self,debug_mode):
        image=np.zeros((self.height,self.width,3),dtype=np.int32)
        # for rect in self.graph_rect.nodes():

        for rect in self.list_rect:
            rect.x_max=rect.x2
            rect.x_min=rect.x1
            rect.y_min=rect.y1
            rect.y_max=rect.y3


            if self.max_tink_wall>20 :
                if (rect.get_min_len()>(float(self.max_tink_wall)*0.4) and rect.get_min_len()<(float(self.max_tink_wall)*2 )):
                    rect.type = 'wall'
                    self.graph_wall.add_node(rect)
            else:
                if (rect.get_min_len()>(float(self.max_tink_wall)*0.6) and rect.get_min_len()<(float(self.max_tink_wall)*2)):
                    rect.type = 'wall'
                    self.graph_wall.add_node(rect)

        # for rec in self.graph_wall.nodes():
        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(0,0,255)
        # cv2.imwrite(self.path_debug+'wall/'+str(self.file_name[0:len(self.file_name)-4])+'_2_ExternalWall.png', image)

        for rect in self.graph_wall.nodes():
            r = 0
            g = 0
            b = 255

            #if rect.type=="oblique":
            p = np.array([[[rect.x1, rect.y1],[rect.x2, rect.y2],[rect.x3, rect.y3],[rect.x4, rect.y4]]], dtype=np.int32)
            cv2.drawContours(image, [p], -1, (r,g,b), -1)

        # if debug_mode:
        #     cv2.imwrite(self.path_debug+'wall/'+str(self.file_name[0:len(self.file_name)-4])+'_2_ExternalWall.png', image)

    def classification_interior_walls(self, debug_mode):
        check = 1
       # print len(self.graph_wall.nodes())


        image=np.zeros((self.height,self.width,3),dtype=np.uint8)

        if(len(self.graph_image_output.nodes())>0):
            list_walls = self.graph_image_output.nodes()
        else:
            list_walls = self.graph_wall.nodes()
        while(len(list_walls)>0):


            list_new_walls = []
            if self.max_tink_wall >= 20:
                # for rect in self.graph_rect.nodes():
                #     if (rect.get_min_len()>(float(self.max_tink_wall)*0.2) and rect.get_min_len()<(float(self.max_tink_wall)*2 ) and not(self.graph_wall.isIn(rect))):
                #         rect.type = 'wall'
                #         self.graph_wall.add_node(rect)

                stop=True
                lim = float(self.max_tink_wall)

                #while(stop):
                    # lim = lim*0.4
                    # if lim<=5:
                    #     stop = False
                    #     lim=5
                lim=5
                for n in list_walls :
                    for e in n.edges():
                        rect = e.get_node()
                        #if  rect.get_min_len()>=float(self.max_tink_wall)*0.1 and rect.get_min_len()<(float(self.max_tink_wall)*2 ) and not(self.graph_wall.isIn(rect)) :
                        if  rect.get_min_len()>=lim and rect.get_min_len()<(float(self.max_tink_wall)*2 ) and rect.get_max_len()>self.max_tink_wall*1.2 and not(self.graph_wall.isIn(rect)) :
                            stop = False
                            rect.type = 'wall'
                            list_new_walls.append(rect)
                            self.graph_wall.add_node(rect)
            else:
                 for n in list_walls :
                    for e in n.edges():
                        rect = e.get_node()
                        if  rect.get_min_len()>=float(self.max_tink_wall)*0.3 and rect.get_min_len()<(float(self.max_tink_wall)*2 ) and not(self.graph_wall.isIn(rect)) :
                            rect.type = 'wall'
                            list_new_walls.append(rect)
                            self.graph_wall.add_node(rect)




            list_walls = list_new_walls
            if len(list_new_walls)>0:
                for r in list_new_walls:
                    self.graph_wall.add_node(r)
        # for rec in self.graph_wall.nodes():
        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(0,0,255)
        for rect in self.graph_wall.nodes():
            r = 0
            g = 0
            b = 255

            #if rect.type=="oblique":
            p = np.array([[[rect.x1, rect.y1],[rect.x2, rect.y2],[rect.x3, rect.y3],[rect.x4, rect.y4]]], dtype=np.int32)
            cv2.drawContours(image, [p], -1, (r,g,b), -1)

            #else:
            #image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)

        # if debug_mode:
        #     if(len(self.graph_image_output.nodes())>0):
        #         cv2.imwrite(self.path_debug+'wall/'+str(self.file_name[0:len(self.file_name)-4])+'_3_InternalWall_withDoor.png', image)
        #
        #     else:
        #         cv2.imwrite(self.path_debug+'wall/'+str(self.file_name[0:len(self.file_name)-4])+'_3_InternalWall.png', image)

    def classification_oblique_walls(self):

        index=0
        l2=[]
        #image = np.zeros((self.height,self.width,3),dtype=np.int8)
        image_point = np.array(self.color_image)
        image_rette = np.array(self.color_image)
        image = np.array(self.color_image)
        image2 = np.array(self.color_image)
        list_new_walls=[]
        for cc in self.list_cc:
            l=[]
            # cc.extract_rectangle_in_cc_2(index)
            # l.extend(cc.extract_oblique_rect_in_CC2(image_point,image_rette))
            l.extend(cc.extract_oblique_rect_in_CC(image_point,image_rette))
            if len(l)>0:
                id=0
                #for x1,y1,x2,y2,x4,y4,x3,y3,m,c in l:
                for x1,y1,x2,y2,x4,y4,x3,y3,m in l:
                    r = random.randint(0, 255)
                    g = random.randint(0, 255)
                    b = random.randint(0, 255)
                    cv2.line(image2,(int(x1+cc.x_min),int(y1+cc.y_min)),(int(x2+cc.x_min),int(y2+cc.y_min)),(r,g,b),3)
                    cv2.line(image2,(int(x3+cc.x_min),int(y3+cc.y_min)),(int(x4+cc.x_min),int(y4+cc.y_min)),(r,g,b),3)
                    cv2.line(image2,(int(x2+cc.x_min),int(y2+cc.y_min)),(int(x3+cc.x_min),int(y3+cc.y_min)),(r,g,b),3)
                    cv2.line(image2,(int(x4+cc.x_min),int(y4+cc.y_min)),(int(x1+cc.x_min),int(y1+cc.y_min)),(r,g,b),3)
                    # costruisco immagine col il solo rettangolo presente
                    img = np.zeros((self.height,self.width))
                    # ricalcolo il coeff angolare
                    d12 = math.sqrt((x1-x2)**2 +(y2-y1)**2)
                    d14 = math.sqrt((x1-x4)**2 +(y4-y1)**2)

                    if d12 > d14:
                        if x1 != x2:
                            m = float(y2-y1)/float(x2-x1)
                    else:
                        if x1 != x4:
                            m = float(y4-y1)/float(x4-x1)

                    pts = np.array([[x1+cc.x_min,y1+cc.y_min],[x2+cc.x_min,y2+cc.y_min],[x3+cc.x_min,y3+cc.y_min],[x4+cc.x_min,y4+cc.y_min]], np.int32)

                    pts = pts.reshape((-1,1,2))
                    cv2.fillPoly(img,[pts],True,1)

                    #cv2.imwrite(self.path_debug+'oblique_walls/'+str(id)+'.png',img*255)

                    if d12>1 and d14>1:
                        l2.append([x1+cc.x_min, y1+cc.y_min, x2+cc.x_min, y2+cc.y_min, x3+cc.x_min, y3+cc.y_min, x4+cc.x_min, y4+cc.y_min, m, img, id])
                        id = id+1


                l_rec=[]

                while(len(l2)>1):

                    x1,y1,x2,y2,x3,y3,x4,y4,m,img,id1 = l2.pop()
                    l2_copy=l2[:]

                    merge = False
                    d1 = min(math.sqrt((x1-x2)**2 +(y2-y1)**2),math.sqrt((x1-x4)**2 +(y4-y1)**2))

                    for x21,y21,x22,y22,x23,y23,x24,y24,m2,img2,id2 in l2_copy:
                        d2 = min(math.sqrt((x21-x22)**2 +(y22-y21)**2),math.sqrt((x21-x24)**2 +(y24-y21)**2))
                        if (id1 != id2):
                            if m>0:
                                #if m > m2*0.8 and m < m2*1.2:
                                    # print [d1,d2]
                                if m > m2*0.8 and m < m2*1.2 and d2>d1*0.8 and d2<d1*1.2:
                                    # print '#########################################'
                                    # print x21,y21,x22,y22,x23,y23,x24,y24,m2
                                    # print  [x1,y1,x2,y2,x3,y3,x4,y4,m]
                                    logic_and = img*img2
                                    if np.sum(logic_and)>0:
                                        i = np.zeros((self.height,self.width))
                                      #  if m>0:
                                        pts = np.array([[max(x1,x21),max(y1,y21)],[max(x2,x22),max(y2,y22)],[min(x3,x23),min(y3,y23)],[min(x4,x24),min(y4,y24)]], np.int32)
                                        pts = pts.reshape((-1,1,2))
                                        cv2.fillPoly(i,[pts],True,1)
                                        new_rect=[max(x1,x21),max(y1,y21),max(x2,x22),max(y2,y22),min(x3,x23),min(y3,y23),min(x4,x24),min(y4,y24),m,i,id]
                                       # cv2.imwrite(self.path_debug+'oblique_walls/'+str(id)+'.png',i*255)
                                        id=id+1
                                        l2.append(new_rect)
                                        l2.remove([x21,y21,x22,y22,x23,y23,x24,y24,m2,img2,id2])
                                      #  print [max(x1,x21),max(y1,y21),max(x2,x22),max(y2,y22),min(x3,x23),min(y3,y23),min(x4,x24),min(y4,y24),m]
                                        merge = True
                                        break
                            else:
                                if m < m2*0.8 and m > m2*1.2 and d2>d1*0.8 and d2<d1*1.2:

                                    logic_and = img*img2
                                    if np.sum(logic_and)>0:
                                        i = np.zeros((self.height,self.width))
                                        pts = np.array([[max(x1,x21),min(y1,y21)],[max(x2,x22),min(y2,y22)],[min(x3,x23),max(y3,y23)],[min(x4,x24),max(y4,y24)]], np.int32)
                                        pts = pts.reshape((-1,1,2))
                                        cv2.fillPoly(i,[pts],True,1)
                                     #   cv2.imwrite(self.path_debug+'oblique_walls/'+str(id)+'.png',i*255)
                                        new_rect=[max(x1,x21),min(y1,y21),max(x2,x22),min(y2,y22),min(x3,x23),max(y3,y23),min(x4,x24),max(y4,y24),m,i,id]
                                        id=id+1
                                        l2.append(new_rect)
                                        l2.remove([x21,y21,x22,y22,x23,y23,x24,y24,m2,img2,id2])
                                        merge = True

                                        break
                    ins = [x1,y1,x2,y2,x3,y3,x4,y4,m,img,id1]
                    if not(merge) and not(ins in l_rec):
                        l_rec.append([x1,y1,x2,y2,x3,y3,x4,y4,m,img,id1])

                if (len(l2)>0):
                    ins = l2.pop()
                    if not(ins in l_rec):
                        l_rec.append(ins)

                for x1,y1,x2,y2,x3,y3,x4,y4,m,img,id in l_rec:
                    list_new_walls.append([x1,y1,x2,y2,x3,y3,x4,y4])
                    # r=random.randint(0, 255)
                    # g=random.randint(0, 255)
                    # b=random.randint(0, 255)
                    cv2.line(image,(int(x1),int(y1)),(int(x2),int(y2)),(0,0,255),3)
                    cv2.line(image,(int(x3),int(y3)),(int(x4),int(y4)),(0,0,255),3)
                    cv2.line(image,(int(x2),int(y2)),(int(x3),int(y3)),(0,0,255),3)
                    cv2.line(image,(int(x4),int(y4)),(int(x1),int(y1)),(0,0,255),3)

        cv2.imwrite(self.path_debug+'oblique_walls/'+str(self.file_name)+'_1.png',image_point)
        cv2.imwrite(self.path_debug+'oblique_walls/'+str(self.file_name)+'_2.png',image_rette)
        cv2.imwrite(self.path_debug+'oblique_walls/'+str(self.file_name)+'_3.png', self.color_image)
        cv2.imwrite(self.path_debug+'oblique_walls/'+str(self.file_name[0:len(self.file_name)-4])+'_5.png', image)
        if len(list_new_walls)>0:
            c = 0
            for r in list_new_walls:
                c=c+1

                id = len(self.list_rect)+c
                new_r = Rectangle(id)
                new_r.setData(r)
                new_r.type = 'wall'
                new_r.oblique = True
                self.graph_wall.add_node(new_r)

    def calculate_mean_area(self,l):
        area = 0
        for cc in l:
            # area = area + ((cc.y_max - cc.y_min) + 1)*((cc.y_max - cc.y_min) + 1)
            area = area + (cc.height*cc.width)
        return int(area/len(l))

    def recostructed_image_whit_rectangle_wall(self,debug_mode):


            image=np.zeros((self.height,self.width,3),dtype=np.uint8)
            for rec in self.graph_wall.nodes():
                if rec.oblique:
                   countors = np.array([[[rec.x1,rec.y1]],[[rec.x2,rec.y2]],[[rec.x3,rec.y3]],[[rec.x4,rec.y4]]])
                   cv2.drawContours(image, [countors], -1, (255,0,0), -1)

                else:
                    image[rec.y_min-1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
           #  if debug_mode:
           #      cv2.imwrite(self.path_debug+'wall/'+str(self.file_name[0:len(self.file_name)-4])+'_4_ObliqueWall.png', image)
           #      cv2.imwrite(self.path_debug+'merge_all/'+str(self.file_name[0:len(self.file_name)-4])+'_Wall.png', image)

    def recostructed_image_whit_rectangle_oblique(self,debug_mode):

        #image=np.zeros((self.height,self.width,3),dtype=np.uint8)
        image = np.array(self.color_image,dtype=np.int32)
        #image2=np.zeros((self.height,self.width),dtype=np.uint8)
        #self.color_image[:]
        list_id = []
        for rect in self.list_rect:
            #  print rect.id
            r=random.randint(0, 255)
            g=random.randint(0, 255)
            b=random.randint(0, 255)
            # if rect.id in list_id:
                # print 'ATTENZIONE!!!!!!!!!!!!'
                # print rect.id
            list_id.append(rect.id)
            if rect.type=="oblique":

                p = np.array([[[rect.x1, rect.y1],[rect.x2, rect.y2],[rect.x3, rect.y3],[rect.x4, rect.y4]]], dtype=np.int32)
                cv2.drawContours(image, [p], -1, (r,g,b), -1)

            else:
                image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)


        # if debug_mode:
        #     cv2.imwrite(self.path_debug+'wall/'+str(self.file_name[0:len(self.file_name)-4])+'_ricostruzione.png', image)
        #     cv2.imwrite(self.path_debug+'door/'+str(self.file_name), image)









    # find door
    def classification_door(self,debug_mode):
        list_door = self.find_door_select_for_length(debug_mode)
        dic_rect = self.find_orientation_rectangle(list_door,debug_mode)
        self.find_left_arc_door(dic_rect)
        self.find_right_arc_door(dic_rect)
        self.find_down_arc_door(dic_rect)
        self.find_up_arc_door(dic_rect)
        image2 =  np.array(self.color_image)
        #print 'max tinck wall: '+str(self.max_tink_wall)
        for rec in self.list_door:
            #if rec.type == 'door':
            image2[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(0,255,0)
        if debug_mode:
            cv2.imwrite(self.path_debug+'door/'+str(self.file_name[0:len(self.file_name)-4])+'_final.png', image2)
            cv2.imwrite(self.path_debug+'merge_all/'+str(self.file_name[0:len(self.file_name)-4])+'_Door.png', image2)

    def find_door_select_for_length(self,debug_mode):
        list_door = []

        # cv2.imwrite(self.path_debug+'door/ante/'+str(self.file_name[0:len(self.file_name)-4])+'.png', self.color_image)
        for r in self.list_rect:
            if self.max_tink_wall<=19:
                if r.get_max_len()>(self.max_tink_wall*3)and r.get_max_len()<(self.max_tink_wall*10) and r.get_min_len() < (self.max_tink_wall*0.8):
                   # if r.type!='wall':
                    list_door.append(r)
            if self.max_tink_wall>19:
                if r.get_max_len()>(self.max_tink_wall) and r.get_max_len()<(self.max_tink_wall*5)and r.get_min_len() < (self.max_tink_wall*0.4) :
                    #if r.type!='wall':
                        list_door.append(r)


        image =  np.array(self.color_image)
        for rec in list_door:
            r=0
            g=255
            b=0
            image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)
        if debug_mode:
            cv2.imwrite(self.path_debug+'door/'+str(self.file_name[0:len(self.file_name)-4])+'_1.png', image)
        return list_door

    def find_orientation_rectangle(self,list_door,debug_mode):

        r=0
        g=255
        b=0
        image =  np.array(self.color_image)
        dic_rect = {'left':[],'right':[],'down':[],'up':[]}
        for rect in list_door:
            ori = rect.get_orientation()
            #thickness_door = rect.get_min_len()+1
            r_length = rect.get_max_len()
            if ori == 'vertical':
                if self.max_tink_wall<20:
                    # coordinate del riquadro a sinistra del rettangolo
                    x_min_left = rect.x2 - r_length - self.max_tink_wall
                    y_min_left = rect.y2 - int(self.max_tink_wall)
                    x_max_left = rect.x3 + 1
                    y_max_left = rect.y3 + int(self.max_tink_wall)
                    # coordinate del riquadro a destra del rettangolo
                    x_min_right = rect.x1 -1
                    y_min_right = rect.y1 - int(self.max_tink_wall)
                    x_max_right = rect.x4 + r_length + (self.max_tink_wall)
                    y_max_right = rect.y4 + int(self.max_tink_wall)

                    list_left_rect = []
                    list_right_rect = []
                    for rect2 in self.list_rect:
                        if rect2.x1 >= x_min_left and rect2.x3 <= x_max_left and rect2.y1 >= y_min_left and rect2.y3 <= y_max_left:
                            list_left_rect.append(rect2)
                        if rect2.x1 >= x_min_right and rect2.x3 <= x_max_right and rect2.y1 >= y_min_right and rect2.y3 <= y_max_right:
                            list_right_rect.append(rect2)

                    if len(list_left_rect)>(r_length/4):
                        dic_rect['left'].append(rect)
                        rect.list_left_rect.append(list_left_rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_left,y_min_left),(x_max_left,y_max_left),(255,255,0),1)
                        # for rec in list_left_rect:
                        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_right_rect)>(r_length/4):
                        dic_rect['right'].append(rect)
                        rect.list_right_rect.append(list_right_rect)
                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_right,y_min_right),(x_max_right,y_max_right),(255,255,0),1)
                else:
                    # coordinate del riquadro a sinistra del rettangolo
                    x_min_left = rect.x2 - r_length - self.max_tink_wall
                    y_min_left = rect.y2 - int(self.max_tink_wall*2/3)
                    x_max_left = rect.x3 + 1
                    y_max_left = rect.y3 + int(self.max_tink_wall*2/3)
                    # coordinate del riquadro a destra del rettangolo
                    x_min_right = rect.x1 -1
                    y_min_right = rect.y1 - int(self.max_tink_wall*2/3)
                    x_max_right = rect.x4 + r_length + (self.max_tink_wall)
                    y_max_right = rect.y4 + int(self.max_tink_wall*2/3)
                    # coordinate del riquadro a sinistra del rettangolo
                    x_min_left2 = rect.x2 - r_length - self.max_tink_wall
                    y_min_left2 = rect.y2 - int(self.max_tink_wall/2)
                    x_max_left2 = rect.x3 + 1
                    y_max_left2 = rect.y3 + int(self.max_tink_wall/2)
                    # coordinate del riquadro a destra del rettangolo
                    x_min_right2 = rect.x1 -1
                    y_min_right2 = rect.y1 - int(self.max_tink_wall/2)
                    x_max_right2 = rect.x4 + r_length + (self.max_tink_wall)
                    y_max_right2 = rect.y4 + int(self.max_tink_wall/2)


                    list_left_rect = []
                    list_right_rect = []
                    list_left_rect2 = []
                    list_right_rect2 = []
                    for rect2 in self.list_rect:
                        if rect2.x1 >= x_min_left and rect2.x3 <= x_max_left and rect2.y1 >= y_min_left and rect2.y3 <= y_max_left:
                            list_left_rect.append(rect2)
                        if rect2.x1 >= x_min_right and rect2.x3 <= x_max_right and rect2.y1 >= y_min_right and rect2.y3 <= y_max_right:
                            list_right_rect.append(rect2)
                    for rect2 in self.list_rect:
                        if rect2.x1 >= x_min_left2 and rect2.x3 <= x_max_left2 and rect2.y1 >= y_min_left2 and rect2.y3 <= y_max_left2:
                            list_left_rect2.append(rect2)
                        if rect2.x1 >= x_min_right2 and rect2.x3 <= x_max_right2 and rect2.y1 >= y_min_right2 and rect2.y3 <= y_max_right2:
                            list_right_rect2.append(rect2)

                    if len(list_left_rect)>(r_length/4):
                        if not(rect in dic_rect['left']):
                            dic_rect['left'].append(rect)
                            rect.list_left_rect.append(list_left_rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_left,y_min_left),(x_max_left,y_max_left),(255,255,0),1)
                        # for rec in list_left_rect:
                        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_right_rect)>(r_length/4):
                        if not(rect in dic_rect['right']):
                            dic_rect['right'].append(rect)
                            rect.list_right_rect.append(list_right_rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_right,y_min_right),(x_max_right,y_max_right),(255,255,0),1)
                        # for rec in list_right_rect:
                        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_left_rect2)>(r_length/4):
                        if not(rect in dic_rect['left']):
                            dic_rect['left'].append(rect)
                            rect.list_left_rect.append(list_left_rect2)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_left,y_min_left),(x_max_left,y_max_left),(255,255,0),1)
                        # for rec in list_left_rect:
                        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_right_rect2)>(r_length/4):
                        if not(rect in dic_rect['right']):
                            dic_rect['right'].append(rect)
                            rect.list_right_rect.append(list_right_rect2)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_right,y_min_right),(x_max_right,y_max_right),(255,255,0),1)

            else:
                # coordinate del riquadro sotto del rettangolo
                #   if self.max_tink_wall>19:
                if self.max_tink_wall<20:
                    x_min_down = rect.x1 - int(self.max_tink_wall)
                    y_min_down = rect.y1 - 1
                    x_max_down = rect.x2 + int(self.max_tink_wall)
                    y_max_down = rect.y2 + r_length + (self.max_tink_wall)
                    # coordinate del riquadro sopra del rettangolo
                    x_min_up = rect.x4 - int(self.max_tink_wall)
                    y_min_up = rect.y4 -  r_length - (self.max_tink_wall)
                    x_max_up = rect.x3 + int(self.max_tink_wall)
                    y_max_up = rect.y3 + 1
                    list_down_rect = []
                    list_up_rect = []
                    for rect2 in self.list_rect:
                        if rect2.x1 >= x_min_down and rect2.x3 <= x_max_down and rect2.y1 >= y_min_down and rect2.y3 <= y_max_down:
                            list_down_rect.append(rect2)
                        if rect2.x1 >= x_min_up and rect2.x3 <= x_max_up and rect2.y1 >= y_min_up and rect2.y3 <= y_max_up:
                            list_up_rect.append(rect2)

                    if len(list_down_rect)>(r_length/4):
                        rect.list_down_rect.append(list_down_rect)
                        dic_rect['down'].append(rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_down,y_min_down),(x_max_down,y_max_down),(255,255,0),1)
                        # for rec in list_down_rect:
                        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_up_rect)>(r_length/4):
                        rect.list_up_rect.append(list_up_rect)
                        dic_rect['up'].append(rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_up,y_min_up),(x_max_up,y_max_up),(255,255,0),1)
                else:
                    x_min_down = rect.x1 - int(self.max_tink_wall*2/3)
                    y_min_down = rect.y1 - 1
                    x_max_down = rect.x2 + int(self.max_tink_wall*2/3)
                    y_max_down = rect.y2 + r_length + (self.max_tink_wall)
                    # coordinate del riquadro sopra del rettangolo
                    x_min_up = rect.x4 - int(self.max_tink_wall*2/3)
                    y_min_up = rect.y4 -  r_length - (self.max_tink_wall)
                    x_max_up = rect.x3 + int(self.max_tink_wall*2/3)
                    y_max_up = rect.y3 + 1

                    x_min_down2 = rect.x1 - int(self.max_tink_wall*2/3)
                    y_min_down2 = rect.y1 - 1
                    x_max_down2 = rect.x2 + int(self.max_tink_wall*2/3)
                    y_max_down2 = rect.y2 + r_length + (self.max_tink_wall)
                    # coordinate del riquadro sopra del rettangolo
                    x_min_up2 = rect.x4 - int(self.max_tink_wall*2/3)
                    y_min_up2 = rect.y4 -  r_length - (self.max_tink_wall)
                    x_max_up2 = rect.x3 + int(self.max_tink_wall*2/3)
                    y_max_up2 = rect.y3 + 1


                    list_down_rect = []
                    list_up_rect = []
                    list_down_rect2 = []
                    list_up_rect2 = []
                    for rect2 in self.list_rect:
                        if rect2.x1 >= x_min_down and rect2.x3 <= x_max_down and rect2.y1 >= y_min_down and rect2.y3 <= y_max_down:
                            list_down_rect.append(rect2)
                        if rect2.x1 >= x_min_up and rect2.x3 <= x_max_up and rect2.y1 >= y_min_up and rect2.y3 <= y_max_up:
                            list_up_rect.append(rect2)
                    for rect2 in self.list_rect:
                        if rect2.x1 >= x_min_down2 and rect2.x3 <= x_max_down2 and rect2.y1 >= y_min_down2 and rect2.y3 <= y_max_down2:
                            list_down_rect2.append(rect2)
                        if rect2.x1 >= x_min_up2 and rect2.x3 <= x_max_up2 and rect2.y1 >= y_min_up2 and rect2.y3 <= y_max_up2:
                            list_up_rect2.append(rect2)


                    if len(list_down_rect)>(r_length/4):
                        if not(rect in dic_rect['down']):
                            rect.list_down_rect.append(list_down_rect)
                            dic_rect['down'].append(rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_down,y_min_down),(x_max_down,y_max_down),(255,255,0),1)
                        # for rec in list_down_rect:
                        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_up_rect)>(r_length/4):
                        if not(rect in dic_rect['up']):
                            rect.list_up_rect.append(list_up_rect)
                            dic_rect['up'].append(rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_up,y_min_up),(x_max_up,y_max_up),(255,255,0),1)
                    # for rec in list_up_rect:
                    #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_down_rect2)>(r_length/4):
                        if not(rect in dic_rect['down']):
                            rect.list_down_rect.append(list_down_rect2)
                            dic_rect['down'].append(rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_down,y_min_down),(x_max_down,y_max_down),(255,255,0),1)
                        # for rec in list_down_rect:
                        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_up_rect2)>(r_length/4):
                        if not(rect in dic_rect['up']):
                            rect.list_up_rect.append(list_up_rect2)
                            dic_rect['up'].append(rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_up,y_min_up),(x_max_up,y_max_up),(255,255,0),1)
                    for rec in list_up_rect:
                        image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)
        if debug_mode:
            cv2.imwrite(self.path_debug+'door/'+str(self.file_name[0:len(self.file_name)-4])+'_2.png', image)
        return dic_rect

    def find_left_arc_door(self,dic_rect):

        for rect in dic_rect['left']:

            thickness_door = rect.get_min_len()+1
            if thickness_door<0:
                thickness_door=np.abs(thickness_door)
            elif thickness_door==0:
                thickness_door=1
            #aggiungo lo spessore del muro alla porta

            c1,o1,l1 = self.check_door_left( rect.x_min, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_left_rect, 'left')
            if(not(c1)):

                c2,o2,l2 = self.check_door_left( rect.x_min, rect.y_min, rect.x_max, rect.y_max+self.max_tink_wall, thickness_door, rect.list_left_rect, 'left')
                if(not(c2)):

                    c3,o3,l3 = self.check_door_left( rect.x_min, rect.y_min - self.max_tink_wall, rect.x_max, rect.y_max, thickness_door, rect.list_left_rect, 'left')
                    if(not(c3)):
                        c4,o4,l4 = self.check_door_left( rect.x_min, rect.y_min, rect.x_max, rect.y_max + int(self.max_tink_wall/2), thickness_door, rect.list_left_rect, 'left')
                        if(not(c4)):
                            c5,o5,l5 = self.check_door_left( rect.x_min, rect.y_min - int(self.max_tink_wall/2), rect.x_max, rect.y_max, thickness_door, rect.list_left_rect, 'left')
            #tolgo lo spessore del muro alla porta
                            if(not(c5)):
                                c6,o6,l6 = self.check_door_left( rect.x_min, rect.y_min, rect.x_max, rect.y_max - self.max_tink_wall, thickness_door, rect.list_left_rect, 'left')
                                if(not(c6)):
                                    c7,o7,l7 = self.check_door_left( rect.x_min, rect.y_min + self.max_tink_wall, rect.x_max, rect.y_max, thickness_door, rect.list_left_rect, 'left')
                                    if(c7):
                                        rect.opening = [o7,'left']
                                        self.list_door.append(rect)
                                        # for rd in l7:
                                        #     # for rr in self.list_rect:
                                        #     #     if rr.id==rd.id or rr.id==rect.id:
                                        #     rd.type = 'door'
                                else:
                                    rect.opening = [o6,'left']
                                    self.list_door.append(rect)
                                    # for rd in l6:
                                    #     # for rr in self.list_rect:
                                    #     #     if rr.id==rd.id or rr.id==rect.id:
                                    #     rd.type = 'door'
                            else:
                                rect.opening = [o5,'left']
                                self.list_door.append(rect)
                                # for rd in l5:
                                #     # for rr in self.list_rect:
                                #     #     if rr.id==rd.id or rr.id==rect.id:
                                #     rd.type = 'door'

                        else:
                            rect.opening = [o4,'left']
                            self.list_door.append(rect)
                            # for rd in l4:
                            #     # for rr in self.list_rect:
                            #     #     if rr.id==rd.id or rr.id==rect.id:
                            #     rd.type = 'door'
                    else:
                        rect.opening = [o3,'left']
                        self.list_door.append(rect)
                        # for rd in l3:
                        #     # for rr in self.list_rect:
                        #     #     if rr.id==rd.id or rr.id==rect.id:
                        #     rd.type = 'door'
                else:
                    rect.opening = [o2,'left']
                    self.list_door.append(rect)
                    # for rd in l2:
                    #     # for rr in self.list_rect:
                    #     #     if rr.id==rd.id or rr.id==rect.id:
                    #     rd.type = 'door'
            else:
                rect.opening = [o1,'left']
                self.list_door.append(rect)
                # for rd in l1:
                #     # for rr in self.list_rect:
                #     #     if rr.id==rd.id or rr.id==rect.id:
                #     rd.type = 'door'

    def find_right_arc_door(self,dic_rect):

        for rect in dic_rect['right']:
            thickness_door = rect.get_min_len()+1
            if thickness_door<0:
                thickness_door=np.abs(thickness_door)
            elif thickness_door==0:
                thickness_door=1
            #aggiungo lo spessore del muro alla porta

            c1,o1,l1 = self.check_door_right( rect.x_min, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_right_rect, 'right')
            if(not(c1)):
                c2,o2,l2 = self.check_door_right( rect.x_min, rect.y_min, rect.x_max, rect.y_max+self.max_tink_wall, thickness_door, rect.list_right_rect, 'right')
                if(not(c2)):
                    c3,o3,l3 = self.check_door_right( rect.x_min, rect.y_min - self.max_tink_wall, rect.x_max, rect.y_max, thickness_door, rect.list_right_rect, 'right')
                    if(not(c3)):
                        c4,o4,l4 = self.check_door_right( rect.x_min, rect.y_min, rect.x_max, rect.y_max + int(self.max_tink_wall/2), thickness_door, rect.list_right_rect, 'right')
                        if(not(c4)):
                            c5,o5,l5 = self.check_door_right( rect.x_min, rect.y_min - int(self.max_tink_wall/2), rect.x_max, rect.y_max, thickness_door, rect.list_right_rect, 'right')
            #tolgo lo spessore del muro alla porta
                            if(not(c5)):
                                c6,o6,l6 = self.check_door_right( rect.x_min, rect.y_min, rect.x_max, rect.y_max - self.max_tink_wall, thickness_door, rect.list_right_rect, 'right')
                                if(not(c6)):
                                    c7,o7,l7 = self.check_door_right( rect.x_min, rect.y_min + self.max_tink_wall, rect.x_max, rect.y_max, thickness_door, rect.list_right_rect, 'right')
                                    if(c7):
                                        rect.opening = [o7,'right']
                                        self.list_door.append(rect)

                                        # for rd in l7:
                                        #     # for rr in self.list_rect:
                                        #     #     if rr.id==rd.id or rr.id==rect.id:
                                        #     rd.type = 'door'
                                else:
                                    rect.opening = [o6,'right']
                                    self.list_door.append(rect)
                                    # for rd in l6:
                                    #     # for rr in self.list_rect:
                                    #     #     if rr.id==rd.id or rr.id==rect.id:
                                    #     rd.type = 'door'
                            else:
                                rect.opening = [o5,'right']
                                self.list_door.append(rect)
                                # for rd in l5:
                                #     # for rr in self.list_rect:
                                #     #     if rr.id==rd.id or rr.id==rect.id:
                                #     rd.type = 'door'

                        else:
                            rect.opening = [o4,'right']
                            self.list_door.append(rect)
                            # for rd in l4:
                            #     # for rr in self.list_rect:
                            #     #     if rr.id==rd.id or rr.id==rect.id:
                            #     rd.type = 'door'
                    else:
                        rect.opening = [o3,'right']
                        self.list_door.append(rect)
                        # for rd in l3:
                        #     # for rr in self.list_rect:
                        #     #     if rr.id==rd.id or rr.id==rect.id:
                        #     rd.type = 'door'
                else:
                    rect.opening = [o2,'right']
                    self.list_door.append(rect)
                    # for rd in l2:
                    #     # for rr in self.list_rect:
                    #     #     if rr.id==rd.id or rr.id==rect.id:
                    #     rd.type = 'door'
            else:
                rect.opening = [o1,'right']
                self.list_door.append(rect)
                # for rd in l1:
                #     # for rr in self.list_rect:
                #     #     if rr.id==rd.id or rr.id==rect.id:
                #     rd.type = 'door'

    def find_down_arc_door(self,dic_rect):

        for rect in dic_rect['down']:
            thickness_door = rect.get_min_len()+1
            if thickness_door<0:
                thickness_door=np.abs(thickness_door)
            elif thickness_door==0:
                thickness_door=1
            #aggiungo lo spessore del muro alla porta

            c1,o1,l1 = self.check_door_down( rect.x_min, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_down_rect, 'down')
            if(not(c1)):
                c2,o2,l2 = self.check_door_down( rect.x_min, rect.y_min, rect.x_max + self.max_tink_wall, rect.y_max, thickness_door, rect.list_down_rect, 'down')
                if(not(c2)):
                    c3,o3,l3 = self.check_door_down( rect.x_min  - self.max_tink_wall, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_down_rect, 'down')
                    if(not(c3)):
                        c4,o4,l4 = self.check_door_down( rect.x_min, rect.y_min, rect.x_max + int(self.max_tink_wall/2) , rect.y_max , thickness_door, rect.list_down_rect, 'down')
                        if(not(c4)):
                            c5,o5,l5 = self.check_door_down( rect.x_min - int(self.max_tink_wall/2), rect.y_min , rect.x_max, rect.y_max, thickness_door, rect.list_down_rect, 'down')
            #tolgo lo spessore del muro alla porta
                            if(not(c5)):
                                c6,o6,l6 = self.check_door_down( rect.x_min, rect.y_min, rect.x_max - self.max_tink_wall, rect.y_max, thickness_door, rect.list_down_rect, 'down')
                                if(not(c6)):
                                    c7,o7,l7 = self.check_door_down( rect.x_min  + self.max_tink_wall, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_down_rect, 'down')
                                    if(c7):
                                        rect.opening = [o7,'down']
                                        self.list_door.append(rect)
                                        # for rd in l7:
                                        #     # for rr in self.list_rect:
                                        #     #     if rr.id==rd.id or rr.id==rect.id:
                                        #     rd.type = 'door'
                                else:
                                    rect.opening = [o6,'down']
                                    self.list_door.append(rect)
                                    # for rd in l6:
                                    #     # for rr in self.list_rect:
                                    #     #     if rr.id==rd.id or rr.id==rect.id:
                                    #     rd.type = 'door'
                            else:
                                rect.opening = [o5,'down']
                                self.list_door.append(rect)
                                # for rd in l5:
                                #     # for rr in self.list_rect:
                                #     #     if rr.id==rd.id or rr.id==rect.id:
                                #     rd.type = 'down'

                        else:
                            rect.opening = [o4,'down']
                            self.list_door.append(rect)
                            # for rd in l4:
                            #     # for rr in self.list_rect:
                            #     #     if rr.id==rd.id or rr.id==rect.id:
                            #     rd.type = 'door'
                    else:
                        rect.opening = [o3,'down']
                        self.list_door.append(rect)
                        # for rd in l3:
                        #     # for rr in self.list_rect:
                        #     #     if rr.id==rd.id or rr.id==rect.id:
                        #     rd.type = 'door'
                else:
                    rect.opening = [o2,'down']
                    self.list_door.append(rect)
                    # for rd in l2:
                    #     # for rr in self.list_rect:
                    #     #     if rr.id==rd.id or rr.id==rect.id:
                    #     rd.type = 'door'
            else:
                rect.opening = [o1,'down']
                self.list_door.append(rect)
                # for rd in l1:
                #     # for rr in self.list_rect:
                #     #     if rr.id==rd.id or rr.id==rect.id:
                #     rd.type = 'door'

    def find_up_arc_door(self,dic_rect):

        for rect in dic_rect['up']:
            thickness_door = rect.get_min_len()+1
            if thickness_door<0:
                thickness_door=np.abs(thickness_door)
            elif thickness_door==0:
                thickness_door=1
            #aggiungo lo spessore del muro alla porta
            c1,o1,l1 = self.check_door_up( rect.x_min, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_up_rect, 'up',1)

            if(not(c1)):
                c2,o2,l2 = self.check_door_up( rect.x_min, rect.y_min, rect.x_max + self.max_tink_wall, rect.y_max, thickness_door, rect.list_up_rect, 'up',2)
                if(not(c2)):
                    c3,o3,l3 = self.check_door_up( rect.x_min  - self.max_tink_wall, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_up_rect, 'up',3)
                    if(not(c3)):
                        c4,o4,l4 = self.check_door_up( rect.x_min, rect.y_min, rect.x_max + int(self.max_tink_wall/2) , rect.y_max , thickness_door, rect.list_up_rect, 'up',4)
                        if(not(c4)):
                            c5,o5,l5 = self.check_door_up( rect.x_min - int(self.max_tink_wall/2), rect.y_min , rect.x_max, rect.y_max, thickness_door, rect.list_up_rect, 'up',5)
            #tolgo lo spessore del muro alla porta
                            if(not(c5)):
                                c6,o6,l6 = self.check_door_up( rect.x_min, rect.y_min, rect.x_max - self.max_tink_wall, rect.y_max, thickness_door, rect.list_up_rect, 'up',6)
                                if(not(c6)):
                                    c7,o7,l7 = self.check_door_up( rect.x_min  + self.max_tink_wall, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_up_rect, 'up',7)
                                    if(c7):
                                        rect.opening = [o7,'up']
                                        self.list_door.append(rect)
                                        # for rd in l7:
                                        #     # for rr in self.list_rect:
                                        #     #     if rr.id==rd.id or rr.id==rect.id:
                                        #     rd.type = 'door'
                                else:
                                    rect.opening = [o6,'up']
                                    self.list_door.append(rect)
                                    # for rd in l6:
                                    #     # for rr in self.list_rect:
                                    #     #     if rr.id==rd.id or rr.id==rect.id:
                                    #     rd.type = 'door'
                            else:
                                rect.opening = [o5,'up']
                                self.list_door.append(rect)
                                # for rd in l5:
                                #     # for rr in self.list_rect:
                                #     #     if rr.id==rd.id or rr.id==rect.id:
                                #     rd.type = 'door'

                        else:
                            rect.opening = [o4,'up']
                            self.list_door.append(rect)
                            # for rd in l4:
                            #     # for rr in self.list_rect:
                            #     #     if rr.id==rd.id or rr.id==rect.id:
                            #     rd.type = 'door'
                    else:
                        rect.opening = [o3,'up']
                        self.list_door.append(rect)
                        # for rd in l3:
                        #     # for rr in self.list_rect:
                        #     #     if rr.id==rd.id or rr.id==rect.id:
                        #     rd.type = 'door'
                else:
                    rect.opening = [o2,'up']
                    self.list_door.append(rect)
                    # for rd in l2:
                    #     # for rr in self.list_rect:
                    #     #     if rr.id==rd.id or rr.id==rect.id:
                    #     rd.type = 'door'
            else:
                rect.opening = [o1,'up']
                self.list_door.append(rect)
                # for rd in l1:
                #     # for rr in self.list_rect:
                #     #     if rr.id==rd.id or rr.id==rect.id:
                #     rd.type = 'door'

    def check_door_left(self,x_min, y_min, x_max, y_max, thickness_door, list_rectangle, side):

        if self.max_tink_wall > 20:
            t1=3
            t2=3
            perc=0.5
        else:
            t1=1
            t2=1
            perc=0.6
        # calcolo il baricentro di ogni rettangolo e guardo se la distanza dal centro el cerchio e' minore di r1  e maggiore di r2
        for rectangle in list_rectangle:
            h = y_max-y_min
            img =  np.array(self.image_rect)
            r1 = int(h*0.8)
            r2 = int((float(h)*1.2))
            check = False
            list_rect_door_1 = []
            list_rect_door_2 = []
            if self.max_tink_wall > 20:
                z=(int(h/thickness_door))*0.25
            else:
                z=(int(h/thickness_door))*0.5
            # conto i numeri di rettangoli che hanno il baricentro a una distanza compresa tra r1 e r2
            for rec in rectangle:
                dist_1 = rec.get_distance_to_centroid(x_max,y_max)
                dist_2 = rec.get_distance_to_centroid(x_max,y_min)
                if dist_1 < r2 and dist_1 > r1:
                    list_rect_door_1.append(rec)
                if dist_2 < r2 and dist_2 > r1:
                    list_rect_door_2.append(rec)

            # guardo apertura della porta e dal basso verso alto o dal alto verso il basso
            # e controllo se i rettangoli sono disposti in modo omogeneo
            if len(list_rect_door_1) > math.ceil(len(rectangle)*perc):
                arc = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                arc2 = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                list_rip_11 = []
                list_rip_12 = []
                l11 = list_rect_door_1[:]
                l12 = list_rect_door_1[:]
                for i in range(0,(int((y_max-y_min)/thickness_door))+1):
                    p1 = y_min + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l11:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                           # arc[i] = 1
                            if (list_rip_11.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l11:
                            #         l11.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_11.append(r.id)
                    p3 = (x_min-h) + (i*thickness_door)
                    p4 = p3+thickness_door
                    for r in l12:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                            # arc2[i] = 1
                            if (list_rip_12.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l12:
                            #         l12.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_12.append(r.id)
                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0

                #controllo se ci sono dei "buchi" nel presunto arco
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0

                if count_zeros > max_zeros:
                    max_zeros = count_zeros

                if count2_zeros > max2_zeros:
                    max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                  #   print '####################'
                  #   print 'left 1 si'
                  # # print self.max_tink_wall
                  #   print math.ceil(len(rectangle)*perc)
                  #   print len(list_rect_door_1)
                  # #  print len(list_rect_door_2)
                  #   print arc
                  #   print arc2
                  #   cv2.circle(img,(x_max,y_max),r1,(255,255,255),1)
                  #   cv2.circle(img,(x_max,y_max),r2,(255,255,255),1)
                  #   # cv2.circle(img,(x_max,y_min),r1,(255,255,0),1)
                  #   # cv2.circle(img,(x_max,y_min),r2,(255,255,0),1)
                  #   cv2.rectangle(img,(x_min,y_min),(x_max,y_max),(255,255,0),1)
                  #   img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                  #   plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min-r2:x_max+1], cmap = 'gray', interpolation = 'bicubic')
                  #   # plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
                  #   plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                  #   plt.title('left-prima')
                  #   plt.show()

                    check = True
                    opening = 'up-down'
                   # print len(list_rect_door_1)
                    return check,opening,list_rect_door_1
                #else:
                    # print '####################'
                    # print 'left 1 no'
                    # #print self.max_tink_wall
                    # print math.ceil(len(rectangle)*perc)
                    # print len(list_rect_door_1)
                    # #print len(list_rect_door_2)
                    # print arc
                    # print arc2
                    # cv2.circle(img,(x_max,y_max),r1,(255,255,255),1)
                    # cv2.circle(img,(x_max,y_max),r2,(255,255,255),1)
                    # # cv2.circle(img,(x_max,y_min),r1,(255,255,0),1)
                    # # cv2.circle(img,(x_max,y_min),r2,(255,255,0),1)
                    # cv2.rectangle(img,(x_min,y_min),(x_max,y_max),(255,255,0),1)
                    # img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                    # plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min-r2:x_max+1], cmap = 'gray', interpolation = 'bicubic')
                    # # plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
                    # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                    # plt.title('left-prima')
                    # plt.show()


            if len(list_rect_door_2)> math.ceil(len(rectangle)*perc):

                arc2 = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                arc = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                list_rip_21 = []
                list_rip_22 = []
                l21 = list_rect_door_2[:]
                l22 = list_rect_door_2[:]
                for i in range(0,(int((y_max-y_min)/thickness_door))+1):
                    p1 = y_min + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l21:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                            #arc[i] = 1
                            if (list_rip_21.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l21:
                            #         l21.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_21.append(r.id)
                    p3 = (x_min-h)+ (i*thickness_door)
                    p4 = p3+thickness_door
                    for r in l22:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                            # arc2[i] = 1
                            if (list_rip_22.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l22:
                            #         l22.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_22.append(r.id)

                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0
                if count_zeros > max_zeros:
                        max_zeros = count_zeros
                if count2_zeros > max2_zeros:
                        max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                    # print '####################'
                    # print 'left 2 si'
                    # # print self.max_tink_wall
                    # print math.ceil(len(rectangle)*perc)
                    # # print len(list_rect_door_1)
                    # print len(list_rect_door_2)
                    # print arc
                    # print arc2
                    # # cv2.circle(img,(x_max,y_max),r1,(255,255,255),1)
                    # # cv2.circle(img,(x_max,y_max),r2,(255,255,255),1)
                    # cv2.circle(img,(x_max,y_min),r1,(255,255,0),1)
                    # cv2.circle(img,(x_max,y_min),r2,(255,255,0),1)
                    # cv2.rectangle(img,(x_min,y_min),(x_max,y_max),(255,255,0),1)
                    # img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                    # plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min-r2:x_max+1], cmap = 'gray', interpolation = 'bicubic')
                    # # plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
                    # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                    # plt.title('left-prima')
                    # plt.show()
                    check = True
                    opening = 'down-up'
                   # print len(list_rect_door_2)
                    return check,opening,list_rect_door_2
                #else:
                    # print '####################'
                    # print 'left 2 no'
                    # # print self.max_tink_wall
                    # print math.ceil(len(rectangle)*perc)
                    # #print len(list_rect_door_1)
                    # print len(list_rect_door_2)
                    # print arc
                    # print arc2
                    # # cv2.circle(img,(x_max,y_max),r1,(255,255,255),1)
                    # # cv2.circle(img,(x_max,y_max),r2,(255,255,255),1)
                    # cv2.circle(img,(x_max,y_min),r1,(255,255,0),1)
                    # cv2.circle(img,(x_max,y_min),r2,(255,255,0),1)
                    # cv2.rectangle(img,(x_min,y_min),(x_max,y_max),(255,255,0),1)
                    # img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                    # plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min-r2:x_max+1], cmap = 'gray', interpolation = 'bicubic')
                    # # plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
                    # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                    # plt.title('left-prima')
                    # plt.show()


        list_none = []

        return False,'None', list_none

    def check_door_right(self,x_min, y_min, x_max, y_max, thickness_door, list_rectangle, side):

        if self.max_tink_wall > 20:
            t1=3
            t2=3
            perc=0.5
        else:
            t1=1
            t2=1
            perc=0.6

        for rectangle in list_rectangle:
            h = y_max-y_min
            img =  np.array(self.image_rect)
            r1 = int(h*0.8)
            r2 = int((float(h)*1.2))

            check = False
            list_rect_door_1 = []
            list_rect_door_2 = []
            if self.max_tink_wall > 20:
                z=(int(h/thickness_door))*0.25
            else:
                z=(int(h/thickness_door))*0.5
            for rec in rectangle:
                dist_1 = rec.get_distance_to_centroid(x_min,y_max)
                dist_2 = rec.get_distance_to_centroid(x_min,y_min)
                if dist_1 < r2 and dist_1 > r1:
                    list_rect_door_1.append(rec)
                if dist_2 < r2 and dist_2 > r1:
                    list_rect_door_2.append(rec)


            if len(list_rect_door_1) > math.ceil(len(rectangle)*perc):


                arc = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                arc2 = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                list_rip_11 = []
                list_rip_12 = []
                l11 = list_rect_door_1[:]
                l12 = list_rect_door_1[:]
                for i in range(0,(int((y_max-y_min)/thickness_door))+1):
                    p1 = y_min + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l11:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                            # arc[i] = 1
                            if (list_rip_11.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l11:
                            #         l11.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_11.append(r.id)
                    p3 = x_max + (i*thickness_door)
                    p4 = p3+thickness_door

                    for r in l12:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                           # arc2[i] = 1
                            if (list_rip_12.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l12:
                            #         l12.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_12.append(r.id)
                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0
                if count_zeros > max_zeros:
                    max_zeros = count_zeros

                if count2_zeros > max2_zeros:
                    max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                   # print '####################'
                   # print 'right  1 si'
                   # # print self.max_tink_wall
                   # print math.ceil(len(rectangle)*perc)
                   # print len(list_rect_door_1)
                   # #  print len(list_rect_door_2)
                   # print arc
                   # print arc2
                   # # cv2.circle(img,(x_min,y_min),r1,(255,255,255),1)
                   # # cv2.circle(img,(x_min,y_min),r2,(255,255,255),1)
                   # cv2.circle(img,(x_min,y_max),r1,(255,255,0),1)
                   # cv2.circle(img,(x_min,y_max),r2,(255,255,0),1)
                   # img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                   # plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min:x_max+r2], cmap = 'gray', interpolation = 'bicubic')
                   # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                   # plt.title('left-prima')
                   # plt.show()
                   check = True
                   opening = 'up-down'
                   return check,opening,list_rect_door_1
                # else:
                #     print '####################'
                #     print 'right  1 no'
                #     # print self.max_tink_wall
                #     print math.ceil(len(rectangle)*perc)
                #     print len(list_rect_door_1)
                #     # print len(list_rect_door_2)
                #     print arc
                #     print arc2
                #     # cv2.circle(img,(x_min,y_min),r1,(255,255,255),1)
                #     # cv2.circle(img,(x_min,y_min),r2,(255,255,255),1)
                #     cv2.circle(img,(x_min,y_max),r1,(255,255,0),1)
                #     cv2.circle(img,(x_min,y_max),r2,(255,255,0),1)
                #     img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                #     plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min:x_max+r2], cmap = 'gray', interpolation = 'bicubic')
                #     plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                #     plt.title('left-prima')
                #     plt.show()
            # else:
            #     print '####################'
            #     print 'right  1 no'
            #     # print self.max_tink_wall
            #     print math.ceil(len(rectangle)*perc)
            #     print len(list_rect_door_1)
            #     #  print len(list_rect_door_2)
            #     # cv2.circle(img,(x_min,y_min),r1,(255,255,255),1)
            #     # cv2.circle(img,(x_min,y_min),r2,(255,255,255),1)
            #     cv2.circle(img,(x_min,y_max),r1,(255,255,0),1)
            #     cv2.circle(img,(x_min,y_max),r2,(255,255,0),1)
            #     img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
            #     plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min:x_max+r2], cmap = 'gray', interpolation = 'bicubic')
            #     plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
            #     plt.title('left-prima')
            #     plt.show()



            if len(list_rect_door_2) > math.ceil(len(rectangle)*perc):

                arc2 = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                arc = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                list_rip_21 = []
                list_rip_22 = []
                l21 = list_rect_door_2[:]
                l22 = list_rect_door_2[:]
                for i in range(0,(int((y_max-y_min)/thickness_door))+1):
                    p1 = y_min + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l21:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                            #arc[i] = 1
                            if (list_rip_21.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l21:
                            #         l21.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_21.append(r.id)
                    p3 = x_max + (i*thickness_door)
                    p4 = p3+thickness_door
                    for r in l22:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                            #arc2[i] = 1
                            if (list_rip_22.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l22:
                            #         l22.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_22.append(r.id)

                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0
                if count_zeros > max_zeros:
                    max_zeros = count_zeros
                if count2_zeros > max2_zeros:
                    max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                    # print '####################'
                    # print 'right list 2 si'
                    # # print self.max_tink_wall
                    # print math.ceil(len(rectangle)*perc)
                    # # print len(list_rect_door_1)
                    # print len(list_rect_door_2)
                    # print arc
                    # print arc2
                    # cv2.circle(img,(x_min,y_min),r1,(255,255,255),1)
                    # cv2.circle(img,(x_min,y_min),r2,(255,255,255),1)
                    # # cv2.circle(img,(x_min,y_max),r1,(255,255,0),1)
                    # # cv2.circle(img,(x_min,y_max),r2,(255,255,0),1)
                    # img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                    # plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min:x_max+r2], cmap = 'gray', interpolation = 'bicubic')
                    # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                    # plt.title('left-prima')
                    # plt.show()
                    check = True
                    opening = 'down-up'
                    return check,opening,list_rect_door_2
                # else:
                #     print '####################'
                #     print 'right list 2 no'
                #     # print self.max_tink_wall
                #     print math.ceil(len(rectangle)*perc)
                #     # print len(list_rect_door_1)
                #     print len(list_rect_door_2)
                #     print arc
                #     print arc2
                #     cv2.circle(img,(x_min,y_min),r1,(255,255,255),1)
                #     cv2.circle(img,(x_min,y_min),r2,(255,255,255),1)
                #     # cv2.circle(img,(x_min,y_max),r1,(255,255,0),1)
                #     # cv2.circle(img,(x_min,y_max),r2,(255,255,0),1)
                #     img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                #     plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min:x_max+r2], cmap = 'gray', interpolation = 'bicubic')
                #     plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                #     plt.title('left-prima')
                #     plt.show()
            # else:
            #     print '####################'
            #     print 'right list 2 no'
            #     # print self.max_tink_wall
            #     print math.ceil(len(rectangle)*perc)
            #     # print len(list_rect_door_1)
            #     print len(list_rect_door_2)
            #
            #     cv2.circle(img,(x_min,y_min),r1,(255,255,255),1)
            #     cv2.circle(img,(x_min,y_min),r2,(255,255,255),1)
            #     # cv2.circle(img,(x_min,y_max),r1,(255,255,0),1)
            #     # cv2.circle(img,(x_min,y_max),r2,(255,255,0),1)
            #     img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
            #     plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min:x_max+r2], cmap = 'gray', interpolation = 'bicubic')
            #     plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
            #     plt.title('left-prima')
            #     plt.show()

        list_none = []

        return False,'None', list_none

    def check_door_down(self,x_min, y_min, x_max, y_max, thickness_door, list_rectangle, side):

        if self.max_tink_wall > 20:
            t1=3
            t2=3
            perc=0.5
        else:
            t1=1
            t2=1
            perc=0.6

        for rectangle in list_rectangle:
            h = x_max-x_min
            img =  np.array(self.image_rect)
            r1 = int(h*0.8)
            r2 = int((float(h)*1.2))

            check = False
            list_rect_door_1 = []
            list_rect_door_2 = []

            if self.max_tink_wall > 20:
                z=(int(h/thickness_door))*0.25
            else:
                z=(int(h/thickness_door))*0.5

            # conto i numeri di rettangoli che hanno il baricentro a una distanza compresa tra r1 e r2
            for rec in rectangle:
                dist_1 = rec.get_distance_to_centroid(x_min,y_min)
                dist_2 = rec.get_distance_to_centroid(x_max,y_min)
                if dist_1 < r2 and dist_1 > r1:
                    list_rect_door_1.append(rec)
                if dist_2 < r2 and dist_2 > r1:
                    list_rect_door_2.append(rec)

            # guardo apertura della porta e dal basso verso alto o dal alto verso il basso
            # e controllo se i rettangoli sono disposti in modo omogeneo sia sull asse x che sull assey
            if len(list_rect_door_1) > math.ceil(len(rectangle)*perc):
                list_rip_11 = []
                list_rip_12 = []
                l11 = list_rect_door_1[:]
                l12 = list_rect_door_1[:]
                arc = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)
                arc2 = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)
                for i in range(0,(int(h/thickness_door))+1):
                    p1 = y_min + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l11:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                            # arc[i] = 1
                            if (list_rip_11.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l11:
                            #         l11.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_11.append(r.id)
                    p3 = x_min + (i*thickness_door)
                    p4 = p3+thickness_door
                    for r in l12:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                            # arc2[i] = 1
                            if (list_rip_12.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l12:
                            #         l12.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_12.append(r.id)
                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0

                #controllo se ci sono dei "buchi" nel presunto arco
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0

                if count_zeros > max_zeros:
                    max_zeros = count_zeros

                if count2_zeros > max2_zeros:
                    max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                    check = True
                    opening = 'right-left'
                    return check,opening,list_rect_door_1

            if len(list_rect_door_2)> math.ceil(len(rectangle)*perc):

                arc2 = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)
                arc = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)
                list_rip_21 = []
                list_rip_22 = []
                l21 = list_rect_door_2[:]
                l22 = list_rect_door_2[:]
                for i in range(0,(int(h/thickness_door))+1):
                    p1 = y_min + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l21:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                            # arc[i] = 1
                            if (list_rip_21.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l21:
                            #         l21.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_21.append(r.id)
                    p3 = x_min+ (i*thickness_door)
                    p4 = p3+thickness_door
                    for r in l22:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                            #arc2[i] = 1
                            if (list_rip_22.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l22:
                            #         l22.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_22.append(r.id)

                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0
                if count_zeros > max_zeros:
                    max_zeros = count_zeros
                if count2_zeros > max2_zeros:
                    max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                    check = True
                    opening = 'left-right'
                 #   print len(list_rect_door_1)
                    return check,opening,list_rect_door_2



        list_none = []

        return False,'None', list_none

    def check_door_up(self,x_min, y_min, x_max, y_max, thickness_door, list_rectangle, side,nn):

        if self.max_tink_wall > 20:
            t1=4
            t2=4
            perc=0.5
        else:
            t1=1
            t2=1
            perc=0.6

        for rectangle in list_rectangle:
            h = x_max-x_min
            img =  np.array(self.image_rect)
            r1 = int(h*0.8)
            r2 = int((float(h)*1.2))

            check = False
            list_rect_door_1 = []
            list_rect_door_2 = []
            if self.max_tink_wall > 20:
                z=(int(h/thickness_door))*0.25
            else:
                z=(int(h/thickness_door))*0.5
            # conto i numeri di rettangoli che hanno il baricentro a una distanza compresa tra r1 e r2
            for rec in rectangle:
                dist_1 = rec.get_distance_to_centroid(x_min,y_max)
                dist_2 = rec.get_distance_to_centroid(x_max,y_max)
                if dist_1 < r2 and dist_1 > r1:
                    list_rect_door_1.append(rec)
                if dist_2 < r2 and dist_2 > r1:
                    list_rect_door_2.append(rec)

            # guardo apertura della porta e dal basso verso alto o dal alto verso il basso
            # e controllo se i rettangoli sono disposti in modo omogeneo sia sull asse x che sull assey
            # print '#####################'

            if len(list_rect_door_1) > math.ceil(len(rectangle)*perc):
                arc = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)
                arc2 = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)
                list_rip_11 = []
                list_rip_12 = []
                l11 = list_rect_door_1[:]
                l12 = list_rect_door_1[:]
                for i in range(0,int(h/thickness_door)+1):
                    p1 = (y_min-h) + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l11:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                            # arc[i] = 1
                            if (list_rip_11.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l11:
                            #         l11.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_11.append(r.id)
                    p3 = x_min + (i*thickness_door)
                    p4 = p3+thickness_door
                    for r in l12:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                            # arc2[i] = 1
                            if (list_rip_12.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l12:
                            #         l12.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_12.append(r.id)

                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0

                #controllo se ci sono dei "buchi" nel presunto arco
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0

                if count_zeros > max_zeros:
                    max_zeros = count_zeros

                if count2_zeros > max2_zeros:
                    max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                   check = True
                   opening = 'right-left'
                   #   print len(list_rect_door_1)
                   return check,opening,list_rect_door_1



            if len(list_rect_door_2)> math.ceil(len(rectangle)*perc):

                arc2 = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)
                arc = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)

                list_rip_21 = []
                list_rip_22 = []
                l21 = list_rect_door_2[:]
                l22 = list_rect_door_2[:]

                for i in range(0,(int(h/thickness_door))+1):
                    p1 = (y_min-h) + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l21:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                            # arc[i] = 1
                            if (list_rip_21.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l21:
                            #         l21.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_21.append(r.id)

                    p3 = x_min+ (i*thickness_door)
                    p4 = p3+thickness_door
                    for r in l22:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                            #arc2[i] = 1
                            if (list_rip_22.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l22:
                            #         l22.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_22.append(r.id)

                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0
                if count_zeros > max_zeros:
                    max_zeros = count_zeros
                if count2_zeros > max2_zeros:
                    max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                    check = True
                    opening = 'left-right'
                    return check,opening,list_rect_door_2


        list_none = []

        return False,'None', list_none

    # merge walls and door
    def merge_walls_doors(self,debug_mode):
        image = np.zeros((self.height,self.width,3),dtype=np.uint8)
        image2 = np.zeros((self.height,self.width,3),dtype=np.uint8)
        list_wall = self.graph_wall.nodes()

       # print 'numero di porte trovate: '+str(len(self.list_door))
        if self.max_tink_wall>20:
            lim_max = self.max_tink_wall
        else:
            lim_max = self.max_tink_wall*0.3

        for d in self.list_door:
            opening = d.opening[0]
            direction = d.opening[1]
            length = d.get_max_len()
            min_dist1 = max(self.height,self.width)
            min_dist2 = max(self.height,self.width)
            wall_1 = None
            wall_2 = None
            if (opening=='up-down' and direction=='left'):

                for w1 in list_wall:
                    if w1.id!= d.id:
                        dist1 = w1.get_minimum_distance_from_the_point(d.x3, d.y3)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x1-length, d.y1+length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1
                # questo perche potrebbero esserci muri non trovati
                if min_dist1 > length*0.3 or min_dist2 > length*0.3:
                    for rect in self.list_rect:
                        if rect.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x3, d.y3)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x1-length, d.y1+length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x3, d.y3)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x1-length, d.y1+length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect
                if wall_1 and wall_2:
                    if not(wall_1.y_max > wall_2.y_min and wall_2.y_max>wall_1.y_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)

            elif (opening=='down-up' and direction=='left'):
                for w1 in list_wall:
                    if w1.id!= d.id:
                        dist1 = w1.get_minimum_distance_from_the_point(d.x2, d.y2)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x3-length, d.y3-length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1

                if min_dist1 > length*0.3 or min_dist2 > length*0.3:
                    for rect in self.list_rect:
                        if rect.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x2, d.y2)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x3-length, d.y3-length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x2, d.y2)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x3-length, d.y3-length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect

                if wall_1 and wall_2:
                    if not(wall_1.y_max > wall_2.y_min and wall_2.y_max>wall_1.y_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)

            elif (opening=='up-down' and direction=='right'):
                for w1 in list_wall:
                    if w1.id!= d.id:
                        dist1 = w1.get_minimum_distance_from_the_point(d.x4, d.y4)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x1+length, d.y1+length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1

                if min_dist1 > length*0.3 or min_dist2 > length*0.3:
                    for rect in self.list_rect:
                        if rect.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x4, d.y4)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x1+length, d.y1+length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x4, d.y4)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x1+length, d.y1+length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect
                if wall_1 and wall_2:
                    if not(wall_1.y_max > wall_2.y_min and wall_2.y_max>wall_1.y_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)

            elif (opening=='down-up' and direction=='right'):
                for w1 in list_wall:
                    if w1.id!= d.id:
                        dist1 = w1.get_minimum_distance_from_the_point(d.x1, d.y1)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x4+length, d.y4-length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1

                if min_dist1 > length*0.3 or min_dist2 > length*0.3:
                    for rect in self.list_rect:
                        if rect.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x1, d.y1)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x4+length, d.y4-length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x1, d.y1)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x4+length, d.y4-length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect
                if wall_1 and wall_2:
                    if not(wall_1.y_max > wall_2.y_min and wall_2.y_max>wall_1.y_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)

            elif (opening=='left-right' and direction=='down'):
                for w1 in list_wall:
                    if w1.id!= d.id:
                    # dist1 = w1.get_minimum_distance_from_the_point(d.x3, d.y3)
                        dist1 = w1.get_minimum_distance_from_the_point(d.x2, d.y2)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x1+length, d.y1+length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1

                if min_dist1 > length*0.3 or min_dist2 > length*0.3:
                    for rect in self.list_rect:
                        if w1.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x2, d.y2)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x1+length, d.y1+length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x2, d.y2)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x1+length, d.y1+length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect
                if wall_1 and wall_2:
                    if not(wall_1.x_max > wall_2.x_min and wall_2.x_max>wall_1.x_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)

            elif (opening=='right-left' and direction=='down'):
                for w1 in list_wall:
                    if w1.id!= d.id:
                        dist1 = w1.get_minimum_distance_from_the_point(d.x1, d.y1)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x2-length, d.y2+length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1
                if min_dist1 > length*0.3 or min_dist2 > length*0.3:
                    for rect in self.list_rect:
                        if rect.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x1, d.y1)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x2-length, d.y2+length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x1, d.y1)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x2-length, d.y2+length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect
                if wall_1 and wall_2:
                    if not(wall_1.x_max > wall_2.x_min and wall_2.x_max>wall_1.x_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)

            elif (opening=='left-right' and direction=='up'):
                for w1 in list_wall:
                    if w1.id!= d.id:
                    # dist1 = w1.get_minimum_distance_from_the_point(d.x3, d.y3)
                        dist1 = w1.get_minimum_distance_from_the_point(d.x3, d.y3)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x4+length, d.y4-length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1

                if min_dist1 > length*0.3 or min_dist2 > length*0.3:
                    for rect in self.list_rect:
                        if rect.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x3, d.y3)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x4+length, d.y4-length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x3, d.y3)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x4+length, d.y4-length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect
                if wall_1 and wall_2:
                    if not(wall_1.x_max > wall_2.x_min and wall_2.x_max>wall_1.x_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)

            elif (opening=='right-left' and direction=='up'):
                for w1 in list_wall:
                    if w1.id!= d.id:
                        dist1 = w1.get_minimum_distance_from_the_point(d.x4, d.y4)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x3-length, d.y3-length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1
                if min_dist1 > length*0.3 or min_dist2 > length*0.3:

                    for rect in self.list_rect:
                        if rect.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x4, d.y4)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x3-length, d.y3-length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x4, d.y4)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x3-length, d.y3-length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect
                if wall_1 and wall_2:
                    if not(wall_1.x_max > wall_2.x_min and wall_2.x_max>wall_1.x_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)
            r=random.randint(0, 255)
            g=random.randint(0, 255)
            b=random.randint(0, 255)

            dist_bw_walls = self.minimum_distance_between_two_rectangles(wall_1,wall_2);
            if min_dist1 < length*0.3 and min_dist2 < length*0.3 and wall_1.id != wall_2.id and dist_bw_walls> 0.2*length:

                self.list_door_2.append(d)
                # se il rettangolo della porta e gia presente come muro lo tolgo
                if (self.graph_wall.isIn(d)):
                    self.graph_wall.remove_node(d)

                image[d.y_min-1:d.y_max-1,d.x_min-1:d.x_max-1]=(r,g,b)
                image[wall_1.y_min-1:wall_1.y_max-1,wall_1.x_min-1:wall_1.x_max-1]=(r,g,b)
                image[wall_2.y_min-1:wall_2.y_max-1,wall_2.x_min-1:wall_2.x_max-1]=(r,g,b)
                thinckness_1 = wall_1.get_min_len()
                thinckness_2 = wall_2.get_min_len()
                thinckness = max(thinckness_1,thinckness_2)
                orientation_door = d.get_orientation()
                if orientation_door == 'vertical':
                    #because the it rotates 90 degrees
                    orientation_door == 'horizontal'
                    d_y_min = max(wall_1.y_min,wall_2.y_min)
                    d_y_max = min(wall_1.y_max,wall_2.y_max)
                    d_x_max = max(wall_1.x_min,wall_2.x_min)
                    d_x_min = min(wall_1.x_max,wall_2.x_max)
                    if d_y_max-d_y_min>thinckness*1.2:
                        if np.abs(d_y_max-d.y1) > np.abs(d_y_min-d.y1):
                            d_y_max = thinckness + d_y_min
                        else:
                            d_y_min = d_y_max - thinckness
                    # print 'x_min: '+str( d_x_min)+'   y_min: '+str(d_y_min)+'   x_max: '+str(d_x_max)+'   y_max: '+str(d_y_max)
                    image2[d_y_min-1:d_y_max-1,d_x_min-1:d_x_max-1]=(r,g,b)
                    image2[wall_1.y_min-1:wall_1.y_max-1,wall_1.x_min-1:wall_1.x_max-1]=(r,g,b)
                    image2[wall_2.y_min-1:wall_2.y_max-1,wall_2.x_min-1:wall_2.x_max-1]=(r,g,b)

                    wall_1.type = 'wall'
                    wall_2.type = 'wall'
                    self.numberDoors+=1
                    new_rect_door = Rectangle(len(self.list_rect)+self.numberDoors+self.numberWindows+1)
                    # self.list_rect.append(new_rect_door)

                    # if d_x_min>d_x_max:
                    #     print 'primo  x_min: '+str( d_x_min)+'   y_min: '+str(d_y_min)+'   x_max: '+str(d_x_max)+'   y_max: '+str(d_y_max)
                    # if d_y_min>d_y_max:
                    #     print 'primo x_min: '+str( d_x_min)+'   y_min: '+str(d_y_min)+'   x_max: '+str(d_x_max)+'   y_max: '+str(d_y_max)
                    new_rect_door.x_max = d_x_max
                    new_rect_door.y_max = d_y_max
                    new_rect_door.x_min = d_x_min
                    new_rect_door.y_min = d_y_min
                    new_rect_door.type = 'door'
                    new_rect_door.opening = d.opening

                    self.graph_image_output.add_node(new_rect_door)
                    self.graph_image_output.add_node(wall_1)
                    self.graph_image_output.add_node(wall_2)
                    id_edge = str(max(wall_1.id,new_rect_door.id)) + str(min(wall_1.id,new_rect_door.id)) + str(min(wall_1.id,new_rect_door.id)) + str(max(wall_1.id,new_rect_door.id))
                    a1 = Edge(id_edge,new_rect_door,'ADIACENT')
                    a2 = Edge(id_edge,wall_1,'ADIACENT')
                    wall_1.add_edge(a1)
                    new_rect_door.add_edge(a2)

                    id_edge = str(max(wall_2.id,new_rect_door.id)) + str(min(wall_2.id,new_rect_door.id)) + str(min(wall_2.id,new_rect_door.id)) + str(max(wall_2.id,new_rect_door.id))
                    a3 = Edge(id_edge,new_rect_door,'ADIACENT')
                    a4 = Edge(id_edge,wall_2,'ADIACENT')
                    wall_2.add_edge(a3)
                    new_rect_door.add_edge(a4)



                else:
                    orientation_door == 'vertical'
                    d_y_max = max(wall_1.y_min,wall_2.y_min)
                    d_y_min = min(wall_1.y_max,wall_2.y_max)
                    d_x_min = max(wall_1.x_min,wall_2.x_min)
                    d_x_max = min(wall_1.x_max,wall_2.x_max)
                    if d_x_max-d_x_min>thinckness*1.2:
                        if np.abs(d_x_max-d.x1) > np.abs(d_x_min-d.x1):
                            d_x_max = thinckness + d_x_min
                        else:
                            d_x_min = d_x_max - thinckness
                    #print 'x_min: '+str( d_x_min)+'   y_min: '+str(d_y_min)+'   x_max: '+str(d_x_max)+'   y_max: '+str(d_y_max)
                    image2[d_y_min-1:d_y_max-1,d_x_min-1:d_x_max-1]=(r,g,b)
                    image2[wall_1.y_min-1:wall_1.y_max,wall_1.x_min-1:wall_1.x_max]=(r,g,b)
                    image2[wall_2.y_min-1:wall_2.y_max,wall_2.x_min-1:wall_2.x_max]=(r,g,b)
                    wall_1.type = 'wall'
                    wall_2.type = 'wall'
                    self.numberDoors+=1
                    new_rect_door = Rectangle(len(self.list_rect)+self.numberDoors+self.numberWindows+1)
                    # self.list_rect.append(new_rect_door)
                    # if d_x_min>d_x_max:
                    #     print 'secondo x_min: '+str( d_x_min)+'   y_min: '+str(d_y_min)+'   x_max: '+str(d_x_max)+'   y_max: '+str(d_y_max)+" "
                    #     print d.opening
                    # if d_y_min>d_y_max:
                    #     print 'secondo x_min: '+str( d_x_min)+'   y_min: '+str(d_y_min)+'   x_max: '+str(d_x_max)+'   y_max: '+str(d_y_max)+" " + d.opening
                    #     print d.opening
                    new_rect_door.x_max = d_x_max
                    new_rect_door.y_max = d_y_max
                    new_rect_door.x_min = d_x_min
                    new_rect_door.y_min = d_y_min
                    new_rect_door.type = 'door'
                    new_rect_door.opening = d.opening

                    self.graph_image_output.add_node(new_rect_door)
                    self.graph_image_output.add_node(wall_1)
                    self.graph_image_output.add_node(wall_2)
                    id_edge = str(max(wall_1.id,new_rect_door.id)) + str(min(wall_1.id,new_rect_door.id)) + str(min(wall_1.id,new_rect_door.id)) + str(max(wall_1.id,new_rect_door.id))
                    a1 = Edge(id_edge,new_rect_door,'ADIACENT')
                    a2 = Edge(id_edge,wall_1,'ADIACENT')
                    wall_1.add_edge(a1)
                    new_rect_door.add_edge(a2)

                    id_edge = str(max(wall_2.id,new_rect_door.id)) + str(min(wall_2.id,new_rect_door.id)) + str(min(wall_2.id,new_rect_door.id)) + str(max(wall_2.id,new_rect_door.id))
                    a3 = Edge(id_edge,new_rect_door,'ADIACENT')
                    a4 = Edge(id_edge,wall_2,'ADIACENT')
                    wall_2.add_edge(a3)
                    new_rect_door.add_edge(a4)


                # image[d.y_min-1:d.y_max-1,d.x_min-1:d.x_max-1]=(r,g,b)
                # image[wall_1.y_min-1:wall_1.y_max-1,wall_1.x_min-1:wall_1.x_max-1]=(r,g,b)
                # image[wall_2.y_min-1:wall_2.y_max-1,wall_2.x_min-1:wall_2.x_max-1]=(r,g,b)
                # c=c+1
        s = self.file_name[0:-4]
        # cv2.imwrite(self.path_debug+'merge_walls_doors_windows/'+s+'_1.png', image)
        # cv2.imwrite(self.path_debug+'merge_walls_doors_windows/'+s+'_2.png', image2)


        for w in self.graph_wall.nodes():
            self.graph_image_output.add_node(w)

        if debug_mode:
            self.print_graph_image(self.graph_image_output.nodes(),debug_mode)

    #find windows
    def minimum_distance_between_two_rectangles(self,w1,w2):
        #calculated minumun distance between two rectangle
        if w1.x_max >= w2.x_min and w1.x_min <= w2.x_max:
            p1 = max(w1.y_min,w2.y_min)
            p2 = min(w1.y_max,w2.y_max)
            mind_dist = p1 - p2
        elif w1.y_max >= w2.y_min and w1.y_min <= w2.y_max:
            p1 = max(w1.x_min,w2.x_min)
            p2 = min(w1.x_max,w2.x_max)
            mind_dist = p1 - p2
        else:
            mind_dist=max(self.height,self.width)
            points_1 = [[w1.x1,w1.y1],[w1.x2,w1.y2],[w1.x3,w1.y3],[w1.x4,w1.y4]]
            points_2 = [[w2.x1,w2.y1],[w2.x2,w2.y2],[w2.x3,w1.y3],[w2.x4,w2.y4]]

            for x1,y1 in points_1:
                for x2,y2 in points_2:
                    d = math.sqrt(float(x1 - x2)**2 + float(y1 - y2)**2)
                    if d < mind_dist:
                        mind_dist = d

        return mind_dist


    def find_adjacent_loop(self,rect,pre_rect):
        pre_rect.append(rect)
        adjacent = rect.edges()
        list_return = []
        for a in adjacent:
            list_new = []
            n = a.get_node()
            if n.type != 'wall':
                if not( n in pre_rect ):
                    #if (a.get_type()=='ADIACENT' or a.get_type()=='ALIGNED')                   :
                    list_new.extend(self.find_adjacent_loop2(n,pre_rect))
                    if len( list_new)>0:
                        for e2 in list_new:
                            e2.append(n)
            else:
                if not( n in pre_rect ):
                    list_new.append([n])
            for l in list_new:
                list_return.append(l)

        return list_return

    def find_adjacent_loop2(self,rect,pre_rect):
        pre_rect.append(rect)
        adjacent = rect.edges()
        for a in adjacent:
            check_window = 0
            n = a.get_node()
            if n.type != 'wall' and n.type != 'door':
                if not( n in pre_rect ):
                    #if (a.get_type()=='ADIACENT' or a.get_type()=='ALIGNED')                   :

                   self.find_adjacent_loop(n,pre_rect)
            elif n.type == 'wall':
                if not( n in pre_rect ):
                    w_x_min = self.width
                    w_x_max = 0
                    w_y_min = self.height
                    w_y_max = 0
                    check_window = False
                    for p in pre_rect:
                        if p.type != 'wall':
                            if w_x_max < p.x_max:
                                w_x_max = p.x_max
                            if w_x_min > p.x_min:
                                w_x_min = p.x_min
                            if w_y_max < p.y_max:
                                w_y_max = p.y_max
                            if w_y_min > p.y_min:
                                w_y_min = p.y_min
                        else:
                            check_window = True
                            w = p

                    if check_window:
                        m = min((w_x_max-w_x_min),(w_y_max-w_y_min))
                        if m<(self.max_tink_wall*4):
                            for p in pre_rect:
                                if p.type != 'wall':
                                    p.type = 'window'
                        else:
                            check_window = False
                            for p in pre_rect:
                                if p.type != 'wall':
                                    if p.type !='window':
                                        p.type = 'line-structure'

                    if check_window and not([w.id,n.id] in self.walls_window):
                        self.numberWindows+=1
                        r = Rectangle(len(self.list_rect)+self.numberDoors+self.numberWindows+1)
                        #self.list_rect.append(r)
                        if (w_x_max - w_x_min) < (w_y_max - w_y_min):
                            r.y_max = max(w.y_min,n.y_min)
                            r.y_min = min(w.y_max,n.y_max)
                            r.x_min = max(w.x_min,n.x_min)
                            r.x_max = min(w.x_max,n.x_max)

                            if r.x_min < r.x_max and r.y_min<r.y_max:
                                # print  "primo "+ str(r.x_min)+"  " +str(r.y_min)+"  " +str(r.x_max)+"  " +str(r.y_max)
                                r.type = 'window'
                                r.side_touch_wall=['l1','l3']
                                #print 'verticale    x_min: '+str( r.x_min)+'   y_min: '+str(r.y_min)+'   x_max: '+str(r.x_max)+'   y_max: '+str(r.y_max)
                                self.list_windows.append(r)
                                self.walls_window.append([n.id,w.id])
                                self.walls_window.append([w.id,n.id])

                        else:
                            r.y_min = max(w.y_min,n.y_min)
                            r.y_max = min(w.y_max,n.y_max)
                            r.x_max = max(w.x_min,n.x_min)
                            r.x_min = min(w.x_max,n.x_max)
                            if r.x_min < r.x_max and r.y_min<r.y_max:
                                # print  "secondo "+ str(r.x_min)+"  " +str(r.y_min)+"  " +str(r.x_max)+"  " +str(r.y_max)
                                r.type = 'window'
                                r.side_touch_wall=['l2','l4']
                                self.list_windows.append(r)
                               # print 'orizzontale    x_min: '+str( r.x_min)+'   y_min: '+str(r.y_min)+'   x_max: '+str(r.x_max)+'   y_max: '+str(r.y_max)
                                self.walls_window.append([n.id,w.id])
                                self.walls_window.append([w.id,n.id])


    def print_graph_image(self,list_rect_graph,debug_mode):
        image=np.zeros((self.height,self.width,3),dtype=np.uint8)
        for rec in list_rect_graph:


            if rec.type == 'door':
                image[rec.y_min-1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,0)
            if rec.type == 'wall':
                p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
                cv2.drawContours(image, [p], -1, (0,0,255), -1)


                #image[rec.y_min-1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
            if rec.type == 'window':
                image[rec.y_min-1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,255)
        if debug_mode:
            cv2.imwrite(self.path_debug+'wall/'+str(self.file_name[0:len(self.file_name)-4])+'_WallAndDoor.png', image)
            cv2.imwrite(self.path_debug+'merge_all/'+str(self.file_name[0:len(self.file_name)-4])+'_WallAndDoor.png', image)

    def find_connected_componet_white(self):
        # here convert in gray-scale becouse the function  ndimage.label
        img=np.array(self.original_image)
        self.height,self.width=img.shape
        # s1 = [[0, 1, 0], [1,1,1], [0,1,0]] # elemento strutturato
        s2 = [[1, 1, 1], [1,1,1], [1,1,1]]
        # plt.imshow(self.labels_background*10, cmap = 'gray', interpolation = 'bicubic')
        # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
        # plt.show()
        self.labels, self.numlabels = ndimage.label(img>127,s2)
        # print 'number of connected components: '+str(self.numlabels)
        # print self.labels


        s=self.file_name[0:-4]

        for i in range(1,self.numlabels+1):
            cc=CC(i)
            point= np.argwhere(self.labels == i)
            # find x_max, x_min, y_max, y_min
            cc.y_min=point[:,0].min()
            cc.x_min=point[:,1].min()
            cc.y_max=point[:,0].max()
            cc.x_max=point[:,1].max()
            cc.height= cc.y_max-cc.y_min+1
            cc.width= cc.x_max-cc.x_min+1
            # I create image of the connected compis an open image with black background and white lines, i want as output an image with white background and black linesonent
            cc.image=(np.where(self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
            cc.numPixel=(cc.image.sum()/255)
            # add padding to cc.image
            h,w=cc.image.shape
            # z1=np.zeros((1,w))
            # z2=np.zeros((h+2,1))
            # cc.image=np.concatenate((z1,cc.image),axis=0)
            # cc.image=np.concatenate((cc.image,z1),axis=0)
            # cc.image=np.concatenate((z2,cc.image),axis=1)
            # cc.image=np.concatenate((cc.image,z2),axis=1)
            # cc.image_dist_tran=ndimage.morphology.distance_transform_cdt(cc.image)
            # if not(os.path.exists(self.path_debug+'preprocessing/'+s+'/')):
            #     os.mkdir(self.path_debug+'preprocessing/'+s+'/')
            # cv2.imwrite(self.path_debug+'preprocessing/'+s+'/'+str(i)+'.png',cc.image)
            #I add the connected component to the list
            self.list_cc.append(cc)

    def find_run_pixel_white(self):

        image = np.array(self.original_image)
        ret,image = cv2.threshold(image,127,255,cv2.THRESH_BINARY)
        run_hist=[]
        i=0

        m=0

        #find run pixel

        i=i+1
        #cv2.imwrite(path+self.file_name+'_CC_num_'+str(i)+'.png', cc.image)
        run=[]
        a=[]
        h,w = image.shape

        for i in range(0,h):
            row = image[i,:]
            bbb = [(len(list(group)),name) for name, group in groupby(row)]
            a.extend(bbb)
        run.extend([item for item in a if item[1] == 255])

        b=[]
        for i in range(0,w):
            col=image[:,i].transpose()
            b.extend([(len(list(group)),name) for name, group in groupby(col)])

        run.extend([item for item in b if item[1] == 255])


        run_np = np.asarray(run)
        run_hist.extend(run_np[:,0])


        mini=min(h,w)
        list_min=[]



        maxi = np.max(run_hist)
        hist, bins =np.histogram(run_hist,bins= range(1,int(max(run_hist))+2), range=(int(min(run_hist)), int(max(run_hist))))
        for i in range(0,len(hist)):
            if hist[i]>mini:
                list_min.append(i+1)

        # width =  (bins[1] - bins[0])
        # center = (bins[:-1] + bins[1:]) / 2
        # barlist=plt.bar(range(1,101), hist[0:100], align='center', width=width)
        # plt.show()
        return list_min

    def color_walls_white(self,list_max):

        self.image_white =np.zeros((self.height,self.width),dtype=np.int32)

        for cc in self.list_cc:

            #ret,image = cv2.threshold(image,127,255,cv2.THRESH_BINARY)
            run_hist=[]
            #cv2.imwrite(path+self.file_name+'_CC_num_'+str(i)+'.png', cc.image)
            run=[]
            a=[]
            h,w = cc.image.shape
            for i in range(0,h):
                row = cc.image[i,:]
                a.extend([(len(list(group)),name) for name, group in groupby(row)])
            run.extend([item for item in a if item[1] == 255])

            b=[]
            for i in range(0,w):
                col=cc.image[:,i].transpose()
                b.extend([(len(list(group)),name) for name, group in groupby(col)])

            run.extend([item for item in b if item[1] == 255])
            run_np = np.asarray(run)
            run_hist.extend(run_np[:,0])
            maxi = np.max(run_hist)
            hist, bins =np.histogram(run_hist,bins= range(1,int(max(run_hist))+2), range=(int(min(run_hist)), int(max(run_hist))))
            list_app = list_max[len(list_max)-2:len(list_max)]
            for i in range(0,len(hist)):
                if hist[i]>0:
                    if float(hist[i])/float(sum(hist))>0.5:
                        for m in list_app:
                            if (i+1)>(m*0.9) and (i+1)<(m*1.1):
                                self.image_white[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] = self.image_white[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] + cc.image

        cv2.imwrite(self.path_debug+'preprocessing/'+str(self.file_name[0:len(self.file_name)-4])+'_colorBlack_1.png',self.image_white)

        i2 = np.array(self.original_image,dtype=np.int32)

        cv2.imwrite(self.path_debug+'preprocessing/'+str(self.file_name[0:len(self.file_name)-4])+'_colorBlack_2.png',i2)

        i3 = np.abs(i2-255)+self.image_white

        cv2.imwrite(self.path_debug+'preprocessing/'+str(self.file_name[0:len(self.file_name)-4])+'_colorBlack_3.png',np.abs(i3-255))

        return np.abs(i3-255)

    #Pierpaolo's
    # def find_connected_componet_no_wall(self,input_image,return_value):
    def find_connected_componet_no_wall(self,path_gravvitas,current_directory,only_image_name,debug_mode):
        name = str(self.file_name[0:-4])
        #--non-testing
        input_path = str(self.path_output+name+'/')
        output_path = str(self.path_output+name+'/')
        #-- end non-testing

        # #--testing paths
        # input_path = '/home/ub/Documents/floorplan_recognition/tesimagistrale/input_fps/object_test/output_test/fp_1/'
        # output_path = str(self.path_output+name+'/')
        # #--end testing



        #---check if CC (Conected Component) is a
        #---window
        window = GraphCC()
        #---maybe a window
        may_win = GraphCC()
        #---external CC
        external = GraphCC()
        #---maybe a external or maybe internal CC - If CC is surrounded by walls its internal
        #--but when it has walls only on 2 sides maybe internal, maybe external
        may_be = GraphCC()



        #i add it because i have to reset the lists once extracted text
        del self.list_cc[:]


        #--testing
        # path = '/home/ub/Documents/floorplan_recognition/tesimagistrale/input_fps/object_test/output_test/fp_1/'
        #--non-testing
        path = str(self.path_output+name+'/')
        ## correct_path = path

        #---get text from text_cordinates.txt -its a file written using Methods_Text_Recognition_Based_Analysis_cc identification
        text_data = self.load_text_coordinates(input_path)
        del self.text_coordinates[:]
        self.text_coordinates = text_data
        # print self.text_coordinates
        # load stefanos output in grascale and color

        #-------------------testing
        # self.img_walls_and_doors = cv2.imread(input_path+'Stefano_output.png',cv2.IMREAD_GRAYSCALE)
        # self.img_walls_and_doors_color = cv2.imread(input_path+'Stefano_output.png',cv2.IMREAD_COLOR)
        # use_height, use_width = self.img_walls_and_doors.shape
        # print test_path+'Stefano_output.png'
        #--end testing

        #--normal run
        self.img_walls_and_doors = cv2.imread(path+'Stefano_output.png',cv2.IMREAD_GRAYSCALE)
        use_height, use_width = self.img_walls_and_doors.shape
        self.img_walls_and_doors_color = cv2.imread(path+'Stefano_output.png',cv2.IMREAD_COLOR)
        #--end normal run

        #-- find average door width. If dw<40 fix to 40, otherwise get new value
        avg_doors = imop.avg_doors_width(self.img_walls_and_doors_color)


        #--discarded CC is where all the CC too small to be anything is stored
        if not(os.path.exists(output_path)):
            os.mkdir(output_path)
        path_discarded = output_path+'discarded_cc/'
        if not(os.path.exists(path_discarded)):
            os.mkdir(path_discarded)
        #-------------------
        # self.remove_text = cv2.imread(test_path+'no_text.png', 0)
        self.remove_text = cv2.imread(input_path+'no_text.png', 0)
        #--thresholding graysale image. Meaning take pixels between 1-255 color values. So dont take black and draw in White
        #---inRange returns binary mask: white pixels (255) are pixels within upper and lower limit range and black pixels (0) are ones that do not
        img_tmp = cv2.inRange(self.img_walls_and_doors,1,255)
        if debug_mode:
            cv2.imwrite(output_path+'img_tmp.png', img_tmp)
        #--- remove all stefano identification(walls+doors+windows) from no text image (this image is original image with text erased)
        #---intention being final image to have objects+doors, windows not identified by stefano
        self.remove_text = self.remove_text-img_tmp
        img = self.remove_text
        if debug_mode:
            cv2.imwrite(output_path+'image_input.png', img)

        #---initial operation to find rooms
        rooms_cc = imop.find_connected_componet_rooms(self.img_walls_and_doors, img, output_path)
        self.img_no_wall = self.no_walls()
        self.avg_width = imop.calculate_avg_width(self.img_no_wall)
        #-------------------
        # self.img_red_wall = cv2.imread(test_path+'red_wall.png', cv2.IMREAD_COLOR)
        self.img_red_wall = cv2.imread(input_path+'red_wall.png', cv2.IMREAD_COLOR)
        cv2.imwrite(output_path+'no_wall.png', self.img_no_wall)
        #### print 'len(rooms_cc)',len(rooms_cc)
        for i in range(0,len(rooms_cc)):
            room = Room(i, rooms_cc[i], output_path)
            room.set_images(self.img_red_wall,  self.img_no_wall, img)
            room.width=self.avg_width
            self.rooms.append(room)
            self.room_index[room] = len(self.rooms)-1
        self.height,self.width=img.shape
        self.labels, self.numlabels =ndimage.label(img>127)
        self.graph_cc.image=self.labels
        if debug_mode:
            s=self.file_name[0:-4]
        for i in range(1,self.numlabels+1):
            cc=CC(i)
            point= np.argwhere(self.labels == i)
            # find x_max, x_min, y_max, y_min
            cc.y_min=point[:,0].min()
            cc.x_min=point[:,1].min()
            cc.y_max=point[:,0].max()
            cc.x_max=point[:,1].max()
            #I create image of the connected compis an open image with black background and white lines, i want as output an image with white background and black linesonent
            cc.image=np.where(self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, 255)#self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
            cc.numPixel=(cc.image.sum()/255)
            cc.image = imop.add_pad_single_pixel(cc.image)
            cc.set_coordinates()
            cc.image_dist_tran=ndimage.morphology.distance_transform_cdt(cc.image)
            cc.find_contour()
            if i == 1: #external cc
                external.add_node(cc)
            elif cc.area>=20:
                cc.check_shape()
                # I add the connected component to the list
                self.list_cc.append(cc)
                isin = False
                for room in self.room_index:
                    if isin == False:
                        isin = imop.is_inside(cc, room.contour_cc)
                        if isin == True and len(room.label) == 0:
                            # print 'add node '+str(cc.id)+' to the room '+str(room.id)
                            cc.create_histogram(bins1,bins2)
                            room.add_node(cc)
                            self.num_cc_inside+=1
                        elif isin == True and len(room.label) != 0:
                            check = self.check_extern(cc)
                            if check:
                                cc.create_histogram(bins1,bins2)
                                may_be.add_node(cc)

                            else:
                                checkWin = self.check_windows(cc)
                                if checkWin:
                                    cc.create_histogram(bins1,bins2)
                                    may_win.add_node(cc)
                                external.add_node(cc)
                                # print 'connected component '+str(cc.id)+' added to external ambient'
                                # cv2.imwrite(path_external+str(cc.id)+'.png', cc.image)
                if isin == False:
                    # print 'discarded connected component '+str(cc.id)
                    cv2.imwrite(path_discarded+str(cc.id)+'.png', cc.image)


        ymax, mxax = self.img_walls_and_doors.shape
        #----update room identification
        new_contours, self.rooms[0].contour_cc = imop.update_room(self.rooms[0].contour_cc, img, external, ymax, mxax, self.avg_width)
        if len(new_contours) > 0:
            id = len(self.rooms)
            for i in range(0, len(new_contours)):
                room = Room(i+id, new_contours[i], output_path)
                room.set_images(self.img_red_wall,  self.img_no_wall, img)
                room.width = self.avg_width
                self.rooms.append(room)
                self.room_index[room] = len(self.rooms)-1
            for cc in may_be.get_indices():
                isin = False
                for room in self.room_index:
                    if room.id >= id and isin == False:
                        isin = imop.is_inside(cc, room.contour_cc)
                        if isin == True:
                            check = self.check_windows(cc)
                            touching, dst = imop.touching_dilation(cc, self.rooms[0].contour_cc, self.avg_width)
                            if check and touching == True:
                                # print 'add new window cc: '+str(cc.id)+' to the room '+str(room.id)
                                room.add_window(cc)
                                window.add_node(cc)
                            else:
                                # print 'add node '+str(cc.id)+' to the room '+str(room.id)
                                room.add_node(cc)
                                self.num_cc_inside+=1
            for cc in may_win.get_indices():
                touching = False
                for room in self.room_index:
                    if room.id >= id and touching == False:
                        touching, dst = imop.touching_dilation(cc, room.contour_cc, self.avg_width)
        else:
            id = 0


        self.save_windows_2(id, output_path,self.img_walls_and_doors_color,avg_doors)

        self.set_room_labels(output_path)
        self.color_rooms(output_path,debug_mode)
        for room in self.room_index:
            if room.id == 0:
                room.graph = external
                room.set_images(self.img_red_wall,  self.img_no_wall, img)
            room.calculate_graph(avg_doors)
            room.save_images()
            room.save_windows(debug_mode)
        self.save_windows(window, output_path,debug_mode)
        img_edges = cv2.cvtColor(self.remove_text, cv2.COLOR_GRAY2RGB)
        for room in self.room_index:
            if room.id != 0:
                img_edges = room.draw_graph(img_edges)

        cv2.imwrite(output_path+'new_walls_windows_door-test.png', self.img_walls_and_doors_color)
        cv2.imwrite(output_path+'new_red_wall-test.png', self.img_red_wall)

        # print 'Stefano details',use_width,use_height
        iteration_obj = iterative_functions_class()
        cropped_image_image_new_walls = iteration_obj.crop_image(output_path+'new_walls_windows_door-test.png',use_height,use_width)
        cropped_image_image_new_walls.save(output_path+'new_walls_windows_door.png')
        cropped_image_new_red_wall = iteration_obj.crop_image(output_path+'new_red_wall-test.png',use_height,use_width)
        cropped_image_new_red_wall.save(output_path+'new_red_wall.png')
        cropped_image_red_wall = iteration_obj.crop_image(input_path+'red_wall.png',use_height,use_width)
        cropped_image_red_wall.save(output_path+'red_wall.png')
        cropped_image_no_text = iteration_obj.crop_image(input_path+'no_text.png',use_height,use_width)
        cropped_image_no_text.save(output_path+'no_text.png')
        cropped_image_stefano_output = iteration_obj.crop_image(input_path+'Stefano_output.png',use_height,use_width)
        cropped_image_stefano_output.save(output_path+'Stefano_output.png')
        cropped_image_walls_and_doors = iteration_obj.crop_image(input_path+'walls_and_doors.png',use_height,use_width)
        cropped_image_walls_and_doors.save(output_path+'walls_and_doors.png')

        self.find_window_cordinates(output_path,path_gravvitas,only_image_name)

    def find_window_cordinates(self,path,path_gravvitas,only_image_name):
        pil_image = Image.open(path+'new_walls_windows_door.png')
        pixels_2 = pil_image.load()
        empty_new_image = ~(np.zeros((self.height,self.width,3), np.uint8))
        cv2.imwrite(path+'_INTERMEDIATE.png', empty_new_image)
        image_3 = Image.open(path+'_INTERMEDIATE.png')
        pixels_3 = image_3.load()
        for img_row in range(self.height-1):
            for img_column in range(self.width-1):
                color = pixels_2[img_column,img_row]
                #--find yellow color (windows)
                if color[0]==255 and color[1]==255 and color[2]==0:
                    pixels_3[img_column,img_row]= (0,0,0)
        image_3.save(path+'only_windows.png')
        os.remove(path+'_INTERMEDIATE.png')

        gray_room = cv2.imread(path+'only_windows.png',cv2.IMREAD_GRAYSCALE)
        ret,thresh = cv2.threshold(gray_room,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)
        image_contour = []
        if len(contours) > 0:
            for cnt in contours:
                temp_image_contour = []
                for l, level1 in enumerate(cnt):
                    for level2 in level1:
                        x,y = level2
                        temp_image_contour.append([x,y])
                if len(temp_image_contour)>0:
                    image_contour.append(temp_image_contour)

        window_details_file = open(path_gravvitas+only_image_name+'_4_window_details.txt', 'w')
        for r,row in enumerate(image_contour):
            window =''
            for c,cord in enumerate(row):
                if c==0:
                    window  = window+'['+str(cord[0])+','+str(cord[1])+']'
                else:
                    window  = window+',['+str(cord[0])+','+str(cord[1])+']'
            window_details_file.write('Window '+str(r)+' : '+window+'\n')
        window_details_file.close()

    def set_room_labels(self, path):
        for text in self.text_coordinates:
            cx, cy = text[1]
            isin = False
            for room in self.room_index:
                if room.id != 0 and isin == False:
                    isin = room.contour_cc.check_point_inside(cx, cy)
                    if isin:
                        # print 'add label '+text[0]+' to the room '+str(room.id)
                        room.add_label(text)
            # if isin == False:
            #     print 'add label '+text[0]+' to the room external'
            #     self.rooms[0].add_label(text)
        for room in self.room_index:
            if len(room.label) == 0:
                room.add_none()

    def color_rooms(self,path,debug_mode):
        ccWhole = CC(0)
        ccWhole.image = np.array(self.img_walls_and_doors_color, dtype=np.uint8)
        # ccWhole.image = imop.add_pad_single_pixel(ccWhole.image)
        # ccWhole.image = imop.add_pad_single_pixel(ccWhole.image)
        ccWhole.x_min = 1
        ccWhole.y_min = 1
        ccWhole.set_coordinates()
        # image = deepcopy(self.img_walls_and_doors_color)
        font = cv2.FONT_HERSHEY_COMPLEX
        j = 0
        for room in self.room_index:
            if room.label[0].name!='external':
                cc= room.contour_cc
                cc.image = np.where(room.contour_cc.image<127, 0, 255)
                cc.y_min = cc.y_min+1
                cc.x_min = cc.x_min+1
                cc.set_coordinates()
                img, ccWhole.image = imop.same_size(room.contour_cc, ccWhole)
                img = np.array(img, dtype=np.uint8)
                img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
                color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

                ccWhole.image = np.where(img!=(0,0,0), color, ccWhole.image)
                # cv2.imwrite(path+str(j)+'rooms.png', ccWhole.image)
                j+=1
                # image[cc.y_min-1:cc.y_max, cc.x_min-1:cc.y_max]=np.where(img!=(0,0,0),color,image[cc.y_min-1:cc.y_max, cc.x_min-1:cc.y_max])
                # h1,w1,c1 = img.shape
                # i = 0
                # go = True
                # while go and i < 20:
                #     h2,w2,c2 = image.shape
                #     if h1 == h2 and w1 == w2:
                #         go = False
                #     else:
                #         image = imop.add_pad_single_pixel(image)
                #         i+=1
                #         print 'ciclo '+str(i)
                # color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
                # ccWhole.image = np.where(img!=(0,0,0), color, ccWhole.image)


                # cv2.imwrite(path+str(j)+'more_text_rooms.png', ccWhole.image)
                # j+=1
                # print 'inserito il testo '+room.label[0].name+' alla altezza ('+str(room.label[0].x_min)+', '+str(room.label[0].y_min)+')'
        for text in self.text_coordinates:
            x1, y1, x2, y2 = text[2]
            # print 'added text '+text[0]+' at the (x,y) = '+str(text[2])
            cv2.putText(ccWhole.image,text[0],(x1, y1), font, 2,(0,0,0),2)
        if debug_mode:
            cv2.imwrite(path+'rooms.png', ccWhole.image)

    def save_windows_2(self, id, path,img_walls_and_doors_color,avg_doors):
        # print 'saving the floorplan with new windows!'
        # isbreak1 = False
        if id != 0:
        
            ccWhole = CC(0)
            img = np.array(self.img_walls_and_doors_color, dtype=np.uint8)
            ccWhole.image=cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            ccWhole.x_min = 1
            ccWhole.y_min = 1
            ccWhole.set_coordinates()
            self.rooms[0].contour_cc.image, ccWhole.image = imop.same_size(self.rooms[0].contour_cc, ccWhole)
            ccWhole.image = imop.add_pad_single_pixel(ccWhole.image)

            for r,room in enumerate(self.room_index):
                if room.id >= id:
                    labels, numlabels =ndimage.label(self.img_walls_and_doors<10)
                    num = numlabels
                    # final_dst = np.zeros(ccWhole.image.shape, dtype=np.uint8)
                    j = 0
                    # cv2.imwrite(path+'room_'+str(room.id)+str(r)+'.png', room.contour_cc.image)
                    # isbreak= False
                    while num <= numlabels and j < 50*self.avg_width:
                        # print 'in loop - ',str(room.id)+str(r)+str(j)
                        touching, dst = imop.touching_dilation(room.contour_cc, self.rooms[0].contour_cc, self.avg_width+j)
                        # cv2.imwrite(path+'room_'+str(room.id)+str(r)+'_and_external_'+str(j)+'.png', dst)
                        #--anuradhas addition
                        # preprocess_object = pre_process_class()
                        # contour_to_delete = False
                        # contour_to_delete,input_image = preprocess_object.edit_image(dst,img_walls_and_doors_color,input_image,path,str(room.id)+str(r)+str(j),avg_doors,contour_to_delete)
                        # if contour_to_delete:
                        #     j=10*self.avg_width
                        #     isbreak=True
                        #     break
                        # else:
                        if touching == True:
                            dst = np.array(dst, dtype=np.uint8)
                            dst = np.where(dst<127, 0, 255)
                            dst = np.array(dst, dtype=np.uint8)
                            dst2 = deepcopy(dst)
                            # final_dst+=dst

                            dst = cv2.cvtColor(dst, cv2.COLOR_GRAY2RGB)
                            # cv2.imwrite(path+'new_walls_windows_door-0'+str(r)+str(j)+'.png', dst)

                            go = True
                            h1,w1,c1 = dst.shape
                            i = 0
                            while go and i < 20:
                                h2,w2,c2 = self.img_walls_and_doors_color.shape
                                if h1 == h2 and w1 == w2:
                                    go = False
                                else:
                                    self.img_walls_and_doors_color = imop.add_pad_single_pixel(self.img_walls_and_doors_color)
                                    i+=1
                            go = True
                            h1,w1,c1 = dst.shape

                            i = 0
                            while go and i < 20:
                                h2,w2,c2 = self.img_red_wall.shape
                                if h1 == h2 and w1 == w2:
                                    go = False
                                else:
                                    self.img_red_wall = imop.add_pad_single_pixel(self.img_red_wall)
                                    i+=1
                            self.img_walls_and_doors_color = np.where(dst!=(0,0,0), (0,255,255), self.img_walls_and_doors_color)
                            self.img_red_wall = np.where(dst!=(0,0,0), (0,0,255), self.img_red_wall)
                            self.img_walls_and_doors = np.array(self.img_walls_and_doors_color, dtype=np.uint8)
                            self.img_walls_and_doors = cv2.cvtColor(self.img_walls_and_doors, cv2.COLOR_RGB2GRAY)
                            labels, num =ndimage.label(self.img_walls_and_doors<50)
                        j+=1

    def save_windows(self, window, path,debug_mode):
        if window.size()>0:
            path = path+'windows/'
            if not(os.path.exists(path)):
                os.mkdir(path)
            window.save_graph(path,'graph.txt',dictionary=False)
            window.save_images(path)
            ccWhole = CC(0)
            img = np.array(self.img_red_wall, dtype=np.uint8)
            ccWhole.image=cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            ccWhole.x_min = 1
            ccWhole.y_min = 1
            ccWhole.set_coordinates()
            img = self.img_red_wall
            for cc in window.get_indices():
                imgWin, image = imop.same_size(cc,ccWhole)
                imgWin = np.array(imgWin,dtype=np.uint8)
                imgWin = cv2.cvtColor(imgWin, cv2.COLOR_GRAY2BGR)
                img = np.where(imgWin!=(0,0,0), (0,255,0), img )
            if debug_mode:
                cv2.imwrite(path+'room_with_new_windows.png', img)

    def check_extern(self, cc):
        walls = 0
        xmin, xmax, ymin, ymax = cc.get_extremes_from_center()
        # print 'estremi della cc '+str(cc.id)+': '+str(cc.get_extremes())
        cx = cc.cx[0]+cc.x_min
        cy = cc.cy[0]+cc.y_min
        # print 'cui centroide: ('+str(cx)+', '+str(cy)+')'
        find = imop.count_red(self.img_red_wall, 1, ymin, cx, cx+1)
        if find:
            walls+=1
        find = imop.count_red(self.img_red_wall, cy, cy+1, 1, xmin)
        if find:
            walls+=1
        find = imop.count_red(self.img_red_wall, ymax, self.height-1, cx, cx+1)
        if find:
            walls+=1
        find = imop.count_red(self.img_red_wall, cy, cy+1, xmax, self.width-1)
        if find:
            walls+=1
        if walls == 4:
            #cc surrounded by 4 walls!
            return True
        elif walls == 3:
            #suspected: maybe a door or window didn't found
            #self.check_again(): --- to be implemented
            return True
        else:
            return False

    #check if a cc is a window
    def check_windows(self, cc):

        walls = 0
        xmin, xmax, ymin, ymax = cc.get_extremes_from_center()
        # print 'estremi della cc '+str(cc.id)+': '+str(cc.get_extremes())
        cx = cc.center_x
        cy = cc.center_y
        # print 'cui centroide: ('+str(cx)+', '+str(cy)+')'
        find = imop.count_red(self.img_red_wall, 0, ymin, cx, cx+1)
        if find:
            walls+=1
        find = imop.count_red(self.img_red_wall, cy, cy+1, 0, xmin)
        if find:
            walls+=1
        find = imop.count_red(self.img_red_wall, ymax, self.height-1, cx, cx+1)
        if find:
            walls+=1
        find = imop.count_red(self.img_red_wall, cy, cy+1, xmax, self.width-1)
        if find:
            walls+=1
        if walls == 4:
            #cc surrounded by 4 walls!
            return False
        else:
            # print 'forse una finestra: cc '+str(cc.id)
            ccWhole = CC(0)
            ccWhole.image=self.image_remove_text
            ccWhole.x_min = 0
            ccWhole.y_min = 0
            ccWhole.set_coordinates()
            imgRed, img = imop.same_size(cc,ccWhole)
            i = 2
            numlabels = 0
            go = True
            kernel = np.ones((8,8),np.uint8)
            imgRed2 = cv2.dilate(imgRed,kernel,iterations = 1)
            imgRed2 = np.array(imgRed2,dtype=np.uint8)
            imgRed2 = cv2.cvtColor(imgRed2, cv2.COLOR_GRAY2BGR)
            image = np.where(imgRed2!=(0,0,0), self.img_red_wall, (0,0,0))
            image = cv2.inRange(image, (0,0,255), (0,0,255))
            # kernel = np.ones((1,1),np.uint8)
            # image = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
            s2 = [[1, 1, 1], [1,1,1], [1,1,1]]
            labels, numlabels = ndimage.label(image!=0, s2)


            if numlabels == 2:
                corners = [0,0]
                area = []
                #calcualte, for each red cc, the max angle between the centroid and walls
                for i in range(1,3):
                    ccRed = CC(1)
                    point= np.argwhere(labels == i)

                    ccRed.y_min=point[:,0].min()
                    ccRed.x_min=point[:,1].min()
                    ccRed.y_max=point[:,0].max()
                    ccRed.x_max=point[:,1].max()
                    #I create image of the connected compis an open image with black background and white lines, i want as output an image with white background and black linesonent
                    ccRed.image=np.where(labels[ccRed.y_min:ccRed.y_max+1,ccRed.x_min:ccRed.x_max+1] == i, 255, labels[ccRed.y_min:ccRed.y_max+1,ccRed.x_min:ccRed.x_max+1])#self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)

                    ccRed.image = imop.add_pad_single_pixel(ccRed.image)
                    ccRed.set_coordinates()

                    ccRed.find_contour()
                    over = imop.check_over_90(ccRed, cc.center_x, cc.center_y)

                    if over == False:
                        area.append(ccRed.area)

                        for xA in ccRed.contourSimple[0]:
                            for xB in ccRed.contourSimple[0]:
                                corner = mm.calculate_angle(xA[0][0], xA[0][1], xB[0][0],xB[0][1], cc.center_x, cc.center_y)
                                # print 'corner = '+str(corner)
                                if corner > corners[i-1]:
                                    corners[i-1] = corner
                    else:
                        return False

                diff = abs(corners[0]-corners[1])
                if min(area[0], area[1])!=0:
                    areaRatio = max(area[0], area[1])/min(area[0], area[1])
                else:
                    areaRatio = 9999
                # print 'diff = '+str(diff)
                # print 'corners = '+str(corners)
                if diff < 20 and corners[0] < 60 and corners[1] < 60 and areaRatio < 2:

                    return True
                else:
                    return False
        return False

    def save_text(self,path):
        #---write 1.text, 2.centroid of text, 3.topleft and bottomright of text
        file = open(path+'recognized_text.txt', 'w')
        file.write(str(len(self.text_coordinates))+'\n')
        for text in self.text_coordinates:
            for i in range(0, len(text)):
                if i == 0:
                    file.write(str(text[i])+'\n')
                elif i == 1:
                    for j in range(0, 2):
                        file.write(str(text[i][j])+'\n')
                elif i ==2:
                    for j in range(0, 4):
                        file.write(str(text[i][j])+'\n')

        file.close()

    def load_text_coordinates(self, path):
        # print path+'/recognized_text.txt'
        file = open(path+'/recognized_text.txt', 'r')
        num = int(file.readline())
        text_data = []
        for k in range(0, num):
            text = []
            name = str(file.readline())
            while name[-1] == '\n':
                name = name[0:-1]
            text.append(name)
            cx = int(file.readline())
            cy = int(file.readline())
            text.append([cx,cy])
            x1 = int(file.readline())
            y1 = int(file.readline())
            x2 = int(file.readline())
            y2 = int(file.readline())
            text.append([x1,y1,x2,y2])
            text_data.append(text)
        file.close()
        return text_data

    def find_connected_componet_dictionary(self):
        del self.list_cc[:]
        img=self.image_remove_text
        self.height,self.width=img.shape
        self.labels, self.numlabels =ndimage.label(img>127)
        cv2.imwrite(self.path_debug+self.file_name, img)
        # print 'number of connected components: '+str(self.numlabels)
        s=self.file_name[0:-4]
        if self.numlabels == 1:
            b = 1
        else:
            b = 2
        for i in range(b,self.numlabels+1):

                cc=CC(i)
                point= np.argwhere(self.labels == i)
                # find x_max, x_min, y_max, y_min
                cc.y_min=point[:,0].min()
                cc.x_min=point[:,1].min()
                cc.y_max=point[:,0].max()
                cc.x_max=point[:,1].max()
                # print 'the component ' +str(i) +' has max coordinates [(' +str(cc.x_min) +', ' +str(cc.x_max) +'), (' +str(cc.y_min) +', '+str(cc.y_max)+')]'
                cc.height= cc.y_max-cc.y_min+1
                cc.width= cc.x_max-cc.x_min+1
                #the center of the cc
                cc.center_x = cc.y_min+(cc.height/2)
                cc.center_y = cc.x_min+(cc.width/2)
                #I create image of the connected comps an open image with black background and white lines, I want as output an image with white background and black lines onent
                cc.image=(np.where(self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
                #print self.path_output+'white_'+str(cc.id)+'.png'
                #cc2.imwrite(self.path_output+'white_'+str(cc.id)+'.png', cc.image_dist_tran)

                cc.numPixel=(cc.image.sum()/255)
                #add padding to cc.image
                cc.image = imop.add_pad_single_pixel(cc.image)
                cc.set_coordinates()
                cv2.imwrite(self.path_output+'white_'+str(cc.id)+'.png', cc.image)

                cc.image_dist_tran=ndimage.morphology.distance_transform_cdt(cc.image)
                #print self.path_output+'dst_'+str(cc.id)+'.png'
                #cv2.imwrite(self.path_output+'dst_'+str(cc.id)+'.png', cc.image_dist_tran)
                # I add the connected component to the list
                cc.find_contour()
                cc.check_shape()
                #print cc.area
                if cc.area>20:
                    cc.create_histogram(bins1, bins2)
                    self.list_cc.append(cc)
                    self.graph_cc.add_node(cc)
        self.avg_width=imop.calculate_avg_width(img)
        self.graph_cc.img_no_text=img
        self.graph_cc.create_edges(self.avg_width, dictionary = True)

        if len(self.list_cc) > 1:
            # print len(self.list_cc)
            floor = self.graph_cc.getMaxNode()
            self.graph_cc.floor_adjacence(floor, dictionary=True)
            #----removes nodes (reason can be: that it has bad features)
            #self.graph_cc.remove_node_and_edge(floor)
            img, img2 = self.graph_cc.save_edges(floor)
            cv2.imwrite(self.path_output+'edges.png', img)
            cv2.imwrite(self.path_output+'edges_graph.png', img2)
        self.graph_cc.save_graph(self.path_output, self.file_name, dictionary=True)
        self.graph_cc.save_images(self.path_output)

        self.graph_cc.save_graph(self.path_output, self.file_name, dictionary=True)
        self.graph_cc.save_images(self.path_output)

    def no_walls(self):
        img_bw = np.array(self.image_remove_text)
        # print img_bw.shape
        # print self.img_walls_and_doors.shape
        img_bw = np.where(self.img_walls_and_doors>10, 255, img_bw)
        return img_bw

    def classification_windows(self,debug_mode):
        self.walls_window=[]
        list_return = []
        for w1 in self.graph_wall.nodes():
            adjacent = w1.edges()

            for a in adjacent:
                list_new = []
                n = a.get_node()
                #if a.get_type() == 'TANGENT'and n.type!='wall':
                if n.type!='wall'and  n.type!="door":
                    list_new.extend(self.find_adjacent_loop2(n,[w1]))
                    if len( list_new)>0:
                        for e2 in list_new:
                            e2.append(n)
                            e2.append(w1)
                    for l in list_new:
                        list_return.append(l)

        dic_list = {}

        while len(list_return)>0:
            l = list_return.pop()
            w = []
            for e in l:
                if e.type=="wall":
                    w.append(e)
            if len(w)==2:
                l_key = dic_list.keys()
                w1 = w[0]
                w2 = w[1]
                m1 = max(w1.id,w2.id)
                m2 = min(w1.id,w2.id)
                key = str(m1)+'-'+str(m2)
                if key in l_key:
                    dic_list[key].append(l)
                else:
                    dic_list[key] = [l]

        dic_final = {}
        for k in dic_list.keys():
            min_leng = max(self.height,self.height)
            min_leng2 = max(self.height,self.height)
            check_app= False
            for l in dic_list[k]:

                leng = 0
                for e in l:
                    pp = e.get_max_len()
                    leng = leng + e.get_max_len()
                leng2 = len(l)

                if leng/min_leng<=2:
                    if leng2/min_leng2<=2:
                        min_leng2 = leng2
                        min_leng = leng
                        check_app=True
                        app = l
                # if leng2 < min_leng2:
                #     min_leng2 = leng2
                #     check_app=True
                #     app = l

            if(check_app):
                dic_final[k] = app

        for k in dic_final.keys():
            l_w = []
            w_x_min = self.width
            w_x_max = 0
            w_y_min = self.height
            w_y_max = 0
            check_window = 0
            for p in dic_final[k]:
                if p.type != 'wall':
                    if w_x_max < p.x_max:
                        w_x_max = p.x_max
                    if w_x_min > p.x_min:
                        w_x_min = p.x_min
                    if w_y_max < p.y_max:
                        w_y_max = p.y_max
                    if w_y_min > p.y_min:
                        w_y_min = p.y_min
                    p.type = 'window'
                else:
                    check_window = True
                    l_w.append(p)
            #
            # if check_window:
            #     m = min((w_x_max-w_x_min),(w_y_max-w_y_min))
            #     if m<(self.max_tink_wall*4):
            #         for p in dic_final[k]:
            #             if p.type != 'wall':
            #                 p.type = 'window'
            #     else:
            #         check_window = False
            #         for p in dic_final[k]:
            #             if p.type != 'wall':
            #                 if p.type !='window':
            #                     p.type = 'line-structure'

            if check_window:
                self.numberWindows+=1
                r = Rectangle(len(self.list_rect)+self.numberDoors+self.numberWindows+1)
                #self.list_rect.append(r)
                w = l_w[0]
                n = l_w[1]
                if (w_x_max - w_x_min) < (w_y_max - w_y_min):
                    r.y_max = max(w.y_min,n.y_min)
                    r.y_min = min(w.y_max,n.y_max)
                    r.x_min = max(w.x_min,n.x_min)
                    r.x_max = min(w.x_max,n.x_max)

                    if r.x_min < r.x_max and r.y_min<r.y_max:
                        # print  "primo "+ str(r.x_min)+"  " +str(r.y_min)+"  " +str(r.x_max)+"  " +str(r.y_max)
                        r.type = 'window'
                        r.side_touch_wall=['l1','l3']
                        #print 'verticale    x_min: '+str( r.x_min)+'   y_min: '+str(r.y_min)+'   x_max: '+str(r.x_max)+'   y_max: '+str(r.y_max)
                        self.list_windows.append(r)
                        # self.walls_window.append([n.id,w.id])
                        # self.walls_window.append([w.id,n.id])

                else:
                    r.y_min = max(w.y_min,n.y_min)
                    r.y_max = min(w.y_max,n.y_max)
                    r.x_max = max(w.x_min,n.x_min)
                    r.x_min = min(w.x_max,n.x_max)
                    if r.x_min < r.x_max and r.y_min<r.y_max:
                        # print  "secondo "+ str(r.x_min)+"  " +str(r.y_min)+"  " +str(r.x_max)+"  " +str(r.y_max)
                        r.type = 'window'
                        r.side_touch_wall=['l2','l4']
                        self.list_windows.append(r)
                        # print 'orizzontale    x_min: '+str( r.x_min)+'   y_min: '+str(r.y_min)+'   x_max: '+str(r.x_max)+'   y_max: '+str(r.y_max)
                        # self.walls_window.append([n.id,w.id])
                        # self.walls_window.append([w.id,n.id])

          # stampo immagine  con i rettangoli
        image_rect=np.zeros((self.height,self.width,3),dtype=np.uint8)
        for rec in self.list_rect:
            r=random.randint(0, 255)
            g=random.randint(0, 255)
            b=random.randint(0, 255)
            p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
            cv2.drawContours(image_rect, [p], -1, (r,g,b), -1)
            #image_rect[rec.y_min-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)
        if debug_mode:
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_1.png', image_rect)

        # primo step stampa immagine in bianco e nero con i rettangoli delle presnte finestre in giallo
        image2=np.zeros((self.height,self.width,3),dtype=np.uint8)
        for rec in self.list_rect:
            if rec.type == 'window':
                image2[rec.y_min-1:rec.y4,rec.x1-1:rec.x2]=(0,255,255)
            elif rec.type =='line-structure':
                image2[rec.y_min-1:rec.y4,rec.x1-1:rec.x2]=(255,0,255)
            else:
                image2[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(255,255,255)
        if debug_mode:
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_2.png', image2)

        # secondo step, stampa le immagine delle finestre(presunte) trovate tra due muri, con porte e muri
        # for w in self.list_windows:
        #     self.graph_image_output.add_node(w)

        image3=np.zeros((self.height,self.width,3),dtype=np.uint8)
        for rec in self.graph_image_output.nodes():
            if rec.type == 'door':
                image3[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,0)
            elif rec.type == 'wall':
                p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
                cv2.drawContours(image3, [p], -1, (0,0,255), -1)
                #image3[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
        for rec in self.list_windows:
            if rec.type == 'window':
                #image3[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,255)
                p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
                cv2.drawContours(image3, [p], -1, (0,255,255), -1)
        if debug_mode:
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_3.png', image3)

        # rimuovo le finestre che si sovrappongono ai muri
        list_windows_remove=[]
        i = 0
        for rec in self.list_windows:
            check_window=False
            i1 = np.zeros((self.height,self.width),dtype=np.uint8)
            i1[rec.y_min:rec.y_max+1,rec.x_min:rec.x_max+1]=1

            for rec2 in self.graph_image_output.nodes():
                if rec2.type=='wall' or rec2.type=='door':
                    i2 = np.zeros((self.height,self.width),dtype=np.uint8)
                    i2[rec2.y_min:rec2.y_max+1,rec2.x_min:rec2.x_max+1]=1
                    s = np.sum(i2)
                    # print str(rec2.y_min)+" "+str(rec2.y_max)+" "+str(rec2.x_min)+" "+str(rec2.x_max)
                    logic_and = i1*i2
                    ss = np.sum(logic_and)
                    i+=1
                    # print 'ciclo '+str(i)
                    if float(s/2) < ss:
                        list_windows_remove.append(rec)
                        break
        # print str(len(list_windows_remove))+' finestre da rimuovere'
        k = len(list_windows_remove)
        i = 0
        while len(list_windows_remove)>0 and i<k+1:
            rem = list_windows_remove.pop()
            self.list_windows.remove(rem)
            i+=0

        image6 = np.zeros((self.height,self.width,3),dtype=np.uint8)
        for rec in self.graph_image_output.nodes():
                if rec.type == 'door':
                    image6[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,0)
                elif rec.type == 'wall':
                    p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
                    cv2.drawContours(image6, [p], -1, (0,0,255), -1)
                    #image6[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
        for rec in self.list_windows:
            image6[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,255)
        if debug_mode:
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_7.png', image6)




        # terzo step, vado a trovare le componenti connesse del background per determinare se e una finestra
        imageBackground=np.zeros((self.height,self.width),dtype=np.uint8)
        list_app = self.list_windows[:]
        list_app.extend(self.graph_image_output.nodes())
        for rec in list_app:
            if rec.type=='wall':
                p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
                cv2.drawContours(imageBackground, [p], -1, 255, -1)
            else:
                imageBackground[rec.y_min-1:rec.y_max,rec.x_min-1:rec.x_max]=255

        s1 = [[0, 1, 0], [1,1,1], [0,1,0]] # elemento strutturato
        s2 = [[1, 1, 1], [1,1,1], [1,1,1]]
        labels,numlabels =ndimage.label(imageBackground<127,s2)
        labels2 = (np.where(labels != 1, 0, labels)*255)

        if debug_mode:
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_4.png', labels*10)
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_5.png', labels2)



        labels2 = np.abs(labels2 - (np.ones((self.height,self.width))*255))
        list_windows_remove=[]
        for win in self.list_windows:
            check_l1 = 0
            check_l2 = 0
            check_l3 = 0
            check_l4 = 0
            if not('l1' in win.side_touch_wall[0]):

                s = np.sum(labels2[win.y_min-2:win.y_min-1,win.x_min+3:win.x_max-3])
                if s!=0:
                   check_l1 = 1

            if not('l2' in win.side_touch_wall[0]):

                s = np.sum(labels2[win.y_min+3:win.y_max-3,win.x_max+1:win.x_max+2 ])
                if s!=0:
                   check_l2 = 1

            if not('l3' in win.side_touch_wall[0]):

                s = np.sum(labels2[win.y_max+1:win.y_max+2,win.x_min+3:win.x_max-3 ])
                if s!=0:
                    check_l3 = 1

            if not('l4' in win.side_touch_wall[0]):

                s = np.sum(labels2[win.y_min+3:win.y_max-3,win.x_min-2:win.x_min-1 ])
                if s!=0:
                    check_l4 = 1

            if check_l1 and check_l3:
                list_windows_remove.append(win)
            if check_l2 and check_l4:
                list_windows_remove.append(win)

        # list_dww = []
        # for rec in list_app():
        #     if not(rec in list_windows_remove):
        #         list_dww.append(rec)
        while len(list_windows_remove)>0:
            rem = list_windows_remove.pop()
            self.list_windows.remove(rem)


        # for rec in self.list_windows:
        #     if (rec in list_windows_remove):
        #         self.list_windows.remove(rec)

        image4 = np.zeros((self.height,self.width,3),dtype=np.uint8)
        for rec in self.list_windows:
            image4[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,255)

        for rec in self.graph_image_output.nodes():
            if rec.type == 'door':
                image4[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,0)
            elif rec.type == 'wall':
                p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
                cv2.drawContours(image4, [p], -1, (0,0,255), -1)
                #image4[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
        if debug_mode:
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_6.png', image4)





        # rimuovo le finestre che hanno uno spessore superiore allo spessore del muro
        l_remove=[]
        for rec in self.list_windows:
            l_win =  min(np.abs(rec.x_max-rec.x_min),np.abs(rec.y_max-rec.y_min))
            if l_win>(self.max_tink_wall*1.5) or l_win < (self.max_tink_wall*0.2):

                l_remove.append(rec)
            else:

                self.graph_image_output.add_node(rec)
        while len(l_remove)>0:
            rem = l_remove.pop()
            self.list_windows.remove(rem)

        # list_dww.extend(self.list_windows)
        # list_dww.extend(self.graph_image_output.nodes())

        image7 = np.zeros((self.height,self.width,3),dtype=np.uint8)
        image8 = np.zeros((self.height,self.width,3),dtype=np.uint8)
        image9 = np.array(self.color_image)
        for rec in self.graph_image_output.nodes():
                if rec.type == 'window':
                    image7[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,255)
                    image8[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
                    image9[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
                elif rec.type == 'door':
                    image7[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,0)
                    image8[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
                    image9[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
                elif rec.type == 'wall':
                    p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
                    cv2.drawContours(image7, [p], -1, (0,0,255), -1)
                    cv2.drawContours(image8, [p], -1, (0,0,255), -1)
                    cv2.drawContours(image9, [p], -1, (0,0,255), -1)
                    #image7[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
                else:
                    image7[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(255,255,0)
                    image8[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
                    image9[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
        if debug_mode:
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_8.png', image7)
        if len(self.list_scale)>0:
            for s in self.list_scale:
                cv2.drawContours(image7, [s], -1, (255,0,255), -1)
                cv2.drawContours(image8, [s], -1, (0,0,255), -1)
                cv2.drawContours(image9, [s], -1, (0,0,255), -1)


        name = str(self.file_name[0:-4])
        path = str(self.path_output+name+'/')
        cv2.imwrite(path+'Stefano_output.png', image7)
        cv2.imwrite(path+'walls_and_doors.png', image8)
        cv2.imwrite(path+'red_wall.png', image9)
        # cv2.imwrite(reiterate_path+'Stefano_output.png', image7)
        # cv2.imwrite(reiterate_path+'walls_and_doors.png', image8)
        # cv2.imwrite(reiterate_path+'red_wall.png', image9)
        self.img_red_wall = image9
        self.img_walls_and_doors = image8
        self.list_wwd.extend(self.graph_image_output.nodes())

    def anu_function_remove_text(self,img,x1,y1,x2,y2):
        #--check later
        cv2.rectangle(img, (x1-1, y1-1), (x2+2, y2+2), (255,255,255), -1)
        return img

    def draw_all_open_plan_lines(self,output_path,all_open_plan_lines,debug_mode):
        #--testing
        output_path = '/home/ub/Documents/floorplan_recognition/tesimagistrale/input_fps/object_test/output_test/fp_1/'
        #--end testing
        pierpaolo_image = cv2.imread(output_path+'new_walls_windows_door.png',cv2.IMREAD_COLOR)
        walls_and_doors_image = cv2.imread(output_path+'walls_and_doors.png',cv2.IMREAD_COLOR)
        red_wall_image = cv2.imread(output_path+'red_wall.png',cv2.IMREAD_COLOR)

        # print output_path+'new_walls_windows_door.png'
        #--fill in missing line segments
        bougs_path1,bogus_path_2 ,name = '','',''
        line_opt = line_optimization_class(bougs_path1,bogus_path_2,name)
        new_max_lines = line_opt.find_closest_contour_point_region_grow(output_path,'new_walls_windows_door.png',all_open_plan_lines)

        # test_image = cv2.imread(output_path+'Stefano_output.png',cv2.IMREAD_GRAYSCALE)
        # height_1, width_1 = test_image.shape
        # print 'Stefano_output',height_1, width_1
        # test_image_1 = cv2.imread(output_path+'new_walls_windows_door.png',cv2.IMREAD_GRAYSCALE)
        # height_2, width_2 = test_image_1.shape
        # print 'new_walls_windows_door', height_2, width_2



        output_path_list = os.listdir(output_path)
        output_path_list.sort()

        for each_item in output_path_list:
            content_name = str(each_item)
            if content_name != 'no_text.png' and content_name != 'recognized_text.txt':
                if os.path.isdir(output_path+content_name):
                    shutil.rmtree(output_path+content_name)
                else:
                    os.remove(output_path+content_name)

        for each_line in new_max_lines:
            # print 'each_line',each_line
            cv2.line(pierpaolo_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,255),5,cv2.cv.CV_AA)
            cv2.line(walls_and_doors_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,255),5,cv2.cv.CV_AA)
            cv2.line(red_wall_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,255),5,cv2.cv.CV_AA)
        cv2.imwrite(output_path+'Stefano_output.png', pierpaolo_image)
        cv2.imwrite(output_path+'walls_and_doors.png', walls_and_doors_image)
        cv2.imwrite(output_path+'red_wall.png', red_wall_image)

    #not used
    # def print_image_windows_doors_walls_stairs(self):
    #
    #     image=np.zeros((self.height,self.width),dtype=np.uint8)
    #
    #     for cc in self.list_scale:
    #         # cc_rgb = cv2.cvtColor(cc.image.astype(np.uint8),cv2.COLOR_GRAY2RGB)/255
    #         image[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] = image[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+cc.image
    #
    #     k=int(self.max_tink_wall/4)
    #     kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(k,k))
    #     #kernel_1=cv2.getStructuringElement(cv2.MORPH_RECT,(4,4),(0,2))
    #     closing = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel)
    #     cv2.imwrite(self.path_debug+'merge_all/'+self.file_name[0:len(self.file_name)-4]+'_3.png', closing)
    #
    #     for rec in self.list_wwd:
    #         closing[rec.y_min-1:rec.y_max,rec.x_min-1:rec.x_max]=255
    #     cv2.imwrite(self.path_debug+'merge_all/'+self.file_name[0:len(self.file_name)-4]+'_4.png',closing )
    #
    #     cc_rgb = cv2.cvtColor(closing.astype(np.uint8),cv2.COLOR_GRAY2RGB)
    #     cc_rgb2 = cv2.cvtColor(self.image_remove_text.astype(np.uint8),cv2.COLOR_GRAY2RGB)
    #
    #     image_final= cc_rgb+cc_rgb2
    #     cv2.imwrite(self.path_debug+'merge_all/'+self.file_name[0:len(self.file_name)-4]+'_5.png', image_final)

    # def save_log(self, path):
    #     file = open(path+'log.txt','w')
    #     file.write('number of cc found = '+str(len(self.list_cc))+'\n')
    #     file.write('number of cc inside some room = '+str(self.num_cc_inside)+'\n')
    #     file.write('number of cc discarded = '+str(len(self.list_cc)-self.num_cc_inside)+'\n')
    #     file.close()

    # def delete_walls(self):
    #     image = np.array(self.color_image)
    #     img_bw = self.image_remove_text
    #     r = 0
    #     g = 0
    #     b = 255
    #     for rect in self.graph_wall.nodes():
    #         #if rect.type=="oblique":
    #         p = np.array([[[rect.x1-1, rect.y1-1],[rect.x2+1, rect.y2-1],[rect.x3+1, rect.y3+1],[rect.x4-1, rect.y4+1]]], dtype=np.int32)
    #         cv2.drawContours(image, [p], -1, (r,g,b), -1)
    #         cv2.drawContours(img_bw, [p], -1, (b), -1)
    #
    #         #else:
    #         #    image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
    #
    #     for rect in self.list_door_2:
    #         #if rect.type=="oblique":
    #         p = np.array([[[rect.x1-1, rect.y1-1],[rect.x2+1, rect.y2-1],[rect.x3+1, rect.y3+1],[rect.x4-1, rect.y4+1]]], dtype=np.int32)
    #         cv2.drawContours(image, [p], -1, (r,g,b), -1)
    #         if rect.opening[1]=="left":
    #             for e in rect.list_left_rect:
    #                 image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
    #                 img_bw[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=b
    #         if rect.opening[1]=="right":
    #             for e in rect.list_right_rect:
    #                 image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
    #                 img_bw[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=b
    #         if rect.opening[1]=="up":
    #             for e in rect.list_up_rect:
    #                 image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
    #                 img_bw[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=b
    #         if rect.opening[1]=="down":
    #             for e in rect.list_down_rect:
    #                 image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
    #                 img_bw[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=b
    #
    #     cv2.imwrite(self.path_output+str(self.file_name[0:len(self.file_name)-4])+'red_wall.png', image)
    #     cv2.imwrite(self.path_output+str(self.file_name[0:len(self.file_name)-4])+'no_wall.png', img_bw)
    #     return image, img_bw
    #

    # def my_function_remove_text(self,span,img):
    #     bbox_data = (span.get('title'))
    #     only_numbers =  re.findall(r'\d+',bbox_data)
    #     x1 = int(only_numbers[0])
    #     y1 = int(only_numbers[1])
    #     x2 = int(only_numbers[2])
    #     y2 = int(only_numbers[3])
    #
    #     value_bbox = (span.get('title'))
    #     numbers = re.findall('\d+', value_bbox)
    #     x1 = int(numbers[0])
    #     y1 = int(numbers[1])
    #     x2 = int(numbers[2])
    #     y2 = int(numbers[3])
    #     #--pier method
    #     numPixel = float(self.image_remove_text[y1:y2,x1:x2].sum()/255)
    #     if y2 == y1:
    #         y2+=1
    #     if x2 == x1:
    #         x2+=1
    #     density =  numPixel/(float((y2-y1)*(x2-x1)))
    #     if (density>0.2 and density<0.9):
    #         cv2.rectangle(self.image_remove_text, (x1-1, y1-1), (x2-1, y2-1), 255, -1)
    #     #--anu method
    #     # cv2.rectangle(img, (x1, y1), (x2, y2), (255,255,255), -1)
    #     return img

    # def new_rooms(self, img, room, new_room_list, path):
    #     none_rooms = []
    #     new_rooms, numrooms = ndimage.label(img>127)
    #     for i in range(1, numrooms+1):
    #         cc=CC(i)
    #         point= np.argwhere(new_rooms == i)
    #         cc.y_min=point[:,0].min()
    #         cc.x_min=point[:,1].min()
    #         cc.y_max=point[:,0].max()
    #         cc.x_max=point[:,1].max()
    #         cc.image=np.where(new_rooms[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, 255)
    #         cc.image = imop.add_pad_single_pixel(cc.image)
    #         cc.set_coordinates()
    #         # cc.image_dist_tran=ndimage.morphology.distance_transform_cdt(cc.image)
    #         cc.find_contour()
    #         isin = False
    #         for lab in room.label:
    #             if isin == False:
    #                 isin = cc.check_point_inside(lab.cx, lab.cy)
    #                 if isin:
    #                     label = lab
    #         if isin and cc.area > 400:
    #             r = Room(len(self.rooms)+len(new_room_list), cc, path)
    #             r.label.append(label)
    #             r.width = self.avg_width
    #             r.set_images(self.img_red_wall,  self.img_no_wall, self.remove_text)
    #             new_room_list.append(r)
    #         else:
    #             # print 'found a none room'
    #             none_rooms.append(cc)
    #     while len(none_rooms) > 0:
    #         # print 'none_rooms = '+str(len(none_rooms))
    #         for cc_none in none_rooms:
    #             white = 0
    #             for r in new_room_list:
    #                 touch, dst = imop.touching_dilation(cc_none, r.contour_cc, self.avg_width)
    #                 if touch:
    #                     dst = cv.fromarray(dst)
    #                     wh = cv.CountNonZero(dst)
    #                     if wh > white:
    #                         white = wh
    #                         rum = r
    #             if white > 0:
    #                 img1 = imop.restore_original_size(cc_none, self.img_walls_and_doors_color)
    #                 img2 = imop.restore_original_size(rum.contour_cc, self.img_walls_and_doors_color)
    #                 s1 = img1.shape
    #                 s2 = img2.shape
    #                 if s1[0]!=s2[0] or s1[1]!=s2[1]:
    #                     # print 'img1 = '+str(img1.shape)
    #                     # print 'img2 = '+str(img2.shape)
    #                     cv2.imwrite(path+'none_room.png', img1)
    #                     cv2.imwrite(path+'room_'+str(rum.label[0].name)+'_'+str(rum.id)+'.png', img2)
    #                 img = img1+img2
    #                 # cv2.imwrite(path+'room_'+rum.label[0].name+'_'+str(rum.id)+'_addiction.png',img)
    #                 kernel = np.ones((3,3),np.uint8)
    #                 closure = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)
    #                 # cv2.imwrite(path+'room_'+rum.label[0].name+'_'+str(rum.id)+'_closure.png',closure)
    #                 new_room, numlabel = ndimage.label(closure>127)
    #                 cc = rum.contour_cc
    #                 point= np.argwhere(new_room == 1)
    #                 cc.y_min=point[:,0].min()
    #                 cc.x_min=point[:,1].min()
    #                 cc.y_max=point[:,0].max()
    #                 cc.x_max=point[:,1].max()
    #                 cc.image=np.where(new_room[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] == 1, 255, 0)
    #                 cc.image = imop.add_pad_single_pixel(cc.image)
    #                 cc.set_coordinates()
    #                 # cc.image_dist_tran=ndimage.morphology.distance_transform_cdt(cc.image)
    #                 cc.find_contour()
    #                 rum.contour_cc = cc
    #                 # cv2.imwrite(path+'room_'+rum.label[0].name+'_'+str(rum.id)+'.png',cc.image)
    #                 none_rooms.remove(cc_none)
    #
    #     for r in new_room_list:
    #         for cc2 in room.graph.get_indices():
    #                 isin = imop.is_inside(cc2, cc)
    #                 over = imop.over_wall(self.img_red_wall, label.transform_to_cc(), cc2)
    #                 # print 'Is cc '+str(cc2.id)+' inside the new room '+label.name+'? '+str(isin)
    #                 # print 'Is it passed through a red wall? '+str(over)
    #                 if isin and over == False:
    #                     r.graph.add_node(cc2)
    #     return new_room_list
    #
    # def purge_rooms(self, purgandi):
    #     if len(purgandi) > 0 and len(purgandi) != len(self.rooms):
    #         for i in purgandi:
    #             self.remove_room(i)
    #
    # def remove_room(self,u):
    #     if u in self.room_index:
    #         self.rooms.remove(u)
    #         for n in self.rooms[self.room_index[u]:len(self.rooms)]:
    #             self.room_index[n] = self.room_index[n] - 1
    #         del self.room_index[u]

     # def classification_windows2(self, debug_mode):
    #     self.walls_window=[]
    #     # image0 = np.array(self.color_image)
    #     # for w1 in self.graph_wall.nodes():
    #     #     if w1.type!='door':
    #     #         adjacent = w1.edges()
    #     #         for a in adjacent:
    #     #             n = a.get_node()
    #     #             adjacent2 = n.edges()
    #     #             for a2 in adjacent2:
    #     #                 n2 = a2.get_node()
    #     #                 if n2.type!='door' and n2.type!='wall':
    #     #                     image0[n2.y_min-1:n2.y4,n2.x1-1:n2.x2]=(255,0,255)
    #     #             if n.type!='door' and n.type!='wall':
    #     #                 image0[n.y_min-1:n.y4,n.x1-1:n.x2]=(255,255,0)
    #     #         image0[w1.y_min-1:w1.y4,w1.x1-1:w1.x2]=(0,0,255)
    #     # cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_0.png', image0)
    #
    #     for w1 in self.graph_wall.nodes():
    #         adjacent = w1.edges()
    #         for a in adjacent:
    #             n = a.get_node()
    #             #if a.get_type() == 'TANGENT'and n.type!='wall':
    #             if n.type!='wall' and n.type!='door':
    #                self.find_adjacent_loop(n,[w1])
    #
    #     # stampo immagine  con i rettangoli
    #     image_rect=np.zeros((self.height,self.width,3),dtype=np.uint8)
    #     for rec in self.list_rect:
    #         r=random.randint(0, 255)
    #         g=random.randint(0, 255)
    #         b=random.randint(0, 255)
    #         image_rect[rec.y_min-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)
    #     if debug_mode:
    #         cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_1.png', image_rect)
    #
    #     # primo step stampa immagine in bianco e nero con i rettangoli delle presnte finestre in giallo
    #     image2=np.zeros((self.height,self.width,3),dtype=np.uint8)
    #     for rec in self.list_rect:
    #         if rec.type == 'window':
    #             image2[rec.y_min-1:rec.y4,rec.x1-1:rec.x2]=(0,255,255)
    #         elif rec.type =='line-structure':
    #             image2[rec.y_min-1:rec.y4,rec.x1-1:rec.x2]=(255,0,255)
    #         else:
    #             image2[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(255,255,255)
    #     if debug_mode:
    #         cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_2.png', image2)
    #
    #     # secondo step, stampa le immagine delle finestre(presunte) trovate tra due muri, con porte e muri
    #     # for w in self.list_windows:
    #     #     self.graph_image_output.add_node(w)
    #
    #     image3=np.zeros((self.height,self.width,3),dtype=np.uint8)
    #     for rec in self.graph_image_output.nodes():
    #         if rec.type == 'door':
    #             image3[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,0)
    #
    #         elif rec.type == 'wall':
    #             p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
    #             cv2.drawContours(image3, [p], -1, (0,0,255), -1)
    #     for rec in self.list_windows:
    #         if rec.type == 'window':
    #             image3[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,255)
    #     if debug_mode:
    #         cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_3.png', image3)
    #
    #
    #     # terzo step, vado a trovare le componenti connesse del background per determinare se e una finestra
    #     imageBackground=np.zeros((self.height,self.width),dtype=np.uint8)
    #     list_app = self.list_windows[:]
    #     list_app.extend(self.graph_image_output.nodes())
    #     for rec in list_app:
    #         p = np.array([[[rec.x1-1, rec.y1-1],[rec.x2-1, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4-1]]], dtype=np.int32)
    #         cv2.drawContours(imageBackground, [p], -1, 255, -1)
    #             #imageBackground[rec.y_min-1:rec.y_max,rec.x_min-1:rec.x_max]=255
    #
    #     s1 = [[0, 1, 0], [1,1,1], [0,1,0]] # elemento strutturato
    #     s2 = [[1, 1, 1], [1,1,1], [1,1,1]]
    #     labels,numlabels =ndimage.label(imageBackground<127,s2)
    #     labels2 = (np.where(labels != 1, 0, labels)*255)
    #
    #     if debug_mode:
    #         cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_4.png', labels*10)
    #         cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_5.png', labels2)
    #
    #
    #
    #     labels2 = np.abs(labels2 - (np.ones((self.height,self.width))*255))
    #     list_windows_remove=[]
    #     for win in self.list_windows:
    #         check_l1 = 0
    #         check_l2 = 0
    #         check_l3 = 0
    #         check_l4 = 0
    #         if not('l1' in win.side_touch_wall[0]):
    #
    #             s = np.sum(labels2[win.y_min-2:win.y_min-1,win.x_min+3:win.x_max-3])
    #             if s!=0:
    #                check_l1 = 1
    #
    #         if not('l2' in win.side_touch_wall[0]):
    #
    #             s = np.sum(labels2[win.y_min+3:win.y_max-3,win.x_max+1:win.x_max+2 ])
    #             if s!=0:
    #                check_l2 = 1
    #
    #         if not('l3' in win.side_touch_wall[0]):
    #
    #             s = np.sum(labels2[win.y_max+1:win.y_max+2,win.x_min+3:win.x_max-3 ])
    #             if s!=0:
    #                 check_l3 = 1
    #
    #         if not('l4' in win.side_touch_wall[0]):
    #
    #             s = np.sum(labels2[win.y_min+3:win.y_max-3,win.x_min-2:win.x_min-1 ])
    #             if s!=0:
    #                 check_l4 = 1
    #
    #         if check_l1 and check_l3:
    #             list_windows_remove.append(win)
    #         if check_l2 and check_l4:
    #             list_windows_remove.append(win)
    #
    #     # list_dww = []
    #     # for rec in list_app():
    #     #     if not(rec in list_windows_remove):
    #     #         list_dww.append(rec)
    #     while len(list_windows_remove)>0:
    #         rem = list_windows_remove.pop()
    #         self.list_windows.remove(rem)
    #
    #
    #     # for rec in self.list_windows:
    #     #     if (rec in list_windows_remove):
    #     #         self.list_windows.remove(rec)
    #
    #     image4 = np.zeros((self.height,self.width,3),dtype=np.uint8)
    #     for rec in self.list_windows:
    #         image4[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,255)
    #
    #     for rec in self.graph_image_output.nodes():
    #         if rec.type == 'door':
    #             image4[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,0)
    #         elif rec.type == 'wall':
    #             p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
    #             cv2.drawContours(image4, [p], -1, (0,0,255), -1)
    #     if debug_mode:
    #         cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_6.png', image4)
    #
    #
    #
    #
    #     # rimuovo le finestre che si sovrappongono ai muri
    #     list_windows_remove=[]
    #     for rec in self.list_windows:
    #         check_window=False
    #         i1 = np.zeros((self.height,self.width),dtype=np.uint8)
    #         i1[rec.y_min:rec.y_max+1,rec.x_min:rec.x_max+1]=1
    #         s = np.sum(i1)
    #         for rec2 in self.graph_image_output.nodes():
    #             if rec2.type=='wall' or rec2.type=='door':
    #                 i2 = np.zeros((self.height,self.width),dtype=np.uint8)
    #                 i2[rec2.y_min:rec2.y_max+1,rec2.x_min:rec2.x_max+1]=1
    #                 # print str(rec2.y_min)+" "+str(rec2.y_max)+" "+str(rec2.x_min)+" "+str(rec2.x_max)
    #                 logic_and = i1*i2
    #                 ss = np.sum(logic_and)
    #             if float(s/3) < ss:
    #                 list_windows_remove.append(rec)
    #                 break
    #     while len(list_windows_remove)>0:
    #         rem = list_windows_remove.pop()
    #         self.list_windows.remove(rem)
    #
    #     image6 = np.zeros((self.height,self.width,3),dtype=np.uint8)
    #     for rec in self.graph_image_output.nodes():
    #             if rec.type == 'door':
    #                 image6[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,0)
    #             elif rec.type == 'wall':
    #                 p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
    #                 cv2.drawContours(image6, [p], -1, (0,0,255), -1)
    #     for rec in self.list_windows:
    #         image6[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,255)
    #     if debug_mode:
    #         cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_7.png', image6)
    #
    #     # rimuovo le finestre che hanno uno spessore superiore allo spessore del muro
    #     l_remove=[]
    #     for rec in self.list_windows:
    #         l_win =  min(np.abs(rec.x_max-rec.x_min),np.abs(rec.y_max-rec.y_min))
    #         if l_win>(self.max_tink_wall*1.5) or l_win < (self.max_tink_wall*0.2):
    #
    #             l_remove.append(rec)
    #         else:
    #
    #             self.graph_image_output.add_node(rec)
    #     while len(l_remove)>0:
    #         rem = l_remove.pop()
    #         self.list_windows.remove(rem)
    #
    #     # list_dww.extend(self.list_windows)
    #     # list_dww.extend(self.graph_image_output.nodes())
    #
    #     image7 = np.zeros((self.height,self.width,3),dtype=np.uint8)
    #     for rec in self.graph_image_output.nodes():
    #             if rec.type == 'window':
    #                 image7[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,255)
    #             elif rec.type == 'door':
    #                 image7[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,0)
    #             elif rec.type == 'wall':
    #                 p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
    #                 cv2.drawContours(image7, [p], -1, (0,0,255), -1)
    #                 #image7[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
    #             else:
    #                 image7[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(255,255,0)
    #     if debug_mode:
    #         cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_8.png', image7)
    #     if len(self.list_scale)>0:
    #         for s in self.list_scale:
    #             cv2.drawContours(image7, [s], -1, (255,0,255), -1)
    #
    #     cv2.imwrite(self.path_output+self.file_name, image7)
    #
    #
    #     self.list_wwd.extend(self.graph_image_output.nodes())
    #     # for rec in self.graph_image_output.nodes():
    #     #
    #     #         image5[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=0
    #     #
    #     # if not(os.path.exists(self.path_debug+'Images_Anuradha/')):
    #     #         os.mkdir(self.path_debug+'Images_Anuradha/')
    #     # cv2.imwrite(self.path_debug+'Images_Anuradha/'+str(self.file_name[0:len(self.file_name)-4])+'.png', image5*255)
    #
    #     # self.print_graph_image(self.graph_image_output.nodes())


    # def print_file_log(self):
    #     if not(os.path.exists(self.path_debug+'output/')):
    #             os.mkdir(self.path_debug+'output/')
    #    # file = open(self.path_debug+'output/'+str(self.file_name[0:len(self.file_name)-4])+".log","w")
    #     file = open(self.path_output+str(self.file_name[0:len(self.file_name)-4])+".txt","w")
    #
    #     list_d=[]
    #     list_w=[]
    #     list_f=[]
    #
    #     for rec in self.graph_image_output.nodes():
    #         if rec.type == 'window':
    #             list_f.append(rec)
    #             rec.x1 = rec.x_min
    #             rec.x4 = rec.x_min
    #             rec.x2 = rec.x_max
    #             rec.x3 = rec.x_max
    #             rec.y1 = rec.y_min
    #             rec.y4 = rec.y_max
    #             rec.y2 = rec.y_min
    #             rec.y3 = rec.y_max
    #         elif rec.type == 'door':
    #             list_d.append(rec)
    #             rec.x1 = rec.x_min
    #             rec.x4 = rec.x_min
    #             rec.x2 = rec.x_max
    #             rec.x3 = rec.x_max
    #             rec.y1 = rec.y_min
    #             rec.y4 = rec.y_max
    #             rec.y2 = rec.y_min
    #             rec.y3 = rec.y_max
    #         elif rec.type == 'wall':
    #             list_w.append(rec)
    #     file.write(str(self.height)+";"+str(self.width)+";\n")
    #     file.write("WALLS\n")
    #     for i in list_w:
    #         string = str(i.x1)+";"+str(i.y1)+";"+str(i.x2)+";"+str(i.y2)+";"+str(i.x3)+";"+str(i.y3)+";"+str(i.x4)+";"+str(i.y4)+"\n"
    #         file.write(string)
    #     file.write("DOORS\n")
    #     for i in list_d:
    #         string = str(i.x1)+";"+str(i.y1)+";"+str(i.x2)+";"+str(i.y2)+";"+str(i.x3)+";"+str(i.y3)+";"+str(i.x4)+";"+str(i.y4)+"\n"
    #         file.write(string)
    #     file.write("WINDOWS\n")
    #     for i in list_f:
    #         string = str(i.x1)+";"+str(i.y1)+";"+str(i.x2)+";"+str(i.y2)+";"+str(i.x3)+";"+str(i.y3)+";"+str(i.x4)+";"+str(i.y4)+"\n"
    #         file.write(string)
    #     file.write("STRAIN\n")
    #     if len(self.list_scale)>0:
    #         for s in self.list_scale:
    #             #string = ''.join(s
    #
    #             for item in s:
    #                 file.write("%s\n" % item)
    #     # image = np.zeros((self.height,self.width),dtype=np.uint8)
    #     # file.write("STRAIN\n")
    #     # for cc in self.list_scale:
    #     #     # cc_rgb = cv2.cvtColor(cc.image.astype(np.uint8),cv2.COLOR_GRAY2RGB)/255
    #     #     image[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] = image[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+cc.image
    #     #
    #     # k=int(self.max_tink_wall/4)
    #     # kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(k,k))
    #     # #kernel_1=cv2.getStructuringElement(cv2.MORPH_RECT,(4,4),(0,2))
    #     # closing = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel)
    #     # i = np.array(closing,dtype=np.uint8)
    #     # cv2.imwrite(self.path_debug+'merge_all/'+self.file_name[0:len(self.file_name)-4]+'_3.png', closing)
    #     # contours, hierarchy = cv2.findContours(i,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    #     # cv2.drawContours(image,contours,-1,(0,255,0),3)
    #
    #     file.close()
    #
    # # def recostructed_skeleton_image(self,debug_mode):
    # #     for cc in self.list_cc:
    # #         cc.set_skeleton()
    # #     self.print_skeleton(debug_mode)
    # #     for cc in self.list_cc:
    # #         cc.merge_skeleton()
    # #
    # #     self.print_skeleton2()
    #
    # # def print_skeleton(self,debug_mode):
    # #     image=np.zeros((self.height,self.width,3),dtype=np.uint8)
    # #     for cc in self.list_cc:
    # #         for r in cc.list_rect:
    # #
    # #             if r.skeleton[0]==r.skeleton[2]:# verticale
    # #                 h_r=r.skeleton[3]-r.skeleton[1]
    # #
    # #                 if h_r==self.height-1 or r.skeleton[3]+1>self.height :
    # #                     h_r=h_r-1
    # #                 if h_r<0:
    # #                     h_r=0
    # #                 vec=np.ones((h_r+1,1,3),dtype=np.uint8)*255
    # #                 image[r.skeleton[1]:r.skeleton[3]+1,r.skeleton[0]:r.skeleton[2]+1]=vec
    # #
    # #                 #image[cc.y_min+r.skeleton[1]:cc.y_min+r.skeleton[3]+1,cc.x_min+r.skeleton[0]:cc.x_min+r.skeleton[2]+1]=r.image_skeleton
    # #
    # #             else:
    # #                 w_r=r.skeleton[2]-r.skeleton[0]
    # #                 if w_r==self.width-1 or r.skeleton[2]+1>self.width:
    # #                     w_r=w_r-1
    # #                 if w_r<0:
    # #                     w_r=0
    # #                 vec=np.ones((1,w_r+1,3),dtype=np.uint8)*255
    # #                 # print image[r.skeleton[1]:r.skeleton[3]+1,r.skeleton[0]:r.skeleton[2]+1].shape
    # #                 # print vec.shape
    # #                 image[r.skeleton[1]:r.skeleton[3]+1,r.skeleton[0]:r.skeleton[2]+1]=vec
    # #
    # #
    # #     s=self.file_name[0:-4]
    # #     if debug_mode:
    # #         cv2.imwrite(self.path_debug+'windows/'+s+'first_skeleton.png', image)

#     def print_skeleton2(self):
#         image=np.zeros((self.height,self.width,3),dtype=np.uint8)
#         for cc in self.list_cc:
#             for r in cc.list_rect:
#
#                 if r.skeleton[0]==r.skeleton[2]:# verticale
#                     h_r=r.skeleton[3]-r.skeleton[1]
#
#                     if h_r==self.height-1 or r.skeleton[3]+1>self.height :
#                         h_r=h_r-1
#                     if h_r<0:
#                         h_r=0
#                     vec=np.ones((h_r+1,1,3),dtype=np.uint8)*255
#                     image[r.skeleton[1]:r.skeleton[3]+1,r.skeleton[0]:r.skeleton[2]+1]=vec
#
#                     #image[cc.y_min+r.skeleton[1]:cc.y_min+r.skeleton[3]+1,cc.x_min+r.skeleton[0]:cc.x_min+r.skeleton[2]+1]=r.image_skeleton
#
#                 else:
#                     w_r=r.skeleton[2]-r.skeleton[0]
#                     if w_r==self.width-1 or r.skeleton[2]+1>self.width:
#                         w_r=w_r-1
#                     if w_r<0:
#                         w_r=0
#                     vec=np.ones((1,w_r+1,3),dtype=np.uint8)*255
#                     # print image[r.skeleton[1]:r.skeleton[3]+1,r.skeleton[0]:r.skeleton[2]+1].shape
#                     # print vec.shape
#                     image[r.skeleton[1]:r.skeleton[3]+1,r.skeleton[0]:r.skeleton[2]+1]=vec
#                 for a in r.list_adjacent_rect:
#                     coor=a[4]
#                     if a[0]=='ADIACENT':
#                         cv2.circle(image,(coor[0],coor[1]), 2, (0,0,255), -1)#red
#
#                     elif a[0]=='TANGENT':
#                         cv2.circle(image,(coor[0],coor[1]), 2, (0,255,0), -1)#green
#
#                     else:
#                         cv2.circle(image,(coor[0],coor[1]), 2, (255,0,0), -1)#blu
#
#
#         s=self.file_name[0:-4]
#         if debug_mode:
#             cv2.imwrite(self.path_debug+'windows/'+s+'_second_skeleton.png', image)
#
# # qui ci sono i metodi per le immagini con muri bianchi


#----------------------------------------------------------redeclarations--------------------------------------------------------------------------------------
    # def find_connected_componet_white(self):
    #     # here convert in gray-scale becouse the function  ndimage.label
    #     img=np.array(self.original_image)
    #     self.height,self.width=img.shape
    #     s1 = [[0, 1, 0], [1,1,1], [0,1,0]] # elemento strutturato
    #     s2 = [[1, 1, 1], [1,1,1], [1,1,1]]
    #     # plt.imshow(self.labels_background*10, cmap = 'gray', interpolation = 'bicubic')
    #     # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
    #     # plt.show()
    #     self.labels, self.numlabels = ndimage.label(img>127,s2)
    #     # print 'number of connected components: '+str(self.numlabels)
    #     s=self.file_name[0:-4]
    #
    #     for i in range(1,self.numlabels+1):
    #         cc=CC(i)
    #         point= np.argwhere(self.labels == i)
    #         # find x_max, x_min, y_max, y_min
    #         cc.y_min=point[:,0].min()
    #         cc.x_min=point[:,1].min()
    #         cc.y_max=point[:,0].max()
    #         cc.x_max=point[:,1].max()
    #         cc.height= cc.y_max-cc.y_min+1
    #         cc.width= cc.x_max-cc.x_min+1
    #         # I create image of the connected compis an open image with black background and white lines, i want as output an image with white background and black linesonent
    #         cc.image=(np.where(self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
    #         cc.numPixel=(cc.image.sum()/255)
    #         # add padding to cc.image
    #         h,w=cc.image.shape
    #         # z1=np.zeros((1,w))
    #         # z2=np.zeros((h+2,1))
    #         # cc.image=np.concatenate((z1,cc.image),axis=0)
    #         # cc.image=np.concatenate((cc.image,z1),axis=0)
    #         # cc.image=np.concatenate((z2,cc.image),axis=1)
    #         # cc.image=np.concatenate((cc.image,z2),axis=1)
    #         # cc.image_dist_tran=ndimage.morphology.distance_transform_cdt(cc.image)
    #         # if not(os.path.exists(self.path_debug+'preprocessing/'+s+'/')):
    #         #     os.mkdir(self.path_debug+'preprocessing/'+s+'/')
    #         # cv2.imwrite(self.path_debug+'preprocessing/'+s+'/'+str(i)+'.png',cc.image)
    #         #I add the connected component to the list
    #         self.list_cc.append(cc)
    #
    # def find_run_pixel_white(self):
    #
    #     image = np.array(self.original_image)
    #     ret,image = cv2.threshold(image,127,255,cv2.THRESH_BINARY)
    #     run_hist=[]
    #     i=0
    #
    #     m=0
    #
    #     #find run pixel
    #
    #     i=i+1
    #     #cv2.imwrite(path+self.file_name+'_CC_num_'+str(i)+'.png', cc.image)
    #     run=[]
    #     a=[]
    #     h,w = image.shape
    #
    #     for i in range(0,h):
    #         row = image[i,:]
    #         bbb = [(len(list(group)),name) for name, group in groupby(row)]
    #         a.extend(bbb)
    #     run.extend([item for item in a if item[1] == 255])
    #
    #     b=[]
    #     for i in range(0,w):
    #         col=image[:,i].transpose()
    #         b.extend([(len(list(group)),name) for name, group in groupby(col)])
    #
    #     run.extend([item for item in b if item[1] == 255])
    #
    #
    #     run_np = np.asarray(run)
    #     run_hist.extend(run_np[:,0])
    #
    #
    #     mini=min(h,w)
    #     list_min=[]
    #
    #
    #
    #     maxi = np.max(run_hist)
    #     hist, bins =np.histogram(run_hist,bins= range(1,int(max(run_hist))+2), range=(int(min(run_hist)), int(max(run_hist))))
    #     for i in range(0,len(hist)):
    #         if hist[i]>mini:
    #             list_min.append(i+1)
    #
    #     # width =  (bins[1] - bins[0])
    #     # center = (bins[:-1] + bins[1:]) / 2
    #     # barlist=plt.bar(range(1,101), hist[0:100], align='center', width=width)
    #     # plt.show()
    #     return list_min
    #
    # # una volta che ho trovato i run di pixel ed ho trovato le componente connesse
    # #       - per ogni componente connessa ricalocolo i run di pixel
    # #       - se la maggioranza dei run e uguale a un picco allora coloro di nero la componente nell'immagine originale
    # #
    # def color_walls_white(self,list_max):
    #
    #     self.image_white =np.zeros((self.height,self.width),dtype=np.int32)
    #
    #     for cc in self.list_cc:
    #
    #         #ret,image = cv2.threshold(image,127,255,cv2.THRESH_BINARY)
    #         run_hist=[]
    #         #cv2.imwrite(path+self.file_name+'_CC_num_'+str(i)+'.png', cc.image)
    #         run=[]
    #         a=[]
    #         h,w = cc.image.shape
    #         for i in range(0,h):
    #             row = cc.image[i,:]
    #             a.extend([(len(list(group)),name) for name, group in groupby(row)])
    #         run.extend([item for item in a if item[1] == 255])
    #
    #         b=[]
    #         for i in range(0,w):
    #             col=cc.image[:,i].transpose()
    #             b.extend([(len(list(group)),name) for name, group in groupby(col)])
    #
    #         run.extend([item for item in b if item[1] == 255])
    #         run_np = np.asarray(run)
    #         run_hist.extend(run_np[:,0])
    #         maxi = np.max(run_hist)
    #         hist, bins =np.histogram(run_hist,bins= range(1,int(max(run_hist))+2), range=(int(min(run_hist)), int(max(run_hist))))
    #         list_app = list_max[len(list_max)-2:len(list_max)]
    #         for i in range(0,len(hist)):
    #             if hist[i]>0:
    #                 if float(hist[i])/float(sum(hist))>0.5:
    #                     for m in list_app:
    #                         if (i+1)>(m*0.9) and (i+1)<(m*1.1):
    #                             self.image_white[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] = self.image_white[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] + cc.image
    #
    #     cv2.imwrite(self.path_debug+'preprocessing/'+str(self.file_name[0:len(self.file_name)-4])+'_colorBlack_1.png',self.image_white)
    #
    #     i2 = np.array(self.original_image,dtype=np.int32)
    #
    #     cv2.imwrite(self.path_debug+'preprocessing/'+str(self.file_name[0:len(self.file_name)-4])+'_colorBlack_2.png',i2)
    #
    #     i3 = np.abs(i2-255)+self.image_white
    #
    #     cv2.imwrite(self.path_debug+'preprocessing/'+str(self.file_name[0:len(self.file_name)-4])+'_colorBlack_3.png',np.abs(i3-255))
    #
    #     return np.abs(i3-255)
    #
    #


    # def recostructed_image_whit_rectangle(self,debug_mode):
    #
    #     image=np.zeros((self.height,self.width,3),dtype=np.uint8)
    #     #image2=np.zeros((self.height,self.width),dtype=np.uint8)
    #     #self.color_image[:]
    #     for cc in self.list_cc:
    #         for rec in cc.list_rect:
    #
    #             r=random.randint(0, 255)
    #             g=random.randint(0, 255)
    #             b=random.randint(0, 255)
    #
    #             image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)
    #
    #         #image2[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=cc.image[1:cc.y_max-cc.y_min+2,1:cc.x_max-cc.x_min+2]
    #
    #     self.image_rect = image
    #     if debug_mode:
    #         cv2.imwrite(self.path_debug+'wall/'+str(self.file_name[0:len(self.file_name)-4])+'_ricostruzione.png', image)
    #         cv2.imwrite(self.path_debug+'door/'+str(self.file_name), image)

    def find_adjacent_loop2(self,rect,pre_rect):
        pre_rect.append(rect)
        adjacent = rect.edges()
        list_return = []
        for a in adjacent:
            list_new = []
            n = a.get_node()
            if n.type != 'wall':

                if not( n in pre_rect ):
                    #if (a.get_type()=='ADIACENT' or a.get_type()=='ALIGNED')                   :
                    list_new.extend(self.find_adjacent_loop2(n,pre_rect))
                    if len( list_new)>0:
                        for e2 in list_new:
                            e2.append(n)
            else:
                if not( n in pre_rect ):
                    list_new.append([n])
            for l in list_new:
                list_return.append(l)

        return list_return
