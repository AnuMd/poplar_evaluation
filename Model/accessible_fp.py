__author__ = 'anu'

import cv2,shutil,os, re, random, math, numpy as np,PIL.ImageOps
import xml.etree.ElementTree as ET
from xml.dom import minidom
from natsort import natsorted
from scipy import misc, ndimage
from operator import itemgetter
from PIL import Image
from resizeimage import resizeimage

from open_plan import planClass
from calculations import calculations_class
from iterative_functions import iterative_functions_class


class accessible_fp_generator:

    def __init__(self,current_directory,language):
        self.iterative_obj = iterative_functions_class()
        self.current_directory = current_directory
        self.language = language

    def get_fp_outer_contour(self,current_directory, path_output,file_name,avg_door_width):
        # path_output = current_directory+'/input_fps/object_test/gravitas_test/'


        # #--iterative running test paths
        path_output = current_directory+'/input_fps/new_walls/walls_2/'
        only_image_name = file_name[0:-4]
        outer_contour_image_path = path_output + only_image_name+'_output.png'
        # print 'outer_contour_image_path', outer_contour_image_path
        outer_cont_image = cv2.imread(outer_contour_image_path, cv2.IMREAD_GRAYSCALE)
        # --end test

        # --comment test to work
        # only_image_name = file_name[0:-4]
        # outer_contour_image_path = path_output + only_image_name + '/'
        # # --extract outer contour and get improved contour lines
        # outer_cont_image = cv2.imread(outer_contour_image_path + 'new_walls_windows_door.png', cv2.IMREAD_GRAYSCALE)
        # --end comment test to work

        # ---get outer contour to be used : for finding entrance door
        ret, thresh = cv2.threshold(outer_cont_image, 0, 255, 1)
        contours, hierachy = cv2.findContours(thresh, 1, 2)
        # ---"outer_contour_wth_point_data" has contour points
        # ---"outer_contour_lines" has contour lines
        outer_contour_wth_point_data = 0
        img_height, img_width = outer_cont_image.shape
        img_size = img_height * img_width
        for cont, contour in enumerate(contours):
            contour_size = cv2.contourArea(contour)
            contour_ratio = contour_size / img_size
            if contour_ratio > 0.2 and contour_ratio < 1.0:
                # if cont==len(contours)-2:
                outer_contour_wth_point_data = contour
                break
        simplified_outer_contour, simplified_outer_contour_lines = self.iterative_obj.simplify_contour(outer_contour_wth_point_data,avg_door_width)

        return simplified_outer_contour, simplified_outer_contour_lines


    def modify_cordinates(self,current_directory,path_gravvitas,file_name,outer_contour, outer_contour_lines,img_height,img_width):
        #--test path
        path_gravvitas = current_directory+'/input_fps/test_svg/text/'
        test_image_path = current_directory+'/input_fps/test_svg/image_results/'

        only_image_name = file_name[0:-4]

        rooms_point_list = self.read_room_detail_file(path_gravvitas,only_image_name)
        object_point_list = self.read_object_detail_file(path_gravvitas, only_image_name)
        stairs_point_list = self.read_stairs_detail_file(path_gravvitas, only_image_name)
        windows_point_list = self.read_windows_detail_file(path_gravvitas, only_image_name)
        doors_rect_list = self.read_doors_detail_file(path_gravvitas, only_image_name)
        # wall_point_list = self.read_walls_detail_file(path_gravvitas, only_image_name)


        #-- get bounding box of the outer_contour
        x, y, bb_w, bb_h = cv2.boundingRect(outer_contour)

        #--fix starting cords(0,0) of new image
        new_start_x, new_start_y = 0, 0

        #--calculate shift_x and shift_y(with a safe margin/border of 50)
        shift_x, shift_y = x-50, y-50

        #--calculate new max img_height and max_width
        new_width, new_height = bb_w + 100, bb_h + 100

        modified_rooms_point_list = self.get_new_points_for_text_and_point_list(rooms_point_list,shift_x, shift_y)
        modified_object_point_list = self.get_new_points_for_text_and_point_list(object_point_list, shift_x, shift_y)
        modified_stairs_point_list = self.get_new_points_for_only_point_list(stairs_point_list, shift_x, shift_y)
        modified_windows_point_list = self.get_new_points_for_only_point_list(windows_point_list, shift_x, shift_y)
        modified_doors_rect_list = self.get_new_points_for_only_point_list(doors_rect_list, shift_x, shift_y)
        # modified_wall_point_list = self.get_new_points_for_only_point_list(wall_point_list, shift_x, shift_y)

        #--modify outer conotur data too
        modified_outer_contour = self.get_new_contour(outer_contour,shift_x, shift_y)
        modified_outer_contour_lines = self.get_new_conotur_lines(outer_contour_lines,shift_x, shift_y)

        #--test results image-delete later
        test_image = ~(np.zeros((new_height,new_width, 3), np.uint8))
        # cv2.drawContours(test_image, [outer_contour], -1, (0, 0, 0), 2)
        # cv2.rectangle(test_image, (x, y), (x + bb_w, y + bb_h), (0, 0, 255), 2)
        color = [[102, 0, 0], [102, 102, 0], [255, 204, 255], [255, 204, 153], [255, 0, 127], [0, 150, 0],
                 [150, 0, 255], [128, 0, 255], [0, 128, 255]]
        for each_room in modified_rooms_point_list:
            room_contour = each_room [1]
            room_contour = self.iterative_obj.convert_points_to_contour(room_contour)
            color_val = random.choice(color)
            cv2.drawContours(test_image, [room_contour], -1, (tuple(color_val)), -1)
        for each_row in modified_object_point_list:
            points = each_row [1]
            contour = self.iterative_obj.convert_points_to_contour(points)
            cv2.drawContours(test_image, [contour], -1, (255,0,255), -1)
        for points in modified_stairs_point_list:
            contour = self.iterative_obj.convert_points_to_contour(points)
            cv2.drawContours(test_image, [contour], -1, (255,0,0), -1)
        for points in modified_windows_point_list:
            contour = self.iterative_obj.convert_points_to_contour(points)
            cv2.drawContours(test_image, [contour], -1, (0,255,0), -1)
        for points in modified_doors_rect_list:
            x1, y1 = points[0]
            x2, y2 = points[1]
            cv2.rectangle(test_image, (x1, y1), (x2, y2), (0, 0, 255), -1)
        cv2.imwrite(test_image_path+'test_1.png',test_image)
        #--end results


        all_floorplan_components = [modified_rooms_point_list,modified_object_point_list,
                                    modified_stairs_point_list,modified_windows_point_list,
                                    modified_doors_rect_list]
        fp_new_dimensions = [new_height,new_width]
        outer_contour_data = [modified_outer_contour,modified_outer_contour_lines]

        return all_floorplan_components,fp_new_dimensions,outer_contour_data

    def read_room_detail_file(self,path_gravvitas,only_image_name):
        rooms_point_list = []
        room_details_file = open(path_gravvitas + only_image_name + '_1_room_details.txt', 'r')
        for row_num, text_line in enumerate(room_details_file):
            line_seperate = text_line.split(':')
            if len(line_seperate) > 3:
                contour_details = re.findall(r"[\w']+", line_seperate[5])
                int_contour_details = [int(x) for x in contour_details]
                room_contour = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                room_text = line_seperate[2].strip()
                rooms_point_list.append([room_text, room_contour])
        return rooms_point_list

    def read_object_detail_file(self,path_gravvitas,only_image_name):
        object_point_list = []
        object_details_file = open(path_gravvitas + only_image_name + '_2_object_details.txt', 'r')
        for text_line in object_details_file:
            line_seperate = text_line.split(':')
            if len(line_seperate) > 1:
                object_contour = re.findall(r"[\w']+", line_seperate[1])
                int_object_details = [int(x) for x in object_contour]
                object_chunks = [int_object_details[x:x + 2] for x in xrange(0, len(int_object_details), 2)]
                # ---temp fix delete later
                x1, y1 = object_chunks[0]
                x2, y2 = object_chunks[1]
                object_contour_new = [[x1, y1], [x2, y1], [x2, y2], [x1, y2]]

                # --get object_name
                object_name = line_seperate[0].split()[0]
                # print object_name
                if len(object_point_list) == 0:
                    object_point_list.append([])
                    object_point_list[0].append(object_name)
                    object_point_list[0].append(object_contour_new)
                else:
                    row_ID = -1
                    for obj_row, each_object in enumerate(object_point_list):
                        if each_object[0] == object_name:
                            row_ID = obj_row
                            break
                    if row_ID != -1:
                        object_point_list[row_ID].append(object_contour_new)
                    else:
                        object_point_list.append([])
                        object_point_list[len(object_point_list) - 1].append(object_name)
                        object_point_list[len(object_point_list) - 1].append(object_contour_new)
        return object_point_list

    def read_stairs_detail_file(self,path_gravvitas,only_image_name):
        stairs_point_list = []
        stair_details_file = open(path_gravvitas + only_image_name + '_3_stairs_details.txt', 'r')
        for stair_num, text_line in enumerate(stair_details_file):
            line_seperate = text_line.split(':')
            if len(line_seperate) == 2:
                contour_details = re.findall(r"[\w']+", line_seperate[1])
                int_contour_details = [int(x) for x in contour_details]
                stair_points = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                stairs_point_list.append(stair_points)
        return stairs_point_list

    def read_windows_detail_file(self,path_gravvitas,only_image_name):
        windows_point_list = []
        window_details_file = open(path_gravvitas + only_image_name + '_4_window_details.txt', 'r')
        for win_num, text_line in enumerate(window_details_file):
            contour_seperate = text_line.split(':')
            if len(contour_seperate) == 2:
                contour_details = re.findall(r"[\w']+", contour_seperate[1])
                int_contour_details = [int(x) for x in contour_details]
                window_data = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                windows_point_list.append(window_data)
        return windows_point_list

    def read_doors_detail_file(self,path_gravvitas,only_image_name):
        doors_rect_list = []
        door_details_file = open(path_gravvitas + only_image_name + '_5_door_details.txt', 'r')
        for door_num, text_line in enumerate(door_details_file):
            line_seperate = text_line.split(':')
            if len(line_seperate) == 2:
                contour_details = re.findall(r"[\w']+", line_seperate[1])
                int_contour_details = [int(x) for x in contour_details]
                door_rectangle = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                doors_rect_list.append(door_rectangle)
        return doors_rect_list

    # def read_walls_detail_file(self,path_gravvitas,only_image_name):
    #     wall_point_list = []
    #     wall_details_file = open(path_gravvitas + only_image_name + '_7_wall_details.txt', 'r')
    #     for wall_num, text_line in enumerate(wall_details_file):
    #         line_seperate = text_line.split(':')
    #         if len(line_seperate) == 2:
    #             contour_details = re.findall(r"[\w']+", line_seperate[1])
    #             int_contour_details = [int(x) for x in contour_details]
    #             wall_contour = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
    #             wall_point_list.append(wall_contour)
    #     return wall_point_list


    def get_new_points_for_text_and_point_list(self,point_list,shift_x, shift_y):
        new_point_list = []
        for row_num, row in enumerate(point_list):
            text = row[0]
            room_contour_points = row[1]
            new_point_list.append([])
            new_point_list[row_num].append(text)
            new_individual_points = []
            for room_point in room_contour_points:
                x, y = room_point
                new_individual_points.append([x-shift_x,y-shift_y])
            new_point_list[row_num].append(new_individual_points)
        return new_point_list

    def get_new_points_for_only_point_list(self,point_list,shift_x, shift_y):
        new_point_list = []
        for row_num, row in enumerate(point_list):
            new_row = []
            for each_cord in row:
                x, y = each_cord
                new_row.append([x-shift_x,y-shift_y])
            new_point_list.append(new_row)
        return new_point_list

    def get_new_contour(self,outer_contour,shift_x, shift_y):
        cont_point_list = []
        for con_point_l1 in outer_contour:
            for con_point_l2 in con_point_l1:
                cont_x, cont_y = con_point_l2
                cont_point_list.append([cont_x-shift_x, cont_y-shift_y])
        new_contour = self.iterative_obj.convert_points_to_contour(cont_point_list)
        return new_contour

    def get_new_conotur_lines(self,outer_contour_lines,shift_x, shift_y):
        new_outer_contour_lines = []
        for each_contour_line in outer_contour_lines:
            x1,y1 = each_contour_line[0]
            x2, y2 = each_contour_line[1]
            new_outer_contour_lines.append([[x1-shift_x,y1-shift_y],[x2-shift_x, y2-shift_y]])
        return new_outer_contour_lines

    #-----------other methods
    def get_room_order(self,current_directory,file_name,path_gravvitas,outer_contour,
                       outer_contour_lines,img_height,img_width,all_floorplan_components):
        path_gravvitas = current_directory+'/input_fps/test_svg/text/'
        # results_path = current_directory+'/input_fps/object_test/gravitas_test/results/'

        only_image_name = file_name[0:-4]
        # --extract floorplan_data
        rooms_point_list = all_floorplan_components[0]
        doors_rect_list = all_floorplan_components[4]

        #---find_shortest_distance_from_contour_to_rooms
        shortest_distance_to_room, internal_rooms = self.find_shortest_distance_from_contour_to_rooms(outer_contour_lines,rooms_point_list)


        #--finding all entrance doors (doors closest to contour)
        door_details = self.find_all_entrance_doors(outer_contour,doors_rect_list)

        #---find contour lines closest to all doors
        door_contour_data = self.find_contour_lines_for_doors(door_details,outer_contour_lines)

        room_contour_data = self.find_contour_line_closest_to_each_room(shortest_distance_to_room)

        possible_rooms_for_each_door = self.find_possible_rooms_for_each_door(door_contour_data,room_contour_data)

        door_room_distance = self.find_room_door_distances(possible_rooms_for_each_door,self.iterative_obj)

        entrance_door = self.find_main_entrance_door(door_room_distance,current_directory)

        door_positions = self.get_door_positions(door_details,outer_contour,img_height,img_width)

        door_text = self.generate_position_text(entrance_door,door_positions)

        external_room_names_in_order = self.get_external_room_order(entrance_door,outer_contour_lines,shortest_distance_to_room)

        fp_text = self.create_text(only_image_name, external_room_names_in_order,internal_rooms,door_text)

        return fp_text



    def find_shortest_distance_from_contour_to_rooms(self,outer_contour_lines,rooms_point_list):
        # --find lines for all rooms and store in "room_lines_list[room_name,room_contour_lines_list]"
        room_lines_list = []
        for room_row in rooms_point_list:
            room_name = room_row[0]
            room_points = room_row[1]
            room_in_lines = []
            first_element = 0
            for l, cordinate in enumerate(room_points):
                x, y = cordinate
                if l == 0:
                    room_in_lines.append([])
                    room_in_lines[l].append([x, y])
                    first_element = [x, y]
                elif l == len(room_points) - 1:
                    room_in_lines.append([])
                    room_in_lines[l].append([x, y])
                    room_in_lines[l - 1].append([x, y])
                    room_in_lines[l].append(first_element)
                else:
                    room_in_lines.append([])
                    room_in_lines[l].append([x, y])
                    room_in_lines[l - 1].append([x, y])
            room_lines_list.append([room_name, room_in_lines])

        # --per room find the room line that has shortest distance to a contour line and
        # -- store it in "shortest_distance_to_room[room_name,room_line_closest_to_contour]"
        shortest_distance_to_room = []
        internal_rooms = []
        for room_row, each_room in enumerate(room_lines_list):
            room_name = each_room[0]
            room_lines = each_room[1]
            room_lines_distances = []
            for room_line in room_lines:
                room_x, room_y = self.iterative_obj.find_centre_of_line(room_line)
                min_distance_to_contour = 3000
                min_contour_line, min_room_line = 0, 0
                cont_intersect_x, cont_intersect_y, min_room_x, min_room_y = 0, 0, 0, 0
                for cont_index, cont_line in enumerate(outer_contour_lines):
                    distance_from_text, intersect_x, intersect_y = self.iterative_obj.calculate_distance_from_point_to_line(
                        [room_x, room_y], cont_line)
                    if distance_from_text != -1 and distance_from_text < min_distance_to_contour:
                        min_distance_to_contour = distance_from_text
                        min_contour_line = [cont_line, cont_index]
                        cont_intersect_x, cont_intersect_y = int(intersect_x), int(intersect_y)
                        min_room_line = room_line
                        min_room_x, min_room_y = room_x, room_y
                room_lines_distances.append(
                    [min_room_line, min_contour_line[0], min_contour_line[1],
                     min_distance_to_contour, min_room_x,
                     min_room_y, cont_intersect_x, cont_intersect_y])
            room_lines_distances.sort(key=itemgetter(3))
            if room_lines_distances[0][3] < 100:
                shortest_distance_to_room.append([room_name, room_lines_distances[0], room_lines])
            else:
                internal_rooms.append(room_name)

        return shortest_distance_to_room,internal_rooms



    def find_all_entrance_doors(self, outer_contour_wth_point_data,doors_rect_list):
        door_details = []
        for door_row in doors_rect_list:
            center_x, center_y = self.iterative_obj.find_centre_of_line(door_row)
            # find distance from centre_point to contour
            distance_from_contour = cv2.pointPolygonTest(outer_contour_wth_point_data, (center_x, center_y), True)
            if distance_from_contour < 25:
                door_details.append([[door_row[0], door_row[1]], [center_x, center_y],
                                     distance_from_contour])
        door_details.sort(key=itemgetter(2))
        return door_details

    def find_contour_lines_for_doors(self,door_details,outer_contour_lines):
        for row_num, door_row in enumerate(door_details):
            door_centre_x, door_centre_y = door_row[1]
            min_distance = 1000
            #--find cont_index for each door_row
            cont_index = -1
            for c, cont_line in enumerate(outer_contour_lines):
                distance_from_text, intersect_x, intersect_y = self.iterative_obj.calculate_distance_from_point_to_line(
                    [door_centre_x, door_centre_y], cont_line)
                if distance_from_text != -1 and distance_from_text < min_distance:
                    min_distance = distance_from_text
                    cont_index = c
            if cont_index != -1:
                door_details[row_num].append(cont_index)
            else:
                print 'Error: No Cont Line for ',door_row

        return door_details

    def find_contour_line_closest_to_each_room(self,shortest_distance_to_room):
        room_contour_data = []
        for room_row in shortest_distance_to_room:
            room_name = room_row[0]
            room_contour_index = room_row[1][2]
            room_lines = room_row[2]
            room_contour_data.append([room_name,room_lines,room_contour_index])
        return room_contour_data

    def find_possible_rooms_for_each_door(self,door_contour_data,room_contour_data):
        possible_rooms_for_each_door = []
        for door_index, door_row in enumerate(door_contour_data):
            contour_line_index = door_row[3]
            possible_rooms_for_each_door.append([door_row])
            for room_row in room_contour_data:
                if room_row[2] == contour_line_index:
                    # room_data = [room_row[0]]
                    room_data = [room_row[0], contour_line_index, room_row[1]]
                    possible_rooms_for_each_door[door_index].append(room_data)
        return possible_rooms_for_each_door

    def find_room_door_distances(self,possible_rooms_for_each_door,iterative_object):
        door_room_distance = []
        for door_num, possible_row in enumerate(possible_rooms_for_each_door):
            door_room_distance.append(possible_row[0])
            door_centre = possible_row[0][1]
            if len(possible_row) > 1:
                room_data = possible_row[1:]
                for each_room in room_data:
                    room_name = each_room[0]
                    contour_index = each_room[1]
                    each_room_lines = each_room[2]
                    min_distance = 1000
                    for single_line in each_room_lines:
                        distance_from_door,intersect_x,intersect_y = iterative_object.calculate_distance_from_point_to_line(door_centre,single_line)
                        if distance_from_door != -1 and distance_from_door < min_distance:
                            min_distance = distance_from_door
                    door_room_distance[door_num].append([room_name,min_distance,contour_index])
        return door_room_distance

    def find_main_entrance_door(self,door_room_distance,current_directory):
        entrance_door = []
        entry_room_file = open(current_directory + '/Model/floor_plan_text/entrance_door_rooms.txt', 'r+')
        entry_room_string = entry_room_file.read()
        entry_room__words_list = entry_room_string.split()

        for door_room_row in door_room_distance:
            room_data = door_room_row[4:]
            room_data.sort(key=itemgetter(1))
            if len(room_data)>0:
                closest_contour_index = -1
                # --remove leading and ending white spaces in string
                closest_room = room_data[0][0].strip()
                for entry_room in entry_room__words_list:
                    if closest_room == entry_room:
                        closest_contour_index = room_data[0][2]
                        entrance_door.append([closest_contour_index, door_room_row[:3]])
        return entrance_door

    def get_external_room_order(self,entrance_door,outer_contour_lines,shortest_distance_to_room):
        room_names_in_order = []
        if len(entrance_door)>0:
            # ----entrance_door can have more than one if more than one door is connected to several hall, entry kind of rooms: hence 'entrance_door[0]'
            min_cont_index = entrance_door[0][0]
            door_data = entrance_door[0][1]
            door_centre_x, door_centre_y = door_data[1]

            # --re-order contour to start from cont_line closest to entrance door
            re_ordered_contour_lines = outer_contour_lines[min_cont_index:] + outer_contour_lines[:min_cont_index]

            room_cont_relation = []
            for cont_row, each_cont_line in enumerate(re_ordered_contour_lines):
                room_cont_relation.append([each_cont_line])
                x1, y1 = each_cont_line[0]
                x2, y2 = each_cont_line[1]
                for room_cont_data in shortest_distance_to_room:
                    # --get closest contour line cordinates
                    x3, y3 = room_cont_data[1][1][0]
                    x4, y4 = room_cont_data[1][1][1]
                    if (x1 == x3 and y1 == y3) and (x2 == x4 and y2 == y4):
                        # --room name
                        text = room_cont_data[0]
                        # --closest room line
                        room_line = room_cont_data[1][0]
                        # --room & contour possible intersection point
                        cont_intersect_x, cont_intersect_y = room_cont_data[1][6], room_cont_data[1][7]
                        room_cont_relation[cont_row].append([text, room_line, [cont_intersect_x, cont_intersect_y]])

            elements_to_delete = []
            # --check and delete contour lines that are not related to a room
            for row_num, each_row in enumerate(room_cont_relation):
                if len(each_row) < 2:
                    elements_to_delete.append(row_num)

            # --delete contour lines that do not have a connected room
            room_cont_relation = [i for j, i in enumerate(room_cont_relation) if j not in elements_to_delete]

            for row_num, each_contour_line_data in enumerate(room_cont_relation):
                x1, y1 = each_contour_line_data[0][0]
                for element_num, each_room_line in enumerate(each_contour_line_data[1:]):
                    cont_intersect_x, cont_intersect_y = each_room_line[2]
                    if row_num == 0:
                        distance = math.hypot(door_centre_x - cont_intersect_x, door_centre_y - cont_intersect_y)
                    else:
                        distance = math.hypot(x1 - cont_intersect_x, y1 - cont_intersect_y)
                    each_contour_line_data[1:][element_num].append(distance)
                connected_rooms = each_contour_line_data[1:]
                del room_cont_relation[row_num][1:]
                connected_rooms.sort(key=itemgetter(3))
                room_cont_relation[row_num].append(connected_rooms)

            # ---find external room names in the correct order
            for each_contour_line in room_cont_relation:
                for each_room in each_contour_line[1]:
                    room_names_in_order.append(each_room[0])
        return room_names_in_order


    def create_text(self,only_image_name, external_room_names_in_order,internal_rooms,door_text):
        fp_name = 'This is floorplan '+only_image_name+'.'
        external_room_names_string = self.put_list_to_string(external_room_names_in_order)
        external_room_text = 'Starting from the main door, room names are '+external_room_names_string+'.'
        internal_room_names_string = self.put_list_to_string(internal_rooms)
        internal_room_text = 'Rooms '+internal_room_names_string+' are situated internally.'

        fp_text = fp_name+external_room_text+internal_room_text
        return fp_text


    def put_list_to_string(self,room_names_in_order):
        room_names_string = ''
        for r,ex_room_name in enumerate(room_names_in_order):
            #--if room has not text
            if ex_room_name=='0':
                ex_room_name = 'No name detected'

            if r==len(room_names_in_order)-1:
                room_names_string = room_names_string + ' and '+ str(ex_room_name)
            elif r==len(room_names_in_order)-2:
                room_names_string = room_names_string + str(ex_room_name)
            else:
                room_names_string = room_names_string + str(ex_room_name)+', '
        return room_names_string

    def get_door_positions(self,door_details,outer_contour_line, height, width):
        x1, x2, x3, x4, y1, y2, y3, y4, angle, centerx, centery = self.get_bounding_box_cordinates(outer_contour_line)

        #----------need to see how to get door position for rotated bounding boxes
        #-- http://matthiaseisen.com/pp/patterns/p0201/
        #-- http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_geometric_transformations/py_geometric_transformations.html
        # path_gravvitas = '/home/ub/Documents/floorplan_recognition/tesimagistrale/input_fps/object_test/gravitas_test/text/'
        # contour_image = ~(np.zeros((height, width, 3), np.uint8))
        # cv2.drawContours(contour_image, [outer_contour_line], -1, (0, 0, 0), 5)
        # cv2.imwrite(path_gravvitas + 'temp_contour.png', contour_image)
        # contour_image_PIL = Image.open(path_gravvitas + 'temp_contour.png')
        # x1, x2, x3, x4, y1, y2, y3, y4, angle, centerx, centery = self.get_bounding_box_cordinates(outer_contour_line)
        # angle = 10
        # if int(angle) != 0:
        #     rotate_angle = angle + 90
        #     self.rotate_image(path_gravvitas,contour_image_PIL,rotate_angle)
        #--testing
        # cv2.line(contour_image, (x1, y2), (x4, y2), (0, 255, 0), 2, cv2.cv.CV_AA)
        # cv2.line(contour_image, (x1, y3), (x4, y3), (0, 255, 0), 2, cv2.cv.CV_AA)
        # cv2.line(contour_image, (x2, y1), (x2, y4), (0, 255, 0), 2, cv2.cv.CV_AA)
        # cv2.line(contour_image, (x3, y1), (x3, y4), (0, 255, 0), 2, cv2.cv.CV_AA)
        # cv2.rectangle(contour_image, (x1, y1), (x4, y4), (0, 255, 0), 3)
        # --end testing

        door_positions = []
        for door_row in door_details:
            door_centre_x, door_centre_y = door_row [1]
            door_position = ''
            # cv2.circle(contour_image, (door_centre_x, door_centre_y), 5, (0, 0, 255), -1)
            #--top row
            if (door_centre_x >= x1 and door_centre_x <= x2) and (door_centre_y >= y1 and door_centre_y <= y2):
                door_position = 'Top Left'
            elif (door_centre_x > x2 and door_centre_x <= x3) and (door_centre_y >= y1 and door_centre_y <= y2):
                door_position = 'Top Centre'
            elif (door_centre_x > x3 and door_centre_x <= x4) and (door_centre_y >= y1 and door_centre_y <= y2):
                door_position = 'Top Right'
            #--centre row
            elif (door_centre_x >= x1 and door_centre_x <= x2) and (door_centre_y > y2 and door_centre_y <= y3):
                door_position = 'Centre Left'
            elif (door_centre_x > x2 and door_centre_x <= x3) and (door_centre_y > y2 and door_centre_y <= y3):
                door_position = 'Centre Middle'
            elif (door_centre_x > x3 and door_centre_x <= x4) and (door_centre_y > y2 and door_centre_y <= y3):
                door_position = 'Centre Right'
            #--bottom row
            elif (door_centre_x >= x1 and door_centre_x <= x2) and (door_centre_y > y3 and door_centre_y <= y4):
                door_position = 'Bottom Left'
            elif (door_centre_x > x2 and door_centre_x <= x3) and (door_centre_y > y3 and door_centre_y <= y4):
                door_position = 'Bottom Centre'
            elif (door_centre_x > x3 and door_centre_x <= x4) and (door_centre_y > y3 and door_centre_y <= y4):
                door_position = 'Bottom Right'
            else:
                door_position = 'Outside Contour Bounding Box'
            door_positions.append([[door_centre_x, door_centre_y],door_position])

        #---testing
        #     cv2.putText(contour_image, door_position, (door_centre_x+5, door_centre_y+5),
        #                 cv2.FONT_HERSHEY_PLAIN, 2, (255, 0, 0), 2)
        # cv2.imwrite(path_gravvitas + 'Door_positions.png', contour_image)
        #--end testing

        #---find main door position
        return door_positions

    def get_bounding_box_cordinates(self,outer_contour_line):
        bbox = cv2.minAreaRect(outer_contour_line)

        centerx, centery = bbox[0]
        width, height = bbox[1]
        angle = bbox[2]

        x1 = int(centerx - width/2)
        x4 = int(centerx + width/2)
        y1 = int(centery - height/2)
        y4 = int(centery + height/2)

        x2 = int((2*x1+x4)/3)
        x3 = int((x1+2*x4)/3)
        y2 = int((2*y1+y4)/3)
        y3 = int((y1+2*y4)/3)

        return x1,x2,x3,x4, y1,y2,y3,y4, angle,centerx, centery

    def rotate_image(self,path_gravvitas,contour_image,rotate_angle):
        rotated_image = contour_image.rotate(rotate_angle, expand=True,resample=Image.BICUBIC)
        rotated_image.save(path_gravvitas+'Rotated_image.jpg')

    def generate_position_text(self,entrance_door,door_positions):
        if len(entrance_door)>0:
            main_door_x, main_door_y = entrance_door[0][1][1]
            main_door_position  = ''
            other_positions = "other doors are located at"
            for door_row in door_positions:
                door_x, door_y = door_row[0]
                positon = door_row[1]
                if door_x == main_door_x and door_y == main_door_y:
                    main_door_position = positon
                else:
                    other_positions = other_positions + ' '+ positon +','

            door_text = 'Main door is located at '+main_door_position+ ' and ' + other_positions[:-1]
        else:
            door_text = 'No Main Door Detected'

        return door_text

    def get_building_shape(self,current_directory,path_gravvitas,file_name,outer_contour_line,orginal_img_height,orginal_img_width):
        path_gravvitas = current_directory+'/input_fps/test_svg/text/'

        contour_shape = []

        #---- approxpoyDP image to reduce jagged lines
        #----implementation of Douglas-Peucker algorithm
        epsilon = 0.0025 * cv2.arcLength(outer_contour_line, True)
        approx = cv2.approxPolyDP(outer_contour_line, epsilon, True)
        approx_poly_image = np.zeros((orginal_img_height, orginal_img_width, 3), dtype='uint8')
        cv2.drawContours(approx_poly_image, [approx], -1, (255, 255, 255), -1)
        # cv2.imwrite(path_gravvitas + 'temp_1_approx_poly_image.png', approx_poly_image)

        #----dilate approxpolyDP image
        kernel = np.ones((10, 10), 'uint8')
        dilated = cv2.dilate(approx_poly_image, kernel, iterations=2)
        cv2.imwrite(path_gravvitas + file_name+'temp_2_dilated_image.png', dilated)

        #-- read both 1.dilated & 2.approx images. Store their contours in two arrays: both have to refer same contour by row1, row2

        #----draw image- black, contour-black, bbbox- white and find spaces in bbbox not belonging to contour
        #--get new contours of dilated image
        dilated_contour_img = cv2.imread(path_gravvitas + file_name+'temp_2_dilated_image.png', cv2.IMREAD_GRAYSCALE)
        ret, thresh = cv2.threshold(dilated_contour_img, 0, 255, 1)
        dilated_contours, hierachy = cv2.findContours(thresh, 1, 2)
        image_area = orginal_img_height*orginal_img_width
        total_num_contours  = len(dilated_contours)
        detected_shapes = 0
        for cont, contour in enumerate(dilated_contours):
            contour_area = cv2.contourArea(contour)
            #---remove image contour
            if contour_area/image_area < 0.95:
                #---- draw the old approx contour since we want bb_box to be smaller than contour
                #---- to remove unwanted white spaces between black contour and white bb_box
                rect = cv2.minAreaRect(approx)
                v1, v2, v3 = rect
                bb_width, bb_height = v2

                # ----find rectangle shape using bb_area, contour_area: ratio
                bb_area = bb_width * bb_height
                #---1.27 coz dilated contour is bigger than approx contour bb_box
                if contour_area / bb_area > 0.95 and contour_area / bb_area < 1.27:
                    # ----find square shape using bb_w, bb_h: ratio
                    bb_ratio = bb_width / bb_height
                    #---fp_30.png bb_ratio: 1.26215-----------------------------> check with eyes
                    if bb_ratio >= 0.95 and bb_ratio <= 1.05:
                        contour_shape.append("Square Shape")
                        detected_shapes += 1
                    else:
                        contour_shape.append("Rectangle Shape")
                        detected_shapes += 1
                else:
                    #--special case for all other shapes
                    box = cv2.cv.BoxPoints(rect)
                    box = np.int0(box)
                    bb_image = np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8)
                    cv2.drawContours(bb_image, [box], -1, (255, 255, 255), -1)
                    cv2.drawContours(bb_image, [contour], -1, (0, 0, 0), -1)
                    cv2.imwrite(path_gravvitas + file_name+'temp_3_bb_image.png', bb_image)

        #----check if there are un-shape_detected contours
        if detected_shapes < total_num_contours-1:
            #---find if contour is L shape by looking of bb has only one left over component with rect/square shape
            left_over_comp_img = cv2.imread(path_gravvitas + file_name+'temp_3_bb_image.png', cv2.IMREAD_GRAYSCALE)
            ret, thresh = cv2.threshold(left_over_comp_img, 0, 255, 1)
            left_over_contours, hierachy = cv2.findContours(thresh, 1, 2)
            rect_contour_count = 0
            for cont, lftov_contour in enumerate(left_over_contours):
                contour_area = cv2.contourArea(lftov_contour)
                if contour_area / image_area < 0.95:
                    rect = cv2.minAreaRect(lftov_contour)
                    v1, v2, v3 = rect
                    bb_width, bb_height = v2
                    bb_area = bb_width* bb_height
                    if contour_area /bb_area > 0.85 and contour_area /bb_area < 1.05:
                        rect_contour_count += 1
            #----if len(left over contours) < 2 && shape(left over contours) == rectangle: L shape
            if rect_contour_count == 1 and len(left_over_contours)<=2:
                contour_shape.append("L Shape")
            else:
                contour_shape.append("Other Shape")


            # os.remove(path_gravvitas + 'temp_3_bb_image.png')
        # os.remove(path_gravvitas + 'temp_2_dilated_image.png')
        print 'The Shape is : ',contour_shape

    def extract_walls(self,current_directory,path_gravvitas,outer_contour_line,
                      orginal_img_height,orginal_img_width,file_name,all_floorplan_components):
        path_gravvitas = current_directory+'/input_fps/test_svg/text/'
        results_path = path_gravvitas+'temp/'
        self.iterative_obj.make_directory(path_gravvitas+'temp/',True)


        only_image_name = file_name[0:-4]
        # --extract floorplan_data
        rooms_point_list = all_floorplan_components[0]
        object_point_list = all_floorplan_components[1]
        stairs_point_list = all_floorplan_components[2]
        windows_point_list = all_floorplan_components[3]
        doors_rect_list = all_floorplan_components[4]
        contour_image = ~(np.zeros((orginal_img_height,orginal_img_width,3), np.uint8))
        cv2.drawContours(contour_image,[outer_contour_line],-1,(0,0,0),-1)

        dilate_components_image = np.zeros((orginal_img_height,orginal_img_width,3), np.uint8)

        for room_row in rooms_point_list:
            room_points = room_row[1]
            room_contour = self.iterative_obj.convert_points_to_contour(room_points)
            cv2.drawContours(contour_image, [room_contour], -1, (255, 255, 255), -1)

        for object_row in object_point_list:
            object_points = object_row[1]
            object_contour = self.iterative_obj.convert_points_to_contour(object_points)
            cv2.drawContours(dilate_components_image, [object_contour], -1, (255, 255, 255), -1)

        for stair_row in stairs_point_list:
            stair_contour = self.iterative_obj.convert_points_to_contour(stair_row)
            cv2.drawContours(dilate_components_image, [stair_contour], -1, (255, 255, 255), -1)

        for window_row in windows_point_list:
            window_contour = self.iterative_obj.convert_points_to_contour(window_row)
            cv2.drawContours(dilate_components_image, [window_contour], -1, (255, 255, 255), -1)

        for door_row in doors_rect_list:
            x1, y1 = door_row[0]
            x2, y2 = door_row[1]
            cv2.rectangle(dilate_components_image, (x1, y1), (x2, y2), (255, 255, 255), -1)


        cv2.imwrite(results_path+'only_components.png',dilate_components_image)
        img = cv2.imread(results_path+'only_components.png',0)
        kernel = np.ones((5,5),np.uint8)
        dilation = cv2.dilate(img,kernel,iterations = 1)
        cv2.imwrite(results_path+'only_components.png',dilation)
        image = Image.open(results_path+'only_components.png')
        inverted_image = PIL.ImageOps.invert(image)
        inverted_image.save(results_path+'only_components.png')

        dilated_inverted = cv2.imread(results_path+'only_components.png',0)
        ret,thresh = cv2.threshold(dilated_inverted,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)
        for contour in contours:
            cv2.drawContours(contour_image,[contour],-1,(255,255,255),-1)
        cv2.imwrite(results_path+'all_components.png',contour_image)

        #--extract for text file and for wall_point_list
        wall_point_list = []
        wall_details_file = open(path_gravvitas+only_image_name+'_7_wall_details.txt', 'w')
        all_components = cv2.imread(results_path+'all_components.png',0)
        ret,thresh = cv2.threshold(all_components,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)
        wall_num = 0
        for contour in contours:
            contour_data = ''
            single_wall_data = []
            for c,cont_point in enumerate(contour):
                single_wall_data.append([cont_point[0][0],cont_point[0][1]])
                if c==0:
                    contour_data  = contour_data+'['+str(cont_point[0][0])+' '+str(cont_point[0][1])+']'
                else:
                    contour_data  = contour_data+',['+str(cont_point[0][0])+' '+str(cont_point[0][1])+']'
            wall_details_file.write('Wall '+str(wall_num)+': ')
            wall_details_file.write(contour_data+'\n')
            wall_num += 1
            wall_point_list.append(single_wall_data)

        all_floorplan_components.append(wall_point_list)
        shutil.rmtree(results_path)

        return all_floorplan_components


    def draw_svg(self, file_name, svg_path, img_for_svg_height, img_for_svg_width,
                 all_floorplan_components,fp_text):
        # test path
        # svg_path = '/home/ub/Documents/floorplan_recognition/tesimagistrale/input_fps/object_test/gravitas_test/results/'
        # input_path = '/home/ub/Documents/floorplan_recognition/tesimagistrale/input_fps/object_test/output_test/fp_10/Stefano_output.png'
        # # door_imageo = cv2.imread(input_path,0)
        # # orginal_img_height,orginal_img_width = door_imageo.shape
        # # door_image = ~(np.zeros((orginal_img_height,orginal_img_width,3), np.uint8))

        only_image_name = file_name[0:-4]

        # --extract floorplan_data
        rooms_point_list = all_floorplan_components[0]
        object_point_list = all_floorplan_components[1]
        stairs_point_list = all_floorplan_components[2]
        windows_point_list = all_floorplan_components[3]
        doors_rect_list = all_floorplan_components[4]
        wall_point_list = all_floorplan_components[5]

        l2_space, l3_space = '  ', '    '
        svg_file = open(svg_path + str(only_image_name) + '.svg', 'w')
        svg_file.write(
            '<svg id="svg-graphic" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="' + str(
                img_for_svg_width) + '" height="' + str(img_for_svg_height) + '" viewBox="0 0 ' + str(
                img_for_svg_width + 300) + ' ' + str(
                img_for_svg_height + 100) + '" preserveAspectRatio="xMinYMin meet">' + '\n')
        svg_file.write(l2_space + '<title>'+only_image_name+'</title>' + '\n')
        svg_file.write(l2_space + '<g xmlns="http://www.w3.org/2000/svg" id="svg-graphic-components">' + '\n')

        # --place to store gravvitas meta data
        gravvitas_meta_data = ''

        # print text_path+only_image_name+'_7_wall_details.txt'
        wall_ID_int = 001
        wall_color_int, wall_color_hex = self.get_color('Wl', 'F')
        r, g, b = wall_color_int
        wall_color = str(r) + ',' + str(g) + ',' + str(b)
        wall_text = 'Wall'
        for wall_num, wall_row in enumerate(wall_point_list):
            contour_points_string = ''
            for cordinate in wall_row:
                x, y = cordinate
                contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '
            wall_ID_int = wall_ID_int + wall_num
            wall_ID = 'fp' + str(wall_ID_int)
            svg_file.write(l3_space + '<polygon points="'
                           + contour_points_string + '" id="'
                           + str(wall_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('
                           + wall_color + '); fill: rgb('
                           + wall_color + '); cursor: default;">')
            svg_file.write('<title>' + wall_text + '</title>' + '\n')
            svg_file.write('</polygon>' + '\n')
            current_element_data = self.meta_data_tag_creation(l3_space, wall_ID,
                                                               wall_color_hex, wall_text)
            gravvitas_meta_data = gravvitas_meta_data + current_element_data

        room_number = 100
        for room_row in rooms_point_list:
            room_text = room_row[0]
            room_contour = room_row[1]
            room_number = room_number + 1
            room_ID = 'fp' + str(room_number)
            color_int, color_hex = self.get_color('R', room_text)
            r, g, b = color_int
            room_color = str(r) + ',' + str(g) + ',' + str(b)


            contour_points_string = ''
            for cordinate in room_contour:
                x, y = cordinate
                contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '

            svg_file.write(l3_space + '<polygon points="'
                           + contour_points_string + '" id="'
                           + str(room_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb(0,0,0); fill: rgb('
                           + room_color + '); cursor: default;">')
            svg_file.write('<title>' + room_text + '</title>' + '\n')
            svg_file.write('</polygon>' + '\n')
            current_element_data = self.meta_data_tag_creation(l3_space, room_ID,
                                                               color_hex, room_text)
            gravvitas_meta_data = gravvitas_meta_data + current_element_data



        object_number = 200
        # --set color
        object_color_int, object_color_hex = self.get_color('O', 'F')
        r, g, b = object_color_int
        object_color = str(r) + ',' + str(g) + ',' + str(b)
        for each_object_row in object_point_list:
            object_number = object_number + 1
            object_ID = 'fp' + str(object_number)
            object_text = str(each_object_row[0])
            # # --getting color
            # # g = (int(math.floor(object_number/10)% 10))*100
            # # b = (object_number% 10)*25
            # # r = (b+g)% 255
            # object_color = str(147) + ',' + str(21) + ',' + str(185)

            for each_object in each_object_row[1:]:
                contour_points_string = ''
                for cordinate in each_object:
                    x, y = cordinate
                    contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '
                svg_file.write(l3_space + '<polygon points="'
                               + contour_points_string + '" id="'
                               + str(object_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('
                               + object_color + '); fill: rgb('
                               + object_color + '); cursor: default;">')
                svg_file.write('<title>' + object_text + '</title>' + '\n')
                svg_file.write('</polygon>' + '\n')
                current_element_data = self.meta_data_tag_creation(l3_space, object_ID,
                                                                   object_color_hex,
                                                                   object_text)
                gravvitas_meta_data = gravvitas_meta_data + current_element_data

        stair_ID_int = 300
        # --set color
        stair_color_int, stair_color_hex = self.get_color('S', 'F')
        r, g, b = stair_color_int
        stair_color = str(r) + ',' + str(g) + ',' + str(b)
        stair_text = 'Stairs'
        for stair_num,stair_row in enumerate(stairs_point_list):
            stair_ID_int = stair_ID_int + stair_num
            stair_ID = 'fp' + str(stair_ID_int)
            contour_points_string = ''
            for cordinate in stair_row:
                x, y = cordinate
                contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '

            svg_file.write(l3_space + '<polygon points="'
                           + contour_points_string + '" id="'
                           + str(stair_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 1px; stroke: rgb('
                           + stair_color + '); fill: rgb('
                           + stair_color + '); cursor: default;">')
            svg_file.write('<title>' + stair_text + '</title>' + '\n')
            svg_file.write('</polygon>' + '\n')
            current_element_data = self.meta_data_tag_creation(l3_space, stair_ID,
                                                               stair_color_hex,
                                                               stair_text)
            gravvitas_meta_data = gravvitas_meta_data + current_element_data

        door_ID_int = 400
        # --set color
        door_color_int, door_color_hex = self.get_color('D', 'F')
        r, g, b = door_color_int
        door_color = str(r) + ',' + str(g) + ',' + str(b)
        door_text = 'Door'
        for door_num, door_row in enumerate(doors_rect_list):
            door_ID_int = door_ID_int + door_num
            door_ID = 'fp' + str(door_ID_int)
            x1, y1 = door_row[0]
            x2, y2 = door_row[1]
            width = x2 - x1
            height = y2 - y1
            svg_file.write(l3_space + '<rect x="'
                           + str(x1) + '" y="'
                           + str(y1) + '" width="'
                           + str(width) + '" height="'
                           + str(height) + '" class="" id="'
                           + str(door_ID) + '" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('
                           + door_color + '); fill: rgb('
                           + door_color + '); cursor: default;">')
            svg_file.write('<title>' + door_text + '</title>' + '\n')
            svg_file.write('</rect>' + '\n')
            current_element_data = self.meta_data_tag_creation(l3_space, door_ID,
                                                               door_color_hex, door_text)
            gravvitas_meta_data = gravvitas_meta_data + current_element_data

        window_ID_int = 500
        # --set color
        window_color_int, window_color_hex = self.get_color('Win', 'F')
        r, g, b = window_color_int
        window_color = str(r) + ',' + str(g) + ',' + str(b)
        window_text = 'Window'
        for win_num, window_row in enumerate(windows_point_list):
            window_ID_int = window_ID_int + win_num
            window_ID = 'fp' + str(window_ID_int)
            contour_points_string = ''
            for cordinate in window_row:
                x, y = cordinate
                contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '

            svg_file.write(l3_space + '<polygon points="'
                           + contour_points_string + '" id="'
                           + str(window_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 1px; stroke: rgb('
                           + window_color + '); fill: rgb('
                           + window_color + '); cursor: default;">')
            svg_file.write('<title>' + window_text + '</title>' + '\n')
            svg_file.write('</polygon>' + '\n')
            current_element_data = self.meta_data_tag_creation(l3_space, window_ID,
                                                               window_color_hex,
                                                               window_text)
            gravvitas_meta_data = gravvitas_meta_data + current_element_data

        svg_file.write(l2_space + '</g>')

        svg_file.write(
            l2_space + '<metadata id="rp-928bd05a-4b51-432c-8ff4-2f4c82a3c5b2" rpid="rp-928bd05a-4b51-432c-8ff4-2f4c82a3c5b2" title="Moscone Center Second Floor WWDC 2016" description="Floor Plan of the Moscone Centre for WWDC 2016" category="map" group="" subgroup="" keywords="" collections="" orientation="Landscape" targetdevicename="iPad all">' + '\n')
        svg_file.write(
            l3_space + '<summary>'+fp_text+'</summary>' + '\n')
        svg_file.write(gravvitas_meta_data)
        svg_file.write(l2_space + '</metadata>' + '\n')

        svg_file.write('</svg>')
        svg_file.close()


    def meta_data_tag_creation(self,l3_space,component_ID,color_ID,text):
        id_tag = l3_space+'<gravvitas><id>'+str(component_ID)+'</id>'
        color_tag = '<interiorcolor>'+str(color_ID)+'</interiorcolor><bordercolor/><cornercolor/>'
        audio_tag = '<audio>26</audio>'
        text_tag = '<text>'+str(text)+'</text>'
        rest_of_tag = '<vibration/><annotation/></gravvitas>'+'\n'
        meta_tag = id_tag+color_tag+audio_tag+text_tag+rest_of_tag

        return meta_tag


    def get_color(self,element_type,text):
        color_int, color_hex = (), ''
        element_number = ''
        if self.language == 'E':
            element_number = 3
        elif self.language == 'F':
            element_number = 4


        if element_type=='R':
            room_color_file_path = self.current_directory+'/Model/floor_plan_text/room_types.txt'
            room_color_file = open(room_color_file_path,'r')

            for room_color_data in room_color_file:
                #--structure - '[1 : [0,0,0] :[FFFFF]: [bedroom] : [chambre]]'
                line_seperate = room_color_data.split(':')
                break_list = False
                if len(line_seperate) > 2:
                    room_text_list = line_seperate[element_number].split(',')
                    # room_text_list = re.findall(r"[\w']+", line_seperate[element_number])
                    for each_room in room_text_list:
                        pattern = re.compile("[^\w ]")
                        room_name = pattern.sub('', each_room)
                        # print len(text),len(room_name.strip())
                        # print text,room_name.strip()
                        if text == room_name.strip():
                            room_color_string_list = re.findall(r"[\w']+", line_seperate[1])
                            color_int = tuple([int(x) for x in room_color_string_list])
                            color_hex = re.findall(r"[\w']+", line_seperate[2])[0]
                            break_list = True
                            break
                    if break_list:
                        break
            if text == '0':
                color_int, color_hex = (1,158,151), '979E01'
            print text, color_int,color_hex


        elif element_type=='O':
            color_int, color_hex = (147, 21, 185), '9315b9'
        elif element_type=='S':
            color_int, color_hex = (0, 0, 255), '0000FF'
        elif element_type=='Win':
            color_int, color_hex = (0, 255,0), 'FFFF00'
        elif element_type=='D':
            color_int, color_hex = (255, 0, 0), '00FF00'
        elif element_type=='Wl':
            color_int, color_hex = (0, 0, 0), '000000'
        else:
            color_int, color_hex = (255,255,255), 'FFFFFF'


        return color_int, color_hex


    def check_validity(self,y,x,labeled_list,last_element,direction):
        orginal_y, orginal_x = y,x
        passed= False
        if direction=='BL':
            if labeled_list[y+1][x-1] != 0:
                iteration_count = 1
            else:
                iteration_count = 20
            y,x = y+iteration_count,x-iteration_count
        elif direction=='LL':
            if labeled_list[y][x-1] != 0:
                iteration_count = 1
            else:
                iteration_count = 20
            y,x = y,x-iteration_count
        elif direction=='TL':
            if labeled_list[y-1][x-1] != 0:
                iteration_count = 1
            else:
                iteration_count = 20
            y,x = y-iteration_count,x-iteration_count
        elif direction=='TT':
            if labeled_list[y-1][x] == 0:
                iteration_count = 20
            else:
                iteration_count = 1
            y,x = y-iteration_count,x
        elif direction=='TR':
            if labeled_list[y-1][x+1] == 0:
                iteration_count = 20
            else:
                iteration_count = 1
            y,x = y-iteration_count,x+iteration_count
        elif direction=='RR':
            if labeled_list[y][x+1] == 0:
                iteration_count = 20
            else:
                iteration_count = 1
            y,x = y,x+iteration_count
        elif direction=='BR':
            if labeled_list[y+1][x+1] == 0:
                iteration_count = 20
            else:
                iteration_count = 1
            y,x = y+iteration_count,x+iteration_count
        elif direction=='BB':
            if labeled_list[y+1][x] == 0:
                iteration_count = 20
            else:
                iteration_count = 1
            y,x = y+iteration_count,x

        if labeled_list[y][x] != 0 and labeled_list[y][x]!= 1 and labeled_list[y][x]!= -1:
            last_element = labeled_list[y][x]
            labeled_list[orginal_y][orginal_x] = -1
            passed = True

        if passed==False:
            y,x = orginal_y,orginal_x
        # else:
        #     print orginal_x,orginal_y,'--',direction,last_element,passed,'--',x,y

        return last_element, labeled_list,y,x,passed

    def assign_fp_data_to_rooms(self,all_floorplan_components,path_gravvitas,img_height,img_width):
        # --extract floorplan_data
        rooms_point_list = all_floorplan_components[0]
        object_point_list = all_floorplan_components[1]
        windows_point_list = all_floorplan_components[3]
        doors_rect_list = all_floorplan_components[4]


        #--get rooms
        room_data = []
        #--structure [room_text,room_points,object_data]
        for room_num,room_row in enumerate(rooms_point_list):
            room_name = str(room_row[0])
            room_points = room_row[1]

            # --temp image to get walls
            temp_wall_image = np.zeros((img_height, img_width, 3), np.uint8)

            # --to check if objects and others are inside room
            room_contour = self.iterative_obj.convert_points_to_contour(room_points)
            cv2.drawContours(temp_wall_image, [room_contour], -1, (255, 255, 255), -1)

            object_data = []
            for objrow_num, each_object_row in enumerate(object_point_list):
                object_contour = self.iterative_obj.convert_points_to_contour(each_object_row[1])
                M = cv2.moments(object_contour)
                cx = int(M['m10'] / M['m00'])
                cy = int(M['m01'] / M['m00'])
                object_place = cv2.pointPolygonTest(room_contour, (cx, cy), False)

                if object_place == 1:
                    object_data.append(each_object_row)
                    cv2.drawContours(temp_wall_image, [object_contour], -1, (255, 255, 255), -1)

            window_data = []
            for win_num, window_row in enumerate(windows_point_list):
                for each_win_point in window_row:
                    cx, cy = each_win_point
                    window_dist = cv2.pointPolygonTest(room_contour, (cx, cy), True)
                    if window_dist < 1 and window_dist > -1:
                        window_data.append(window_row)
                        window_contour = self.iterative_obj.convert_points_to_contour(window_row)
                        cv2.drawContours(temp_wall_image, [window_contour], -1, (255, 255, 255), -1)
                        break

            door_data = []
            for door_num,door_row in enumerate(doors_rect_list):
                #--get door rect corner points
                x1,y1 = door_row[0]
                x2,y2 = door_row[1]

                #--get lines from door_rect
                L1, L2, L3, L4 = self.iterative_obj.find_lines_rectangle(x1,y1,x2,y2)
                #--- check if centre point of any door line is inside rooms
                for L in (L1, L2, L3, L4):
                    #---get centre point of each line
                    cx, cy = self.iterative_obj.find_centre_of_line(L)
                    door_place = cv2.pointPolygonTest(room_contour, (cx, cy), True)
                    if door_place > -2:
                        door_data.append(door_row)
                        cv2.rectangle(temp_wall_image, (x1, y1), (x2, y2), (255, 255, 255), -1)
                        break

            cv2.imwrite(path_gravvitas + 'all_components.png', temp_wall_image)

            #---need to dilate walls or get only wall rectangles
            #--dilating walls to get outside_contour
            img = cv2.imread(path_gravvitas + 'all_components.png', 0)
            kernel = np.ones((30, 30), np.uint8)
            dilation = cv2.dilate(img, kernel, iterations=1)
            cv2.imwrite(path_gravvitas + 'dilated.png', dilation)
            image = Image.open(path_gravvitas + 'dilated.png')
            inverted_image = PIL.ImageOps.invert(image)
            inverted_image.save(path_gravvitas + 'dilated.png')
            dilated_inverted = cv2.imread(path_gravvitas + 'dilated.png', 0)
            ret, thresh = cv2.threshold(dilated_inverted, 0, 255, 1)
            contours, hierachy = cv2.findContours(thresh, 1, 2)
            outside_contour = min(contours, key=cv2.contourArea)

            # --get inside_contour
            small_contour_image = cv2.imread(path_gravvitas + 'all_components.png', 0)
            ret, thresh = cv2.threshold(small_contour_image, 0, 255, 1)
            contours, hierachy = cv2.findContours(thresh, 1, 2)
            inside_contour = min(contours, key=cv2.contourArea)

            #--convert inside_contour to list -> reverse the list -> turn back to contour
            #--paths has to be in 2 different directions for clipping to work
            inside_contour_point_list = self.iterative_obj.get_points_from_contour(inside_contour)
            inside_contour_reversed_point_list = inside_contour_point_list[::-1]
            reveresed_inside_contour = self.iterative_obj.convert_points_to_contour(inside_contour_reversed_point_list)

            wall_data = [outside_contour,reveresed_inside_contour]

            room_data.append([room_name, room_points,
                              object_data, window_data, door_data,wall_data])

        return room_data

    def modify_room_cordinates(self,room_data):
        modified_cordinates = []
        for room_row in room_data:
            # room_name = str(room_row[0])
            room_points_modify = [room_row[0],room_row[1]]
            object_data = room_row[2]
            window_data = room_row[3]
            door_data = room_row[4]
            wall_data = room_row[5]

            outside_contour,inner_contour = wall_data

            # -- get bounding box of the outer_contour
            x, y, bb_w, bb_h = cv2.boundingRect(outside_contour)

            # --calculate shift_x and shift_y(with a safe margin/border of 50)
            shift_x, shift_y = x - 50, y - 50

            # --calculate new max img_height and max_width
            new_width, new_height = bb_w + 100, bb_h + 100
            modified_rooms_point_list = self.get_new_points_for_text_and_point_list([room_points_modify], shift_x, shift_y)
            modified_object_point_list = self.get_new_points_for_text_and_point_list(object_data, shift_x,
                                                                                     shift_y)
            modified_windows_point_list = self.get_new_points_for_only_point_list(window_data, shift_x, shift_y)
            modified_doors_rect_list = self.get_new_points_for_only_point_list(door_data, shift_x, shift_y)

            # --modify outer conotur data too
            modified_outside_contour = self.get_new_contour(outside_contour, shift_x, shift_y)
            modified_inner_contour = self.get_new_contour(inner_contour, shift_x, shift_y)

            modified_wall_data = [modified_outside_contour,modified_inner_contour]
            modified_dimensions = [new_width, new_height]
            modified_cordinates.append([modified_rooms_point_list[0],modified_object_point_list,
                                   modified_windows_point_list,modified_doors_rect_list,
                                   modified_wall_data,modified_dimensions])
        return modified_cordinates

    def generate_room_svg(self,file_name,room_data,
                          path_gravvitas):

        only_image_name=file_name[0:-4]

        #--place to store gravvitas meta data
        gravvitas_meta_data = ''
        room_number = 100
        meta_data_room_color = 'FFFF00'
        room_color = str(255)+','+str(255)+','+str(0)
        for room_row in room_data:
            room_name = str(room_row[0][0])
            room_points = room_row[0][1]
            img_width, img_height = room_row[5]
            room_number = room_number+1
            room_ID = 'fp'+str(only_image_name)+'Room No: '+str(room_number)
            l2_space,l3_space ='  ','    '
            svg_file = open(path_gravvitas+only_image_name+'_'+room_name+'_plan.svg', 'w')
            svg_file.write('<svg id="svg-graphic" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="'
                           +str(img_width)+'" height="'+str(img_height)+'" viewBox="0 0 '
                           +str(img_width)+' '+str(img_height)+'" preserveAspectRatio="xMinYMin meet">'+'\n')
            svg_file.write(l2_space+'<title>'+room_name+' Floorplan</title>'+'\n')
            svg_file.write(l2_space+'<g xmlns="http://www.w3.org/2000/svg" id="svg-graphic-components">'+'\n')

            # #--to check if objects and others are inside room
            # room_contour = self.iterative_obj.convert_points_to_contour(room_points)

            contour_points_string = ''
            for cordinate in room_points:
                x,y = cordinate
                contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '

            svg_file.write(l3_space+'<polygon points="'+contour_points_string+'" id="'+str(room_ID)+'" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('+room_color+'); fill: rgb('+room_color+'); cursor: default;">')
            svg_file.write('<title>'+room_name+'</title>'+'\n')
            svg_file.write('</polygon>'+'\n')
            current_element_data = self.meta_data_tag_creation(l3_space, room_ID, meta_data_room_color,room_name)
            gravvitas_meta_data = gravvitas_meta_data+current_element_data


            object_data = room_row[1]
            window_data = room_row[2]
            door_data = room_row[3]
            wall_data = room_row[4]

            #--extract wall data
            wall_ID_int = 001
            wall_ID = 'fp' + str(wall_ID_int)
            wall_color_int, wall_color_hex = self.get_color('Wl', 'F')
            r, g, b = wall_color_int
            wall_color = str(r) + ',' + str(g) + ',' + str(b)
            wall_text = 'Wall'
            outside_contour,reveresed_inside_contour = wall_data
            contour_string = 'M '
            for i in xrange(len(outside_contour)):
                x, y = outside_contour[i][0]
                contour_string = contour_string+str(x) + ',' + str(y) + ' '
            contour_string = contour_string +'z M '

            for i in xrange(len(reveresed_inside_contour)):
                x, y = reveresed_inside_contour[i][0]
                contour_string = contour_string +str(x) + ',' + str(y) + ' '
            contour_string = contour_string + 'z'
            svg_file.write(l3_space + '<path d="'
                           + contour_string + '" id="'
                           + str(wall_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('
                           + wall_color + '); fill: rgb('
                           + wall_color + '); fill-rule:evenodd; cursor: default;">')

            svg_file.write('<title>' + wall_text + '</title>' + '\n')
            svg_file.write('</path>' + '\n')
            current_element_data = self.meta_data_tag_creation(l3_space, wall_ID,
                                                               wall_color_hex, wall_text)
            gravvitas_meta_data = gravvitas_meta_data + current_element_data



            object_number = 200
            # --set color
            object_color_int, object_color_hex = self.get_color('O', 'F')
            r, g, b = object_color_int
            object_color = str(r) + ',' + str(g) + ',' + str(b)
            for objrow_num, each_object_row in enumerate(object_data):
                object_number = object_number + objrow_num
                object_ID = 'fp' + str(object_number)
                object_text = str(each_object_row[0])
                object_points = each_object_row[1]

                contour_points_string = ''
                for cordinate in object_points:
                    x, y = cordinate
                    contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '
                svg_file.write(l3_space + '<polygon points="'
                               + contour_points_string + '" id="'
                               + str(
                    object_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('
                               + object_color + '); fill: rgb('
                               + object_color + '); cursor: default;">')
                svg_file.write('<title>' + object_text + '</title>' + '\n')
                svg_file.write('</polygon>' + '\n')
                current_element_data = self.meta_data_tag_creation(l3_space, object_ID,
                                                                   object_color_hex,
                                                                   object_text)
                gravvitas_meta_data = gravvitas_meta_data + current_element_data

            door_ID_int = 300
            # --set color
            door_color_int, door_color_hex = self.get_color('D', 'F')
            r, g, b = door_color_int
            door_color = str(r) + ',' + str(g) + ',' + str(b)
            door_text = 'Door'
            for door_num, door_contour in enumerate(door_data):
                door_ID_int = door_ID_int + door_num
                door_ID = 'fp' + str(door_ID_int)
                # --get door rect corner points
                x1, y1 = door_contour[0]
                x2, y2 = door_contour[1]
                width = x2 - x1
                height = y2 - y1
                svg_file.write(l3_space + '<rect x="'
                               + str(x1) + '" y="'
                               + str(y1) + '" width="'
                               + str(width) + '" height="'
                               + str(height) + '" class="" id="'
                               + str(door_ID) + '" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('
                               + door_color + '); fill: rgb('
                               + door_color + '); cursor: default;">')
                svg_file.write('<title>' + door_text + '</title>' + '\n')
                svg_file.write('</rect>' + '\n')
                current_element_data = self.meta_data_tag_creation(l3_space, door_ID,
                                                                   door_color_hex, door_text)
                gravvitas_meta_data = gravvitas_meta_data + current_element_data

            window_ID_int = 400
            window_color_int, window_color_hex = self.get_color('Win', 'F')
            r, g, b = window_color_int
            window_color = str(r) + ',' + str(g) + ',' + str(b)
            window_text = 'Window'
            for win_num, window_row in enumerate(window_data):
                window_ID_int = window_ID_int + win_num
                window_ID = 'fp' + str(window_ID_int)
                contour_points_string = ''
                for cordinate in window_row:
                    x, y = cordinate
                    contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '

                svg_file.write(l3_space + '<polygon points="'
                               + contour_points_string + '" id="'
                               + str(window_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 1px; stroke: rgb('
                               + window_color + '); fill: rgb('
                               + window_color + '); cursor: default;">')
                svg_file.write('<title>' + window_text + '</title>' + '\n')
                svg_file.write('</polygon>' + '\n')
                current_element_data = self.meta_data_tag_creation(l3_space, window_ID,
                                                                   window_color_hex,
                                                                   window_text)
                gravvitas_meta_data = gravvitas_meta_data + current_element_data




            svg_file.write(l2_space + '</g>')

            svg_file.write(
                l2_space + '<metadata id="rp-928bd05a-4b51-432c-8ff4-2f4c82a3c5b2" rpid="rp-928bd05a-4b51-432c-8ff4-2f4c82a3c5b2" title="Moscone Center Second Floor WWDC 2016" description="Floor Plan of the Moscone Centre for WWDC 2016" category="map" group="" subgroup="" keywords="" collections="" orientation="Landscape" targetdevicename="iPad all">' + '\n')
            svg_file.write(
                l3_space + '<summary>This floor plan is showing '+room_name+'</summary>' + '\n')
            svg_file.write(gravvitas_meta_data)
            svg_file.write(l2_space + '</metadata>' + '\n')

            svg_file.write('</svg>')
            svg_file.close()

            # wall_ID_int = 001
            # wall_color = str(0) + ',' + str(0) + ',' + str(0)
            # meta_data_wall_color = '000000'
            # wall_text = 'Wall'
            # for wall_num, wall_row in enumerate(wall_point_list):
            #     wall_ID_int = wall_ID_int + wall_num
            #     wall_ID = 'fp' + str(wall_ID_int)
            #     wall_lines = self.iterative_obj.convert_points_to_lines(wall_row)
            #     wall_belong_to_room = False
            #     for each_wall_line in wall_lines:
            #         # ---get centre point of each line
            #         cx, cy = self.iterative_obj.find_centre_of_line(each_wall_line)
            #         wall_place = cv2.pointPolygonTest(room_contour, (cx, cy), True)
            #         if wall_place > -2:
            #             wall_belong_to_room = True
            #             break
            #
            #     if wall_belong_to_room:
            #         contour_points_string = ''
            #         for cordinate in wall_row:
            #             x, y = cordinate
            #             contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '
            #         svg_file.write(l3_space + '<polygon points="'
            #                        + contour_points_string + '" id="'
            #                        + str(wall_ID)+ '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('
            #                        + wall_color + '); fill: rgb('
            #                        + wall_color + '); cursor: default;">')
            #         svg_file.write('<title>' + wall_text + '</title>' + '\n')
            #         svg_file.write('</polygon>' + '\n')
            #         current_element_data = self.meta_data_tag_creation(l3_space, wall_ID,
            #                                                            meta_data_wall_color,
            #                                                            wall_text)
            #         gravvitas_meta_data = gravvitas_meta_data + current_element_data


            # object_number = 200
            # meta_data_object_color = '9315b9'
            # for objrow_num, each_object_row in enumerate(object_data):
            #     object_number = object_number+objrow_num
            #     object_ID = 'fp'+str(object_number)
            #     object_text = str(each_object_row[0])
            #     object_points = each_object_row[1]
            #     #--getting color
            #     # g = (int(math.floor(object_number/10)% 10))*100
            #     # b = (object_number% 10)*25
            #     # r = (b+g)% 255
            #     object_color = str(147)+','+str(21)+','+str(185)
            #     object_contour = self.iterative_obj.convert_points_to_contour(object_points)
            #     M = cv2.moments(object_contour)
            #     cx = int(M['m10']/M['m00'])
            #     cy = int(M['m01']/M['m00'])
            #     object_place = cv2.pointPolygonTest(room_contour,(cx,cy),False)
            #
            #     if object_place == 1:
            #         contour_points_string = ''
            #         for cordinate in object_points:
            #             x,y = cordinate
            #             contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '
            #         svg_file.write(l3_space+'<polygon points="'+contour_points_string+'" id="'+str(object_ID)+'" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('+object_color+'); fill: rgb('+object_color+'); cursor: default;">')
            #         svg_file.write('<title>'+object_text+'</title>'+'\n')
            #         svg_file.write('</polygon>'+'\n')
            #         current_element_data = self.meta_data_tag_creation(l3_space,object_ID,meta_data_object_color,object_text)
            #         gravvitas_meta_data = gravvitas_meta_data+current_element_data
            #
            # door_ID_int = 300
            # door_color = str(255)+','+str(0)+','+str(0)
            # meta_data_door_color = 'ff0000'
            # door_text = 'Door'
            # for door_num,door_contour in enumerate(doors_rect_list):
            #     door_ID_int = door_ID_int+door_num
            #     door_ID = 'fp'+str(door_ID_int)
            #     #--get door rect corner points
            #     x1,y1 = door_contour[0]
            #     x2,y2 = door_contour[1]
            #     #--get lines from door_rect
            #     L1, L2, L3, L4 = self.iterative_obj.find_lines_rectangle(x1,y1,x2,y2)
            #     #--- check if centre point of any door line is inside rooms
            #     for L in (L1, L2, L3, L4):
            #         #---get centre point of each line
            #         cx, cy = iterative_obj.find_centre_of_line(L)
            #         door_place = cv2.pointPolygonTest(room_contour, (cx, cy), True)
            #         if door_place > -2:
            #             width = x2 - x1
            #             height = y2 - y1
            #             svg_file.write(l3_space + '<rect x="' + str(x1) + '" y="' + str(y1) + '" width="' + str(
            #                 width) + '" height="' + str(height) + '" class="" id="' + str(
            #                 door_ID) + '" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb(' + door_color + '); fill: rgb(' + door_color + '); cursor: default;">')
            #             svg_file.write('<title>' + door_text + '</title>' + '\n')
            #             svg_file.write('</rect>' + '\n')
            #             current_element_data = self.meta_data_tag_creation(l3_space, door_ID, meta_data_door_color,
            #                                                                door_text)
            #             gravvitas_meta_data = gravvitas_meta_data + current_element_data
            #             break
            #
            #
            # window_ID_int = 400
            # window_color = str(0)+','+str(0)+','+str(255)
            # meta_data_window_color = '0000ff'
            # window_text = 'Window'
            # for win_num, window_row in enumerate(windows_point_list):
            #     window_ID_int = window_ID_int+win_num
            #     window_ID = 'fp'+str(window_ID_int)
            #     for each_win_point in window_row:
            #         cx,cy = each_win_point
            #         window_dist = cv2.pointPolygonTest(room_contour,(cx,cy),True)
            #         if window_dist<1 and window_dist>-1:
            #             contour_points_string = ''
            #             for cordinate in window_row:
            #                 x,y = cordinate
            #                 contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '
            #
            #             svg_file.write(l3_space+'<polygon points="'+contour_points_string+'" id="'+str(window_ID)+'" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 1px; stroke: rgb('+window_color+'); fill: rgb('+window_color+'); cursor: default;">')
            #             svg_file.write('<title>'+window_text+'</title>'+'\n')
            #             svg_file.write('</polygon>'+'\n')
            #             current_element_data = self.meta_data_tag_creation(l3_space,window_ID,meta_data_window_color,window_text)
            #             gravvitas_meta_data = gravvitas_meta_data+current_element_data
            #             break
            #
            # svg_file.write(l2_space+'</g>')
            #
            # svg_file.write(l2_space+'<metadata id="rp-928bd05a-4b51-432c-8ff4-2f4c82a3c5b2" rpid="rp-928bd05a-4b51-432c-8ff4-2f4c82a3c5b2" title="Moscone Center Second Floor WWDC 2016" description="Floor Plan of the Moscone Centre for WWDC 2016" category="map" group="" subgroup="" keywords="" collections="" orientation="Landscape" targetdevicename="iPad all">'+'\n')
            # svg_file.write(l3_space+'<summary>This floor plan is showing the second floor of the Moscone Centre in San Francisco configured for Apples WWDC 2016.</summary>'+'\n')
            # svg_file.write(gravvitas_meta_data)
            # svg_file.write(l2_space+'</metadata>'+'\n')
            #
            # svg_file.write('</svg>')
            # svg_file.close()

    def generate_tactile_fp(self,text_path, output_tactile_path, tactile_symbol_path, img_height, img_width, filename, avg_door_width):
        only_image_name = filename[0:-4]

        #--testing
        text_path = '/home/ub/Documents/floorplan_recognition/tesimagistrale/input_fps/object_test/gravitas_test/text/'
        # output_tactile_path = '/home/ub/Documents/floorplan_recognition/tesimagistrale/input_fps/tactile/'
        #--end testing path

        #--create path to store tactile results

        tactile_fp = ~(np.zeros((img_height,img_width,3), np.uint8))
        # print text_path+only_image_name+'_7_wall_details.txt'
        wall_details_file = open(text_path+only_image_name+'_7_wall_details.txt', 'r')
        for text_line in wall_details_file:
            line_seperate = text_line.split(':')
            if len(line_seperate)==2:
                contour_details = re.findall(r"[\w']+", line_seperate[1])
                int_contour_details = [int(x) for x in contour_details]
                chunks = [int_contour_details[x:x+2] for x in xrange(0, len(int_contour_details), 2)]
                add_bracket = []
                for each_chunk in chunks:
                    add_bracket.append([each_chunk])
                wall_contour = np.asarray(add_bracket)
                cv2.drawContours(tactile_fp,[wall_contour],-1,(0,0,0),-1)

        room_details_file = open(text_path+only_image_name+'_1_room_details.txt', 'r')
        for text_line in room_details_file:
            line_seperate = text_line.split(':')
            if len(line_seperate)>3:
                contour_details = re.findall(r"[\w']+", line_seperate[5])
                int_contour_details = [int(x) for x in contour_details]
                chunks = [int_contour_details[x:x+2] for x in xrange(0, len(int_contour_details), 2)]
                add_bracket = []
                for each_chunk in chunks:
                    add_bracket.append([each_chunk])
                room_contour = np.asarray(add_bracket)
                cv2.drawContours(tactile_fp,[room_contour],-1,(0,0,0),5)


        door_details_file = open(text_path+only_image_name+'_5_door_details.txt', 'r')
        for text_line in door_details_file:
            line_seperate = text_line.split(':')
            if len(line_seperate)==2:
                contour_details = re.findall(r"[\w']+", line_seperate[1])
                int_contour_details = [int(x) for x in contour_details]
                door_contour = [int_contour_details[x:x+2] for x in xrange(0, len(int_contour_details), 2)]
                x1,y1 = door_contour[0]
                x2,y2 = door_contour[1]

                # print x1,y1,x2,y2
                # #--increase width to erase room contours closing doors
                # #--finding shorter side
                # iterative_obj = iterative_functions_class()
                # L1,L2,L3,L4 = iterative_obj.find_lines_rectangle(x1,y1,x2,y2)
                # #--find length of first two lines
                # L1x1,L1y1 = L1[0]
                # L1x2,L1y2 = L1[1]
                # L1_length = math.hypot(L1x2-L1x1,L1y2-L1y1)
                # L2x1,L2y1 = L2[0]
                # L2x2,L2y2 = L2[1]
                # L2_length = math.hypot(L2x2-L2x1,L2y2-L2y1)
                #
                #
                # #--****************check later what to do for squares
                # if L1_length > L2_length:
                #     extend_l1_x1, extend_l1_y1 = L2[0]
                #     extend_l1_x2, extend_l1_y2 = L2[1]
                #     extend_l2_x1, extend_l2_y1 = L4[0]
                #     extend_l2_x2, extend_l2_y2 = L4[1]
                # else:
                #     extend_l1_x1, extend_l1_y1 = L1[0]
                #     extend_l1_x2, extend_l1_y2 = L1[1]
                #     extend_l2_x1, extend_l2_y1 = L3[0]
                #     extend_l2_x2, extend_l2_y2 = L3[1]
                #
                #
                # left_x, left_y= extend_l1_x1-((extend_l1_x2-extend_l1_x1)/2),extend_l1_y1-((extend_l1_y1-extend_l1_y2)/2)
                # right_x,right_y = extend_l2_x1-((extend_l2_x2-extend_l2_x1)/2),extend_l2_y1-((extend_l2_y1-extend_l2_y2)/2)
                #
                #
                # cv2.rectangle(tactile_fp,(left_x,left_y),(right_x,right_y),(0,255,0),-1)
                cv2.rectangle(tactile_fp,(x1,y1),(x2,y2),(255,255,255),-1)
                # cv2.line(tactile_fp,(extend_l1_x1, extend_l1_y1),(extend_l1_x2, extend_l1_y2),(255,0,0),5)
                # cv2.line(tactile_fp,(extend_l2_x1, extend_l2_y1),(extend_l2_x2, extend_l2_y2),(255,0,0),5)

        object_details_file = open(text_path+only_image_name+'_2_object_details.txt', 'r')
        for text_line in object_details_file:
            line_seperate = text_line.split(':')
            if len(line_seperate)>1:
                object_contour = re.findall(r"[\w']+", line_seperate[1])
                int_object_details = [int(x) for x in object_contour]
                object_chunks = [int_object_details[x:x+2] for x in xrange(0, len(int_object_details), 2)]
                #---temp fix delete later
                x1,y1 = object_chunks[0]
                x2,y2 = object_chunks[1]
                #--uncomment when you get object as contour
                # object_contour_new = [[[x1,y1]],[[x2,y1]],[[x2,y2]],[[x1,y2]]]
                # object_contour = np.asarray(object_contour_new)
                # rect = cv2.minAreaRect(object_contour)
                # box = cv2.cv.BoxPoints(rect)
                # bbx1,bby1 = box[0]
                # bbx2,bby2 = box[1]
                # bbx3,bby3 = box[2]
                # bbx4,bby4 = box[3]
                # width = int(math.hypot(bbx2-bbx1,bby2-bby1))
                # height = int(math.hypot(bbx4-bbx3,bby4-bby3))

                width = abs(x2-x1)
                height = abs(y1-y2)



                symbol_to_draw_path = ''
                object_name = line_seperate[0].strip()
                tactile_symbol_files = os.listdir(tactile_symbol_path)
                for tact_symbol in tactile_symbol_files:
                    tact_symbol_name = tact_symbol[:-4].strip()
                    if tact_symbol_name == object_name:
                        symbol_to_draw_path = tactile_symbol_path+str(tact_symbol)
                        # print tact_symbol_name

                #--resize symbol to fit in to image area
                if symbol_to_draw_path != '':
                    img = Image.open(symbol_to_draw_path)
                    resized_image = img.resize((width,height),Image.ANTIALIAS)
                    resized_image.save(output_tactile_path+'temp.png')

                    symbol_image = cv2.imread(output_tactile_path+'temp.png')
                    tactile_fp[int(y1):int(y1)+symbol_image.shape[0], int(x1):int(x1)+symbol_image.shape[1]] = symbol_image


        stair_details_file = open(text_path+only_image_name+'_3_stairs_details.txt', 'r')
        for text_line in stair_details_file:
            line_seperate = text_line.split(':')
            if len(line_seperate)==2:
                contour_details = re.findall(r"[\w']+", line_seperate[1])
                int_contour_details = [int(x) for x in contour_details]
                stair_chunks = [int_contour_details[x:x+2] for x in xrange(0, len(int_contour_details), 2)]

                #--try later
                #-- get paterrn image size, crop it to fit be less than image size, keep pasting using widht, height sizes untill full
                # stair_pattern = cv2.imread(tactile_symbol_path+'stairs.png',0)
                # stair_height, stair_width = stair_pattern.shape
                # print stair_width, stair_height,'--',img_width,img_height
                # new_stairs_pattern = ~(np.zeros((img_height,img_width,3), np.uint8))
                # if img_width>stair_width:
                #     iterations = img_width/stair_width+1
                #     while iterations != 0:
                #         new_stairs_pattern[]
                # print stair_width, stair, height
                # img_width,img_height

                stair_pattern = cv2.imread(tactile_symbol_path+'stairs.png')
                stencil = np.zeros(stair_pattern.shape).astype(stair_pattern.dtype)
                # stair_contours = [stair_contour]
                stair_contours = [np.array(stair_chunks)]
                # print contours
                # print stair_contours
                color = [255, 255, 255]
                cv2.fillPoly(stencil, stair_contours, color)
                result = cv2.bitwise_and(stair_pattern, stencil)
                cv2.imwrite(output_tactile_path+"result.jpg", result)

                stairs_image = cv2.imread(output_tactile_path+"result.jpg",0)
                # new_image = ~(np.zeros(stairs_image.shape).astype(stairs_image.dtype))
                # cv2.imwrite(output_tactile_path+str(only_image_name)+'_wall.png',tactile_fp)
                # tactile_image = cv2.imread(output_tactile_path+str(only_image_name)+'_wall.png',0)
                # result = cv2.bitwise_and(stairs_image, tactile_image)
                # cv2.imwrite(output_tactile_path+"result-2.jpg", result)
                ret,thresh = cv2.threshold(stairs_image,0,255,1)
                contours,hierachy = cv2.findContours(thresh,1,2)
                for cn,cnt in enumerate(contours):
                    if cn != len(contours)-1:
                        cv2.drawContours(tactile_fp,[cnt],-1,(0,0,0),10)




        window_details_file = open(text_path+only_image_name+'_4_window_details.txt', 'r')
        for text_line in window_details_file:
            contour_seperate = text_line.split(':')
            if len(contour_seperate)==2:
                contour_details = re.findall(r"[\w']+", contour_seperate[1])
                int_contour_details = [int(x) for x in contour_details]
                chunks = [int_contour_details[x:x+2] for x in xrange(0, len(int_contour_details), 2)]
                add_bracket = []
                for each_chunk in chunks:
                    add_bracket.append([each_chunk])
                window_contour = np.asarray(add_bracket)
                # cv2.drawContours(dilate_components_image,[window_contour],-1,(255,255,255),-1)

                rect = cv2.minAreaRect(window_contour)
                box = cv2.cv.BoxPoints(rect)
                box = np.int0(box)
                cv2.drawContours(tactile_fp,[box],0,(255,0,0),-1)


        cv2.imwrite(output_tactile_path+str(only_image_name)+'_wall.png',tactile_fp)
