__author__ = 'anu'


import re
import cv2

from Levenshtein import distance


class text_recogntion_class():
    def __init__(self,file_name, image):
        self. f_name = file_name
        self.img = image

    def hocr_text(self, word, image,isFinalWord):
        numbers = word[1]
        x1 = int(numbers[0])
        y1 = int(numbers[1])
        x2 = int(numbers[2])
        y2 = int(numbers[3])
        font = cv2.FONT_HERSHEY_SIMPLEX
        if isFinalWord:
            if len(word[4])==0:
                text_name = word[0]
            else:
                text_name = word[4]
            font = cv2.FONT_HERSHEY_SIMPLEX
            r_color = 0
            b_color = 255
            cv2.putText(image,text_name,(x1, y1-30), font, 1,(b_color,0,r_color),2,cv2.CV_AA)
        else:
            r_color = 0
            b_color = 255
        #     cv2.putText(image,word[0],(x1-30, y1-10), font, 1,(b_color,0,r_color),2,cv2.CV_AA)
        cv2.rectangle(image, (x1, y1), (x2, y2), (b_color, 0, r_color), 3)
        return x1,y1, x2,y2


    def letter_count(self, suggestion, image_name):
        letter_count = 0
        x = 0
        sug_name1 = suggestion
        for i_letter in image_name:
            for d_letter in sug_name1:
                if i_letter is d_letter:
                    index_val = sug_name1.index(d_letter)
                    next_val = index_val+1
                    sug_name1 = sug_name1[:index_val]+sug_name1[next_val:]
                    letter_count = letter_count +1
                    x = x+1
                    break
        return letter_count

    def similar_letter(self, suggestion, image_name):
        word =""
        count = image_name.count('m')
        while count>0:
            changed_word = image_name.replace("m","in",count)
            if changed_word==suggestion:
                word = suggestion
            if word is not "":
                break
            count = count-1
        if word == "":
            while count>0:
                changed_word = image_name.replace("m","ni",count)
                if changed_word==suggestion:
                    word = suggestion
                if word is not "":
                    break
                count = count-1
        return word

    def check_dictionary(self,dictionary_content,image_word,word_exists):
        existing_word = ""
        suggestion = []
        suggestion_count = 0
        for dictionary_word in dictionary_content:
            #edit distance check
            edit_dist = distance(str(dictionary_word), str(image_word))
            if edit_dist == 0:
                existing_word = dictionary_word
                word_exists = True
                break
            elif edit_dist < 5:
                word_length_threshold = (len(image_word)/100)*90
                if abs(len(image_word)-len(dictionary_word))<=word_length_threshold:
                    letter_similarity_count =0
                    copy_dictionary_word = list(dictionary_word)
                    # print 'dict',dictionary_word,image_word
                    for image_letter in image_word:
                        for num,dict_letter in enumerate(copy_dictionary_word):
                            if dict_letter==image_letter:
                                letter_similarity_count = letter_similarity_count+1
                                del copy_dictionary_word[num]
                                break
                    min_similarity_threshold = (len(image_word)/100)*80
                    if letter_similarity_count>min_similarity_threshold:
                        suggestion.append([])
                        suggestion[suggestion_count].append(dictionary_word)
                        suggestion[suggestion_count].append(edit_dist)
                        suggestion_count += 1
                    elif len(image_word)<3 and len(dictionary_word)<3:
                        if letter_similarity_count>0:
                            suggestion.append([])
                            suggestion[suggestion_count].append(dictionary_word)
                            suggestion[suggestion_count].append(edit_dist)
                            suggestion_count += 1
        return suggestion, word_exists,existing_word


    def check_length_of_words_seperated_by_space(self,original_word,dict_existing_word):
        all_words = original_word.split()
        contains_too_small_word = False
        for each_word in all_words:
            if len(each_word)<3:
                contains_too_small_word = True
        if contains_too_small_word:
            new_word = dict_existing_word
        else:
            new_word = original_word
        return new_word