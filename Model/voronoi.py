author = 'anu'

import cv2,math
import numpy as np

from open_plan_line import line
from iterative_functions import iterative_functions_class

class voronoi_class:
    def draw_voronoi(self,avg_door_width, image_name, text_words, open_plan_cordinates,open_plan_contour_lines,open_plan_image,final_image_path):
        tupled_open_plan_cordinates = [tuple(cord) for cord in open_plan_cordinates]
        self.avg_door_width = avg_door_width
        #--method 02
        # Read in the image.
        img_voronoi = cv2.cvtColor(open_plan_image,cv2.COLOR_GRAY2RGB)

        # Rectangle to be used with Subdiv2D
        size = img_voronoi.shape
        img_height, img_width = size[0],size[1]
        rect = (0, 0, size[1], size[0])

        # Create an instance of Subdiv2D
        subdiv = cv2.Subdiv2D(rect)

        # Create an array of points.
        points = tupled_open_plan_cordinates

        # Insert points into subdiv
        for p in points:
            subdiv.insert(p)

#--if found by color
        voronoi_polylines = self.find_voronoi_lines(subdiv, img_voronoi,open_plan_contour_lines)
        cv2.imwrite(final_image_path + image_name + '_2_red_voronoi.png', img_voronoi)
        self.get_data_within_contour(img_voronoi,final_image_path,image_name)
        self.find_single_lines(img_voronoi, final_image_path, image_name,img_height, img_width)

# --if found by line co-ordinates
#         voronoi_image = np.zeros((img_height, img_width, 3), np.uint8)
#         voronoi_polylines = self.find_voronoi_lines(subdiv,voronoi_image,open_plan_contour_lines)
#         cv2.imwrite(final_image_path + image_name + '_2_red_voronoi.png', voronoi_image)
#         voronoi_line_segments = self.find_voronoi_line_segments(voronoi_polylines)
#         non_repeated_lines = self.combine_lines(voronoi_line_segments)
#         print 'len(non_repeated_lines)', len(non_repeated_lines)
#         self.find_lines_within_contour(open_plan_contour_lines,non_repeated_lines,img_height,img_width,final_image_path,image_name)
#         # print len()
#         # empty_new_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
#         # for each_line in open_plan_contour_lines:
#         #     cv2.line(empty_new_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,0),2,cv2.cv.CV_AA)
#         # cv2.imwrite(final_image_path + image_name + '_3_outer_contour.png',empty_new_image)


#-------------------------
        # Show results
        # for r, row in enumerate(text_words):
        #     cord1 = row[1][0]
        #     cord2 = row[1][1]
        #     x_position = (len(row[0]) / 2) * 25
        #     text = str(row[0]).upper()
        #     cv2.putText(img_voronoi, text, (cord1 - x_position, cord2 + 12), cv2.FONT_HERSHEY_COMPLEX, 1,
        #                 (0, 0, 0), 2, cv2.cv.CV_AA)
        # cv2.imwrite(final_image_path + image_name+'_1_voronoi.png', img_voronoi)

        # # Find voronoi lines with contour method->to be used for GT
        # self.find_single_lines(voronoi_image, final_image_path, image_name, img_height, img_width)

    # Draw voronoi diagram
    def find_voronoi_lines(self, subdiv,voronoi_image,open_plan_contour_lines):
        (facets, centers) = subdiv.getVoronoiFacetList([])
        voronoi_polylines = []
        for i in xrange(0, len(facets)):
            ifacet_arr = []
            for f in facets[i]:
                ifacet_arr.append(f)

            ifacet = np.array(ifacet_arr, np.int)

            ifacets = np.array([ifacet])
            cv2.polylines(voronoi_image, ifacets, True, (0, 0, 255), 5, cv2.CV_AA, 0)
            # cv2.circle(img, (centers[i][0], centers[i][1]), 5, (0, 0, 255), cv2.CV_AA, 0)
            voronoi_polylines.append(ifacet)
        # #--draw open plan to seperate voronoi line segments
        for each_line in open_plan_contour_lines:
            cv2.line(voronoi_image, (tuple(each_line[0])), (tuple(each_line[1])), (0, 0, 0), 3, cv2.cv.CV_AA)
        return voronoi_polylines

    def get_data_within_contour(self,voronoi_image,final_image_path ,image_name):
        # Set values equal to or above 220 to 0.
        # Set values below 220 to 255.
        th, im_th = cv2.threshold(voronoi_image, 1, 255, cv2.THRESH_BINARY_INV)

        # Copy the thresholded image.
        im_floodfill = im_th.copy()

        # Mask used to flood filling.
        # Notice the size needs to be 2 pixels than the image.
        h, w = im_th.shape[:2]
        mask = np.zeros((h + 2, w + 2), np.uint8)

        # Floodfill from point (0, 0)
        cv2.floodFill(im_floodfill, mask, (0, 0), 255)

        # Invert floodfilled image
        im_floodfill_inv = cv2.bitwise_not(im_floodfill)

        # Combine the two images to get the foreground.
        im_out = im_th | im_floodfill_inv

        # Display images.
        cv2.imwrite(final_image_path+image_name[0:-4]+"Thresholded Image.png", im_th)
        cv2.imwrite(final_image_path+image_name[0:-4]+"Floodfilled Image.png", im_floodfill)
        cv2.imwrite(final_image_path+image_name[0:-4]+"Inverted Floodfilled Image.png", im_floodfill_inv)
        cv2.imwrite(final_image_path+image_name[0:-4]+"Foreground.png", im_out)

    def find_voronoi_line_segments(self,voronoi_polylines):
        voronoi_line_segments = []
        for line_row in voronoi_polylines:
            contour_lines = []
            first_element = 0
            for p,line_point in enumerate(line_row):
                x, y = line_point
                # -----based on cordinates get contour lines and append to image_contour_lines
                if p == 0:
                    contour_lines.append([])
                    contour_lines[p].append([x, y])
                    first_element = [x, y]
                elif p == len(line_row) - 1:
                    contour_lines.append([])
                    contour_lines[p].append([x, y])
                    contour_lines[p - 1].append([x, y])
                    contour_lines[p].append(first_element)
                else:
                    contour_lines.append([])
                    contour_lines[p].append([x, y])
                    contour_lines[p - 1].append([x, y])
            for each_element in contour_lines:
                voronoi_line_segments.append(each_element)

        return voronoi_line_segments

    def find_lines_within_contour(self,open_plan_contour_lines,voronoi_lines,img_height,img_width,final_image_path,image_name):
        line_obj = line()
        #--get contour from lines
        contour = []
        for cont_line in open_plan_contour_lines:
            for cont_point in cont_line:
                contour.append([cont_point])
        op_contour = np.asarray(contour)



        final_voronoi_lines = []
        for voronoi_line in voronoi_lines:
            x1, y1 = voronoi_line[0]
            x2, y2 = voronoi_line[1]
            num_of_points_outside_contour = line_obj.check_line_inside_contour(x1,y1,x2,y2,op_contour)
            if num_of_points_outside_contour==0:
                final_voronoi_lines.append(voronoi_line)
            else:
                v_max_x = max(x1, x2)
                v_min_x = min(x1, x2)
                v_max_y = max(y1, y2)
                v_min_y = min(y1, y2)
                for contour_line in open_plan_contour_lines:
                    x3, y3 = contour_line[0]
                    x4, y4 = contour_line[1]
                    c_max_x = max(x3, x4)
                    c_min_x = min(x3, x4)
                    c_max_y = max(y3, y4)
                    c_min_y = min(y3, y4)
                    intersect_x1, intersect_y1 = line_obj.find_contour_intersections(x1,y1,x2,y2,x3,y3, x4,y4)
                    if (intersect_x1 >= v_min_x and intersect_x1 <= v_max_x and intersect_y1 >= v_min_y and intersect_y1 <= v_max_y) and (
                        intersect_x1 >= c_min_x and intersect_x1 <= c_max_x and intersect_y1 >= c_min_y and intersect_y1 <= c_max_y):
                        final_voronoi_lines.append(voronoi_line)
                        break
        return final_voronoi_lines




    #--color based GT find method
    def find_single_lines(self,img_voronoi, final_image_path, image_name,img_height, img_width):
        contour_point_list = self.find_contours(img_voronoi,final_image_path,image_name,img_height, img_width)
        contour_lines = self.find_contour_lines(contour_point_list)
        non_repeated_lines = self.combine_lines(contour_lines)
        for line_num, cont_line in enumerate(contour_lines):
            x1,y1 = cont_line[0]
            x2,y2 = cont_line[1]
            test_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
            cv2.line(test_image, (x1, y1), (x2, y2), (255, 0, 0), 3, cv2.cv.CV_AA)
            cv2.imwrite(final_image_path + image_name[0:-4] + '_5_line_'+str(line_num)+'.png', test_image)
        for line_num, cont_line in enumerate(non_repeated_lines):
            x1,y1 = cont_line[0]
            x2,y2 = cont_line[1]
            test_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
            cv2.line(test_image, (x1, y1), (x2, y2), (0, 0, 255), 3, cv2.cv.CV_AA)
            cv2.imwrite(final_image_path + image_name[0:-4] + '_6_line_'+str(line_num)+'.png', test_image)

    def find_contours(self,img_voronoi,final_image_path,image_name,img_height, img_width):
        #--colorwise seperation
        lower = np.array([0, 0, 250], dtype="uint8")
        upper = np.array([0, 0, 255], dtype="uint8")
        mask = cv2.inRange(img_voronoi, lower, upper)
        img_voronoi = cv2.bitwise_and(img_voronoi, img_voronoi, mask=mask)
        gray = cv2.cvtColor(img_voronoi, cv2.COLOR_BGR2GRAY)
        #--end colorwise separation

        # gray = cv2.imread('/home/ub/Documents/efp/input_fps/evaluation_test/test-2.png',cv2.IMREAD_GRAYSCALE)
        # gray = cv2.cvtColor(img_voronoi, cv2.COLOR_BGR2GRAY)
        cv2.imwrite(final_image_path + image_name[0:-4] + '_3-2_gray.png', gray)
        #--cv2. find contours explanation
        #---cv2.threshold(gray, 0(threshold_value), 255(max_value), 0(type))
        #---if type==0:
            # initial image is max_value (white)
            # for all pixels with value > threshold_value: assign 0
        #---elif type ==1:
            # initial image is 0 (black)
            # for all pixels with value > threshold_value: assign max_value(white)
        ret, thresh = cv2.threshold(gray, 0, 255, 0)
        contours, hierachy = cv2.findContours(thresh, 1, 2)


        #--find conotur with largest permiter and use that as contour for 'voronoi'
        #--when using for GT take all contours unlike this
        largest_perimeter = 0
        contour_point_list = []
        for count, cnt in enumerate(contours):
            # ---simplify contours
            new_cnt = self.improve_contours(cnt,self.avg_door_width)
            outer_cont2 = ~(np.zeros((img_height, img_width, 3), np.uint8))
            cv2.drawContours(outer_cont2, [new_cnt], 0, (0, 0, 255), 2, cv2.CV_AA)
            cv2.imwrite(final_image_path + image_name[0:-4] + '_4-2_' + str(count) + '_approx.png', outer_cont2)

            # perimeter = cv2.arcLength(new_cnt, True)
            # if largest_perimeter < perimeter:
            #     outer_cont2 = ~(np.zeros((img_height, img_width, 3), np.uint8))
            #     # --0.4% of arc length (contour perimeter)
            #     epsilon = 0.004 * cv2.arcLength(new_cnt, True)
            #     approx = cv2.approxPolyDP(new_cnt, epsilon, True)
            #     cv2.drawContours(outer_cont2, [approx], 0, (0, 0, 255), 2, cv2.CV_AA)
            #     # cv2.imwrite(final_image_path + image_name[0:-4] + '_4-2_' + str(count) + '_approx.png', outer_cont2)
            #     largest_perimeter = perimeter
            #     contour_point_list = approx



            #
            # outer_cont = ~(np.zeros((img_height, img_width, 3), np.uint8))
            # cv2.drawContours(outer_cont, [cnt], 0, (0, 0, 0), 2, cv2.CV_AA)
            # cv2.imwrite(final_image_path + image_name[0:-4] + '_4_' + str(count) + '_contour.png', outer_cont)
            # # ---now since we converted img to white in black, contours are detected for outer border too
            # perimeter = cv2.arcLength(cnt, True)
            # if largest_perimeter < perimeter:
            #     outer_cont2 = ~(np.zeros((img_height, img_width, 3), np.uint8))
            #     # --0.4% of arc length (contour perimeter)
            #     epsilon = 0.004 * cv2.arcLength(cnt, True)
            #     approx = cv2.approxPolyDP(cnt, epsilon, True)
            #     cv2.drawContours(outer_cont2, [approx], 0, (0, 0, 255), 2, cv2.CV_AA)
            #     cv2.imwrite(final_image_path + image_name[0:-4] + '_4-2_' + str(count) + '_approx.png', outer_cont2)
            #     largest_perimeter = perimeter
            #     contour_point_list = approx
            #
            # # #---now since we converted img to white in black, contours are detected for outer border too
            # # perimeter = cv2.arcLength(cnt, True)
            # # if largest_perimeter < perimeter:
            # #     # outer_cont = ~(np.zeros((img_height, img_width, 3), np.uint8))
            # #     # --0.4% of arc length (contour perimeter)
            # #     epsilon = 0.004 * cv2.arcLength(cnt, True)
            # #     approx = cv2.approxPolyDP(cnt, epsilon, True)
            # #     # cv2.drawContours(outer_cont, [approx], 0, (0, 0, 0), 2, cv2.CV_AA)
            # #     # cv2.imwrite(final_image_path + image_name[0:-4] + '_4_' + str(count) + '_contour.png', outer_cont)
            # #     largest_perimeter = perimeter
            # #     contour_point_list = approx
        return contour_point_list

    def find_contour_lines(self,contour_point_list):
        contour_lines = []
        first_element =0
        for l, level1 in enumerate(contour_point_list):
            for level2 in level1:
                x, y = level2
                # -----based on cordinates get contour lines and append to image_contour_lines
                if l == 0:
                    contour_lines.append([])
                    contour_lines[l].append([x, y])
                    first_element = [x, y]
                elif l == len(contour_point_list) - 1:
                    contour_lines.append([])
                    contour_lines[l].append([x, y])
                    contour_lines[l - 1].append([x, y])
                    contour_lines[l].append(first_element)
                else:
                    contour_lines.append([])
                    contour_lines[l].append([x, y])
                    contour_lines[l - 1].append([x, y])

        return contour_lines

    def combine_lines(self,line_list):
        #--create 'voronoi_lines[]' to store non repeating voronoi lines
        non_repeated_lines = []
        line_obj = line()
        #--compare
        for l1,line1 in enumerate(line_list):
            #--add first element
            if l1==0:
                non_repeated_lines.append(line1)
            else:
                x1, y1 = line1[0]
                x2, y2 = line1[1]
                l1gradient_x, l1gradient_y = line_obj.find_line_gradient(x1,y1,x2,y2)
                l1gradx, l1grady = abs(round(l1gradient_x,2)), abs(round(l1gradient_y,2))
                line_delete = False
                #---check if 'voronoi_lines[]' already have a line similar to 'contour_lines[]'
                for l2,line2 in enumerate(non_repeated_lines):
                    x3, y3 = line2[0]
                    x4, y4 = line2[1]
                    #--check if 2 lines are close to each other
                    if (abs(x1-x3)<12 and abs(y1-y3)<12 and abs(x2-x4)<12 and abs(y2-y4)<12) or (abs(x1-x4)<12 and abs(y1-y4)<12 and abs(x2-x3)<12 and abs(y2-y3)<12):
                        l2gradient_x, l2gradient_y = line_obj.find_line_gradient(x3, y3, x4, y4)
                        l2gradx, l2grady = abs(round(l2gradient_x,2)), abs(round(l2gradient_y,2))
                        #---check if 2 lines are similar in gradient
                        if abs(l2gradx-l1gradx)<0.02 and abs(l2grady-l1grady)<0.02:
                            line_delete = True
                            break
                if line_delete == False:
                    non_repeated_lines.append(line1)
        return non_repeated_lines

    def improve_contours(self,contour,avg_door_width):
        #define threshold values for
        # 1. minimum possible line length to have
        min_line_length = int(avg_door_width/20)-2
        # 2.minimum possible x and y differences(used to straighten lines)
        # min_x_difference = int(avg_door_width/15)
        # min_y_difference = int(avg_door_width/15)
        min_x_difference = int(avg_door_width/15)-2
        min_y_difference = int(avg_door_width/15)-2
        contour_points  = []
        for l, line_level1 in enumerate(contour):
            for line_level2 in line_level1:
                contour_points.append(line_level2)
                break

        cont_lines_to_pre_process,first_element = [],0
        for l,cordinate in enumerate(contour_points):
            x,y = cordinate
            # print x,y,l
            if l==0:
                cont_lines_to_pre_process.append([])
                cont_lines_to_pre_process[l].append([x,y])
                first_element=[x,y]
            elif l==len(contour_points)-1:
                cont_lines_to_pre_process.append([])
                cont_lines_to_pre_process[l].append([x,y])
                cont_lines_to_pre_process[l-1].append([x,y])
                cont_lines_to_pre_process[l].append(first_element)
            else:
                cont_lines_to_pre_process.append([])
                cont_lines_to_pre_process[l].append([x,y])
                cont_lines_to_pre_process[l-1].append([x,y])

        cont_points_after_pre_process = []
        discarded_lines_total_length = 0
        for l,each_line in enumerate(cont_lines_to_pre_process):
            x1,y1 = each_line[0]
            x2,y2 = each_line[1]
            line_length = math.hypot(x2 - x1, y2 - y1)
            # print each_line,line_length,'discarded_lines_total_length-',discarded_lines_total_length
            if l ==0:
                cont_points_after_pre_process.append(each_line[0])
                if line_length<=min_line_length:
                    discarded_lines_total_length = discarded_lines_total_length+line_length
                    continue
                else:
                    cont_points_after_pre_process.append(each_line[1])
            elif l== len(cont_lines_to_pre_process)-1:
                earlier_discarded_length = 0
                if line_length<=min_line_length:
                    earlier_discarded_length = discarded_lines_total_length
                    discarded_lines_total_length = discarded_lines_total_length+line_length
                    del cont_points_after_pre_process[-1]
                if earlier_discarded_length> min_line_length:
                    cont_points_after_pre_process.append(each_line[0])
                cont_points_after_pre_process.append(each_line[1])
            else:
                if line_length<=min_line_length:
                    discarded_lines_total_length = discarded_lines_total_length+line_length
                else:
                    if discarded_lines_total_length > min_line_length:
                        cont_points_after_pre_process.append(each_line[0])
                        cont_points_after_pre_process.append(each_line[1])
                    else:
                        cont_points_after_pre_process.append(each_line[1])
                        # print 'Other Inserted ',line_length,each_line[1],discarded_lines_total_length
                        discarded_lines_total_length = 0

        final_contour_points,i_point,last_point = [],0,0
        line_obj = line()
        # contour_image = ~(np.zeros((self.height,self.width,3), np.uint8))
        line_gradient_x , line_gradient_y = 0,0
        line_gradient_empty = False
        for i,current_point in enumerate(cont_points_after_pre_process):
            x1, y1 = cont_points_after_pre_process[i-1]
            x2,y2 = current_point
            # print 'current_point',current_point
            if i==0:
                final_contour_points.append(current_point)
                # print '1ST append',current_point
            elif i==len(cont_points_after_pre_process)-1:
                final_contour_points.append(current_point)
                # print 'LAST append',current_point
            else:
                if i==1 or line_gradient_empty:
                    current_line_length = math.hypot(x2 - x1, y2 - y1)
                    if current_line_length>min_line_length:
                        current_gradient_x,current_gradient_y = line_obj.find_line_gradient(x1,y1,x2,y2)
                        line_gradient_x, line_gradient_y = current_gradient_x,current_gradient_y
                        line_gradient_empty = False
                    else:
                        line_gradient_empty= True
                        continue

                x3,y3 = cont_points_after_pre_process[i+1]
                next_gradient_x,next_gradient_y = line_obj.find_line_gradient(x2,y2,x3,y3)
                x_difference = abs(line_gradient_x-next_gradient_x)
                y_difference = abs(line_gradient_y-next_gradient_y)
                #--check if gradients are different
                if ~(x_difference < 0.05 and y_difference< 0.05):
                    next_line_length = math.hypot(x3 - x2, y3 - y2)
                    if next_line_length>min_line_length:
                        # print 'NORMAL append ',current_point
                        final_contour_points.append(current_point)
                        line_gradient_x, line_gradient_y = next_gradient_x,next_gradient_y

        #---to straighten lines that have long length but 1px differences
        new_contour_points  = []
        for p, current_point in enumerate(final_contour_points):
            point_changed = False
            if p == 0:
                new_contour_points.append(current_point)
            else:
                current_x, current_y = current_point
                last_x, last_y = new_contour_points[-1]
                x_difference = abs(last_x-current_x)
                y_difference = abs(last_y-current_y)
                if x_difference<min_x_difference:
                    new_contour_points.append([last_x,current_y])
                    point_changed= True
                elif y_difference<min_y_difference:
                    new_contour_points.append([current_x,last_y])
                    point_changed= True
                else:
                    new_contour_points.append(current_point)

                if p == len(final_contour_points)-1:
                    # if contID==9:
                    #     print 'BEFORE Loop'
                    #     for row in new_contour_points:
                    #         print row
                    first_element_changed = False
                    if point_changed:
                        new_contour_points[0]=new_contour_points[-1]
                        first_element_changed = True
                    if first_element_changed:
                        last_element = 0
                        # if contID==9:
                        #     print 'test 1--'
                        #     for row in new_contour_points:
                        #         print row
                        for p1,current_point in enumerate(new_contour_points):
                            if p1>0:
                                # print '+++++',current_point
                                current_x, current_y = current_point
                                last_x, last_y = new_contour_points[last_element]
                                x_difference = abs(last_x-current_x)
                                y_difference = abs(last_y-current_y)
                                if x_difference<min_x_difference:
                                    new_contour_points[p1]=[last_x,current_y]
                                    last_element += 1
                                elif y_difference<min_y_difference:
                                    new_contour_points[p1]=[current_x,last_y]
                                    last_element += 1
                                else:
                                    # if contID==9:
                                    #     print 'BREAK from', current_point
                                    break
                                # if contID==9:
                                #     print 'test RRRRR--'
                                #     for row in new_contour_points:
                                #         print row


        # if contID==0:
        #     print '----------new_contour_points----------'
        #     for row in new_contour_points:
        #         print row

        #---cehck if some lines are basically the same line broken to segments

        # new_contour_points = final_contour_points
        image_contour_lines,first_element = [],0
        for l,cordinate in enumerate(new_contour_points):
            x,y = cordinate
            if l==0:
                image_contour_lines.append([])
                image_contour_lines[l].append([x,y])
                first_element=[x,y]
            elif l==len(final_contour_points)-1:
                image_contour_lines[l-1].append([x,y])
                if ~(x==first_element[0] and y==first_element[1]):
                    image_contour_lines.append([])
                    image_contour_lines[l].append([x,y])
                    image_contour_lines[l].append(first_element)
            else:
                image_contour_lines.append([])
                image_contour_lines[l].append([x,y])
                image_contour_lines[l-1].append([x,y])
                # last_element = [x,y]

        # if contID==0:
        #     print '---------BEFORE image_contour_lines------------'
        #     for each in image_contour_lines:
        #         x1,y1 = each [0]
        #         x2,y2 = each[1]
        #         print each, math.hypot(x2 - x1, y2 - y1)

        #--if starting line and end line has same gradient, connect them as one line
        line_number = 0
        while line_number<len(image_contour_lines)-1:
            line1_start, line1_end = image_contour_lines[line_number]
            line2_start, line2_end = image_contour_lines[line_number+1]
            x1,y1 = line1_start
            x2,y2 = line1_end
            x3,y3 = line2_start
            x4,y4 = line2_end
            line1_gradient_x,line1_gradient_y = line_obj.find_line_gradient(x1,y1,x2,y2)
            line2_gradient_x,line2_gradient_y = line_obj.find_line_gradient(x3,y3,x4,y4)
            x_difference = abs(line1_gradient_x-line2_gradient_x)
            y_difference = abs(line1_gradient_y-line2_gradient_y)
            if x_difference < 0.05 and y_difference< 0.05:
                image_contour_lines[line_number] = [line1_start,line2_end]
                del image_contour_lines[line_number+1]
                line_number -= 1
            else:
                line_number += 1

        #--- special case for first and last elements
        line1_start, line1_end = image_contour_lines[0]
        x1,y1 = line1_start
        x2,y2 = line1_end
        line1_gradient_x,line1_gradient_y = line_obj.find_line_gradient(x1,y1,x2,y2)
        line2_start, line2_end = image_contour_lines[-1]
        x3,y3 = line2_start
        x4,y4 = line2_end
        line2_gradient_x,line2_gradient_y = line_obj.find_line_gradient(x3,y3,x4,y4)
        x_difference = abs(line1_gradient_x-line2_gradient_x)
        y_difference = abs(line1_gradient_y-line2_gradient_y)
        if x_difference < 0.05 and y_difference< 0.05:
            del image_contour_lines[0]
            image_contour_lines[-1] = [line2_start,line1_end]

        contour_points_final = []
        for row_num,each_line in enumerate(image_contour_lines):
            contour_points_final.append([each_line[0]])
        contour_points_final_to_insert = np.array(contour_points_final)

        all_contour_points = contour_points_final_to_insert

        return all_contour_points



    def find_voronoi_lines_houghlines(self,img_voronoi,final_image_path,image_name):
        result_image = img_voronoi.copy()
        # #--b,g,r
        # lower = np.array([0, 0, 250], dtype="uint8")
        # upper = np.array([0, 0, 255], dtype="uint8")
        # mask = cv2.inRange(img_voronoi, lower, upper)
        # img_voronoi = cv2.bitwise_and(img_voronoi, img_voronoi, mask=mask)

        gray = cv2.cvtColor(img_voronoi, cv2.COLOR_BGR2GRAY)
        # cv2.imwrite(final_image_path + image_name + '_2_gray_voronoi.png', gray)
        edges = cv2.Canny(gray, 50, 150, apertureSize=3)
        cv2.imwrite(final_image_path + image_name + '3_edges_voronoi.png', edges)

        #-- method 01
        lines = cv2.HoughLinesP(image=edges, rho=0.01, theta=np.pi / 180,
                                threshold=3, lines=np.array([]),
                                minLineLength=150, maxLineGap=100)
        if lines is not None:
            a,b,c = lines.shape
            print a,b,c
            for x1,y1,x2,y2 in lines[0]:
                cv2.line(result_image, (x1,y1),(x2,y2), (255, 0, 0), 3, cv2.cv.CV_AA)
            cv2.imwrite(final_image_path + image_name[0:-4] + 'lines.png', result_image)

                #     for i in range(a):
        #         print i
        #         cv2.line(result_image, (lines[i][0][0], lines[i][0][1]), (lines[i][0][2],
        #                                                          lines[i][0][3]), (255, 0, 0), 3, cv2.cv.CV_AA)
        # cv2.imwrite(final_image_path + image_name[0:-4] + 'lines.png', result_image)

        #--method 02
        # lines = cv2.HoughLines(edges, 1, np.pi / 180, 200)
        # print 'lines',lines
        # print 'lines[0]',lines[0]
        # print lines.shape
        # print lines[0].shape
        # count = 0
        # for rho, theta in lines[0]:
        #     copy_image = result_image.copy()
        #     a = np.cos(theta)
        #     b = np.sin(theta)
        #     x0 = a * rho
        #     y0 = b * rho
        #     x1 = int(x0 + 100 * (-b))
        #     y1 = int(y0 + 100 * (a))
        #     x2 = int(x0 - 100 * (-b))
        #     y2 = int(y0 - 100 * (a))
        #
        #     cv2.line(copy_image, (x1, y1), (x2, y2), (255, 0, 0), 2)
        #     cv2.imwrite(final_image_path + image_name[0:-4] +'_'+str(count)+ 'lines.png', copy_image)
        #     print count, rho, theta
        #     count = count+1