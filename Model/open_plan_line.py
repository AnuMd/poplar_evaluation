__author__ = 'anu'

import cv2, math,copy,os,traceback
import xml.etree.cElementTree as ET
import numpy as np
from PIL import Image
from operator import itemgetter

class line:

    def find_intersections(self,output_directory, img_name, xml_file,voronoi_cordinates):
        #prepare index and empty array to store line details
        enum=0
        line_data=[]

        #Load extended contour line data from xml file
        tree_read = ET.parse(xml_file)
        root = tree_read.getroot()
        for edge in root.findall('edge'):
            for points in edge.findall('points'):
                x1 = int(points.find('ref_x').text)
                y1 = int(points.find('ref_y').text)
                x2 = int(points.find('x1').text)
                y2 = int(points.find('y1').text)
                #get m,c for horizontal line
                if y1==y2:
                    m = 0
                    c = y1
                #get m,c for vertical line
                elif x1==x2:
                    m = 'a'
                    c = 'a'
                #get m,c for other lines
                else:
                    m =(y1-y2)/float(x1-x2)
                    c = y1-(m*x1)
                #insert all line [starting cordinate], [ending cordinate], m and c
                line_data.append([])
                line_data[enum].append([x1,y1])
                line_data[enum].append([x2,y2])
                line_data[enum].append(m)
                line_data[enum].append(c)
            enum = enum+1
        #get length of linedata array to append voronoi lines
        current_length = len(line_data)

        #get voronoi lines
        for cordinate in voronoi_cordinates:
            x1,y1 = cordinate[0]
            x2,y2 = cordinate[1]
            #get m,c for horizontal line
            if y1==y2:
                m = 0
                c = y1
            #get m,c for vertical line
            elif x1==x2:
                m = 'a'
                c = 'a'
            #get m,c for other lines
            else:
                m =(y1-y2)/float(x1-x2)
                c = y1-(m*x1)
            #insert all line [starting cordinate], [ending cordinate], m and c
            line_data.append([])
            line_data[current_length].append([x1,y1])
            line_data[current_length].append([x2,y2])
            line_data[current_length].append(m)
            line_data[current_length].append(c)
            current_length=current_length+1

        # empty_new_image = ~(np.zeros((self.height,self.width,3), np.uint8))
        # for line in line_data:
        #     x1,y1 = line[0]
        #     x2,y2 = line[1]
        #     cv2.line(empty_new_image,(x1,y1),(x2,y2),(0,0,0),2,cv2.cv.CV_AA)
        # cv2.imwrite(output_directory+'OpenPlan_Areas/'+img_name+"_step_09_2.png",empty_new_image)

        #------find intersections
        all_line_intersections = []
        #get data from line_data
        for l,line in enumerate(line_data):
            for l2,line2 in enumerate(line_data):
                #if lines from both arrays are not the same
                if l != l2:
                    m1 = line[2]
                    m2 = line2[2]
                    c1 = line[3]
                    c2 = line2[3]
                    x1,y1 = line[0]
                    x2,y2 = line2[0]

                    #if not paralell lines find intersections
                    if m1!= m2:
                        #for vertical lines
                        if (m1 == 'a') or (m2 == 'a'):
                            if m1 == 'a':
                                x = x1
                                y = (m2*x)+c2
                            else:
                                x= x2
                                y = (m1*x)+c1
                            all_line_intersections.append([int(x),int(y)])

                        #for horizontal lines
                        elif (m1 == 0) or (m2 == 0):
                            if m1 == 0:
                                y = y1
                                if m2 == 'a':
                                    x = x2
                                else:
                                    x = (y/m2)-(c2/m2)
                            else:
                                y = y2
                                if m1 == 'a':
                                    x = x1
                                else:
                                    x = (y/m1)-(c1/m1)
                            all_line_intersections.append([int(x),int(y)])

                        #for all other lines
                        else:
                            x = (c2-c1)/(m1-m2)
                            y = (m1*x)+c1
                            all_line_intersections.append([int(x),int(y)])

        intersections_in_contour = []
        open_plan_image = cv2.imread(output_directory+'OpenPlan_Areas/NEWContours_'+img_name+'.png',cv2.IMREAD_GRAYSCALE)
        ret,thresh = cv2.threshold(open_plan_image,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)
        for c, cnt in enumerate(contours):
            if c == 0:
                for intersection in all_line_intersections:
                    x1, y1 = intersection
                    dist = cv2.pointPolygonTest(cnt,(x1,y1),True)
                    if dist > -5:
                        intersections_in_contour.append([x1,y1])


        # height,width = open_plan_image.shape
        # empty_new_image = ~(np.zeros((height,width,3), np.uint8))
        for point in intersections_in_contour:
            x1,y1 = point
            # print(x1,y1)
            cv2.circle(open_plan_image,(x1,y1),10, (0, 0, 255), -1)
        # cv2.drawContours(empty_new_image, [screenCnt], -1, (0, 255, 0), 3)
        cv2.imwrite(output_directory+'OpenPlan_Areas/'+img_name+"_step_09_2.png",open_plan_image)
        return intersections_in_contour

    def find_contour_intersections(self,x1,y1,x2,y2,x3,y3,x4,y4):
        #-------for line1
        #get m,c for horizontal line
        if y1==y2:
            m1 = 0
            c1 = y1
        #get m,c for vertical line
        elif x1==x2:
            m1 = 'a'
            c1 = 'a'
        #get m,c for other lines
        else:
            m1 =(y1-y2)/float(x1-x2)
            c1 = y1-(m1*x1)

        #------for line2
        #get m,c for horizontal line
        if y3==y4:
            m2 = 0
            c2 = y3
        #get m,c for vertical line
        elif x3==x4:
            m2 = 'a'
            c2 = 'a'
        #get m,c for other lines
        else:
             m2 = (y3-y4)/float(x3-x4)
             c2 = y3-(m2*x3)

        intersect_x,intersect_y = 0,0
        # print (m1,m2,c1,c2,x1,y1,x2,y2,'--',x3,y3,x4,y4)
        #------find intersections

        #if not paralell lines find intersections
        if m1!= m2:
            #for vertical lines
            if (m1 == 'a') or (m2 == 'a'):
                if m1 == 'a':
                    intersect_x = x1
                    intersect_y = (m2*intersect_x)+c2
                else:
                    intersect_x= x3
                    intersect_y = (m1*intersect_x)+c1

            #for horizontal lines
            elif (m1 == 0) or (m2 == 0):
                if m1 == 0:
                    intersect_y = y1
                    if m2 == 'a':
                        intersect_x = x3
                    else:
                        intersect_x = (intersect_y/m2)-(c2/m2)
                else:
                    intersect_y = y3
                    if m1 == 'a':
                        intersect_x = x1
                    else:
                        intersect_x = (intersect_y/m1)-(c1/m1)

            #for all other lines
            else:
                intersect_x = (c2-c1)/(m1-m2)
                intersect_y = (m1*intersect_x)+c1
        # print intersect_x,intersect_y

        return int(intersect_x), int(intersect_y)

    def check_line_inside_contour_fixed_distance(self,x1,y1,x2,y2,cnt,):
        p = 5
        x_pivot, y_pivot= x1,y1
        x0 = (x1+x2)/2
        y0 = (y1+y2)/2
        max_x = max(x1,x2)
        min_x = min(x1,x2)
        max_y = max(y1,y2)
        min_y = min(y1,y2)
        num_of_points_outside_contour = 0
        if x2!=x1:
            m = (y2-y1)/(x2-x1)
        # total_number_of_points = 0
        while x0>=min_x and x0<=max_x and y0>=min_y and y0<=max_y:
        #-----find x0 and y0 with a distance of P between them
            if x1!=x2:
                x0 = x_pivot+p/math.sqrt(math.pow(m,2)+1)
                y0 = y_pivot+m*(x0-x_pivot)
            else:
                x0 = x_pivot
                y0 = y_pivot+p

            if ~(x0>=min_x and x0<=max_x and y0>=min_y and y0<=max_y):
                x_pivot,y_pivot = x2,y2
                continue

            #-----check if x0,y0 is outside contour
            dist = cv2.pointPolygonTest(cnt,(x0,y0),False)
            if dist==-1:
                num_of_points_outside_contour= num_of_points_outside_contour+1
            # total_number_of_points = total_number_of_points+1
            x_pivot, y_pivot= x0,y0
        return num_of_points_outside_contour

    def check_line_inside_contour(self,x1,y1,x2,y2,cnt):
        #--changed on 19/03/2017
        line_length = math.hypot(x2-x1,y2-y1)
        num_of_points_to_check = int(line_length/3)
        num_of_points_outside_contour =0
        for a in range(1,num_of_points_to_check):
            m = a
            n = num_of_points_to_check-a

            x0 = ((n*x1)+(m*x2))/(m+n)
            y0 = ((n*y1)+(m*y2))/(m+n)

            dist = cv2.pointPolygonTest(cnt,(x0,y0),False)
            if dist == -1:
                num_of_points_outside_contour= num_of_points_outside_contour+1
                # if num_of_points_outside_contour>2:
                #     break

        return num_of_points_outside_contour

    def check_line_inside_contour_special(self,x1,y1,x2,y2,cnt):
        extension_length = math.hypot(x2-x1,y2-y1)
        num_of_points_to_check = int(extension_length*10)
        num_of_points_outside_contour =0
        # points_outside_contour = False
        for a in range(1,num_of_points_to_check):
            m = a
            n = num_of_points_to_check-a

            x0 = ((n*x1)+(m*x2))/(m+n)
            y0 = ((n*y1)+(m*y2))/(m+n)

            dist = cv2.pointPolygonTest(cnt,(x0,y0),False)
            if dist == -1:
                num_of_points_outside_contour= num_of_points_outside_contour+1

            if num_of_points_outside_contour>11:
                break

        return num_of_points_outside_contour

    def check_line_inside_contour_return_num_of_points(self,x1,y1,x2,y2,cnt):
        num_of_points_to_check = 100
        # num_of_points_outside_contour =0
        point_outside_contour = False
        num_of_points_outside_contour = 0
        for a in range(1,num_of_points_to_check):
            m = a
            n = num_of_points_to_check-a

            x0 = ((n*x1)+(m*x2))/(m+n)
            y0 = ((n*y1)+(m*y2))/(m+n)

            dist = cv2.pointPolygonTest(cnt,(x0,y0),False)

            if dist==-1:
                # num_of_points_outside_contour= num_of_points_outside_contour+1
                num_of_points_outside_contour= num_of_points_outside_contour+1

        return num_of_points_outside_contour

    def line_color_check(self,line_cordinate_1,line_cordinate_2,text_cordinate_1,text_cordinate_2):
        # image_path = '/home/ub/Documents/florence_v3/test_openplan/plan1.png'
        image_path = '/home/ub/Documents/florence_v3/test_openplan/plan2.png'
        image = Image.open(image_path)
        pixels = image.load()

        x1,y1 = line_cordinate_1
        x2,y2 = line_cordinate_2

        num_of_points_to_check = 50
        color_points = 0
        for a in range(1,num_of_points_to_check):
            m = a
            n = num_of_points_to_check-a

            x0 = ((n*x1)+(m*x2))/(m+n)
            y0 = ((n*y1)+(m*y2))/(m+n)

            color = pixels[int(x0),int(y0)]
            area_1 = pixels[text_cordinate_1[0],text_cordinate_1[1]]
            area_2 = pixels[text_cordinate_2[0],text_cordinate_2[1]]

            area1_color = ((color[0] == area_1[0]) and (color[1] == area_1[1]) and (color[2] == area_1[2]))
            area2_color = ((color[0] == area_2[0]) and (color[1] == area_2[1]) and (color[2] == area_2[2]))
            border_color= ((color[0] == 0) and (color[1] == 0) and (color[2] == 0))

            if area1_color or area2_color or border_color:
                color_points = color_points+1
        return color_points

    def find_mc_of_line(self,x1,y1,x2,y2):
        #get m,c for horizontal line
        if y1==y2:
            m = 0
            c = y1
        #get m,c for vertical line
        elif x1==x2:
            m = 'a'
            c = 'a'
        #get m,c for other lines
        else:
            m =(y1-y2)/float(x1-x2)
            c = y1-(m*x1)
        #--returns gradient as tan value of angle
        return m,c

    def find_line_gradient(self,x1,y1,x2,y2):
        a = x1-x2
        b = y1-y2
        np.seterr(divide='ignore', invalid='ignore')
        gradient_x = a/math.sqrt(math.pow(a,2)+math.pow(b,2))
        gradient_y = b/math.sqrt(math.pow(a,2)+math.pow(b,2))
        #--returns gradient as x_differnce and y_difference
        return gradient_x,gradient_y

    def find_points_next_to_end_points_in_line(self,x1,y1,x2,y2,line_length,contour_to_check):
        n = -2
        m = line_length+2
        x0 = ((n*x1)+(m*x2))/(m+n)
        y0 = ((n*y1)+(m*y2))/(m+n)

        x00 = ((m*x1)+(n*x2))/(m+n)
        y00 = ((m*y1)+(n*y2))/(m+n)

        if cv2.pointPolygonTest(contour_to_check,(x0,y0),False)==1:
            point_inside_line= True
        elif cv2.pointPolygonTest(contour_to_check,(x00,y00),False)==1:
            point_inside_line= True
        else:
            point_inside_line= False

        # return point_inside_line, int(x0),int(y0),int(x00),int(y00)
        return point_inside_line

    def special_text_line_number_finder(self,voronoi_scores,edge_extension_scores,improved_voronoi_scores,seperated_voronoi_lines,improved_voronoi_lines,seperated_edge_lines,max_line,max_line_floor_plan,row_value):
        # print '----voronoi_scores----'
        # for row in voronoi_scores:
        #     print row
        #     print 'len(row[1])',len(row[1])
        # print '----edge_extension_scores----'
        # for row in edge_extension_scores:
        #     print row
        #     print 'len(row[1])',len(row[1])
        # print '----improved_voronoi_scores----'
        # for row in improved_voronoi_scores:
        #     print row
        #     print 'len(row[1])',len(row[1])

        priority_vornoi, priority_edge, priority_ivl =[],[],[]
        for r,vor_row in enumerate(voronoi_scores):
            if vor_row[0]==row_value:
                temp=[]
                for b,boundary in enumerate(vor_row[1]):
                    for score in boundary:
                        temp.append([[vor_row[0],b],score])
                priority_vornoi.append(temp)

        for r,edge_row in enumerate(edge_extension_scores):
            if edge_row[0]==row_value:
                temp = []
                for b,boundary in enumerate(edge_row[1]):
                    max_boundary_score = -10
                    max_score_line_details =[[0],-10]
                    for l, line_score in enumerate(boundary):
                        if line_score>max_boundary_score and line_score!=0:
                            max_score_line_details = [[edge_row[0],b,l],line_score]
                            max_boundary_score = line_score
                    temp.append(max_score_line_details)
                priority_edge.append(temp)

        for r,ivl_row in enumerate(improved_voronoi_scores):
            if ivl_row[0]==row_value:
                temp = []
                for b,boundary in enumerate(ivl_row[1]):
                    max_boundary_score = -10
                    max_score_line_details =[[0],-10]
                    for l, line_score in enumerate(boundary):
                        if line_score>max_boundary_score and line_score!=0:
                            max_score_line_details = [[ivl_row[0],b,l],line_score]
                            max_boundary_score = line_score
                    temp.append(max_score_line_details)
                priority_ivl.append(temp)

        # print 'priority_vornoi'
        # for row in priority_vornoi:
        #     print row
        # print 'priority_edge'
        # for row in priority_edge:
        #     print row
        # print 'priority_ivl'
        # for row in priority_ivl:
        #     print row
        # print '--------------------'

        voronoi_element_list,edge_element_list,improved_element_list = [],[],[]
        for r,row in enumerate(priority_vornoi):
            for b,boundary in enumerate(row):
                # print priority_edge[r][b][1],priority_ivl[r][b][1]
                if priority_edge[r][b][1]==-10 and priority_ivl[r][b][1]==-10:
                    voronoi_element_list.append(priority_vornoi[r][b][0])
                elif priority_edge[r][b][1]!=-10:
                    edge_element_list.append(priority_edge[r][b][0])
                elif priority_ivl[r][b][1]>priority_edge[r][b][1]:
                    improved_element_list.append(priority_ivl[r][b][0])
        # print voronoi_element_list,edge_element_list,improved_element_list

        # print '---voronoi_element_list---'
        # for row in seperated_voronoi_lines:
        #     print row
        # print '---edge_element_list---'
        # for row in edge_element_list:
        #     print row
        # print '---improved_element_list---'
        # for row in improved_element_list:
        #     print row
        # print '-----end------'

        #----find lines based on line type (V/E/IVL) and draw it +add to max line list
        for voronoi_element_number in voronoi_element_list:
            voronoi_line_row=[]
            for row in seperated_voronoi_lines:
                if row[0]==voronoi_element_number[0]:
                    voronoi_line_row=row[1]
                    break
            voronoi_boundary_scores = voronoi_line_row[voronoi_element_number[1]]
            for voronoi_max_line in voronoi_boundary_scores:
                cv2.line(max_line_floor_plan,(tuple(voronoi_max_line[0])),(tuple(voronoi_max_line[1])),(0,0,0),2,cv2.cv.CV_AA)
                max_line.append(voronoi_max_line)
        for edge_element_number in edge_element_list:
            edge_line_set=[]
            for row in seperated_edge_lines:
                if row[0]==edge_element_number[0]:
                    edge_line_set=row[1]
                    break
            edge_boundary_scores = edge_line_set[edge_element_number[1]]
            max_line1 = edge_boundary_scores[edge_element_number[2]]
            max_line_i=max_line1[0]
            cv2.line(max_line_floor_plan,(tuple(max_line_i[0])),(tuple(max_line_i[1])),(0,0,0),2,cv2.cv.CV_AA)
            max_line.append(max_line1[0])
        for improved_element_number in improved_element_list:
            improved_line_row=[]
            for row in improved_voronoi_lines:
                if row[0]==improved_element_number[0]:
                    improved_line_row=row[1]
                    break
            improved_boundary_scores = improved_line_row[improved_element_number[1]]
            improved_max_line_set = improved_boundary_scores[improved_element_number[2]]
            for improved_max_line in improved_max_line_set:
                cv2.line(max_line_floor_plan,(tuple(improved_max_line[0])),(tuple(improved_max_line[1])),(0,0,0),2,cv2.cv.CV_AA)
                max_line.append(improved_max_line)
        print '-------max_line-------'
        for row in max_line:
            print row
        return max_line,max_line_floor_plan


    def furniture_best_line_finder(self,voronoi_scores,edge_extension_scores,improved_voronoi_scores,seperated_voronoi_lines,improved_voronoi_lines,seperated_edge_lines,max_line,max_line_floor_plan,row_value):
        priority_vornoi, priority_edge, priority_ivl =[],[],[]
        for r,vor_row in enumerate(voronoi_scores):
            if vor_row[0]==row_value:
                temp=[]
                for b,boundary in enumerate(vor_row[1]):
                    for score in boundary:
                        temp.append([[vor_row[0],b],score])
                priority_vornoi.append(temp)

        for r,edge_row in enumerate(edge_extension_scores):
            if edge_row[0]==row_value:
                temp = []
                for b,boundary in enumerate(edge_row[1]):
                    max_boundary_score = -10
                    max_score_line_details =[[0],-10]
                    for l, line_score in enumerate(boundary):
                        if line_score>max_boundary_score and line_score!=0:
                            max_score_line_details = [[edge_row[0],b,l],line_score]
                            max_boundary_score = line_score
                    temp.append(max_score_line_details)
                priority_edge.append(temp)

        for r,ivl_row in enumerate(improved_voronoi_scores):
            if ivl_row[0]==row_value:
                temp = []
                for b,boundary in enumerate(ivl_row[1]):
                    max_boundary_score = -10
                    max_score_line_details =[[0],-10]
                    for l, line_score in enumerate(boundary):
                        if line_score>max_boundary_score and line_score!=0:
                            max_score_line_details = [[ivl_row[0],b,l],line_score]
                            max_boundary_score = line_score
                    temp.append(max_score_line_details)
                priority_ivl.append(temp)


        voronoi_element_list,edge_element_list,improved_element_list = [],[],[]
        for r,row in enumerate(priority_vornoi):
            for b,boundary in enumerate(row):
                # print priority_edge[r][b][1],priority_ivl[r][b][1]
                if priority_edge[r][b][1]==-10 and priority_ivl[r][b][1]==-10:
                    voronoi_element_list.append(priority_vornoi[r][b][0])
                elif priority_edge[r][b][1]!=-10:
                    edge_element_list.append(priority_edge[r][b][0])
                elif priority_ivl[r][b][1]>priority_edge[r][b][1]:
                    improved_element_list.append(priority_ivl[r][b][0])

        #----find lines based on line type (V/E/IVL) and draw it +add to max line list
        for voronoi_element_number in voronoi_element_list:
            voronoi_line_row=[]
            for row in seperated_voronoi_lines:
                if row[0]==voronoi_element_number[0]:
                    voronoi_line_row=row[1]
                    break
            voronoi_boundary_scores = voronoi_line_row[voronoi_element_number[1]]
            for voronoi_max_line in voronoi_boundary_scores:
                cv2.line(max_line_floor_plan,(tuple(voronoi_max_line[0])),(tuple(voronoi_max_line[1])),(0,0,0),2,cv2.cv.CV_AA)
                max_line.append(voronoi_max_line)
        for edge_element_number in edge_element_list:
            edge_line_set=[]
            for row in seperated_edge_lines:
                if row[0]==edge_element_number[0]:
                    edge_line_set=row[1]
                    break
            edge_boundary_scores = edge_line_set[edge_element_number[1]]
            max_line1 = edge_boundary_scores[edge_element_number[2]]
            max_line_i=max_line1[0]
            cv2.line(max_line_floor_plan,(tuple(max_line_i[0])),(tuple(max_line_i[1])),(0,0,0),2,cv2.cv.CV_AA)
            max_line.append(max_line1[0])
        for improved_element_number in improved_element_list:
            improved_line_row=[]
            for row in improved_voronoi_lines:
                if row[0]==improved_element_number[0]:
                    improved_line_row=row[1]
                    break
            improved_boundary_scores = improved_line_row[improved_element_number[1]]
            improved_max_line_set = improved_boundary_scores[improved_element_number[2]]
            for improved_max_line in improved_max_line_set:
                cv2.line(max_line_floor_plan,(tuple(improved_max_line[0])),(tuple(improved_max_line[1])),(0,0,0),2,cv2.cv.CV_AA)
                max_line.append(improved_max_line)

        return max_line,max_line_floor_plan




    def new_list_creation(self,seperated_voronoi_lines_copy,seperated_edge_lines_copy,improved_voronoi_lines_copy,seperated_voronoi_lines,seperated_edge_lines,improved_voronoi_lines,row_value):
        for row in seperated_voronoi_lines_copy:
            if row[0]==row_value:
                seperated_voronoi_lines.append(row)
        for row in seperated_edge_lines_copy:
            if row[0]==row_value:
                seperated_edge_lines.append(row)
        for row in improved_voronoi_lines_copy:
            if row[0]==row_value:
                improved_voronoi_lines.append(row)

        return seperated_voronoi_lines,seperated_edge_lines,improved_voronoi_lines


    def new_list_creation2(self,seperated_voronoi_lines_copy,boundaries_copy,seperated_voronoi_lines,boundaries,row_value):
        for row in seperated_voronoi_lines_copy:
            if row[0]==row_value:
                seperated_voronoi_lines.append(row)
        for row in boundaries_copy:
            if row[0]==row_value:
                boundaries.append(row)

        return seperated_voronoi_lines,boundaries


    def special_case_cupboard(self,image_contour_lines,text_bounding_boxes,text_words,text_words_row_num,output_path, max_line_floor_plan,boundaries):
        #-----measure distance from text to each contour line
        distance_data = []
        text_data = []

        for t_row_num,t_row in enumerate(text_words):
            if t_row_num==text_words_row_num:
                # print t_row,text_bounding_boxes[t_row_num]
                #--- store text data for later use
                text_data.append(t_row[0])
                text_data.append(t_row[1])
                text_data.append(text_bounding_boxes[t_row_num])

                for contour_line in image_contour_lines:
                    #find minimum distance to each line from PL
                    distance_from_text = self.find_closest_valid_distance(contour_line,t_row[1])
                    x1,y1 = contour_line[0]
                    x2,y2 = contour_line[1]

                    gradient_x,gradient_y = self.find_line_gradient(x1,y1,x2,y2)
                    distance_data.append([contour_line,distance_from_text,[gradient_x,gradient_y]])

        #---find text labels connected to 'PL'
        other_texts = []
        for boundary_row in boundaries:
            if boundary_row[0]==text_words_row_num:
                if len(boundary_row[1][0])==1:
                    element_number = boundary_row[1][0][0]
                    other_texts.append(text_words[element_number][0])
                    other_texts.append(text_bounding_boxes[element_number])


        # print '---distance_data---'
        # for row in distance_data:
        #     print row
        #-------sort distances
        # print '---distance_data_sorted---'
        distance_data_sorted = sorted(distance_data,key=itemgetter(1))
        # for row in distance_data_sorted:
        #     print row
        #-------select only the 3 that is closest : argument is any awkward shapes won't come in since in those cases edge extension wont be empty
        selected_line_data = distance_data_sorted[:3]
        # print '---selected_line_data---'
        # for row in selected_line_data:
        #     print row
        #----- select the line to draw a parallel line for - 'line_to_focus'


        #---case 01: Out of 3 lines, find the 2 lines that are parallel and select line that  is not
        line_to_focus,other_lines = [],[]
        gradient_x0,gradient_y0 = selected_line_data[0][2]
        gradient_x1,gradient_y1 = selected_line_data[1][2]
        gradient_x2,gradient_y2 = selected_line_data[2][2]
        x_difference_01 = abs(abs(gradient_x0)-abs(gradient_x1))
        y_difference_01 = abs(abs(gradient_y0)-abs(gradient_y1))
        x_difference_02 = abs(abs(gradient_x0)-abs(gradient_x2))
        y_difference_02 = abs(abs(gradient_y0)-abs(gradient_y2))
        x_difference_12 = abs(abs(gradient_x1)-abs(gradient_x2))
        y_difference_12 = abs(abs(gradient_y1)-abs(gradient_y2))
        if x_difference_01 < 0.05 and y_difference_01< 0.05:
            other_lines.append(selected_line_data[0][0])
            other_lines.append(selected_line_data[1][0])
            line_to_focus=[selected_line_data[2][0],selected_line_data[2][2]]
        elif x_difference_02 < 0.05 and y_difference_02< 0.05:
            other_lines.append(selected_line_data[0][0])
            other_lines.append(selected_line_data[2][0])
            line_to_focus=[selected_line_data[1][0],selected_line_data[1][2]]
        elif x_difference_12 < 0.05 and y_difference_12< 0.05:
            other_lines.append(selected_line_data[1][0])
            other_lines.append(selected_line_data[2][0])
            line_to_focus=[selected_line_data[0][0],selected_line_data[0][2]]
        # print 'end of case 01', 'line_to_focus',line_to_focus
        # print 'end of case 01', 'other_lines',other_lines


        #---case 02: if parallel lines don't exist
        #--take the line closest to text
        if len(line_to_focus)==0:
            line_to_focus,other_lines = [],[]
            if selected_line_data[0][1]!=selected_line_data[1][1]:
                line_to_focus=[selected_line_data[0][0],selected_line_data[0][2]]
                other_lines.append(selected_line_data[1][0])
                other_lines.append(selected_line_data[2][0])
        # print 'end of case 02', 'line_to_focus',line_to_focus
        # print 'end of case 02', 'other_lines',other_lines


        #---case 03: if parallel lines don't exist
        #1.find points of line between placard and bedroom
        #2. find intersecting points of that line with contour lines
        #3. find intersection point closest to text
        #4. select that line as 'line_to_focus'
        line_distance_data = []
        if len(line_to_focus)==0:
            line_to_focus,other_lines = [],[]
            for each_other_text in other_texts:
                # print 'each_other_text',each_other_text
                #--acccess bedroom list
                if each_other_text[0]=='bedroom' or each_other_text[0]=='chambre':
                    #1.find points of line between placard and bedroom
                    tx1,ty1 = each_other_text[1]
                    #--placard text cordinate
                    tx2,ty2 = text_data[1]
                    for c,contour_line in enumerate(selected_line_data):
                        m = contour_line[2]
                        x1,y1 = contour_line[0][0]
                        x2,y2 = contour_line[0][1]
                        max_x = max(x1,x2)
                        min_x = min(x1,x2)
                        max_y = max(y1,y2)
                        min_y = min(y1,y2)
                        #2. find intersecting points of that line with contour lines
                        intersect_x, intersect_y = self.find_contour_intersections(x1,y1,x2,y2,tx1,ty1,tx2,ty2)
                        if (intersect_x>min_x and intersect_x<max_x and intersect_y>min_y and intersect_y<max_y):
                            distance_from_text = abs((y2-y1)*tx2-(x2-x1)*ty2+x2*y1-y2*x1)/math.sqrt(math.pow((y2-y1),2)+math.pow((x2-x1),2))
                            line_distance_data.append([contour_line[0],m,distance_from_text])
            #3. find intersection point closest to text
            line_distance_data_sorted = sorted(line_distance_data,key=itemgetter(1))
            #4. select that line as 'line_to_focus'
            for l,each_line in enumerate(line_distance_data_sorted):
                if l==0:
                    line_to_focus=[each_line[0],each_line[1]]
                else:
                    other_lines.append(each_line[0])
        # print 'end of case 03', 'line_to_focus',line_to_focus
        # print 'end of case 03', 'other_lines',other_lines

        #-- after all cases find partitioning line
        #----1. select the bounding box point that is furthest from 'line_to_focus'
        #------2. find two points for new line (find only one since we already have 1)
        #------3. find intersection points with other two contour lines
        bb_p1_x,bb_p1_y,bb_p2_x,bb_p2_y =  text_data[2]
        focus_x1,focus_y1 = line_to_focus[0][0]
        focus_x2,focus_y2 = line_to_focus[0][1]
        distance_from_bb_p1 = abs((focus_y2-focus_y1)*bb_p1_x-(focus_x2-focus_x1)*bb_p1_y+focus_x2*focus_y1-focus_y2*focus_x1)/math.sqrt(math.pow((focus_y2-focus_y1),2)+math.pow((focus_x2-focus_x1),2))
        distance_from_bb_p2 = abs((focus_y2-focus_y1)*bb_p2_x-(focus_x2-focus_x1)*bb_p2_y+focus_x2*focus_y1-focus_y2*focus_x1)/math.sqrt(math.pow((focus_y2-focus_y1),2)+math.pow((focus_x2-focus_x1),2))
        if distance_from_bb_p1>distance_from_bb_p2:
            point_furthest_from_line = [bb_p1_x,bb_p1_y]
        else:
            point_furthest_from_line = [bb_p2_x,bb_p2_y]

        #------2. find two points for new line (find only one since we already have 1)
        x1,y1 = point_furthest_from_line
        x_gradient,y_gradient = line_to_focus[1]
        m_selected_contour_line = y_gradient/x_gradient
        #---for vertical lines
        if x_gradient==0:
            x2 = x1
            y2 = 1
        #-- for all other lines
        else:
            x2 = 1
            y2 = m_selected_contour_line+y1-m_selected_contour_line*x1

        #------3. find intersection points with other two contour lines
        x3,y3,x4,y4,x5,y5,x6,y6 = 0,0,0,0,0,0,0,0
        # print 'other_lines',other_lines
        for r,each_line in enumerate(other_lines):
            if r == 0:
                x3,y3 = each_line[0]
                x4,y4 = each_line[1]
            else:
                x5,y5 = each_line[0]
                x6,y6 = each_line[1]
        # print '1: ',x1,y1,x2,y2
        # print '3 nd 4',x3,y3,x4,y4
        # print '5 ## 6',x5,y5,x6,y6
        intersect_x1, intersect_y1 = self.find_contour_intersections(x1,y1,x2,y2,x3,y3,x4,y4)
        intersect_x2, intersect_y2 = self.find_contour_intersections(x1,y1,x2,y2,x5,y5,x6,y6)

        #-- draw on max_line image
        cv2.line(max_line_floor_plan,(intersect_x1, intersect_y1),(intersect_x2, intersect_y2),(0,0,0),2,cv2.cv.CV_AA)
        #-- send data to be processed in next stage
        max_line = [[[intersect_x1, intersect_y1],[intersect_x2, intersect_y2]]]
        # print 'end of PL: ',max_line
        return max_line,max_line_floor_plan


    def get_contour_lines(self,contour_outer_image):
        image_contour = []
        image_contour_lines=[]
        first_element =0
        current_cont=0

        ret,thresh = cv2.threshold(contour_outer_image,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)
        for c,cnt in enumerate(contours):
            if c==0:
                current_cont = cnt
                break
        for l, level1 in enumerate(current_cont):
            for level2 in level1:
                x,y = level2
                #----- append all contour cordinates to new array based on contour
                image_contour.append([x,y])
                #-----based on cordinates get contour lines and append to image_contour_lines
                if l==0:
                    image_contour_lines.append([])
                    image_contour_lines[l].append([x,y])
                    first_element=[x,y]
                elif l==len(current_cont)-1:
                    image_contour_lines.append([])
                    image_contour_lines[l].append([x,y])
                    image_contour_lines[l-1].append([x,y])
                    image_contour_lines[l].append(first_element)
                else:
                    image_contour_lines.append([])
                    image_contour_lines[l].append([x,y])
                    image_contour_lines[l-1].append([x,y])

        return image_contour_lines


    def calculate_distance_between_text(self, boundaries,open_plan_text_cordinate,row_num):
        text_distances = []
        x1,y1 = open_plan_text_cordinate[row_num]
        for boundary_row in boundaries:
            if boundary_row[0]==row_num:
                for each_boundary in boundary_row[1]:
                    for each_text in each_boundary:
                        x2,y2 = open_plan_text_cordinate[each_text]
                        text_distances.append(math.hypot(x2-x1,y2-y1))

        average_distance = sum(text_distances)/len(text_distances)

        return average_distance

    def find_maximum_score(self,edge_extension_scores,
                           improved_voronoi_scores,ipl_scores,voronoi_penalties,
                           seperated_voronoi_lines,improved_voronoi_lines,
                           seperated_edge_lines,seperated_ipl_lines,
                           max_line,max_line_floor_plan):
        # 1.-----------------------get minimum voronoi penalty value
        minimum_voronoi_penalty, i, j, k, l = min((item, i, j, k, l) for i, row in enumerate(voronoi_penalties)
                                 for j, item1 in enumerate(row[1:])
                                 for k, item2 in enumerate(item1)
                                 for l, item in enumerate(item2))
        voronoi_element_number = voronoi_penalties[i][0],k

        # 2. -----------------------get maximum edge score value
        try:
            max_edge_boundary_score, i, j, k, l = max((item, i, j, k, l) for i, row in enumerate(edge_extension_scores)
                                  for j, item1 in enumerate(row[1:])
                                  for k, item2 in enumerate(item1)
                                  for l, item in enumerate(item2))
            edge_element_number = edge_extension_scores[i][0], k, l
        except(ValueError, TypeError):
            max_edge_boundary_score = -100
            edge_element_number = 0

        # 3. -----------------------get maximum improved voronoi score value
        try:
            max_improved_boundary_score, i, j, k, l = max((item[0], i, j, k, l) for i, row in enumerate(improved_voronoi_scores)
                                     for j, item1 in enumerate(row[1:])
                                     for k, item2 in enumerate(item1)
                                     for l, item in enumerate(item2))
            improved_element_number = improved_voronoi_scores[i][0], k, l
        except(ValueError, TypeError):
            max_improved_boundary_score = -100
            improved_element_number = 0

        # 4. -----------------------get maximum ipl score value
        try:
            max_ipl_boundary_score, i, j, k, l = max((item, i, j, k, l) for i, row in enumerate(ipl_scores)
                                                      for j, item1 in enumerate(row[1:])
                                                      for k, item2 in enumerate(item1)
                                                      for l, item in enumerate(item2))
            ipl_element_number = ipl_scores[i][0], k, l
        except(ValueError,TypeError):
            max_ipl_boundary_score = -100
            ipl_element_number = 0

        #--find maximum score from all scores
        all_max_scores = [max_edge_boundary_score,max_improved_boundary_score,max_ipl_boundary_score]
        max_score, max_element_number = max((row, i) for i, row in enumerate(all_max_scores))

        #--find line of the max_score
        #--if max_score == -100: all other lists are empty, so it has to be voronoi line with minimum penalty
        if max_score == -100:
            voronoi_line_row = []
            for row in seperated_voronoi_lines:
                if row[0] == voronoi_element_number[0]:
                    voronoi_line_row = row[1]
                    break
            voronoi_boundary_scores = voronoi_line_row[voronoi_element_number[1]]
            for voronoi_max_line in voronoi_boundary_scores:
                cv2.line(max_line_floor_plan, (tuple(voronoi_max_line[0])), (tuple(voronoi_max_line[1])), (0, 0, 0), 2,
                         cv2.cv.CV_AA)
                max_line.append(voronoi_max_line)
        #--max_element_number==0: list is seperated_edge_lines
        elif max_element_number==0:
            edge_line_set = []
            for row in seperated_edge_lines:
                if row[0]==edge_element_number[0]:
                    edge_line_set=row[1]
                    break
            edge_boundary_scores = edge_line_set[edge_element_number[1]]
            max_line1 = edge_boundary_scores[edge_element_number[2]]
            max_line_i=max_line1[0]
            cv2.line(max_line_floor_plan,(tuple(max_line_i[0])),(tuple(max_line_i[1])),(0,0,0),2,cv2.cv.CV_AA)
            max_line.append(max_line1[0])
        # --max_element_number==1: list is improved_voronoi_lines
        elif max_element_number==1:
            improved_line_row = []
            for row in improved_voronoi_lines:
                if row[0] == improved_element_number[0]:
                    improved_line_row = row[1]
                    break
            improved_boundary_scores = improved_line_row[improved_element_number[1]]
            improved_max_line_set = improved_boundary_scores[improved_element_number[2]]
            for improved_max_line in improved_max_line_set:
                cv2.line(max_line_floor_plan, (tuple(improved_max_line[0])), (tuple(improved_max_line[1])), (0, 0, 0),
                         2, cv2.cv.CV_AA)
                max_line.append(improved_max_line)
        # --max_element_number==2: list is seperated_ipl_lines
        else:
            ipl_line_set = []
            for row in seperated_ipl_lines:
                if row[0] == ipl_element_number[0]:
                    ipl_line_set = row[1]
                    break
            ipl_boundary_scores = ipl_line_set[ipl_element_number[1]]
            max_line1 = ipl_boundary_scores[ipl_element_number[2]]
            max_line_ipl = max_line1[0]
            cv2.line(max_line_floor_plan, (tuple(max_line_ipl[0])), (tuple(max_line_ipl[1])), (0, 0, 0), 2,
                     cv2.cv.CV_AA)
            max_line.append(max_line1[0])


        return max_line,max_line_floor_plan


    #--old method. not used now
    def find_maximum_score2(self,edge_extension_scores,
                           improved_voronoi_scores,ipl_scores,voronoi_penalties,
                           seperated_voronoi_lines,improved_voronoi_lines,
                           seperated_edge_lines,seperated_ipl_lines,
                           max_line,max_line_floor_plan):
        # 1.-----------------------get minimum voronoi penalty value
        min_voronoi_boundary_score, voronoi_element_number, max_voronoi_boundary_score_level1, voronoi_element_number_level1, max_voronoi_boundary_score_level2, voronoi_element_number_level2 = 10, 0, 10, 0, 10, 0
        count = 0
        for row_num1, voronoi_line_set in enumerate(voronoi_penalties):  # -----------******************
            row_num = voronoi_line_set[0]
            for boundary_num, voronoi_boundary_score_list in enumerate(
                    voronoi_line_set[1]):
                voronoi_boundary_penalty = voronoi_boundary_score_list[0]
                if count == 0:
                    max_voronoi_boundary_score_level1 = voronoi_boundary_penalty
                    voronoi_element_number_level1 = row_num, boundary_num
                    count = count + 1
                elif max_voronoi_boundary_score_level1 > voronoi_boundary_penalty:
                    max_voronoi_boundary_score_level1 = voronoi_boundary_penalty
                    voronoi_element_number_level1 = row_num, boundary_num
            if min_voronoi_boundary_score > max_voronoi_boundary_score_level1:
                min_voronoi_boundary_score = max_voronoi_boundary_score_level1
                voronoi_element_number = voronoi_element_number_level1
        print'--min_voronoi--'
        print min_voronoi_boundary_score
        print voronoi_element_number
        #2. -----------------------get maximum edge score value
        max_edge_boundary_score, edge_element_number,max_edge_boundary_score_level1, edge_element_number_level1,max_edge_boundary_score_level2, edge_element_number_level2=-10,0,-10,0,-10,0
        for row_num1,edge_line_set in enumerate(edge_extension_scores):
            row_num=edge_line_set[0]
            for boundary_num,edge_boundary_scores in enumerate(edge_line_set[1]):
                for num,e_row in enumerate(edge_boundary_scores):
                    if max_edge_boundary_score_level2<e_row and e_row != 0:
                        max_edge_boundary_score_level2=e_row
                        edge_element_number_level2 = row_num,boundary_num,num
                if max_edge_boundary_score_level1<max_edge_boundary_score_level2:
                    max_edge_boundary_score_level1=max_edge_boundary_score_level2
                    edge_element_number_level1 = edge_element_number_level2
            if max_edge_boundary_score<max_edge_boundary_score_level1:
                max_edge_boundary_score=max_edge_boundary_score_level1
                edge_element_number = edge_element_number_level1

        #-----------------------get maximum improved voronoi score value
        max_improved_boundary_score, improved_element_number,max_improved_boundary_score_level1, improved_element_number_level1,max_improved_boundary_score_level2, improved_element_number_level2=-10,0,-10,0,-10,0
        for row_num1,improved_line_set in enumerate(improved_voronoi_scores):#-----------******************
            row_num=improved_line_set[0]#-----------******************
            for boundary_num,improved_boundary_scores in enumerate(improved_line_set[1]):#-----------******************
                for num,e_row in enumerate(improved_boundary_scores):
                    if max_improved_boundary_score_level2<e_row and e_row!=0:
                        max_improved_boundary_score_level2=e_row
                        improved_element_number_level2 = row_num,boundary_num,num
                if max_improved_boundary_score_level1<max_improved_boundary_score_level2:
                    max_improved_boundary_score_level1=max_improved_boundary_score_level2
                    improved_element_number_level1 = improved_element_number_level2
            if max_improved_boundary_score<max_improved_boundary_score_level1:
                max_improved_boundary_score=max_improved_boundary_score_level1
                improved_element_number = improved_element_number_level1


        #-----------------------get maximum voronoi score value
        max_voronoi_boundary_score, voronoi_element_number,max_voronoi_boundary_score_level1, voronoi_element_number_level1,max_voronoi_boundary_score_level2, voronoi_element_number_level2=10,0,10,0,10,0
        count = 0
        for row_num1,voronoi_line_set in enumerate(voronoi_scores):#-----------******************
            row_num=voronoi_line_set[0]#-----------******************
            for boundary_num,voronoi_boundary_score_list in enumerate(voronoi_line_set[1]):#-----------******************
                voronoi_boundary_scores =0
                for score_v in voronoi_boundary_score_list:
                    voronoi_boundary_scores = score_v
                if count ==0:
                    max_voronoi_boundary_score_level1=voronoi_boundary_scores
                    voronoi_element_number_level1 = row_num,boundary_num
                    count = count+1
                elif max_voronoi_boundary_score_level1>voronoi_boundary_scores:
                    max_voronoi_boundary_score_level1=voronoi_boundary_scores
                    voronoi_element_number_level1 = row_num,boundary_num
            if max_voronoi_boundary_score>max_voronoi_boundary_score_level1:
                max_voronoi_boundary_score=max_voronoi_boundary_score_level1
                voronoi_element_number = voronoi_element_number_level1

        # -----------------------get maximum edge score value
        max_ipl_boundary_score, ipl_element_number, max_ipl_boundary_score_level1, ipl_element_number_level1, max_ipl_boundary_score_level2, ipl_element_number_level2 = -10, 0, -10, 0, -10, 0
        for row_num1, ipl_line_set in enumerate(ipl_scores):
            row_num = ipl_line_set[0]
            for boundary_num, ipl_boundary_scores in enumerate(ipl_line_set[1]):
                for num, e_row in enumerate(ipl_boundary_scores):
                    if max_ipl_boundary_score_level2 < e_row and e_row != 0:
                        max_ipl_boundary_score_level2 = e_row
                        ipl_element_number_level2 = row_num, boundary_num, num
                if max_ipl_boundary_score_level1 < max_ipl_boundary_score_level2:
                    max_ipl_boundary_score_level1 = max_ipl_boundary_score_level2
                    ipl_element_number_level1 = ipl_element_number_level2
            if max_ipl_boundary_score < max_ipl_boundary_score_level1:
                max_ipl_boundary_score = max_ipl_boundary_score_level1
                ipl_element_number = ipl_element_number_level1

        # max_line=[]
        max_line_data, max_list_type =0 , 0
        if max_edge_boundary_score==-10 and max_improved_boundary_score==-10:
            voronoi_line_row=[]
            for row in seperated_voronoi_lines:
                if row[0]==voronoi_element_number[0]:
                    voronoi_line_row=row[1]
                    break
            voronoi_boundary_scores = voronoi_line_row[voronoi_element_number[1]]
            for voronoi_max_line in voronoi_boundary_scores:
                cv2.line(max_line_floor_plan,(tuple(voronoi_max_line[0])),(tuple(voronoi_max_line[1])),(0,0,0),2,cv2.cv.CV_AA)
                max_line.append(voronoi_max_line)
            # max_line_data = voronoi_element_number
            # max_list_type = 'V'

        elif max_edge_boundary_score<max_improved_boundary_score:
            improved_max_line_list=[]
            improved_line_row=[]
            for row in improved_voronoi_lines:
                if row[0]==improved_element_number[0]:
                    improved_line_row=row[1]
                    break
            improved_boundary_scores = improved_line_row[improved_element_number[1]]
            improved_max_line_set = improved_boundary_scores[improved_element_number[2]]
            for improved_max_line in improved_max_line_set:
                cv2.line(max_line_floor_plan,(tuple(improved_max_line[0])),(tuple(improved_max_line[1])),(0,0,0),2,cv2.cv.CV_AA)
                max_line.append(improved_max_line)
            # max_line_data = improved_element_number
            # max_list_type = 'I'

        elif max_edge_boundary_score < max_ipl_boundary_score:
            ipl_line_set = []
            for row in seperated_ipl_lines:
                if row[0] == ipl_element_number[0]:
                    ipl_line_set = row[1]
                    break
            ipl_boundary_scores = ipl_line_set[ipl_element_number[1]]
            max_line1 = ipl_boundary_scores[ipl_element_number[2]]
            max_line_ipl = max_line1[0]
            cv2.line(max_line_floor_plan, (tuple(max_line_ipl[0])), (tuple(max_line_ipl[1])), (0, 0, 0), 2, cv2.cv.CV_AA)
            max_line.append(max_line1[0])
            # max_line_data = ipl_element_number
            # max_list_type = 'N'

        else:
            edge_line_set=[]
            for row in seperated_edge_lines:
                if row[0]==edge_element_number[0]:
                    edge_line_set=row[1]
                    break
            edge_boundary_scores = edge_line_set[edge_element_number[1]]
            max_line1 = edge_boundary_scores[edge_element_number[2]]
            max_line_i=max_line1[0]
            cv2.line(max_line_floor_plan,(tuple(max_line_i[0])),(tuple(max_line_i[1])),(0,0,0),2,cv2.cv.CV_AA)
            max_line.append(max_line1[0])
            # max_line_data = edge_element_number
            # max_list_type = 'E'

        # print '-------max_line-------'
        # for row in max_line:
        #     print row
        return max_line,max_line_floor_plan


    def find_closest_valid_distance(self,contour_line, text_cordinate):
        x0,y0 = text_cordinate
        x1,y1 = contour_line[0]
        x2,y2 = contour_line[1]
        max_x = max(x1,x2)
        min_x = min(x1,x2)
        max_y = max(y1,y2)
        min_y = min(y1,y2)
        #----find a,b (intersection point from x0,y0 to line)
        if x1==x2:
            intersect_x = x1
            intersect_y = y0
        else:
            m = (y2-y1)/(x2-x1)
            if y1==y2:
                c = y1
            else:
                c = y2-(m*x2)
            intersect_x = (x0+(m*y0)-(m*c))/(math.pow(m,2)+1)
            intersect_y = (m*intersect_x)+c

        #----find if intersection point is on the line segment
        if (intersect_x>=min_x and intersect_x<=max_x and intersect_y>=min_y and intersect_y<=max_y):
            distance_from_text = math.hypot(intersect_x - x0, intersect_y - y0)
        else:
            center_x = (x1+x2)/2
            center_y = (y1+y2)/2
            distance_from_text = math.hypot(center_x - x0, center_y - y0)

        return distance_from_text




#-------score calculation methods
    def calculate_no_segements(self,w,boundary_lines):
        no_segments = len(boundary_lines)
        normalized_no_segments = (no_segments/2.0)*w
        return normalized_no_segments

    def calculate_line_length(self,w,boundary_lines,door_width):
        tot_length,line_gradient,final_gradient = 0,0,0
        for b_line in boundary_lines:
            x1,y1 = b_line[0]
            x2,y2 = b_line[1]
            length = math.hypot(x2 - x1, y2 - y1)
            tot_length = tot_length+length
            m,c = self.find_mc_of_line(x1,y1,x2,y2)
            if m == 0 or m == 'a' or (m<0.2 and m>-0.2) or (m>6 and m<-6):
                line_grad = 0
            else:
                line_grad = 1
            line_gradient = line_gradient+line_grad
        # total_length = round((tot_length/door_width)*w,3)
        #--get log of line_length

        line_length_ratio = tot_length/door_width
        line_length = math.log(line_length_ratio,10)
        if line_length < 0:
            length_value = 0
        elif line_length > 1:
            length_value = 1
        else:
            length_value = line_length

        #---validation for value NOT included since invalid values won't come due to ealier condition

        weighted_length_penalty = round(length_value*w,3)

        return weighted_length_penalty, line_gradient,tot_length

    def wall_extension_length(self,w,extension_length,edge_line_number,edge_extension_data,door_width):
        orginal_wall = edge_extension_data[edge_line_number][0]
        x1,y1 = orginal_wall[0]
        x2,y2 = orginal_wall[1]
        wall_length = math.hypot(x2 - x1, y2 - y1)

        if wall_length>door_width:
            extension_penalty = 0
        else:
            extension_penalty = 1-(wall_length/(wall_length+extension_length))


        if not (extension_penalty >= 0 and extension_penalty <= 1):
            print 'Error: Wall Extension Penalty value'

        final_extension_penalty = round(extension_penalty*w,3)
        return final_extension_penalty

    def calculate_line_gradient(self,w,line_gradient):
        #------if horizontal or vertical set to '0' otherwise '1'
        if line_gradient==0:
            gradient = 0
        else:
            gradient = 1

        # ---validation for value NOT needed coz gradient has only 2 values

        final_gradient = round(gradient*w,3)
        return final_gradient

    def calculate_line_distance(self, w, type, open_plan_text_cordinate,img_height,img_width,textID, boundary_cordinates, partition_lines, shortest_path_data,Test_path):
        # #------test
        # test_image = ~(np.zeros((img_height,img_width,3), np.uint8))
        # # orginal_opath = Test_path+str(edge_line_number)
        # list_dir_output = os.listdir(Test_path)
        # new_list = []
        # if len(list_dir_output)>0:
        #     for name in list_dir_output:
        #         new_list.append(name[0])
        #     new_list.sort()
        #     orginal_opath = Test_path+str(len(new_list)+1)
        # else:
        #     orginal_opath = Test_path+str(1)
        # for row2 in partition_lines:
        #     cv2.line(test_image, (tuple(row2[0])), (tuple(row2[1])), (255, 0, 0), 2, cv2.cv.CV_AA)
        # # for sp_row in shortest_path_data:
        # #     test_image_cp = test_image.copy()
        # #     for line_n, line_segment in enumerate(sp_row[2:]):
        # #         cv2.line(test_image_cp, (tuple(line_segment[0])), (tuple(line_segment[1])), (0, 0, 255), 2, cv2.cv.CV_AA)
        # #     tex1 = open_plan_text_cordinate[sp_row[0]]
        # #     tex2 = open_plan_text_cordinate[sp_row[1]]
        # #     cv2.circle(test_image_cp, (tuple(tex1)), 7, (0, 255, 0), -1)
        # #     cv2.circle(test_image_cp, (tuple(tex2)), 7, (0, 255, 0), -1)
        #     # cv2.imwrite(orginal_opath + '-'+str(sp_row[0])+'_'+str(sp_row[1])+'_text_distance.png', test_image_cp)
        # #----end test

        # if type =='e':
        #     print 'b_lines: ', partition_lines
        distance = []
        intersection_data = []
        #-- get partition lines 1 by 1
        for row2 in partition_lines:
            x1, y1 = row2[0]
            x2, y2 = row2[1]
            edge_max_x,edge_min_x,edge_max_y,edge_min_y = self.get_line_min_max(x1, y1,x2, y2)
            # # sp_flag = False
            # if (x1 == 1397 and y1 == 2414 and x2 == 1413 and y2 == 2040
            #     ) or (x1 == 1413 and y1 == 2040 and x2 == 973 and y2 == 2015):
            #     print '*********************', row2
            # if type == 'e':
            #     print row2

            text1 = textID
            #--get text1 and text2
            for r_num, text2 in enumerate(boundary_cordinates):
                # if type == 'e':
                #     print 'text: ', text1, text2

                #--get each segment from shortest_path
                for sp_row in shortest_path_data:
                    #--test
                    # test_image_cp = test_image.copy()
                    sp_flag = False
                    # intersect_flag = False
                    #--find sp_row==text1 && text2
                    if (sp_row[0] == text1 and sp_row[1] == text2
                        ) or (sp_row[0] == text2 and sp_row[1] == text1):
                        sp_flag = True
                        intersect_flag = False
                        segment, distance_from_start, distance_to_end, intersect_x, intersect_y = 0, 0, 0, 0, 0
                        for line_n, line_segment in enumerate(sp_row[2:]):
                            x3, y3 = line_segment[0]
                            x4, y4 = line_segment[1]
                            max_x,min_x,max_y,min_y = self.get_line_min_max(x3, y3,x4, y4)
                            intersect_x, intersect_y = self.find_contour_intersections(x1, y1, x2, y2, x3, y3, x4, y4)
                            intersect_point_exists = self.check_intersection_point_exists(intersection_data,intersect_x, intersect_y,text1,text2)
                            if intersect_point_exists== False:
                                if intersect_x >= min_x and intersect_x <= max_x and intersect_y >= min_y and intersect_y <= max_y:
                                    if intersect_x >= edge_min_x and intersect_x <= edge_max_x and intersect_y >= edge_min_y and intersect_y <= edge_max_y:
                                        segment = line_n
                                        intersection_data.append([text1,text2,[intersect_x, intersect_y]])
                                        # if type == 'e':
                                        #     print 'intersection--',intersect_x, intersect_y
                                        #     print 'sp seg', line_segment
                                        #     print 'sp_line', sp_row[2:]
                                        intersect_flag = True
                                        break
                        if intersect_flag:
                            total_path_distance,text1_distance, text2_distance = self.measure_text_distance(text1,text2,sp_row,segment,intersect_x,intersect_y)
                            distance.append(total_path_distance)
                            # for line_n, line_segment in enumerate(sp_row[2:]):
                            #     cv2.line(test_image_cp, (tuple(line_segment[0])), (tuple(line_segment[1])), (0, 0, 255), 2,
                            #          cv2.cv.CV_AA)
                            #
                            # cv2.putText(test_image_cp, 'text1_distance is: '+str(text1_distance), (0,100),
                            #             cv2.FONT_HERSHEY_COMPLEX, 1,
                            #             (0, 0, 0), 2, cv2.cv.CV_AA)
                            # cv2.putText(test_image_cp, 'text2_distance is: ' + str(text2_distance), (0, 130),
                            #             cv2.FONT_HERSHEY_COMPLEX, 1,
                            #             (0, 0, 0), 2, cv2.cv.CV_AA)
                            # cv2.putText(test_image_cp, 'total_path_distance is: ' + str(total_path_distance), (0, 160),
                            #             cv2.FONT_HERSHEY_COMPLEX, 1,
                            #             (0, 0, 0), 2, cv2.cv.CV_AA)
                            #
                            # if type == 'e':
                            #     print 'text1', text1, text1_distance, '--text2', text2, text2_distance
                            #     print 'total_path_distance', total_path_distance

                    if sp_flag:
                        # if intersect_flag:
                        #     cv2.imwrite(
                        #         orginal_opath + '-' + str(sp_row[0]) + '_' + str(sp_row[1]) + '_text_distance.png',
                        #         test_image_cp)
                        break
                # if sp_flag:
                #     break


        if len(distance) > 0:
            final_distance = sum(distance) / len(distance)
        #----only going to go to 'else' if there is a voronoi that is problemtic:
        #----can cause improved voronoi to take place too
        else:
            final_distance = -1
            print 'Error: No text distances'

        # distance_edited = round(final_distance * w, 3)
        if not (final_distance >= 0 and final_distance <= 1):
            print 'Error: Txt Distance Penalty value'
        distance_edited = round(final_distance * w, 3)

        # if type == 'e':
        #     print 'distance', distance, 'final_distance', final_distance, 'distance_edited', distance_edited
        #     print '--------------------------------------------'

        return distance_edited

    def check_intersection_point_exists(self,intersection_data,intersect_x, intersect_y,text1,text2):
        intersect_point_exists = False
        for intersect_row in intersection_data:
            ints_text1 = intersect_row[0]
            ints_text2 = intersect_row[1]
            intersect_x1, intersect_y1 = intersect_row[2]
            if intersect_x1==intersect_x and intersect_y1==intersect_y:
                if (ints_text1==text1 and ints_text2==text2) or (ints_text1==text2 and ints_text2==text1):
                    intersect_point_exists = True
        return intersect_point_exists

    def measure_text_distance(self, text1,text2,row3,segment,intersect_x,intersect_y):
        text1_distance, text2_distance, total_path_distance = 0, 0, 0
        shortest_path_lines = row3[2:]
        if row3[0] == text1 and row3[1] == text2 or row3[0] == text2 and row3[1] == text1:
            # --------calculate text1_distance distance
            last_x, last_y = 0, 0
            # print 'segment is : ',segment
            if segment == 0:
                line_seg = shortest_path_lines[segment]
                last_x, last_y = line_seg[0]
                text1_distance = math.hypot(last_x - intersect_x, last_y - intersect_y)
                # print 'segment == 0',text1_distance
            else:
                for r,line_seg in enumerate(shortest_path_lines[:segment]):
                    x5, y5 = line_seg[0]
                    x6, y6 = line_seg[1]
                    length = math.hypot(x6 - x5, y6 - y5)
                    text1_distance = text1_distance + length
                    last_x, last_y = x6, y6
                    # print 't1 length', line_seg, '--',text1_distance
                if last_x != 0 and last_y != 0:
                    text1_distance = text1_distance + math.hypot(last_x - intersect_x,
                                                                 last_y - intersect_y)
                    # print 't1 missing', intersect_x,intersect_y,':',last_x,last_y, '--',text1_distance
            # --------calculate text2_distance distance
            last_x1, last_y1 = 0, 0
            if segment == len(shortest_path_lines) - 1:
                line_seg = shortest_path_lines[segment]
                last_x1, last_y1 = line_seg[1]
                text2_distance = math.hypot(last_x1 - intersect_x, last_y1 - intersect_y)
                # print 'segment == last', text2_distance
            else:
                # print't2222',shortest_path_lines[segment+1:]
                #--segment+1: since segment line element will be used to calculate distance from intersection point
                for r,line_seg in enumerate(shortest_path_lines[segment+1:]):
                    x5, y5 = line_seg[0]
                    x6, y6 = line_seg[1]
                    length = math.hypot(x6 - x5, y6 - y5)
                    text2_distance = text2_distance + length
                    last_x1, last_y1 = x5, y5
                    # print 't2 length', line_seg, '--',text2_distance
                if last_x1 != 0 and last_y1 != 0:
                    text2_distance = text2_distance + math.hypot(last_x1 - intersect_x,
                                                                 last_y1 - intersect_y)
                    # print 't2 missing', intersect_x,intersect_y,':',last_x1,last_y1, '--',text2_distance
            # print 'text1_distance',text1_distance, 'text2_distance',text2_distance
            total_path_distance = abs(text1_distance - text2_distance) / (
                text1_distance + text2_distance)

        return total_path_distance,text1_distance, text2_distance

    def get_line_min_max(self,x1,y1,x2,y2):
        max_x = max(x1, x2)
        min_x = min(x1, x2)
        max_y = max(y1, y2)
        min_y = min(y1, y2)
        return max_x,min_x,max_y,min_y


    def calculate_line_distance2(self,w,type,textID,boundary_cordinates,boundary_lines,shortest_path_data):
        if type =='i':
            print 'b_lines: ', boundary_lines
        text1 = textID
        distance = []
        for r_num,each_boundary_cordinate in enumerate(boundary_cordinates):
            text2 = each_boundary_cordinate
            for row2 in boundary_lines:
                x1,y1 = row2[0]
                x2,y2 = row2[1]
                edge_max_x = max(x1,x2)
                edge_min_x = min(x1,x2)
                edge_max_y = max(y1,y2)
                edge_min_y = min(y1,y2)
                i_flag = False
                for row3 in shortest_path_data:
                    i_flag = False
                    # print row3
                    if (row3[0]==text1 and row3[1]==text2) or (row3[0]==text2 and row3[1]==text1):
                        # print ('both text',text1,text2)
                        i_flag = True
                        intersect_flag = False
                        segment, distance_from_start,distance_to_end,intersect_x,intersect_y = 0,0,0,0,0
                        for line_n, line_segment in enumerate(row3[2:]):
                            x3,y3 = line_segment[0]
                            x4,y4 = line_segment[1]
                            max_x = max(x3,x4)
                            min_x = min(x3,x4)
                            max_y = max(y3,y4)
                            min_y = min(y3,y4)
                            intersect_x, intersect_y = self.find_contour_intersections(x1,y1,x2,y2,x3,y3,x4,y4)
                            if intersect_x>=min_x and intersect_x<=max_x and intersect_y>=min_y and intersect_y<=max_y:
                                if intersect_x>=edge_min_x and intersect_x<=edge_max_x and intersect_y>=edge_min_y and intersect_y<=edge_max_y:
                                    segment = line_n
                                    intersect_flag = True
                                    break
                        if intersect_flag:
                            text1_distance, text2_distance, total_path_distance = 0,0,0
                            shortest_path_lines = row3[2:]
                            if row3[0]==text1 and row3[1]==text2 or row3[0]==text2 and row3[1]==text1:
                                #--------calculate text1_distance distance
                                last_x, last_y = 0,0
                                if segment==0:
                                    line_seg = shortest_path_lines[segment]
                                    last_x, last_y = line_seg[0]
                                    text1_distance = math.hypot(last_x - intersect_x, last_y - intersect_y)
                                else:
                                    for line_seg in shortest_path_lines[:segment]:
                                        x5,y5 = line_seg[0]
                                        x6,y6 = line_seg[1]
                                        length = math.hypot(x6 - x5, y6 - y5)
                                        text1_distance = text1_distance+length
                                        last_x, last_y = x6,y6
                                    if last_x != 0 and last_y != 0:
                                        text1_distance = text1_distance+ math.hypot(last_x - intersect_x, last_y - intersect_y)

                                #--------calculate text2_distance distance
                                last_x1, last_y1 = 0,0
                                if segment == len(shortest_path_lines)-1:
                                    line_seg = shortest_path_lines[segment]
                                    last_x1, last_y1 = line_seg[1]
                                    text2_distance = math.hypot(last_x1 - intersect_x, last_y1 - intersect_y)
                                else:
                                    for line_seg in shortest_path_lines[segment:]:
                                        x5,y5 = line_seg[0]
                                        x6,y6 = line_seg[1]
                                        length = math.hypot(x6 - x5, y6 - y5)
                                        text2_distance = text2_distance+length
                                        last_x1, last_y1 = x5,y5
                                    if last_x1 != 0 and last_y1 != 0:
                                        text2_distance = text2_distance+math.hypot(last_x1 - intersect_x, last_y1 - intersect_y)
                                total_path_distance = abs(text1_distance-text2_distance)/(text1_distance+text2_distance)
                                # if type =='e':
                                #     print 'text1', text1, text1_distance, '--text2',text2, text2_distance
                                #     print 'total_path_distance',total_path_distance
                            distance.append(total_path_distance)
                    if i_flag:
                        break
                if i_flag:
                    break
        # temp_distance = copy.copy(distance)
        # print 'B.distance', distance
        # for r,row in enumerate(distance):
        #     distance[r]= row/len(distance)
        # final_distance = sum(distance)
        #

        # print 'final_distance', final_distance
        # print 'temp_distance',temp_distance
        if len(distance)>0:
            final_distance = sum(distance) / len(distance)
        else:
            final_distance = 0

        if not (final_distance >= 0 and final_distance <= 1):
            print 'Error: Txt Distance Penalty value'
        distance_edited = round(final_distance*w,3)
        # if type == 'e':
        #     print 'fffinal_distance', final_distance
        #     print 'dddistance_edited',distance_edited
        return distance_edited


    def furniture_text_distance(self,w,type,door_width,textID,boundary_cordinates,boundary_lines,shortest_path_data):
        text1 = textID
        distance = []
        # debug_mode = False
        # for e_l in boundary_lines:
        #     if (e_l[0][0]==1412 and e_l[0][1]==1464 and e_l[1][0]== 1768 and  e_l[1][1]==1464):
        #         debug_mode = True
        # if debug_mode:
        #     print 'line is ', boundary_lines
        # if debug_mode:
        #     print 'text 1 is ',text1

        for r_num,each_boundary_cordinate in enumerate(boundary_cordinates):
            text2 = each_boundary_cordinate
            # if debug_mode:
            #     print 'text 2 is ',text2
            for r_n,row2 in enumerate(boundary_lines):
                x1,y1 = row2[0]
                x2,y2 = row2[1]
                edge_max_x = max(x1,x2)
                edge_min_x = min(x1,x2)
                edge_max_y = max(y1,y2)
                edge_min_y = min(y1,y2)
                i_flag = False

                for row3 in shortest_path_data:
                    i_flag = False
                    if (row3[0]==text1 and row3[1]==text2) or (row3[0]==text2 and row3[1]==text1):
                        # if debug_mode:
                        #     # print 'row3[0],row3[1]', row3[0],row3[1]
                        #     contour_image = ~(np.zeros((3508, 2479, 3), np.uint8))
                        #     # print ('both text',text1,text2)
                        #     # print '---------------',row3
                        #     # for line_n, line_segment in enumerate(row3[2:]):
                        #     #     cv2.line(contour_image,(tuple(line_segment[0])),(tuple(line_segment[1])),(0,0,255),2,cv2.cv.CV_AA)

                        i_flag = True
                        intersect_flag = False
                        segment, distance_from_start,distance_to_end,intersect_x,intersect_y = 0,0,0,0,0
                        for line_n, line_segment in enumerate(row3[2:]):
                            # if debug_mode:
                            #     cv2.line(contour_image,(tuple(line_segment[0])),(tuple(line_segment[1])),(0,0,0),3,cv2.cv.CV_AA)
                            x3,y3 = line_segment[0]
                            x4,y4 = line_segment[1]
                            max_x = max(x3,x4)
                            min_x = min(x3,x4)
                            max_y = max(y3,y4)
                            min_y = min(y3,y4)
                            intersect_x, intersect_y = self.find_contour_intersections(x1,y1,x2,y2,x3,y3,x4,y4)
                            # if debug_mode:
                            #     cv2.line(contour_image, (tuple(row2[0])), (tuple(row2[1])), (0, 255, 0), 2,
                            #              cv2.cv.CV_AA)
                            #     cv2.circle(contour_image, (intersect_x, intersect_y), 10, (255, 0, 0), -1)
                            #     cv2.imwrite('/home/ub/Documents/efp2017/Final Lines/' + str(text1) + ' : ' + str(
                            #         text2) + ' : ' + str(row3[0]) + '---' + str(row3[1]) + str(r_n) + '.png',
                            #                 contour_image)
                            if (intersect_x != 0 and intersect_y != 0) and (intersect_x>=min_x and intersect_x<=max_x and intersect_y>=min_y and intersect_y<=max_y):
                                if intersect_x>=edge_min_x and intersect_x<=edge_max_x and intersect_y>=edge_min_y and intersect_y<=edge_max_y:
                                    segment = line_n
                                    intersect_flag = True
                                    # if debug_mode:
                                    #     # print 'inside2', intersect_x, intersect_y
                                    #     cv2.line(contour_image,(tuple(row2[0])),(tuple(row2[1])),(0,255,0),2,cv2.cv.CV_AA)
                                    #     cv2.circle(contour_image, (intersect_x, intersect_y), 10, (255, 0, 0), -1)
                                    #     cv2.imwrite('/home/ub/Documents/efp2017/Final Lines/'+str(text1)+' : '+str(text2)+' : '+str(row2[0])+'---'+str(row2[1])+str(r_n)+'.png',contour_image)
                                    break
                        if intersect_flag:
                            # if debug_mode:
                                # print 'segment',segment
                            text1_distance, text2_distance, total_path_distance = 0,0,0
                            shortest_path_lines = row3[2:]
                            if row3[0]==text1 and row3[1]==text2:
                                #--------calculate text1_distance distance
                                last_x, last_y = 0,0
                                if segment==0:
                                    line_seg = shortest_path_lines[segment]
                                    last_x, last_y = line_seg[0]
                                    text1_distance = math.hypot(last_x - intersect_x, last_y - intersect_y)
                                else:
                                    for line_seg in shortest_path_lines[:segment]:
                                        x5,y5 = line_seg[0]
                                        x6,y6 = line_seg[1]
                                        length = math.hypot(x6 - x5, y6 - y5)
                                        text1_distance = text1_distance+length
                                        last_x, last_y = x6,y6
                                    if last_x != 0 and last_y != 0:
                                        text1_distance = text1_distance+ math.hypot(last_x - intersect_x, last_y - intersect_y)
                                total_path_distance = text1_distance
                                # if debug_mode:
                                #     print 'if',total_path_distance
                            elif row3[0]==text2 and row3[1]==text1:
                                #--------calculate text2_distance distance
                                last_x1, last_y1 = 0,0
                                if segment == len(shortest_path_lines)-1:
                                    line_seg = shortest_path_lines[segment]
                                    last_x1, last_y1 = line_seg[1]
                                    text1_distance = math.hypot(last_x1 - intersect_x, last_y1 - intersect_y)
                                else:
                                    for line_seg in shortest_path_lines[segment:]:
                                        x5,y5 = line_seg[0]
                                        x6,y6 = line_seg[1]
                                        length = math.hypot(x6 - x5, y6 - y5)
                                        text1_distance = text1_distance+length
                                        last_x1, last_y1 = x5,y5
                                    if last_x1 != 0 and last_y1 != 0:
                                        text1_distance = text1_distance+math.hypot(last_x1 - intersect_x, last_y1 - intersect_y)

                                total_path_distance = text1_distance
                                # if debug_mode:
                                #     print 'else',total_path_distance
                            distance.append(total_path_distance)
                    if i_flag:
                        break
                if i_flag:
                    break
        for r,row in enumerate(distance):
            distance[r]= row/len(distance)
        # ------a new method will be created to find best line for PL
        # ------this was a quick fix to penalize lines that are away from PL
        final_distance = (sum(distance)/door_width)
        distance_edited = round(final_distance*w,3)
        # if debug_mode:
        #     print final_distance,' normalized- ',distance_edited
        return distance_edited



    def calculate_area(self,w,door_width,Test_path,lines,score_path,img_height,img_width,non_voronoi_line_data,text_cordinate,seperated_voronoi_for_area_difference):
        contour_image = ~(np.zeros((img_height,img_width,3), np.uint8))
        contour_image_2 = ~(np.zeros((img_height,img_width,3), np.uint8))
        # test_image = ~(np.zeros((img_height,img_width,3), np.uint8))


        for contour_line_set in non_voronoi_line_data:
            for contour_line in contour_line_set:
                cv2.line(contour_image,(tuple(contour_line[0])),(tuple(contour_line[1])),(0,0,0),2,cv2.cv.CV_AA)
                cv2.line(contour_image_2,(tuple(contour_line[0])),(tuple(contour_line[1])),(0,0,0),2,cv2.cv.CV_AA)
                # cv2.line(test_image,(tuple(contour_line[0])),(tuple(contour_line[1])),(0,0,0),2,cv2.cv.CV_AA)

        for contour_line in seperated_voronoi_for_area_difference:
            cv2.line(contour_image,(tuple(contour_line[0])),(tuple(contour_line[1])),(0,0,0),2,cv2.cv.CV_AA)
            # cv2.line(test_image,(tuple(contour_line[0])),(tuple(contour_line[1])),(0,0,255),2,cv2.cv.CV_AA)
        cv2.imwrite(score_path+'_contour.png', contour_image)
        # for p_line in contour_partition_lines:
        #     cv2.line(contour_image,(tuple(p_line[0])),(tuple(p_line[1])),(0,0,0),2,cv2.cv.CV_AA)
        #     cv2.line(test_image,(tuple(p_line[0])),(tuple(p_line[1])),(0,0,0),2,cv2.cv.CV_AA)
        # cv2.imwrite(score_path+'_contour.png', contour_image)
        for e_line in lines:
            cv2.line(contour_image_2,(tuple(e_line[0])),(tuple(e_line[1])),(0,0,0),2,cv2.cv.CV_AA)
            # cv2.line(test_image,(tuple(e_line[0])),(tuple(e_line[1])),(255,0,0),2,cv2.cv.CV_AA)
        cv2.imwrite(score_path+'_contour_with_line.png', contour_image_2)

        #  #------test
        # list_dir_output = os.listdir(Test_path)
        # # sort_list = list_dir_output.sort()
        # new_list = []
        # if len(list_dir_output)>0:
        #     for name in list_dir_output:
        #         new_list.append(name[0])
        #
        #     new_list.sort()
        #     Test_path = Test_path+str(len(new_list)+1)
        #     # os.mkdir(Test_path)
        # else:
        #     Test_path = Test_path+str(1)
        #     # os.mkdir(Test_path)
        # # cv2.imwrite(Test_path+'_contour.png', test_image)
        # #-----end test



        selected_contour_image = cv2.imread(score_path+'_contour.png',cv2.IMREAD_GRAYSCALE)
        ret,thresh = cv2.threshold(selected_contour_image,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)
        total_area=0
        for c,cnt in enumerate(contours):
            if cv2.contourArea(cnt)>door_width*10:
            # if cv2.pointPolygonTest(cnt,tuple(text_cordinate),False)==1:
                total_area = cv2.contourArea(cnt)
                break

        #--------read contour image with new line
        selected_contour_image = cv2.imread(score_path+'_contour_with_line.png',cv2.IMREAD_GRAYSCALE)
        ret,thresh = cv2.threshold(selected_contour_image,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)
        selected_line_area=0
        for c,cnt in enumerate(contours):
            if cv2.pointPolygonTest(cnt,tuple(text_cordinate),False)==1:
                # cv2.circle(test_image, (tuple(text_cordinate)), 5, (255, 0, 0), -1)
                selected_line_area = cv2.contourArea(cnt)
                break

        # cv2.imwrite(Test_path+'_contour.png', test_image)

        area_difference=long(((total_area-selected_line_area)/total_area)*w)
        os.remove(score_path+'_contour.png')
        os.remove(score_path+'_contour_with_line.png')
        # print 'lines',lines
        # print 'total_area',total_area
        # print 'selected_line_area',selected_line_area
        # print 'area_difference',area_difference
        # print '---------------------'
        return area_difference

    def calculate_convexity_defect(self,w,img_height,img_width,score_path,lines,non_voronoi_line_data,door_width,Test_path,text_cordinate,edge_line_number):
        contour_image = ~(np.zeros((img_height,img_width,3), np.uint8))
        # test_image = ~(np.zeros((img_height,img_width,3), np.uint8))
        # print text_cordinate

        for contour_line_set in non_voronoi_line_data:
            for contour_line in contour_line_set:
                cv2.line(contour_image,(tuple(contour_line[0])),(tuple(contour_line[1])),(0,0,0),2,cv2.cv.CV_AA)
                # cv2.line(test_image,(tuple(contour_line[0])),(tuple(contour_line[1])),(0,0,0),1,cv2.cv.CV_AA)

        for e_line in lines:
            # if edge_line_number==0:
            #     edge_line_number = 'i-'+str(e_line[0][0])+str(e_line[0][1])
            cv2.line(contour_image,(tuple(e_line[0])),(tuple(e_line[1])),(0,0,0),2,cv2.cv.CV_AA)
            # cv2.line(test_image,(tuple(e_line[0])),(tuple(e_line[1])),(0,0,255),2,cv2.cv.CV_AA)
        cv2.imwrite(score_path+'_defect_contour.png', contour_image)

        # #------test
        # orginal_opath = Test_path+str(edge_line_number)
        # list_dir_output = os.listdir(Test_path)
        # new_list = []
        # if len(list_dir_output)>0:
        #     for name in list_dir_output:
        #         new_list.append(name[0])
        #     new_list.sort()
        #     orginal_opath = orginal_opath+str(len(new_list)+1)
        # else:
        #     orginal_opath = orginal_opath+str(1)
        # cv2.imwrite(orginal_opath+'_defect_contour.png', test_image)
        # print 'path', orginal_opath+'_defect_contour.png'

        selected_contour_image = cv2.imread(score_path+'_defect_contour.png',cv2.IMREAD_GRAYSCALE)
        ret,thresh = cv2.threshold(selected_contour_image,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)
        # cv2.imwrite(orginal_opath+'_Z.png',selected_contour_image)

        # #--new hull method 2017/04/05
        area_ratio = 0
        for num, cnt in enumerate(contours):

            # --get contour area
            contour_area = cv2.contourArea(cnt)
            if cv2.pointPolygonTest(cnt, tuple(text_cordinate), False) == 1 and num != len(contours)-1:
                hull = cv2.convexHull(cnt)
                convex_hull_area = cv2.contourArea(hull)
                area_ratio = 1-(contour_area/convex_hull_area)
                break
                # # ------test
                # # if edge_line_number==3:
                # #     print 'num', num
                # #     print 'contour_area', contour_area
                # #     print 'convex_hull_area', convex_hull_area
                # orginal_opath = Test_path + str(edge_line_number)
                # list_dir_output = os.listdir(Test_path)
                # new_list = []
                # if len(list_dir_output) > 0:
                #     for name in list_dir_output:
                #         new_list.append(name[0])
                #     new_list.sort()
                #     orginal_opath = orginal_opath + str(len(new_list) + 1)
                # else:
                #     orginal_opath = orginal_opath + str(1)
                # cv2.drawContours(test_image, [hull], 0, (0, 0, 255), 4)
                # cv2.drawContours(test_image, [cnt], -1, (0,0,0), 2)
                # cv2.putText(test_image, 'Contour_area: '+str(contour_area),
                #             (5, 2100), cv2.FONT_HERSHEY_PLAIN, 5, (0, 0, 255), 5)
                # cv2.putText(test_image, 'Convex_hull_area: '+str(convex_hull_area),
                #             (5, 2150), cv2.FONT_HERSHEY_PLAIN, 5, (0, 0, 255), 5)
                # cv2.putText(test_image, 'Area_ratio: '+str(area_ratio),
                #             (5, 2200), cv2.FONT_HERSHEY_PLAIN, 5, (0, 0, 255), 5)
                # cv2.imwrite(orginal_opath + '_defect_contour2.png', test_image)
                # # print orginal_opath + '_defect_contour2.png'
                # #--end test
                # break

        if not (area_ratio >= 0 and area_ratio <= 1):
            print 'Error: Convexity Defect Penalty value'

        convexity_defect = round(area_ratio * w,3)
        os.remove(score_path + '_defect_contour.png')

        return convexity_defect

        # #--old hull method 2017/04/05

        #---convexity using opencv calculate defect method
        # total_defect = 0
        # print '-----------'
        # for num,cnt in enumerate(contours):
        #     #--get contour area
        #     # print 'num', num
        #     if cv2.pointPolygonTest(cnt,tuple(text_cordinate),False)==1 and num < 2:
        #         hull = cv2.convexHull(cnt,returnPoints = False)
        #         # hull2 = cv2.convexHull(cnt)
        #         # outer_cont2 = ~(np.zeros((img_height,img_width,3), np.uint8))
        #         # cv2.drawContours(outer_cont2,[cnt],0,(0,0,0),1)
        #         # cv2.drawContours(outer_cont2,[hull2],0,(0,0,255),2)
        #         # cv2.imwrite(os.getcwd()+'_Contour.png',outer_cont2)
        #         defects = cv2.convexityDefects(cnt,hull)
        #         defect_count  = 0
        #         if defects!=None:
        #             for i in range(defects.shape[0]):
        #                 s,e,f,fixpt_depth = defects[i,0]
        #                 #---'fixpt_depth' = fixed-point approximation (with 8 fractional bits) of the distance between the farthest contour point and the hull
        #                 #--- to get the floating-point value of the depth will be fixpt_depth/256.0
        #                 #----(http://docs.opencv.org/2.4/modules/imgproc/doc/structural_analysis_and_shape_descriptors.html)
        #                 distance = fixpt_depth/256.0
        #                 total_defect= total_defect+distance
        #                 # defect_count = defect_count + 1
        #                 print 'distance', distance
        #             # avg_defect = total_defect/defect_count
        #             print 'total_defect', total_defect
        #             # print 'total_defect',total_defect,'defect_count', defect_count, 'avg_defect', avg_defect
        #             # ------test
        #             orginal_opath = Test_path + str(edge_line_number)
        #             list_dir_output = os.listdir(Test_path)
        #             new_list = []
        #             if len(list_dir_output) > 0:
        #                 for name in list_dir_output:
        #                     new_list.append(name[0])
        #                 new_list.sort()
        #                 orginal_opath = orginal_opath + str(len(new_list) + 1)
        #             else:
        #                 orginal_opath = orginal_opath + str(1)
        #             cv2.drawContours(test_image, [cnt], -1, (0, 0, 0), 3)
        #             hull2 = cv2.convexHull(cnt)
        #             cv2.drawContours(test_image, [hull2], 0, (0, 255, 0), 2)
        #             cv2.putText(test_image,'Defect: ' + str(total_defect),
        #                         (5, 2000), cv2.FONT_HERSHEY_PLAIN, 5, (0, 0, 255), 5)
        #             cv2.imwrite(orginal_opath + '_defect_contour2.png', test_image)
        #             break
        # #---not avg(total_defect) coz it takes furthest point for each line in hull.
        # #---if avg(total_defect) small furthest point cause large furthest points to be insignificant
        # convexity_defect = (total_defect/door_width)*w
        # os.remove(score_path+'_defect_contour.png')
        # return convexity_defect


    def find_if_is_interesting_line(self,w8,line_number,line_data,is_ipl):
        similar_to_ipl_flag = line_data[line_number][1]
        if similar_to_ipl_flag == 'S':
            ipl_penalty = 0
        else:
            ipl_penalty = 1

        return ipl_penalty*w8


    def calculate_score(self, room_type,door_width,weight_list, lines,textID,
                        boundary_cordinates,shortest_path_data,line_type,
                        edge_extension_data,score_path,img_height,img_width,
                        Test_path,non_voronoi_line_data,text_cordinate,open_plan_text_cordinate):
        # print 'Text:',textID,' : ', boundary_cordinates
        # print line_type,lines

        final_score=[]
        all_scores =[]
        w2,w3,w4,w5,w7,w8 = weight_list

        if room_type=='furniture':
            w2, w3, w4, w5, w7 = [0.2, 0.2, 0.2, 0.005, 1.0]
            print 'FURNITURE CAME HERE ERRORRRRRRRRRRR-----------------'
        elif room_type=='NOPRoom' or room_type=='wc' or room_type=='bath':
            w2, w3, w4, w5, w7, w8 = 1.0, 0.1,0.1, 0.25, 0.7, 0.15
        elif room_type=='corridor':
            w2, w3, w4, w5, w7, w8 = 0.8, 0.1, 0.35, 0.6, 0.15, 0.7


        #---- if line type is voronoi or improved voronoi
        if line_type == 'V' or line_type == 'I':
            #------1. calculate penalty for length
            total_length, line_gradient, line_actual_length = self.calculate_line_length(w2,lines,door_width)
            #------2. calculate penalty for line gradient
            final_gradient = self.calculate_line_gradient(w3,line_gradient)
            #------3. calculate penalty for distance from each cordinate
            if room_type=='furniture':
                final_distance = self.furniture_text_distance(w4,'i',door_width,textID,boundary_cordinates,lines,shortest_path_data)
                final_distance = final_distance+200
            else:
                final_distance = self.calculate_line_distance(w4,'i',open_plan_text_cordinate,img_height,img_width,textID,boundary_cordinates,lines,shortest_path_data,Test_path)
            #------4. calculate penalty for eall_extension length
            wall_extension_length=1*w5
            #------5. calculate penalty for total_convexity_defect if is Improved line :
            if line_type=='I':
                total_convexity_defect = self.calculate_convexity_defect(w7,img_height,img_width,score_path,lines,non_voronoi_line_data,door_width,Test_path,text_cordinate,0)
            else:
                total_convexity_defect=0
            # ------6. calculate penalty for ipl_line or not
            ipl_penalty = 1*w8
            #-----total of all scores
            each_score = total_length+final_gradient+final_distance+wall_extension_length+total_convexity_defect+ipl_penalty
            final_score.append(each_score)
            all_scores.append([total_length,final_gradient,final_distance,wall_extension_length,total_convexity_defect,ipl_penalty])
            # #------calculate score for no of segments
            # normalized_no_segments = self.calculate_no_segements(w1,lines)
            # area_difference = self.calculate_area(w6,door_width,Test_path,lines,score_path,img_height,img_width,non_voronoi_line_data,text_cordinate,seperated_voronoi_for_area_difference)

        #---- if line type is an edge extension
        elif line_type == 'E' or line_type== 'N':
            for e_line in lines:
                # print e_line
                edge_line1 = e_line[0]
                edge_line = [edge_line1]
                edge_line_number = e_line[1]
                #------1. calculate penalty for length
                total_length, line_gradient,line_actual_length = self.calculate_line_length(w2,edge_line,door_width)
                # ------2. calculate penalty for line gradient
                final_gradient = self.calculate_line_gradient(w3,line_gradient)
                #------3. calculate penalty for distance from each cordinate
                if room_type=='furniture':
                    final_distance = self.furniture_text_distance(w4,'e',door_width, textID,boundary_cordinates,edge_line,shortest_path_data)
                else:
                    final_distance = self.calculate_line_distance(w4,'e',open_plan_text_cordinate,img_height,img_width,textID,boundary_cordinates,edge_line,shortest_path_data,Test_path)
                #------4. calculate penalty for wall extension length
                if line_type=='N':
                    wall_extension_length = 1*w5
                else:
                    wall_extension_length = self.wall_extension_length(w5,line_actual_length,edge_line_number,edge_extension_data,door_width)
                #------5. calculate penalty for total_convexity_defect
                total_convexity_defect = self.calculate_convexity_defect(w7,img_height,img_width,score_path,edge_line,non_voronoi_line_data,door_width,Test_path,text_cordinate,edge_line_number)
                # ------6. calculate penalty for ipl or not
                #--- if is ipl then we want it: so ipl_penalty=0 else: we penalize ipl_penalty=1
                if line_type=='N':
                    ipl_penalty = 0
                else:
                    ipl_penalty = self.find_if_is_interesting_line(w8,edge_line_number,edge_extension_data,False)

                #-----total of all scores
                each_score = total_length+final_gradient+final_distance+wall_extension_length+total_convexity_defect+ipl_penalty
                final_score.append(each_score)
                all_scores.append([total_length,final_gradient,final_distance,wall_extension_length,total_convexity_defect,ipl_penalty])
                # print '++line is: ',line_type,edge_line
                # print 'total', each_score
                # print '0', total_length
                # print '1', final_gradient
                # print '2', final_distance
                # print '3', wall_extension_length
                # print '4', total_convexity_defect
                # print '5', ipl_penalty
                # # #------calculate score for no of segments
                # # normalized_no_segments = self.calculate_no_segements(w1,edge_line)
                # # ------calculate score for area_difference
                # # area_difference = self.calculate_area(w6,door_width,Test_path,edge_line,score_path,img_height,img_width,non_voronoi_line_data,text_cordinate,seperated_voronoi_for_area_difference)

        # print 'in method',final_score
        # for row in all_scores:
        #     print row
        # print '--------------------------------------'
        return final_score,all_scores