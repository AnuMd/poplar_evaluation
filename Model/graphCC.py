__author__ = 'pierpaolo'

import os
import time
import numpy as np
from edgesCC import EdgeCC
import cv2
import cv2.cv as cv
import math
from connectedcomponent import CC, bins1, bins2
import sys
import mathMethods as mm
from scipy import ndimage
import scipy.ndimage
import matplotlib.pyplot as plt
import imageOperations as imop
from copy import deepcopy


class GraphCC(object):
    def __init__(self):
        self._nodes = []
        self._indices = {}
        self.edges = []
        self.edges_ind = {}
        self.image = None
        self.img_red_wall = None
        self.img_no_wall = None
        self.img_wall = None
        self.img_no_text = None
        self.label = 'None'
        self.ellipses = []
        self.circles = []
        self.rectangles = []
        self.width = 0
        self.threshes = []
        self.wall_cond = 0
        self.main_nodes = []
        self.rule = ''
        self.extend = False


    def contatempo(fn):
        def interna(*args,**kw):
            start=time.time()
            result=fn(*args,**kw)
            tempo=time.time()-start
            print "Function: %s - Execution time: %4.2f" % (fn.__name__,tempo)
            return result
        return interna


    def get_node(self, u):
        return self._nodes[self._indices[u]]

    def get_all_nodes(self):
        return self._nodes


    # def get_edge(self, e):
    #     return self.edges[self.edges_ind[e]]

    def getMaxNode(self):
        if self.size() == 1:
            return self._nodes[0]
        maxArea = 0
        for i in self._indices:
            n = self.get_node(i)
            if maxArea < n.area and n.id >0:
                maxArea = n.area
                node = i
        return self.get_node(node)

    def get_n_max_nodes(self, n):
        if self.size() >= n:
            graph_cc = GraphCC()
            list_cc = []
            while graph_cc.size() < n:
                max = 0
                for cc in self._indices:
                    if not graph_cc.isIn(cc) and cc.area > max:
                        cc_max = cc
                        max = cc.area
                graph_cc.add_node(cc_max)
                list_cc.append(cc_max)
                # print str(graph_cc.size())

            return list_cc
        else:
            print 'cannot extract '+str(n)+' cc from this graph'
            return False

    def add_edge(self, edge):
        if edge not in self.edges_ind:
            self.edges.append(edge)
            self.edges_ind[edge] = len(self.edges_ind) -1

    def add_node(self, u):
        if u not in self._indices:
            self._nodes.append(u)
            self._indices[u] = len(self._nodes) - 1
        return self.get_node(u)

    def add_node2(self, u): #with value return: 1 if the node is not in the graph, 0 if it is in the graph
        if u not in self._indices:
            self._nodes.append(u)
            self._indices[u] = len(self._nodes) - 1
            return 1
        else:
            return 0

    def get_indices(self):
        return self._indices

    #@contatempo
    def create_edges(self, width, dictionary):
        del self.edges[:]
        self.edges_ind.clear()
        for cc in self._indices:
            cc.clean()
        for cc1 in self._indices:
            for cc2 in self._indices:
                if not cc1.islinked(cc2) and cc1.id!=cc2.id and not cc1.isnotlinked(cc2) and cc1.id != 0 and cc2.id != 0:
                    cx1=cc1.center_x
                    cy1=cc1.center_y
                    cx2=cc2.center_x
                    cy2=cc2.center_y
                    distance = mm.distance2D(cx1, cy1, cx2, cy2)
                    # print 'center 1 = '+str(cy1)+', '+str(cx1)
                    # print 'center 2 = '+str(cy2)+', '+str(cx2)
                    # print 'distance between centers = ' +str(distance)
                    dist = cc1.get_max_distance() + cc2.get_max_distance() + int(width*2)+2
                    # print 'sum of max distances = ' + str(dist)
                    if dist > distance:
                        self.__evaluate_adjacent(cc1,cc2,width, dictionary, floor=False)
                    else:
                        cc1.add_notlink(cc2)
                        cc2.add_notlink(cc1)



    #@contatempo
    def __evaluate_adjacent(self,cc1,cc2,width, dictionary, floor):
        # print "floor ",floor
        if floor:
            touching, dst = imop.touching_dilation2(cc1,cc2)
        else:
            # cross_wall = imop.over_wall(self.img_red_wall, cc1,cc2)
            # if cross_wall == False:
            touching, dst = imop.touching_dilation(cc1,cc2,width)
            if self.width == 0:
                # print 'set the stroke width to '+str(width)
                self.width = width
                # print "touching",touching
        if touching == True and floor == False:
            wall = True
            if not dictionary:
                wall_node = self.get_node_from_id(0)
                wall = imop.over_wall2(wall_node, dst, cc1, cc2)
            if wall:
                # print 'add edge to the graph '
                self.calculate_edges_attributes(cc1,cc2,dst)
        else:
            # print 'no edge added'
            cc1.add_notlink(cc2)
            cc2.add_notlink(cc1)
        return touching

    def get_edges_to_node(self, cc):
        edges = []
        for edge in self.edges_ind:
            if edge.node1.id == cc.id or edge.node2.id == cc.id:
                edges.append(edge)
        return edges

    def save_images(self, path):
        for cc in self._indices:
            cv2.imwrite(path+str(cc.id)+'.png', cc.image)

    def floor_adjacence(self,floor, dictionary):
        for edge in self.get_edges_to_node(floor):
            if edge.node1.id != 0:
                edge.type = 'floor'
                if edge.node1.id != floor.id:
                    cc1 = edge.node1
                else:
                    cc1 = edge.node2
                for edge in self.get_edges_to_node(floor):
                    if edge.node1.id != floor.id:
                        cc2 = edge.node1
                    else:
                        cc2 = edge.node2
                    if dictionary:
                        cross_wall = False
                    else:
                        cross_wall = imop.over_wall(self.img_red_wall, cc1, cc2)
                    id_cond = False
                    if cc1.id != 0 and cc2.id != 0 and cc1.id != cc2.id:
                        id_cond = True
                    if self.exist_link(cc1, cc2) == False and cross_wall == False and id_cond:
                        width=1
                        adj = self.__evaluate_adjacent(cc1,cc2,width, dictionary, floor=True)
                        if adj:
                                divisor = max(cc1.area, cc2.area)
                                if divisor != 0:
                                    areaRatio = min(cc1.area, cc2.area)/divisor
                                else:
                                    areaRatio = 0
                                edge = EdgeCC(cc1, cc2, [1,1,areaRatio])
                                edge.type = 'floor'
                                self.add_edge(edge)

    def exist_link(self, cc1, cc2):
        for edge in self.edges_ind:
            id = edge.get_nodes_id()
            if id[0] == min(cc1.id, cc2.id) and id[1] == max(cc1.id, cc2.id):
                return True
        return False


    def save_edges(self, floor):
        img = cv2.cvtColor(self.img_no_text, cv2.COLOR_GRAY2RGB)
        s = self.img_no_text.shape
        img2 = np.zeros(s)
        img2.fill(255)
        img2 = np.array(img2, dtype=np.uint8)
        img2 = cv2.cvtColor(img2, cv2.COLOR_GRAY2RGB)
        for edge in self.edges_ind:
            cc1, cc2 = edge.get_nodes()
            if cc1.id != 0:
                if edge.type == 'touch':
                    cv2.line(img,(cc1.center_x, cc1.center_y),(cc2.center_x, cc2.center_y),(255,0,0), 1)
                    cv2.line(img2,(cc1.center_x, cc1.center_y),(cc2.center_x, cc2.center_y),(255,0,0), 1)
                else:
                    cv2.line(img,(cc1.center_x, cc1.center_y),(cc2.center_x, cc2.center_y),(0,255,0), 1)
                    cv2.line(img2,(cc1.center_x, cc1.center_y),(cc2.center_x, cc2.center_y),(0,255,0), 1)

        for cc in self._indices:
            if cc.id != 0:
                cv2.circle(img,(cc.center_x, cc.center_y), 3, (0,0,255), -1)
                cv2.circle(img2,(cc.center_x, cc.center_y), 3, (0,0,255), -1)
        # cv2.circle(img,(floor.center_x, floor.center_y), 3, (0,0,255), -1)
        # cv2.circle(img2,(floor.center_x, floor.center_y), 3, (0,0,255), -1)
            # else:
            #     cv2.circle(img,(cc.center_x, cc.center_y), 3, (0,0,0), -1)
            #     cv2.circle(img2,(cc.center_x, cc.center_y), 3, (0,0,255), -1)
        return img, img2

    def save_edges2(self, floor, img):
        for edge in self.edges_ind:
            cc1, cc2 = edge.get_nodes()
            if cc1.id != 0:
                if edge.type == 'touch':
                    cv2.line(img,(cc1.center_x, cc1.center_y),(cc2.center_x, cc2.center_y),(255,0,0), 1)
                else:
                    cv2.line(img,(cc1.center_x, cc1.center_y),(cc2.center_x, cc2.center_y),(0,255,0), 1)
        wall = self.get_node_from_id(0)
        for cc in self._indices:
            if cc.id != 0 and cc.is_linked(wall):
                cv2.circle(img,(cc2.center_x, cc2.center_y), 2, (0,0,255), -1)
            else:
                cv2.circle(img,(cc.center_x, cc.center_y), 2, (0,0,0), -1)
        return img

    #@contatempo
    def calculate_edges_attributes(self,cc1,cc2,dst):
        cc=CC(1)
        area1 = float(cc1.area)
        area2 = float(cc2.area)
        areaRatio = min(area1, area2)/max(area1, area2)
        labels, numlabels = ndimage.label(dst<127)
        #print 'after computing the logical and of adjacent ccs there are ' +str(numlabels)+ ' black components'
        if True:
            #print 'warning one cc inside another'
            attributes = [1,1,areaRatio]
        else:
            point= np.argwhere(dst > 127)
            cc.image=dst
            # find x_max, x_min, y_max, y_min
            cc.y_min=point[:,0].min()
            cc.x_min=point[:,1].min()
            cc.set_coordinates()
            cc.find_contour()
            cx1=cc1.center_x
            cy1=cc1.center_y
            cx2=cc2.center_x
            cy2=cc2.center_y
            corner1=0
            for xA in cc.contourSimple[0]:
                for xB in cc.contourSimple[0]:
                    corner = mm.carnot(xA[0][0], xA[0][1], xB[0][0],xB[0][1],cx1,cy1, True)
                    corner1 = max(corner, corner1)
            corner2=0
            for xA in cc.contourSimple[0]:
                for xB in cc.contourSimple[0]:
                    corner = mm.carnot(xA[0][0], xA[0][1], xB[0][0],xB[0][1],cx2,cy2, True)
                    corner2 = max(corner, corner2)

            #print 'angle between cc '+str(cc1.id)+' and cc '+str(cc2.id)+' = '+str(corner1)
            #print 'angle between cc '+str(cc2.id)+' and cc '+str(cc1.id)+' = '+str(corner2)


            attributes = []
            if corner1 == 0:
                corner1 = 0.0001
            if corner2 == 0:
                corner2 = 0.0001
            if corner1 < corner2:
                attributes.append(corner1/corner2)
                attributes.append(corner2/corner1)
            else:
                attributes.append(corner2/corner1)
                attributes.append(corner1/corner2)
            attributes.append(areaRatio)
        edge = EdgeCC(cc1,cc2,attributes)
        # print 'the arc attributes between node ' +str(cc1.id) +' and node ' +str(cc2.id) +' are:'
        # print str(attributes)
        self.edges.append(edge)
        self.edges_ind[edge] = len(self.edges) -1
        cc1.add_link(cc2)
        cc2.add_link(cc1)
        # print 'added a link between cc '+str(cc1.id)+' e '+str(cc2.id)

    def get_edge(self,cc1,cc2):
        for edge in self.edges_ind:
            id = edge.get_nodes_id()
            if id[0] == min(cc1.id, cc2.id) and id[1] == max(cc1.id, cc2.id):
                return edge
        print 'edge not found! '


    def size(self):
        return len(self._nodes)

    def isIn(self,n):
        if n in self._indices:
            return 1
        else:
            return 0

    def isInId(self, id):
        for cc in self._indices:
            if cc.id == id:
                return True
        return False

    def remove_edges(self, node):
        purge = []
        for edge in self.edges_ind:
            id = edge.get_nodes_id()
            if id[0]==node.id or id[1]==node.id:
                purge.append(edge)
                if id[0]==node.id:
                    edge.node2.remove_linked(node)
                else:
                    edge.node1.remove_linked(node)
        for e in purge:
            self.remove_edge(e)

    def get_edges_to(self, cc):
        edges = []
        for edge in self.edges_ind:
            id1, id2 = edge.get_nodes_id()
            if id1 == cc.id or id2 == cc.id:
                edges.append(edge)
        return edges


    def remove_edge(self, edge):
        if edge in self.edges_ind:
            self.edges.remove(edge)
            # print 'edge removed from  '+str(edge.node1.id)+' to '+str(edge.node2.id)
            for n in self.edges[self.edges_ind[edge]:len(self.edges_ind)]:
                self.edges_ind[edge] = self.edges_ind[n] - 1
            del self.edges_ind[edge]


    def remove_node(self,u):
        if u in self._indices:
            self._nodes.remove(u)
            for n in self._nodes[self._indices[u]:len(self._nodes)]:
                self._indices[n] = self._indices[n] - 1
            del self._indices[u]


    def remove_node_and_edge(self,u):
        if u in self._indices:
            self._nodes.remove(u)
            for n in self._nodes[self._indices[u]:len(self._nodes)]:
                self._indices[n] = self._indices[n] - 1
            del self._indices[u]
            self.remove_edges(u)

    def draw_rectangle(self, path, image):
        xmin, xmax, ymin, ymax = self.get_extremes()
        img = deepcopy(image)
        cv2.rectangle(img,(xmin, ymin), (xmax, ymax), (0,255,0), 3)
        cv2.imwrite(path+self.label+'.png', img)
        del img

    def save_graph(self,path_output,file_name, dictionary):
        if self.size() > 0:
            name = str(file_name[0:-4])
            file = open(str(path_output)+name+".txt","w")
            # else:
            #     file = open(str(path_output)+name+'/'+name+".txt","w")
            if (len(self._nodes[0].histogram)) == 0:
                for cc in self._indices:
                    cc.find_contour()
                    cc.create_histogram(bins1, bins2)
            file.write(str(len(self._nodes[0].histogram[0]))+'\n')
            file.write(str(len(self._nodes))+'\n')
            file.write(str(self.width)+'\n')
            for i in self._indices:
                cc = self.get_node(i)
                file.write(str(cc.id)+'\n')
                file.write(cc.shape+'\n')
                # file.write(str(cc.center_x)+"\n"+ str(cc.center_y)+"\n"+ str(cc.area)+'\n')
                file.write(str(cc.x_min)+"\n"+ str(cc.x_max)+"\n"+ str(cc.y_min)+'\n' + str(cc.y_max)+'\n')
                file.write(str(cc.max_distance)+'\n')

                for j in range(0, len(cc.histogram)):
                    for k in range(0,len(cc.histogram[0])):
                        file.write(str(float(cc.histogram[j][k][0]))+'\n')

                file.write(str(cc.ratio)+'\n')

                for j in range(0, len(cc.histogram2)):
                    for k in range(0,len(cc.histogram[0])):
                        file.write(str(float(cc.histogram2[j][k][0]))+'\n')


            file.write(str(len(self.edges))+'\n')
            for k in range(0, len(self.edges_ind)):
                nodes = self.edges[k].get_nodes_id()
                attributes = self.edges[k].get_attributes()
                file.write(str(nodes[0])+'\n'+str(nodes[1])+'\n')
                file.write(str(attributes[0])+'\n'+str(attributes[1])+'\n' +str(attributes[2])+'\n')
                file.write(self.edges[k].type+'\n')
            file.close()

    def load_graph(self,path_input,file_name,dictionary):
        del self._nodes[:]
        del self.edges[:]
        path = str(path_input)+file_name+".txt"
        file = open(path,'r')
        numBins = int(file.readline())
        # numBins: number of values to represent each contour
        numLabels = int(file.readline())
        # numLabels is the number of white connected components in the page?
        # print 'load '+str(numLabels)+' cc'
        self.width = float(file.readline())
        #--- dictionary == False means its room, so use room inout images
        if dictionary == False:
            self.img_red_wall = cv2.imread(path_input+'red_wall.png',cv2.IMREAD_COLOR)
            self.img_wall = cv2.imread(path_input+'walls_and_doors.png',cv2.IMREAD_GRAYSCALE)
            self.img_no_wall = cv2.imread(path_input+'no_wall.png',cv2.IMREAD_GRAYSCALE)
            self.img_no_text = cv2.imread(path_input+'no_text.png',cv2.IMREAD_GRAYSCALE)
        for k in range(0,numLabels):
                id = int(file.readline())
                # id: cc identifier
                cc = CC(id)
                # cc contains the cc pixels labeled as id
                shape = file.readline()
                #shape = rectangle or ellipse or circle or unknown
                cc.shape = shape[0:-1]
                cc.image = cv2.imread(path_input+str(id)+'.png',0)
                #cc.image is the image of the connected component (in white)
                cc.find_contour()
                #find the pixels of the contour
                cc.x_min = int(file.readline())
                cc.x_max = int(file.readline())
                cc.y_min = int(file.readline())
                cc.y_max = int(file.readline())
                # min and max of the upright bounding box of the connected component
                cc.max_distance = float(file.readline())
                # ?
                cc.set_coordinates()
                cc.numPixel=(cc.image.sum()/255)
                for i in range (0, len(cc.contourNone)):
                    histogram = np.zeros((numBins,1))
                    for j in range(0,numBins):
                        histogram[j] = float(file.readline())
                    cc.histogram.append(histogram)
                    # cc.histogram contains the histogram describing the contour of cc

                cc.ratio = float(file.readline())

                for i in range (0, len(cc.contourNone)):
                    histogram2 = np.zeros((numBins,1))
                    for j in range(0,numBins):
                        histogram2[j] = float(file.readline())
                    cc.histogram2.append(histogram2)
                    #cc.histogram2 contains the histogram2 describing the contour of cc
                # cc.create_histogram(bins1,bins2)
                # if dictionary == False:
                #     cc.remove_contours()
                #     if len(cc.histogram) > len(cc.contourSimple):
                #         cc.create_histogram(bins1, bins2)
                cc.set_distances()
                self._nodes.append(cc)
                self._indices[cc] = len(self._nodes) - 1


        numEdges = int(file.readline())
        # number of edges in the room graph
        for k in range(0,numEdges):
                attributes = [0,0,0]
                node1 = int(file.readline())
                # first node connected by the edge
                node2 = int(file.readline())
                # second node connected by the edge
                #print 'edge between  nodes '+str(node1)+' and '+str(node2)
                cc1 = self.get_node_from_id(node1)
                cc2 = self.get_node_from_id(node2)
                cc1.add_link(cc2)
                cc2.add_link(cc1)
                #--reads three lines in graph.txt
                for n in range(0,3):
                    #---1st attribute: inside
                    # 0 if no cc is contained in the other
                    # 1 if cc2 is contained in cc1
                    # 2 if cc1  is contained in cc2
                    # 3 if the bounding box of cc2 contains cc1
                    # 4 if the bounding box of cc1 contains cc2

                    #---2st attribute: area ratio of the two ccs
                    #---3st attribute:
                    attributes[n] = (float(file.readline()))
                    #attributes of the edge

                edge = EdgeCC(cc1,cc2,attributes)
                type = file.readline()
                # touch or floor
                edge.type = type[0:-1]
                self.edges.append(edge)
                self.edges_ind[edge] = len(self.edges)-1
        file.close()
        # self.update_attributes()
        self.count_shapes()

    def update_attributes(self):
        for edge in self.edges_ind:
            c1, c2 = edge.get_nodes()
            divisor = max(c1.numPixel, c2.numPixel)
            if divisor != 0:
                area = min(c1.numPixel, c2.numPixel)/divisor
            else:
                area = 0
            edge.set_area_ratio(area)

    def count_shapes(self):
        del self.ellipses[:]
        del self.circles[:]
        del self.rectangles[:]
        for cc in self._indices:
            shape = cc.check_shape()
            # if cc.id == 18:
            #     print 'cc '+str(cc.id)+' is a '+cc.shape
            if len(shape)!=0:
                if shape == 'rectangle':
                    self.rectangles.append(cc)
                if shape == 'circle':
                    self.circles.append(cc)
                if shape == 'ellipse':
                    self.ellipses.append(cc)
        return len(self.circles), len(self.ellipses), len(self.rectangles)

    def get_num_shapes(self):
        return len(self.circles), len(self.ellipses), len(self.rectangles)

    def save_circles(self, path):
        for cc in self.circles:
            cv2.imwrite(path+self.label+'_'+str(cc.id)+'.png', cc.image)
        # cv2.imwrite(path+self.label+'.png', self.img_no_text)

    def save_ellipses(self, path):
        for cc in self.ellipses:
            cv2.imwrite(path+self.label+'_'+str(cc.id)+'.png', cc.image)
        # cv2.imwrite(path+self.label+'.png', self.img_no_text)

    def save_rectangles(self, path):
        for cc in self.rectangles:
            cv2.imwrite(path+self.label+'_'+str(cc.id)+'.png', cc.image)
        # cv2.imwrite(path+self.label+'.png', self.img_no_text)

    def save_unknown(self, path):
        for cc in self._indices:
            if cc.shape == 'unknown':
                cv2.imwrite(path+self.label+'_'+str(cc.id)+'.png', cc.image)

    def get_node_from_id(self,id):
        found = False
        for i in self._indices:
            node = self.get_node(i)
            if id == node.id:
                return node
        if found == False:
            # print 'node with id = '+str(id)+' not found'
            sys.exit()

    def purge(self, purgandi):
        # print 'purgandi size = '+str(purgandi.size())
        if purgandi.size() > 0 and purgandi.size() != self.size():
            for i in purgandi.get_indices():
                self.remove_node(i)
                # print 'removed node '+str(i.id)
            # print 'we removed '+str(purgandi.size())+' nodes!'
        elif purgandi.size() == self.size():
            del self._nodes[:]
            self._indices.clear()
            # print 'removed all the graph nodes!'

    def save_crop_image(self,path):
        xmin, xmax, ymin, ymax = self.get_extremes()
        cv2.imwrite(path,self.img_no_text[ymin:ymax, xmin:xmax])

    def get_extremes(self):
        xmin = 9999999
        ymin = 9999999
        xmax = 0
        ymax = 0
        for cc in self._indices:
            if cc.id != 0:
                xmin = min(xmin, cc.x_min)
                ymin = min(ymin, cc.y_min)
                xmax = max(xmax, cc.x_max)
                ymax = max(ymax, cc.y_max)
        return xmin, xmax, ymin, ymax

    def add_threshes(self, th):
        if len(th) == 4:
            self.thresh = th
        else:
            print 'wrong thresh assignment!'

    def check_size(self, graph, door, size):
        xmin, xmax, ymin, ymax = graph.get_extremes()
        max_axis = float(max((xmax-xmin),(ymax-ymin)))/door
        min_axis = float(min((xmax-xmin),(ymax-ymin)))/door
        # print 'Current Object: Max Size is- ',max_axis,', Min Size is- ',min_axis
        if self.label[0:7] == 'crossed':
            area = 0
            for cc in graph.get_indices():
                area+=cc.area
            xmin, xmax, ymin, ymax = graph.get_extremes()
            area_rect = (xmax-xmin)*(ymax-ymin)
            ratio = area/area_rect
            size = graph.size()
            if 0.2*size <= ratio <= 0.28*size:
                return True
            else:
                return False
        t1 = self.thresh[1]*size
        t2 = self.thresh[3]*size
        if self.thresh[0] <= min_axis <= t1 and self.thresh[2] <= max_axis <= t2:
            return True
        else:
            return False

    def check_max_size(self, graph, door, size):
        xmin, xmax, ymin, ymax = graph.get_extremes()
        max_axis = float(max((xmax-xmin),(ymax-ymin)))/door
        min_axis = float(min((xmax-xmin),(ymax-ymin)))/door
        t1 = self.thresh[1]*size
        t2 = self.thresh[3]*size
        if min_axis <= t1 and max_axis <= t2:
            return True
        else:
            return False



    def set_cc_to_find(self, num):
        if num <= self.size():
            n = num
        else:
            n = self.size()
        self.main_nodes = self.get_n_max_nodes(n)



