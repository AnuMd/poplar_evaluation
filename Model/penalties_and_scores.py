import cv2, math, sys, copy,os

from open_plan_line import line
from line_optimization_functions import line_optimization_function_class

class penalties_scores_class:

    def __init__(self):
        self.line_obj = line()
        self.line_optimization_function_obj = line_optimization_function_class()

    def calculate_penalties(self,room_type,avg_door_width,
                            img_height, img_width,weight_list,
                            shortest_path_data,edge_extension_data,
                            open_plan_text_cordinate,boundaries,
                            seperated_voronoi_lines,seperated_edge_lines,
                            improved_voronoi_lines,seperated_ipl_lines,
                            non_voronoi_use,floor_plan_copy,
                            score_path,Test_path,debug_mode):
        # Explanation:
        # 1. Calculate voronoi lines(seperated_voronoi_lines) penalty -voronoi_scores
        # 2. Calculate improved voronoi lines(improved_voronoi_lines) penalty - improved_voronoi_scores
        # 3. Calculate edge extension lines(seperated_edge_lines) penalty - edge_extension_score
        print_start_point = 2000

        # 1. Calculate voronoi lines(seperated_voronoi_lines) penalty - voronoi_scores
        voronoi_penalties = []
        for vor_row_count, voronoi_line1 in enumerate(seperated_voronoi_lines):
            line_type = 'V'
            temp_voronoi_penalties = []
            textID = voronoi_line1[0]
            voronoi_line = voronoi_line1[1]
            voronoi_penalties.append([])
            voronoi_penalties[vor_row_count].append(textID)
            for r, boundary in enumerate(voronoi_line):
                if debug_mode:
                    bw_image = floor_plan_copy.copy()
                    floor_plan_boundaries = cv2.cvtColor(bw_image, cv2.COLOR_GRAY2RGB)
                    cv2.putText(floor_plan_boundaries, '------------Voronoi Penalties----------------',
                                (5, print_start_point), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255), 2)

                b_cord_row = []
                for b_row in boundaries:
                    if b_row[0] == textID:
                        b_cord_row = b_row[1]
                boundary_cordinates = b_cord_row[r]

                non_vor_row_lines = []
                for non_voronoi_row in non_voronoi_use:
                    if non_voronoi_row[0] == textID:
                        non_vor_row_lines = non_voronoi_row[1]
                        break
                non_voronoi_line_data = non_vor_row_lines[r]
                final_score, all_scores = self.line_obj.calculate_score(room_type, avg_door_width, weight_list, boundary,
                                                                   textID, boundary_cordinates, shortest_path_data,
                                                                   line_type, edge_extension_data, score_path,
                                                                   img_height, img_width, Test_path,
                                                                   non_voronoi_line_data,
                                                                   0, open_plan_text_cordinate)
                temp_voronoi_penalties.append(final_score)
                score_row = all_scores[0]
                if debug_mode:
                    cv2.putText(floor_plan_boundaries,
                                'v.' + str(textID + 1) + '-' + str(r + 1) + '->T:' + str(final_score) +
                                '=' + 'Len' + str(score_row[0]) + ', G' + str(score_row[1]) + ', ' + 'Txt' + str(
                                    score_row[2]) + ', ' + 'WL' + str(score_row[3]) + ' ,C' + str(
                                    score_row[4]) + ' ,Ipl' + str(score_row[5]),
                                (5, print_start_point + 30), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255), 2)
                    cv2.imwrite(score_path + 'Partition ' + str(textID) + ' Boundary ' + str(r) + 'Line Scores.png',
                                floor_plan_boundaries)

            voronoi_penalties[vor_row_count].append(temp_voronoi_penalties)  # -----------******************

        # 2. Calculate improved voronoi lines(improved_voronoi_lines) penalty - improved_voronoi_scores
        improved_voronoi_penalties = []
        for imp_vor_row_count, improved_voronoi_line1 in enumerate(improved_voronoi_lines):
            line_type = 'I'
            temp_improved_voronoi_penalties = []
            textID = improved_voronoi_line1[0]
            improved_voronoi_line = improved_voronoi_line1[1]
            improved_voronoi_penalties.append([])
            improved_voronoi_penalties[imp_vor_row_count].append(textID)
            for r, boundary in enumerate(improved_voronoi_line):
                b_cord_row = []
                for b_row in boundaries:
                    if b_row[0] == textID:
                        b_cord_row = b_row[1]
                boundary_cordinates = b_cord_row[r]

                non_vor_row_lines = []
                for non_voronoi_row in non_voronoi_use:
                    if non_voronoi_row[0] == textID:
                        non_vor_row_lines = non_voronoi_row[1]
                        break
                non_voronoi_line_data = non_vor_row_lines[r]

                # -----used in area difference score calculation
                # contour_partition_lines = []
                # for row_set in partition_area_lines:
                #     if textID == row_set[1]:
                #         contour_partition_lines = row_set[0]
                #         break
                text_cordinate = open_plan_text_cordinate[textID]

                temp_print_value = 70
                if debug_mode:
                    floor_plan_boundaries = cv2.imread(
                        score_path + 'Partition ' + str(textID) + ' Boundary ' + str(r) + 'Line Scores.png',
                        cv2.IMREAD_COLOR)
                initial_value = temp_print_value
                temp_print_value = temp_print_value + 30
                temp_line_set = []
                exists = False
                for l, line_set in enumerate(boundary):
                    if len(line_set) > 0:
                        exists = True
                        # -------calculate score
                        final_score, all_scores = self.line_obj.calculate_score(room_type, avg_door_width, weight_list,
                                                                           line_set, textID, boundary_cordinates,
                                                                           shortest_path_data, line_type,
                                                                           edge_extension_data, score_path,
                                                                           img_height, img_width, Test_path,
                                                                           non_voronoi_line_data, text_cordinate,
                                                                           open_plan_text_cordinate)
                        temp_line_set.append(final_score)
                        score_row = all_scores[0]
                        if debug_mode:
                            cv2.putText(floor_plan_boundaries, 'i. ' + str(l + 1) + '->T:' + str(final_score) +
                                        '=' + 'Len' + str(score_row[0]) + ', G' + str(
                                score_row[1]) + ', ' + 'Txt' + str(
                                score_row[2]) + ', ' + 'WL' + str(score_row[3]) + ' ,C' + str(
                                score_row[4]) + ' ,Ipl' + str(score_row[5]),
                                        (5, print_start_point + temp_print_value), cv2.FONT_HERSHEY_PLAIN, 2,
                                        (255, 0, 0), 2)
                            temp_print_value = temp_print_value + 30
                    else:
                        temp_line_set.append([])
                if debug_mode:
                    if exists:
                        # cv2.putText(floor_plan_boundaries, '------------Voronoi Improvement Penalties----------------', (5,print_start_point+initial_value), cv2.FONT_HERSHEY_PLAIN, 2, (255, 0, 0), 2)
                        temp_print_value = temp_print_value + 30
                temp_improved_voronoi_penalties.append(temp_line_set)
                if debug_mode:
                    cv2.imwrite(score_path + 'Partition ' + str(textID) + ' Boundary ' + str(r) + 'Line Scores.png',
                                floor_plan_boundaries)
            improved_voronoi_penalties[imp_vor_row_count].append(temp_improved_voronoi_penalties)

        # 3. Calculate edge extension lines(seperated_edge_lines) penalty - edge_extension_score
        edge_extension_penalties = []
        for edge_row_count, edge_line_row1 in enumerate(seperated_edge_lines):
            line_type = 'E'
            temp_edge_extension_penalties = []
            textID = edge_line_row1[0]
            edge_line_row = edge_line_row1[1]
            edge_extension_penalties.append([])
            edge_extension_penalties[edge_row_count].append(textID)
            for r, edge_line_set in enumerate(edge_line_row):

                b_cord_row = []
                for b_row in boundaries:
                    if b_row[0] == textID:
                        b_cord_row = b_row[1]
                boundary_cordinates = b_cord_row[r]

                non_vor_row_lines = []
                for non_voronoi_row in non_voronoi_use:
                    if non_voronoi_row[0] == textID:
                        non_vor_row_lines = non_voronoi_row[1]
                        break
                non_voronoi_line_data = non_vor_row_lines[r]

                # -----used in area difference score calculation
                # contour_partition_lines =[]
                # for row_set in partition_area_lines:
                #     if textID == row_set[1]:
                #         contour_partition_lines = row_set[0]
                #         break
                text_cordinate = open_plan_text_cordinate[textID]

                temp_print_value = 250
                if debug_mode:
                    floor_plan_boundaries = cv2.imread(
                        score_path + 'Partition ' + str(textID) + ' Boundary ' + str(r) + 'Line Scores.png',
                        cv2.IMREAD_COLOR)

                if len(edge_line_set) > 0:
                    if debug_mode:
                        cv2.putText(floor_plan_boundaries, '------------Edge Extensions Penalties----------------',
                                    (5, print_start_point + temp_print_value), cv2.FONT_HERSHEY_PLAIN, 2, (0, 255, 0),
                                    2)
                        temp_print_value = temp_print_value + 30
                        # --------calculate score
                    final_score, all_scores = self.line_obj.calculate_score(room_type, avg_door_width, weight_list,
                                                                       edge_line_set,
                                                                       textID, boundary_cordinates, shortest_path_data,
                                                                       line_type, edge_extension_data, score_path,
                                                                       img_height, img_width, Test_path,
                                                                       non_voronoi_line_data, text_cordinate,
                                                                       open_plan_text_cordinate)
                    temp_edge_extension_penalties.append(final_score)
                    for e, score_row in enumerate(all_scores):
                        if debug_mode:
                            cv2.putText(floor_plan_boundaries, 'e. ' + str(e + 1) + '->T:' + str(final_score[e]) +
                                        '=' + 'Len' + str(score_row[0]) + ', G' + str(
                                score_row[1]) + ', ' + 'Txt' + str(
                                score_row[2]) + ', ' + 'WL' + str(score_row[3]) + ' ,C' + str(
                                score_row[4]) + ' ,Ipl' + str(score_row[5]),
                                        (5, print_start_point + temp_print_value), cv2.FONT_HERSHEY_PLAIN, 2,
                                        (0, 255, 0), 2)
                            temp_print_value = temp_print_value + 30
                            # tx1,ty1 = open_plan_text_cordinate[textID]
                            # cv2.circle(floor_plan_boundaries, (tx1, ty1), 10, (0, 0, 255), -1)
                            # cv2.imwrite(score_path+'Partition '+str(textID)+str(e)+' Boundary '+str(r)+ 'Line Scores.png',floor_plan_boundaries)

                else:
                    temp_edge_extension_penalties.append([])
                if debug_mode:
                    # tx1,ty1 = open_plan_text_cordinate[textID]
                    # cv2.circle(floor_plan_boundaries, (tx1, ty1), 10, (0, 0, 255), -1)
                    cv2.imwrite(score_path + 'Partition ' + str(textID) + ' Boundary ' + str(r) + 'Line Scores.png',
                                floor_plan_boundaries)

            edge_extension_penalties[edge_row_count].append(
                temp_edge_extension_penalties)  # -----------******************

        # 4. Calculate interesting lines(ipl_lines) penalty - ipl_score
        ipl_penalties = []
        for ipl_row_count, ipl_line_row1 in enumerate(seperated_ipl_lines):
            line_type = 'N'
            temp_ipl_extension_penalties = []
            textID = ipl_line_row1[0]
            ipl_line_row = ipl_line_row1[1]
            ipl_penalties.append([])
            ipl_penalties[ipl_row_count].append(textID)
            for r, ipl_line_set in enumerate(ipl_line_row):
                b_cord_row = []
                for b_row in boundaries:
                    if b_row[0] == textID:
                        b_cord_row = b_row[1]
                boundary_cordinates = b_cord_row[r]

                non_vor_row_lines = []
                for non_voronoi_row in non_voronoi_use:
                    if non_voronoi_row[0] == textID:
                        non_vor_row_lines = non_voronoi_row[1]
                        break
                non_voronoi_line_data = non_vor_row_lines[r]

                # # -----used in area difference score calculation
                # contour_partition_lines = []
                # for row_set in partition_area_lines:
                #     if textID == row_set[1]:
                #         contour_partition_lines = row_set[0]
                #         break
                text_cordinate = open_plan_text_cordinate[textID]

                ####-------used in area difference score calculation
                # sep_vor_row_lines = []
                # for sep_voronoi_row in seperated_voronoi_lines:
                #     if sep_voronoi_row[0] == textID:
                #         sep_vor_row_lines = sep_voronoi_row[1]
                #         break
                # seperated_voronoi_for_area_difference = sep_vor_row_lines[r]

                temp_print_value = 250
                if debug_mode:
                    floor_plan_boundaries = cv2.imread(
                        score_path + 'Partition ' + str(textID) + ' Boundary ' + str(r) + 'Line Scores.png',
                        cv2.IMREAD_COLOR)
                    # floor_plan_boundaries = floor_plan_copy.copy()
                if len(ipl_line_set) > 0:
                    if debug_mode:
                        cv2.putText(floor_plan_boundaries, '------------IPL Extensions Penalties----------------',
                                    (5, temp_print_value), cv2.FONT_HERSHEY_PLAIN, 2,
                                    (255, 255, 0), 2)
                        temp_print_value = temp_print_value + 30
                        # --------calculate score
                    final_score, all_scores = self.line_obj.calculate_score(room_type, avg_door_width, weight_list,
                                                                       ipl_line_set, textID, boundary_cordinates,
                                                                       shortest_path_data, line_type,
                                                                       edge_extension_data, score_path,
                                                                       img_height, img_width, Test_path,
                                                                       non_voronoi_line_data, text_cordinate,
                                                                       open_plan_text_cordinate)
                    temp_ipl_extension_penalties.append(final_score)
                    for ip, score_row in enumerate(all_scores):
                        if debug_mode:
                            cv2.putText(floor_plan_boundaries,
                                        'ipl. ' + str(ip + 1) + '->T:' + str(final_score[ip]) +
                                        '=' + 'Len' + str(score_row[0]) + ', G' + str(
                                            score_row[1]) + ', ' + 'Txt' + str(
                                            score_row[2]) + ', ' + 'WL' + str(score_row[3]) + ' ,C' + str(
                                            score_row[4]) + ' ,Ipl' + str(score_row[5]),
                                        (5, temp_print_value), cv2.FONT_HERSHEY_PLAIN, 2,
                                        (255, 255, 0), 2)
                            temp_print_value = temp_print_value + 30

                else:
                    temp_ipl_extension_penalties.append([])
                if debug_mode:
                    cv2.imwrite(score_path + 'Partition ' + str(textID) + ' Boundary ' + str(r) + 'Line Scores.png',
                                floor_plan_boundaries)

            ipl_penalties[ipl_row_count].append(temp_ipl_extension_penalties)

        return voronoi_penalties,edge_extension_penalties,improved_voronoi_penalties,ipl_penalties

    def calculate_scores(self,voronoi_penalties,edge_extension_penalties,
                         improved_voronoi_penalties,ipl_penalties,
                         seperated_voronoi_lines,seperated_edge_lines,
                         improved_voronoi_lines,seperated_ipl_lines,
                         open_plan_text_cordinate, boundaries,
                         score_path, debug_mode):
        # 1. Calculate voronoi score = 0 for all
        voronoi_scores = self.line_optimization_function_obj.calculate_voronoi_score(
            voronoi_penalties)
        # 2. Calculate edge_extension score = voronoi penalty - edge penalty
        edge_extension_scores = self.line_optimization_function_obj.calculate_eel_score(
            voronoi_penalties, edge_extension_penalties)
        # 3.Calculate improved_voronoi score = voronoi penalty - improved voronoi penalty
        improved_voronoi_scores = self.line_optimization_function_obj.calculate_ivl_score(
            voronoi_penalties, improved_voronoi_penalties)
        # 4. Calculate ipl score = voronoi penalty - ipl penalty
        ipl_scores = self.line_optimization_function_obj.calculate_ipl_score(
            voronoi_penalties, ipl_penalties)

        if debug_mode:
            self.line_optimization_function_obj.print_all_scores(
                boundaries, voronoi_scores, edge_extension_scores,
                improved_voronoi_scores, seperated_voronoi_lines,
                seperated_edge_lines, improved_voronoi_lines,
                seperated_ipl_lines, ipl_scores,
                open_plan_text_cordinate, score_path)

        return voronoi_scores, edge_extension_scores, improved_voronoi_scores,ipl_scores