__author__ = 'anu'

import cv2,sys,os,math,itertools,copy
import numpy as np
from PIL import Image
from operator import itemgetter

from open_plan_line import line
from color_check import color_check_class
from weight_assign import weight_define
from penalties_and_scores import penalties_scores_class
from iterative_functions import iterative_functions_class
from line_optimization_functions import line_optimization_function_class


class line_optimization_class:
    def __init__(self,output_directory,best_line_path,image_name):
        self.image_name = image_name
        self.outer_dir = output_directory
        self.output_directory = output_directory+'Line_Optimization/'
        self.best_line_directory = best_line_path
        self.line_optimization_function_obj = line_optimization_function_class()
        self.iterative_obj = iterative_functions_class()
        self.penalties_scores_obj = penalties_scores_class()

    def test(self):
        n=9
        pixel_data = [[0 for column in range(5)] for row in range(2)]
        print 'long rect'
        for row in pixel_data:
            print row
        pixel_data1 = [[0 for column in range(3)] for row in range(6)]
        print 'tall rect'
        for row in pixel_data1:
            print row

    def find_shortest_path_manhattan(self,vornoi_data,open_plan_text_cordinate,current_open_plan_contour_data,name,image_name,image_path,debug_mode,path_final_lines,text_words,thesis_image):
        contour_check = current_open_plan_contour_data[2]

        gray_room = cv2.imread(image_path,cv2.IMREAD_COLOR)
        openplan_height,openplan_width, channel = gray_room.shape
        # ret,thresh = cv2.threshold(gray_room,0,255,1)
        # contours,hierachy = cv2.findContours(thresh,1,2)
        # for count, cnt in enumerate(contours):
        #     if count ==0:
        #         contour_check = cnt
        #         # contour_check = cv2.approxPolyDP(cnt,0.005*cv2.arcLength(cnt,True),True)
        #
        outer_cont = ~(np.zeros((openplan_height,openplan_width,3), np.uint8))
        # outer_cont2 = ~(np.zeros((openplan_height,openplan_width,3), np.uint8))
        #
        # cv2.drawContours(outer_cont,[contour_check],0,(0,0,0),1)
        # cv2.imwrite(self.output_directory+name+'_STEP_01.png',outer_cont)
        #
        # if debug_mode:
        #     cv2.drawContours(outer_cont2,[contour_check],0,(0,0,0),20)
        #     cv2.imwrite(self.output_directory+name+'_STEP_001.png',outer_cont2)
        outer_cont_copy1 = cv2.imread(image_path,cv2.IMREAD_COLOR)
        cv2.imwrite(self.output_directory+name+'_STEP_01.png',outer_cont_copy1)

        #-------start process of rings
        image = Image.open(self.output_directory+name+'_STEP_01.png')
        width, height = image.size
        pixels = image.load()

        # #--test
        # test_image = Image.open(self.output_directory + name + '_STEP_01.png')
        # test_pixels = test_image.load()

        shortest_path_data = []

        for r_count, v_row in enumerate(vornoi_data):
            top_threshold_pixel_array = [0 for c in range(width)]
            bottom_threshold_pixel_array = [height for c in range(width)]
            left_threshold_pixel_array = [0 for c in range(height)]
            right_threshold_pixel_array = [width for c in range(height)]
            one_text, second_text = False, False
            starting_cordinate, end_cordinate, starting_row_ID, end_row_ID = 0,0,0,0
            for row_num, text_cord in enumerate(open_plan_text_cordinate):
                if row_num==v_row[0]:
                    starting_cordinate = text_cord
                    starting_row_ID = row_num
                    one_text = True
                if row_num==v_row[1]:
                    end_cordinate = text_cord
                    end_row_ID = row_num
                    second_text = True
                if one_text and second_text:
                    break



            pixel_data = [[0 for column in range(width)] for row in range(height)]
            shortest_path_points = []
            color_val_obj = color_check_class(pixels, top_threshold_pixel_array,bottom_threshold_pixel_array,left_threshold_pixel_array, right_threshold_pixel_array)
            temp_array_copy = []
            temp_ring_array = []
            #------start of single txt co-ordinate
            end_ring = False

            for ring_count in range(5000):
                if ring_count == 0:
                    column,row = starting_cordinate
                    pixel_data[row][column]= 'S'
                    column1,row1 = end_cordinate
                    pixel_data[row1][column1]= 'F'
                    temp_array_copy.append(starting_cordinate)

                for row in temp_array_copy:
                    col_coord, row_coord = row
                    top_pixel = col_coord, row_coord-1
                    left_pixel = col_coord-1, row_coord
                    bottom_pixel = col_coord, row_coord+1
                    right_pixel = col_coord+1, row_coord



                    top_col,top_row = top_pixel
                    if pixel_data[top_row][top_col] is 'F':
                        shortest_path_points.append([top_col,top_row])
                        shortest_path_points = color_val_obj.get_shortest_path(row,pixel_data,shortest_path_points)
                        end_ring = True
                        break
                    else:
                        if (pixel_data[top_row][top_col] ==0) and (cv2.pointPolygonTest(contour_check,(top_col,top_row ),False)==1):
                            temp_ring_array, pixel_data = color_val_obj.shortest_path_color_match(top_pixel,pixel_data,temp_ring_array,'T')


                    left_col,left_row = left_pixel
                    if pixel_data[left_row][left_col] is 'F':
                        shortest_path_points.append([left_col,left_row])
                        shortest_path_points = color_val_obj.get_shortest_path(row,pixel_data,shortest_path_points)
                        end_ring = True
                        break
                    else:
                        if (pixel_data[left_row][left_col] ==0)and (cv2.pointPolygonTest(contour_check,(left_col,left_row),False)==1):
                            temp_ring_array, pixel_data = color_val_obj.shortest_path_color_match(left_pixel,pixel_data,temp_ring_array,'L')


                    bottom_col,bottom_row = bottom_pixel
                    if pixel_data[bottom_row][bottom_col] is 'F':
                        shortest_path_points.append([bottom_col,bottom_row])
                        shortest_path_points = color_val_obj.get_shortest_path(row,pixel_data,shortest_path_points)
                        end_ring = True
                        break
                    else:
                        if (pixel_data[bottom_row][bottom_col] ==0)and (cv2.pointPolygonTest(contour_check,(bottom_col,bottom_row),False)==1):
                            temp_ring_array, pixel_data = color_val_obj.shortest_path_color_match(bottom_pixel,pixel_data,temp_ring_array,'B')


                    right_col,right_row = right_pixel
                    if pixel_data[right_row][right_col] is 'F':
                        shortest_path_points.append([right_col,right_row])
                        shortest_path_points = color_val_obj.get_shortest_path(row,pixel_data,shortest_path_points)
                        end_ring = True
                        break
                    else:
                        if (pixel_data[right_row][right_col] ==0)and (cv2.pointPolygonTest(contour_check,(right_col,right_row),False)==1):
                            temp_ring_array, pixel_data = color_val_obj.shortest_path_color_match(right_pixel,pixel_data,temp_ring_array,'R')

                # if end_row_ID == 2 and starting_row_ID == 1:
                #     for img_column, img_row in temp_ring_array:
                #         test_pixels[img_column, img_row] = (255, 0, 0)


                temp_array_copy = temp_ring_array
                temp_ring_array = []


                if end_ring:
                    break
            # test_image.save(self.output_directory + name + '_test.png')


            shortest_path =[]
            shortest_path_data.append([])
            shortest_path_data[r_count].append(end_row_ID)
            shortest_path_data[r_count].append(starting_row_ID)

            for p,point in enumerate(shortest_path_points):
                x,y = point
                if p==0:
                    shortest_path.append([])
                    shortest_path[p].append([x,y])
                elif p==len(shortest_path_points)-1:
                    shortest_path[p-1].append([x,y])
                else:
                    shortest_path.append([])
                    shortest_path[p].append([x,y])
                    shortest_path[p-1].append([x,y])



            # outer_cont_copy2 = outer_cont.copy()
            for each_line in shortest_path:
                shortest_path_data[r_count].append(each_line)
                # if debug_mode:
                cv2.line(thesis_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,255),2,cv2.cv.CV_AA)
                cv2.line(thesis_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,255),2,cv2.cv.CV_AA)
            # if debug_mode:
                # cv2.imwrite(path_final_lines+name+'_Thesis_STEP_04_'+str(r_count)+'.png',outer_cont_copy2)
            # --thesis test
        # for row in text_words:
        #     cord1 = row[1][0]
        #     cord2 = row[1][1]
        #     cv2.circle(thesis_image, (cord1,cord2), 10, (0, 0, 255), -1)
        #     x_position = (len(row[0]) / 2) * 25
        #     text = str(row[0]).upper()
        #     cv2.putText(thesis_image, text, (cord1 - x_position, cord2 + 12), cv2.FONT_HERSHEY_COMPLEX,
        #                 1, (0, 0, 0), 2, cv2.cv.CV_AA)
        # cv2.imwrite(path_final_lines + '_Thesis_Step_04_' + image_name, thesis_image)
        # if debug_mode:
            # cv2.imwrite(self.output_directory+name+'_STEP_03.png',outer_cont_copy1)

        # print '----shortest_path_data------'
        # for row_s in shortest_path_data:
        #     print row_s
        # print '-------------------------------'
        # for row in open_plan_text_cordinate:
        #     print row

        return shortest_path_data


    def find_edge_extensions_between_cordinates(self,edge_extensions,shortest_path_data,open_plan_text_cordinate,name,image_name,debug_mode):
        # debug_mode = True
        floor_plan = cv2.imread(self.output_directory+name+'_STEP_01.png',cv2.IMREAD_COLOR)
        floor_plan_copy2 = floor_plan.copy()
        for edge in edge_extensions:
            cv2.line(floor_plan_copy2,(tuple(edge[0])),(tuple(edge[1])),(255,0,0),2,cv2.cv.CV_AA)
        cv2.imwrite(self.output_directory+name+'_STEP_04-2.png',floor_plan_copy2)
        floor_plan_read = Image.open(self.output_directory+name+'_STEP_04-2.png')
        im = floor_plan_read.load()

        line_obj = line()
        edge_extensions_between_coordinates =[]
        for r_count, row in enumerate(shortest_path_data):
            if debug_mode:
                floor_plan_copy = floor_plan.copy()

            txt_cord_1 = row[0]
            txt_cord_2 = row[1]
            for line_segment in row[2:]:
                segment_line1_x, segment_line1_y= line_segment[0]
                segment_line2_x, segment_line2_y = line_segment[1]
                max_x = max(segment_line1_x,segment_line2_x)
                min_x = min(segment_line1_x,segment_line2_x)
                max_y = max(segment_line1_y,segment_line2_y)
                min_y = min(segment_line1_y,segment_line2_y)

                for edge_number,edge_row in enumerate(edge_extensions):
                    edge = edge_row[0]
                    intersect_x, intersect_y =0,0
                    edge_line1_x, edge_line1_y= edge[0]
                    edge_line2_x, edge_line2_y = edge[1]
                    edge_max_x = max(edge_line1_x,edge_line2_x)
                    edge_min_x = min(edge_line1_x,edge_line2_x)
                    edge_max_y = max(edge_line1_y,edge_line2_y)
                    edge_min_y = min(edge_line1_y,edge_line2_y)
                    intersect_x, intersect_y = line_obj.find_contour_intersections(segment_line1_x,segment_line1_y,segment_line2_x,segment_line2_y,edge_line1_x,edge_line1_y,edge_line2_x,edge_line2_y)


                    #-----if shortest path segment and edge extension intersects
                    if intersect_x != 0 and intersect_y != 0:
                        if intersect_x>=min_x and intersect_x<=max_x and intersect_y>=min_y and intersect_y<=max_y:
                            if intersect_x>=edge_min_x and intersect_x<=edge_max_x and intersect_y>=edge_min_y and intersect_y<=edge_max_y:
                                color = im[intersect_x,intersect_y]
                                #--with image.open colors are RGB even though with imread its BGR
                                if color[0]==0 and color[1]==0 and color[2]==255:
                                    if debug_mode:
                                        cv2.circle(floor_plan_copy,(intersect_x,intersect_y),10,(0,255,0),-1)
                                        cv2.line(floor_plan_copy,tuple(edge[0]),tuple(edge[1]),(255,0,0),2,cv2.cv.CV_AA)
                                    current_length = len(edge_extensions_between_coordinates)
                                    row_exists,edge_exists = False, False
                                    if current_length>0:
                                        for between_row_count, between_row in enumerate(edge_extensions_between_coordinates):
                                            if txt_cord_1 == between_row[0] and txt_cord_2 == between_row[1]:
                                                row_exists = True
                                                for element in between_row[2:]:
                                                    if element[1]==edge_number:
                                                        edge_exists=True
                                    if row_exists and edge_exists==False:
                                        edge_extensions_between_coordinates[len(edge_extensions_between_coordinates)-1].append([edge,edge_number])
                                    elif current_length==0 or edge_exists==False:
                                        edge_extensions_between_coordinates.append([])
                                        edge_extensions_between_coordinates[len(edge_extensions_between_coordinates)-1].append(txt_cord_1)
                                        edge_extensions_between_coordinates[len(edge_extensions_between_coordinates)-1].append(txt_cord_2)
                                        edge_extensions_between_coordinates[len(edge_extensions_between_coordinates)-1].append([edge,edge_number])
            if debug_mode:
                cv2.imwrite(self.output_directory+name+'****'+str(r_count)+'*******'+'_STEP_05.png',floor_plan_copy)

        if debug_mode:
            # floor_plan1 = cv2.imread(self.output_directory+name+'_STEP_01.png',cv2.IMREAD_COLOR)
            for r, row in enumerate(edge_extensions_between_coordinates):
                floor_plan_copy3 = floor_plan.copy()
                for row2 in row[2:]:
                    line1=row2[0]
                    cv2.line(floor_plan_copy3,(tuple(line1[0])),(tuple(line1[1])),(0,0,255),2,cv2.cv.CV_AA)
                one_text, second_text = False, False
                for row_num, text in enumerate(open_plan_text_cordinate):
                    if row_num==row[0]:
                        voronoi_text_cordinate = text
                        cv2.circle(floor_plan_copy3, (tuple(voronoi_text_cordinate)), 5, (255, 0, 0), -1)
                        one_text = True
                    if row_num==row[1]:
                        voronoi_text_cordinate = text
                        cv2.circle(floor_plan_copy3, (tuple(voronoi_text_cordinate)), 5, (255, 0, 0), -1)
                        second_text = True
                    if one_text and second_text:
                        break
                cv2.imwrite(self.output_directory+name+' $$$$$$$$$$ '+str(r)+'_STEP_06.png', floor_plan_copy3)

        return edge_extensions_between_coordinates


    def find_lines_in_area(self, current_directory,current_open_plan_contour_data,
                           avg_door_width,weight_list,voronoi_data,all_contour_lines,
                           shortest_path_data,original_edges_and_extensions,interesting_lines,
                           open_plan_text_cordinate,name,image_name,only_image_name,
                           open_area_path,room_name_org,text_words,text_bounding_boxes,
                           path_final_lines,debug_mode):

        #----------take open plan
        room_image_org = cv2.imread(open_area_path+room_name_org,cv2.IMREAD_GRAYSCALE)
        img_height,img_width = room_image_org.shape

        #----------create a copy of floor plan to be used later
        floor_plan_copy = room_image_org.copy()
        max_line_image = room_image_org.copy()

#----------------------------------------------------------------------------------------------------------------
#------- STEP 01 : Order Voronoi Lines
        # Explanation:
            # get contour lines for all partitions and use their order to order voronoi lines for each partition
            # this order will help to get boundaries : if 2 voronoi lines are consecutive in order they will belong to same boundary
        #-sort contours to order them by text_cordinate_ID
        partition_area_lines = sorted(all_contour_lines,key=itemgetter(1))
        # -get ordered voronoi lines
        voronoi_lines_in_contour_line_order = self.line_optimization_function_obj.order_voronoi_in_cont_order(partition_area_lines,voronoi_data)



#----------------------------------------------------------------------------------------------------------------
#------- STEP 02 : Find Boundaries
        # Explanation:
            # Using 'voronoi_lines_in_contour_line_order' find boundaries
            # At the same time order voronoi lines according to boundaries
        #-- how to order voronoi lines:
        #-- in the 'voronoi_lines_in_contour_line_order[]' we had [txt1,[line1,txt2,line_num],[line2,txt3,line_num],[line3,txt4,line_num]]
        #-- so for each row: add line to 'seperated_voronoi_lines' & add textID to 'boundaries'
        #-- order all lines by line_num in voronoi_lines_in_contour_line_order[row1] each boundary
        #-- sort boundaries in each row by textID2s
        seperated_voronoi_lines, boundaries = self.line_optimization_function_obj.find_boundaries(voronoi_lines_in_contour_line_order)
        # print '---boundaries----'
        # for row in boundaries:
        #     print row
        # print '--end--'
        #--create output storage paths
        output_path = self.output_directory+'sequential_output/'
        os.mkdir(output_path)
        image_contour_lines = current_open_plan_contour_data[1]

        if debug_mode:
            # -------------write contour of each partition in to new image
            open_plan_path = self.output_directory + 'open_plan_partitions/'
            os.mkdir(open_plan_path)
            for r, row in enumerate(partition_area_lines):
                contour_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
                for each_line in row[0]:
                    cv2.line(contour_image, (tuple(each_line[0])), (tuple(each_line[1])), (0, 0, 0), 2, cv2.cv.CV_AA)
                cv2.imwrite(open_plan_path + str(row[1]) + "_contour.png", contour_image)

            contour_image = ~(np.zeros((img_height,img_width,3), np.uint8))
            for each_line in image_contour_lines:
                cv2.line(contour_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,0),2,cv2.cv.CV_AA)
            cv2.imwrite(output_path+'Contour DETECTED.png', contour_image)



        # --- find if text has special case
        text_categorized_lists = self.line_optimization_function_obj.find_special_cases(text_words,current_directory)
        #---populate seperated_voronoi_lines,etc based on room type
        seperated_voronoi_lines, boundaries, room_type = self.line_optimization_function_obj.populate_lists_by_room_type(text_categorized_lists,seperated_voronoi_lines,boundaries)
        print '-------------------Room Type is ',room_type


    # ----------------------------------------------------------------------------------------------------------------
    # ------- STEP 03 : Separate Edge Extensions based on Boundaries
        #--seperate edge extensions and its data (original edge, similar to ipl or not
        edge_extensions = []
        edge_extension_data = []
        for eel in original_edges_and_extensions:
            edge_extension_data.append([eel[0],eel[2]])
            edge_extensions.append(eel[1])

        #---assign eel to boundaries
        seperated_edge_lines = []
        for r_num, boundary_row in enumerate(boundaries):
            #--[1,[[2,3],[4]]]
            text1 = boundary_row[0]
            seperated_edge_lines.append([])
            seperated_edge_lines[r_num].append(text1)
            other_text_lines = []
            for text2_set in boundary_row[1]:
                eel_for_text2_set = []
                single_lines = []
                sp_intersect_eel_combined = []
                for text2 in text2_set:
                    shortest_path = self.line_optimization_function_obj.find_relevant_shortest_path(text1,text2,shortest_path_data)
                    sp_intersect_eel = self.line_optimization_function_obj.find_lines_intersect_with_sp(shortest_path,edge_extensions)
                    sp_intersect_eel_combined = self.line_optimization_function_obj.check_if_line_repeats(sp_intersect_eel_combined,sp_intersect_eel)
                #--find if EEL is partitioning text as it should be
                eel_partitioning_text = self.line_optimization_function_obj.find_lines_partitioning_relevant_text(text1,text2_set,sp_intersect_eel_combined,image_contour_lines,
                                                                                          open_plan_text_cordinate,img_height,img_width,avg_door_width)
                eel_for_text2_set.append(eel_partitioning_text)
                for line_set in eel_for_text2_set:
                    for each_line in line_set:
                        single_lines.append(each_line)
                other_text_lines.append(single_lines)
            seperated_edge_lines[r_num].append(other_text_lines)


        # ---assign ipl to boundaries
        seperated_ipl_lines = []
        for r_num, boundary_row in enumerate(boundaries):
            # --[1,[[2,3],[4]]]
            text1 = boundary_row[0]
            seperated_ipl_lines.append([])
            seperated_ipl_lines[r_num].append(text1)
            other_text_lines = []
            for text2_set in boundary_row[1]:
                ipl_for_text2_set = []
                single_lines = []
                sp_intersect_ipl_combined = []
                for text2 in text2_set:
                    shortest_path = self.line_optimization_function_obj.find_relevant_shortest_path(text1, text2,
                                                                                                    shortest_path_data)
                    sp_intersect_ipl = self.line_optimization_function_obj.find_lines_intersect_with_sp(
                        shortest_path, interesting_lines)
                    sp_intersect_ipl_combined = self.line_optimization_function_obj.check_if_line_repeats(
                        sp_intersect_ipl_combined, sp_intersect_ipl)

                ipl_partitioning_text = self.line_optimization_function_obj.find_lines_partitioning_relevant_text(
                    text1, text2_set, sp_intersect_ipl_combined, image_contour_lines,
                    open_plan_text_cordinate, img_height, img_width, avg_door_width)
                ipl_for_text2_set.append(ipl_partitioning_text)

                for line_set in ipl_for_text2_set:
                    for each_line in line_set:
                        single_lines.append(each_line)
                other_text_lines.append(single_lines)
            seperated_ipl_lines[r_num].append(other_text_lines)


        #for display purposes draw all lines in to an image
        if debug_mode:
            black_all_lines_image = floor_plan_copy.copy()
            all_lines_image = cv2.cvtColor(black_all_lines_image,cv2.COLOR_GRAY2RGB)
            for row in voronoi_data:
                for v_line in row[2:]:
                    cv2.line(all_lines_image,(tuple(v_line[0])),(tuple(v_line[1])),(0,150,255),3,cv2.cv.CV_AA)
            for row in seperated_edge_lines:
                for boundary in row[1]:
                    for e_line1 in boundary:
                        e_line = e_line1[0]
                        cv2.line(all_lines_image,(tuple(e_line[0])),(tuple(e_line[1])),(0,255,0),2,cv2.cv.CV_AA)
            for row in seperated_ipl_lines:
                for boundary in row[1]:
                    for ipl_line1 in boundary:
                        ipl_line = ipl_line1[0]
                        cv2.line(all_lines_image, (tuple(ipl_line[0])), (tuple(ipl_line[1])), (255, 255, 0), 2, cv2.cv.CV_AA)

            for cordinate in open_plan_text_cordinate:
                cv2.circle(all_lines_image, (tuple(cordinate)), 5, (0, 0, 255), -1)
            box_lines =[]
            for current_bb in text_bounding_boxes:
                tx0,ty0,tx1,ty1 = current_bb
                box_lines.append([
                    [[tx0,ty0],[tx1,ty0]],
                    [[tx1,ty0],[tx1,ty1]],
                    [[tx1,ty1],[tx0,ty1]],
                    [[tx0,ty1],[tx0,ty0]]
                ])
            for bb_line1 in box_lines:
                for bb_line in bb_line1:
                    cv2.line(all_lines_image,(tuple(bb_line[0])),(tuple(bb_line[1])),(0,0,255),2,cv2.cv.CV_AA)
            cv2.imwrite(self.output_directory+name+"_STEP_07-1_eel_ipl_vor.png", all_lines_image)



        # ----- special case for placard
        max_line = []
        max_line_floor_plan = max_line_image

        # --2017/03/26
        # ---if only 2 txts+seperated_edge_lines is 0+one word is placard : execute special case
        row_num, furniture_exists = -1, False
        furniture_list = text_categorized_lists[4]
        if len(furniture_list) > 0:
            furniture_exists = True
            row_num = furniture_list[0]

        line_drawable = False
        # --new change on 2017/05/04 to have special case if furniture exista at any place
        if furniture_exists:
            line_drawable, max_line, max_line_floor_plan = self.line_optimization_function_obj.partition_cupboard(
                image_contour_lines, text_bounding_boxes,
                text_words, row_num, output_path,
                max_line_floor_plan, avg_door_width)

        if line_drawable == False or furniture_exists == False:
            # print 'Else'
            # --2017/03/26









    #----------------------------------------------------------------------------------------------------------------
    #------- STEP 04 : Separate Improved Voronoi Lines based on Boundaries
            # Explanation:
                # 1. Pre-processing of voronoi lines: Remove line segments<door width and connect other lines
                # 2. Find cordinates of optimized_voronoi_lines
                # 3. Get possible lines from cordinates
                # 4. If these lines don't intersect with contour lines/text cordinates/is in between 2 text cordinates take them as 'improved_voronoi_lines'

            #1. Pre-processing of voronoi lines: Remove line segments<door width and connect other lines
            optimized_voronoi_lines = []
            min_line_length = avg_door_width/10
            for r,voronoi_row1 in enumerate(seperated_voronoi_lines):
                # print voronoi_row1
                boundary_line =[]
                optimized_voronoi_lines.append([])
                optimized_voronoi_lines[r].append(voronoi_row1[0])
                for boundary in voronoi_row1[1]:
                    line_connection =[]
                    first_line_check = False
                    short_length_flag = False
                    first_line = 0
                    for l,each_line in enumerate(boundary):
                        x1,y1 = each_line[0]
                        x2,y2 = each_line[1]
                        line_length = math.hypot(x2 - x1, y2 - y1)
                        if l ==0 and line_length<=min_line_length:
                            first_line_check = True
                            first_line = each_line
                            short_length_flag = False
                        elif l== len(boundary)-1 and line_length<=min_line_length:
                            if first_line_check:
                                line_connection.append([first_line[0],each_line[1]])
                                first_line_check = False
                            else:
                                last_element = line_connection[-1]
                                del line_connection[-1]
                                line_connection.append([last_element[0],each_line[1]])
                            short_length_flag = False
                        else:
                            if line_length>min_line_length:
                                if first_line_check:
                                    line_connection.append([first_line[0],each_line[1]])
                                    first_line_check = False
                                elif short_length_flag and len(line_connection)!= 0:
                                    last_element = line_connection[-1]
                                    line_connection.append([last_element[1],each_line[1]])
                                else:
                                    line_connection.append(each_line)
                                short_length_flag = False
                            else:
                                short_length_flag= True
                    # print line_connection
                    if len(line_connection)==1:
                        boundary_line.append(boundary)
                    else:
                        boundary_line.append(line_connection)
                optimized_voronoi_lines[r].append(boundary_line)


            #2. Find cordinates of optimized_voronoi_lines
            boundary_cordinates = []
            for r,voronoi_row1 in enumerate(optimized_voronoi_lines):
                boundary_line =[]
                boundary_cordinates.append([])
                boundary_cordinates[r].append(voronoi_row1[0])
                voronoi_row=voronoi_row1[1]
                for boundary in voronoi_row:
                    line_connection =[]
                    for l,each_line in enumerate(boundary):
                        if l==0:
                            line_connection.append(each_line[0])
                            line_connection.append(each_line[1])
                        else:
                            line_connection.append(each_line[1])
                    boundary_line.append(line_connection)
                boundary_cordinates[r].append(boundary_line)


            # 2.1 Get all possible combinations from list of cordinates
            improved_voronoi_cordinates=[]
            for b_cord_r,row1 in enumerate(boundary_cordinates):
                improved_voronoi_cordinates.append([])
                improved_voronoi_cordinates[b_cord_r].append(row1[0])
                row=row1[1]#-----------******************
                boundary_line=[]
                for boundary in row:
                    line_connection =[]
                    boundary_length = len(boundary)
                    if boundary_length>2:
                        #--------create cordinate_lookup list with details about number of cordinates to look at in each boundary
                        test_list = []
                        for row in range(2,boundary_length):
                            test_list.append(row)
                        cordinate_lookup =[]
                        for L in range(1,len(test_list)+1):
                            row=[]
                            for subset in itertools.combinations(test_list,L):
                                row.append(list(subset))
                            cordinate_lookup.append(row)
                        for r_count,row in enumerate(cordinate_lookup):
                           for element in row:
                               temp_line=[]
                               for r, each_line in enumerate(boundary):
                                   if r+1 not in element:
                                       temp_line.append(each_line)
                               line_connection.append(temp_line)
                    boundary_line.append(line_connection)
                improved_voronoi_cordinates[b_cord_r].append(boundary_line)



            # 3. Get possible lines from cordinates
            improved_voronoi_lines_temp=[]
            for r,row1 in enumerate(improved_voronoi_cordinates):#-----------******************
                improved_voronoi_lines_temp.append([])#-----------******************
                improved_voronoi_lines_temp[r].append(row1[0])#-----------******************
                row=row1[1]#-----------******************
                boundary_line=[]
                for boundary in row:
                    line_connection =[]
                    for cordinate_set in boundary:
                        cordinate_list =[]
                        for c,cords in enumerate(cordinate_set):
                            if c==0:
                                cordinate_list.append([])
                                cordinate_list[c].append(cords)
                            elif c==len(cordinate_set)-1:
                                cordinate_list[c-1].append(cords)
                            else:
                                cordinate_list.append([])
                                cordinate_list[c].append(cords)
                                cordinate_list[c-1].append(cords)
                        line_connection.append(cordinate_list)
                    boundary_line.append(line_connection)
                improved_voronoi_lines_temp[r].append(boundary_line)

            # --------06/03/2017
            # 4. If these lines don't intersect with contour lines/text cordinates/is in between 2 text cordinates take them as 'improved_voronoi_lines'
            # 4.2 Find lines that don't intersect with contour lines and text cordinates
            contour_non_intersect_improved_voronoi_lines = self.line_optimization_function_obj.find_lines_not_intersecting_with_contour(improved_voronoi_lines_temp,boundaries,open_plan_text_cordinate,image_contour_lines,text_bounding_boxes)

            # 4.3 Find lines that are in between 2 text cords (not putting all text labels in one side)
            improved_voronoi_lines_temp2 = self.line_optimization_function_obj.find_lines_seperating_text(contour_non_intersect_improved_voronoi_lines,boundaries,open_plan_text_cordinate,floor_plan_copy,avg_door_width,output_path)

            # 4.4 if SMPL has > 1 line per boundary: select line with min(segments) as SMPL
            # 4.4 if gradient(SMPL) is ~= 90/0 create another candidate with 90/0 gradient
            # test_image = floor_plan_copy.copy()
            # colored_test_image = cv2.cvtColor(test_image, cv2.COLOR_GRAY2RGB)
            iterative_obj = iterative_functions_class()
            for r_num,row in enumerate(improved_voronoi_lines_temp2):
                #--take each boundary one by one
                for b_num,boundary in enumerate(row[1]):
                    #--sort sublists in boundary (SMPL_LineSets) based on number of lines in each SMPL_LineSet
                    boundary.sort(key=len)
                    if len(boundary)>0:
                        #--select SMPL_LineSet with min(segments) from sorted array
                        new_boundary_lines = [boundary[0]]
                        first_element_length = len(boundary[0])
                        #--if SMPLineSet with  min(segments) is 1, then there's going to be only one such line
                        #--so we check if min(segments) > 1
                        if first_element_length > 1:
                            for line_set_num, improved_line_set in enumerate(boundary):
                                if line_set_num > 0:
                                    #--in sorted list check if 2,3... elements also have the same number of elements as 1
                                    if len(improved_line_set)==first_element_length:
                                        new_boundary_lines.append(improved_line_set)
                                    #--since list is sorted, we can break
                                    else:
                                        break

                        #--finding approx 0/90 line per SMPL
                        for num, smpl_set in enumerate(new_boundary_lines):
                            if len(smpl_set)==1:
                                current_line = smpl_set[0]
                                x1,y1 = current_line[0]
                                x2, y2 = current_line[1]
                                line_gradient, line_c = iterative_obj.find_mc_of_line(x1,y1,x2,y2)
                                if line_gradient == 0 or line_gradient == 'a' or (line_gradient < 0.4 and line_gradient > -0.4) or (line_gradient > 3 and line_gradient < -3):
                                    new_line = self.line_optimization_function_obj.find_approx_straight_line(current_line,line_gradient,image_contour_lines)
                                    new_boundary_lines.append([new_line])
                                    break

                        improved_voronoi_lines_temp2[r_num][1][b_num] = new_boundary_lines

            contour_non_intersect_2 = self.line_optimization_function_obj.find_lines_not_intersecting_with_contour(
                improved_voronoi_lines_temp2, boundaries, open_plan_text_cordinate, image_contour_lines,
                text_bounding_boxes)
            # 4.3 Find lines that are in between 2 text cords (not putting all text labels in one side)
            improved_voronoi_lines = self.line_optimization_function_obj.find_lines_seperating_text(
                contour_non_intersect_2, boundaries, open_plan_text_cordinate, floor_plan_copy,
                avg_door_width, output_path)
            # --------END  06/03/2017


            #for display purposes draw improved lines on an image
            if debug_mode:
                black_improved_lines_image = floor_plan_copy.copy()
                improved_lines_image = cv2.cvtColor(black_improved_lines_image,cv2.COLOR_GRAY2RGB)

                image_test = floor_plan_copy.copy()
                for r1,row in enumerate(improved_voronoi_lines):
                    for b1,boundary in enumerate(row[1]):
                        for i,improved_line_set in enumerate(boundary):
                            # image_test = floor_plan_copy.copy()
                            for improved_line in improved_line_set:
                                cv2.line(improved_lines_image,(tuple(improved_line[0])),(tuple(improved_line[1])),(255,0,0),1,cv2.cv.CV_AA)
                                cv2.line(image_test,(tuple(improved_line[0])),(tuple(improved_line[1])),(255,0,0),2,cv2.cv.CV_AA)
                cv2.imwrite(self.output_directory+name+"IMPORVED LINES.png", image_test)

                for row in voronoi_data:
                    for v_line in row[2:]:
                        cv2.line(improved_lines_image,(tuple(v_line[0])),(tuple(v_line[1])),(0,150,255),1,cv2.cv.CV_AA)
                for cordinate in open_plan_text_cordinate:
                    cv2.circle(improved_lines_image, (tuple(cordinate)), 5, (0, 0, 255), -1)
                cv2.imwrite(self.output_directory+name+"_IMPORVED LINES.png", improved_lines_image)

                #for display purposes draw all lines in to an image
                black_all_lines_image = floor_plan_copy.copy()
                all_lines_image = cv2.cvtColor(black_all_lines_image,cv2.COLOR_GRAY2RGB)
                for r,row in enumerate(optimized_voronoi_lines):
                    for b,boundary in enumerate(row[1]):
                        for v_line in boundary:
                            cv2.line(all_lines_image,(tuple(v_line[0])),(tuple(v_line[1])),(0,0,255),3,cv2.cv.CV_AA)
                # for row in voronoi_data:
                #     for v_line in row[2:]:
                #         cv2.line(all_lines_image,(tuple(v_line[0])),(tuple(v_line[1])),(0,150,255),3,cv2.cv.CV_AA)
                for row in seperated_edge_lines:
                    for boundary in row[1]:
                        for e_line1 in boundary:
                            e_line = e_line1[0]
                            cv2.line(all_lines_image,(tuple(e_line[0])),(tuple(e_line[1])),(0,255,0),2,cv2.cv.CV_AA)
                for row in improved_voronoi_lines:
                    for boundary in row[1]:
                        for improved_line_set in boundary:
                            for improved_line in improved_line_set:
                                cv2.line(all_lines_image,(tuple(improved_line[0])),(tuple(improved_line[1])),(255,0,0),2,cv2.cv.CV_AA)
                # for cordinate in open_plan_text_cordinate:
                #     cv2.circle(all_lines_image, (tuple(cordinate)), 5, (0, 0, 255), -1)
                for r, row in enumerate(text_words):
                    cord1 = row[1][0]
                    cord2 = row[1][1]
                    x_position = (len(row[0]) / 2) * 25
                    cv2.putText(all_lines_image, str(row[0]).upper(), (cord1 - x_position, cord2 + 12), cv2.FONT_HERSHEY_COMPLEX, 1,
                                    (0, 0, 0), 2, cv2.cv.CV_AA)
                box_lines =[]
                for current_bb in text_bounding_boxes:
                    tx0,ty0,tx1,ty1 = current_bb
                    box_lines.append([
                        [[tx0,ty0],[tx1,ty0]],
                        [[tx1,ty0],[tx1,ty1]],
                        [[tx1,ty1],[tx0,ty1]],
                        [[tx0,ty1],[tx0,ty0]]
                    ])
                for bb_line1 in box_lines:
                    for bb_line in bb_line1:
                        cv2.line(all_lines_image,(tuple(bb_line[0])),(tuple(bb_line[1])),(0,0,255),2,cv2.cv.CV_AA)

                cv2.imwrite(self.output_directory+name+"_STEP_07-2__COMPLETE LINE SET.png", all_lines_image)
                cv2.imwrite(path_final_lines + 'A_'+only_image_name + "_COMPLETE LINE SET.png", all_lines_image)


    #----------------------------------------------------------------------------------------------------------------
    #------- STEP 05 : Get non_voronoi contour lines (Required in penalty calculation)
            # Explanation:
                # 1. Extract outer contour lines from 'partition_area_lines'
                # 2. For each boundary find its outer contour lines+voronoi lines that don't contribute to boundary
            # 1. Extract outer contour lines from 'partition_area_lines'
            only_contour_lines =[]
            for cont, partiton_row in enumerate(partition_area_lines):
                # print partiton_row[1]
                contour_text_ID = partiton_row[1]
                only_contour_lines.append([])
                only_contour_lines[cont].append(contour_text_ID)
                for contour_line in partiton_row[0]:
                    x1,y1 = contour_line[0]
                    x2,y2 = contour_line[1]
                    contour_line_is_voronoi_line= False
                    for voronoi_row in voronoi_data:
                        exists = False
                        # if voronoi_row[0]==contour_text_ID:
                        for v_line in voronoi_row[2:]:
                            x3,y3 = v_line[0]
                            x4,y4 = v_line[1]
                            if (abs(x1-x3)<5 and abs(y1-y3)<5 and abs(x2-x4)<5 and abs(y2-y4)<5) or (abs(x1-x4)<5 and abs(y1-y4)<5 and abs(x2-x3)<5 and abs(y2-y3)<5):
                                contour_line_is_voronoi_line = True
                                exists= True
                                break
                        if exists:
                            break
                    if contour_line_is_voronoi_line==False:
                        only_contour_lines[cont].append(contour_line)
            if debug_mode:
                for cont, contour in enumerate(only_contour_lines):
                    test_image =cv2.imread(open_area_path+room_name_org,cv2.IMREAD_COLOR)
                    for l1,each_line in enumerate(contour[1:]):
                        cv2.line(test_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,255),2,cv2.cv.CV_AA)
                    cv2.imwrite(self.output_directory+name+str(cont)+"_TEST_IMAGE.png", test_image)

            # 2. For each boundary find its outer contour lines+voronoi lines that don't contribute to boundary
            non_voronoi_use =[]
            for row_count,row in enumerate(boundaries):
                text1 = row[0]
                non_voronoi_use.append([])
                non_voronoi_use[row_count].append(text1)
                boundary_lines = []
                for boundary_num,boundary in enumerate(row[1]):
                    boundary_row = []
                    # for only_contour in only_contour_lines:
                    #     if only_contour[0]==text1:
                    #         print 'only_contour[1:]', only_contour[1:]
                    #         boundary_row.append(only_contour[1:])
                    #         break
                    boundary_row.append(image_contour_lines)
                    all_texts = [text1]+boundary
                    # for vor_row in voronoi_data:
                    #     if vor_row[0]==text1 or vor_row[1]==text1:
                    #         boundary_row.append(vor_row[2:])
                    #         # for voronoi_line  in vor_row[2:]:
                    #             # cv2.line(contour_image,(tuple(voronoi_line[0])),(tuple(voronoi_line[1])),(0,0,255),2,cv2.cv.CV_AA)

                    for vor_row in voronoi_data:
                        voronoi_texts = [vor_row[0]]+[vor_row[1]]
                        common_texts = list(set(voronoi_texts).intersection(all_texts))
                        if len(common_texts)==1 and common_texts[0]==text1:
                            boundary_row.append(vor_row[2:])
                    # for text2 in boundary:
                    #     for only_contour in only_contour_lines:
                    #         if only_contour[0]==text2:
                    #             boundary_row.append(only_contour[1:])
                    #             break
                    boundary_lines.append(boundary_row)
                non_voronoi_use[row_count].append(boundary_lines)

            if debug_mode:
                for r,row in enumerate(non_voronoi_use):
                    for b,boundary1 in enumerate(row[1]):
                        contour_image = ~(np.zeros((img_height,img_width,3), np.uint8))
                        for non_vornoi_line_set in boundary1:
                            for non_vornoi_line in non_vornoi_line_set:
                                cv2.line(contour_image,(tuple(non_vornoi_line[0])),(tuple(non_vornoi_line[1])),(255,0,0),2,cv2.cv.CV_AA)
                        cv2.imwrite(self.output_directory+name+str(r)+str(b)+"_Z_NEWWWWWW_LINES.png", contour_image)

            # print '-----open_plan_text_cordinate------'
            # for row in text_words:
            #     print row
            # print '-----boundary------'
            # for row in boundaries:
            #     print row
            # print '---seperated_voronoi_lines----'
            # for row in seperated_voronoi_lines:
            #     print row
            # print '---improved_voronoi_lines----'
            # for row in improved_voronoi_lines:
            #     print row
            # print '---seperated_edge_lines----'
            # for row in seperated_edge_lines:
            #     print row

    #----------------------------------------------------------------------------------------------------------------

    #------- STEP 06 : Calculate penalties
            score_path = self.output_directory + 'Scores and Penalties/'
            os.mkdir(score_path)
            # ---Test_path is used in calculate_score->calculate_convexity_defect
            Test_path = self.output_directory + 'Test Outputs/'
            os.mkdir(Test_path)
            voronoi_penalties, edge_extension_penalties, improved_voronoi_penalties, ipl_penalties = self.penalties_scores_obj.calculate_penalties(
                            room_type,avg_door_width,
                            img_height, img_width, weight_list,
                            shortest_path_data,edge_extension_data,
                            open_plan_text_cordinate,boundaries,
                            seperated_voronoi_lines,seperated_edge_lines,
                            improved_voronoi_lines,seperated_ipl_lines,
                            non_voronoi_use,floor_plan_copy,
                            score_path,Test_path,debug_mode)
    #         # Explanation:
    #             # 1. Calculate voronoi lines(seperated_voronoi_lines) penalty -voronoi_scores
    #             # 2. Calculate improved voronoi lines(improved_voronoi_lines) penalty - improved_voronoi_scores
    #             # 3. Calculate edge extension lines(seperated_edge_lines) penalty - edge_extension_score
    #         print_start_point=2000
    #         score_path = self.output_directory+'Scores and Penalties/'
    #         os.mkdir(score_path)
    #         #---Test_path is used in calculate_score->calculate_convexity_defect
    #         Test_path = self.output_directory+'Test Outputs/'
    #         os.mkdir(Test_path)
    #
    #         # 1. Calculate voronoi lines(seperated_voronoi_lines) penalty - voronoi_scores
    #         voronoi_penalties =[]
    #         for vor_row_count,voronoi_line1 in enumerate(seperated_voronoi_lines):
    #             line_type= 'V'
    #             temp_voronoi_penalties=[]
    #             textID = voronoi_line1[0]
    #             voronoi_line = voronoi_line1[1]
    #             voronoi_penalties.append([])
    #             voronoi_penalties[vor_row_count].append(textID)
    #             for r,boundary in enumerate(voronoi_line):
    #                 if debug_mode:
    #                     bw_image = floor_plan_copy.copy()
    #                     floor_plan_boundaries = cv2.cvtColor(bw_image,cv2.COLOR_GRAY2RGB)
    #                     cv2.putText(floor_plan_boundaries, '------------Voronoi Penalties----------------', (5,print_start_point), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255), 2)
    #
    #                 b_cord_row =[]
    #                 for b_row in boundaries:
    #                     if b_row[0]==textID:
    #                         b_cord_row = b_row[1]
    #                 boundary_cordinates = b_cord_row[r]
    #
    #                 non_vor_row_lines=[]
    #                 for non_voronoi_row in non_voronoi_use:
    #                     if non_voronoi_row[0]==textID:
    #                         non_vor_row_lines = non_voronoi_row[1]
    #                         break
    #                 non_voronoi_line_data = non_vor_row_lines[r]
    #             #------calculate score
    #                 line_obj = line()
    #                 final_score,all_scores = line_obj.calculate_score(room_type,avg_door_width,weight_list,boundary,
    #                                                                   textID,boundary_cordinates, shortest_path_data,
    #                                                                   line_type,edge_extension_data,score_path,
    #                                                                   img_height,img_width,Test_path,non_voronoi_line_data,
    #                                                                   0,open_plan_text_cordinate)
    #                 temp_voronoi_penalties.append(final_score)
    #                 score_row= all_scores[0]
    #                 if debug_mode:
    #                     cv2.putText(floor_plan_boundaries,
    #                                 'v.'+str(textID+1)+'-'+str(r+1)+'->T:'+str(final_score)+
    #                                 '='+'Len'+str(score_row[0])+', G'+str(score_row[1])+', '+'Txt'+str(
    #                                     score_row[2])+', '+'WL'+str(score_row[3])+' ,C'+str(
    #                                     score_row[4])+' ,Ipl'+str(score_row[5]),
    #                                 (5,print_start_point+30), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255), 2)
    #                     cv2.imwrite(score_path+'Partition '+str(textID)+' Boundary '+str(r)+ 'Line Scores.png',floor_plan_boundaries)
    #
    #             voronoi_penalties[vor_row_count].append(temp_voronoi_penalties)#-----------******************
    #
    #
    #         # 2. Calculate improved voronoi lines(improved_voronoi_lines) penalty - improved_voronoi_scores
    #         improved_voronoi_penalties =[]
    #         for imp_vor_row_count,improved_voronoi_line1 in enumerate(improved_voronoi_lines):
    #             line_type= 'I'
    #             temp_improved_voronoi_penalties=[]
    #             textID=improved_voronoi_line1[0]
    #             improved_voronoi_line=improved_voronoi_line1[1]
    #             improved_voronoi_penalties.append([])
    #             improved_voronoi_penalties[imp_vor_row_count].append(textID)
    #             for r,boundary in enumerate(improved_voronoi_line):
    #                 b_cord_row =[]
    #                 for b_row in boundaries:
    #                     if b_row[0]==textID:
    #                         b_cord_row = b_row[1]
    #                 boundary_cordinates = b_cord_row[r]
    #
    #                 non_vor_row_lines=[]
    #                 for non_voronoi_row in non_voronoi_use:
    #                     if non_voronoi_row[0]==textID:
    #                         non_vor_row_lines = non_voronoi_row[1]
    #                         break
    #                 non_voronoi_line_data = non_vor_row_lines[r]
    #
    #                 #-----used in area difference score calculation
    #                 contour_partition_lines =[]
    #                 for row_set in partition_area_lines:
    #                     if textID == row_set[1]:
    #                         contour_partition_lines = row_set[0]
    #                         break
    #                 text_cordinate = open_plan_text_cordinate[textID]
    #
    #
    #
    #                 temp_print_value=70
    #                 if debug_mode:
    #                     floor_plan_boundaries = cv2.imread(score_path+'Partition '+str(textID)+' Boundary '+str(r)+ 'Line Scores.png',cv2.IMREAD_COLOR)
    #                 initial_value = temp_print_value
    #                 temp_print_value=temp_print_value+30
    #                 temp_line_set = []
    #                 exists=False
    #                 for l,line_set in enumerate(boundary):
    #                     if len(line_set)>0:
    #                         exists=True
    #             #-------calculate score
    #                         line_obj = line()
    #                         final_score,all_scores = line_obj.calculate_score(room_type,avg_door_width,weight_list,
    #                                                                           line_set,textID,boundary_cordinates,
    #                                                                           shortest_path_data,line_type,
    #                                                                           edge_extension_data,score_path,
    #                                                                           img_height,img_width,Test_path,
    #                                                                           non_voronoi_line_data,text_cordinate,open_plan_text_cordinate)
    #                         temp_line_set.append(final_score)
    #                         score_row= all_scores[0]
    #                         if debug_mode:
    #                             cv2.putText(floor_plan_boundaries, 'i. '+str(l+1)+'->T:'+str(final_score)+
    #                             '=' + 'Len' + str(score_row[0]) + ', G' + str(score_row[1]) + ', ' + 'Txt' + str(
    #                                 score_row[2]) + ', ' + 'WL' + str(score_row[3]) + ' ,C' + str(
    #                                 score_row[4]) + ' ,Ipl' + str(score_row[5]),
    #                                         (5,print_start_point+temp_print_value), cv2.FONT_HERSHEY_PLAIN, 2, (255, 0, 0), 2)
    #                             temp_print_value=temp_print_value+30
    #                     else:
    #                         temp_line_set.append([0])
    #                 if debug_mode:
    #                     if exists:
    #                         # cv2.putText(floor_plan_boundaries, '------------Voronoi Improvement Penalties----------------', (5,print_start_point+initial_value), cv2.FONT_HERSHEY_PLAIN, 2, (255, 0, 0), 2)
    #                         temp_print_value=temp_print_value+30
    #                 temp_improved_voronoi_penalties.append(temp_line_set)
    #                 if debug_mode:
    #                     cv2.imwrite(score_path+'Partition '+str(textID)+' Boundary '+str(r)+ 'Line Scores.png',floor_plan_boundaries)
    #             improved_voronoi_penalties[imp_vor_row_count].append(temp_improved_voronoi_penalties)
    #
    #
    #         # 3. Calculate edge extension lines(seperated_edge_lines) penalty - edge_extension_score
    #         edge_extension_penalties=[]
    #         for edge_row_count,edge_line_row1 in enumerate(seperated_edge_lines):
    #             line_type= 'E'
    #             temp_edge_extension_penalties =[]
    #             textID = edge_line_row1[0]
    #             edge_line_row = edge_line_row1[1]
    #             edge_extension_penalties.append([])
    #             edge_extension_penalties[edge_row_count].append(textID)
    #             for r, edge_line_set in enumerate(edge_line_row):
    #
    #                 b_cord_row =[]
    #                 for b_row in boundaries:
    #                     if b_row[0]==textID:
    #                         b_cord_row = b_row[1]
    #                 boundary_cordinates = b_cord_row[r]
    #
    #                 non_vor_row_lines=[]
    #                 for non_voronoi_row in non_voronoi_use:
    #                     if non_voronoi_row[0]==textID:
    #                         non_vor_row_lines = non_voronoi_row[1]
    #                         break
    #                 non_voronoi_line_data = non_vor_row_lines[r]
    #
    #                 #-----used in area difference score calculation
    #                 # contour_partition_lines =[]
    #                 # for row_set in partition_area_lines:
    #                 #     if textID == row_set[1]:
    #                 #         contour_partition_lines = row_set[0]
    #                 #         break
    #                 text_cordinate = open_plan_text_cordinate[textID]
    #
    #
    #                 temp_print_value=250
    #                 if debug_mode:
    #                     floor_plan_boundaries = cv2.imread(score_path+'Partition '+str(textID)+' Boundary '+str(r)+ 'Line Scores.png',cv2.IMREAD_COLOR)
    #
    #                 if len(edge_line_set)>0:
    #                     if debug_mode:
    #                         cv2.putText(floor_plan_boundaries, '------------Edge Extensions Penalties----------------', (5,print_start_point+temp_print_value), cv2.FONT_HERSHEY_PLAIN, 2, (0, 255, 0), 2)
    #                         temp_print_value=temp_print_value+30
    #             #--------calculate score
    #                     line_obj = line()
    #                     final_score,all_scores = line_obj.calculate_score(room_type,avg_door_width,weight_list,edge_line_set,
    #                                                                       textID,boundary_cordinates, shortest_path_data,
    #                                                                       line_type,edge_extension_data,score_path,
    #                                                                       img_height,img_width,Test_path,
    #                                                                       non_voronoi_line_data,text_cordinate,open_plan_text_cordinate)
    #                     temp_edge_extension_penalties.append(final_score)
    #                     for e,score_row in enumerate(all_scores):
    #                         if debug_mode:
    #                             cv2.putText(floor_plan_boundaries, 'e. '+str(e+1)+'->T:'+str(final_score[e])+
    #                                         '=' + 'Len' + str(score_row[0]) + ', G' + str(score_row[1]) + ', ' + 'Txt' + str(
    #                                 score_row[2]) + ', ' + 'WL' + str(score_row[3]) + ' ,C' + str(
    #                                 score_row[4]) + ' ,Ipl' + str(score_row[5]),
    #                                         (5,print_start_point+temp_print_value), cv2.FONT_HERSHEY_PLAIN, 2, (0, 255, 0), 2)
    #                             temp_print_value=temp_print_value+30
    #                             # tx1,ty1 = open_plan_text_cordinate[textID]
    #                             # cv2.circle(floor_plan_boundaries, (tx1, ty1), 10, (0, 0, 255), -1)
    #                             # cv2.imwrite(score_path+'Partition '+str(textID)+str(e)+' Boundary '+str(r)+ 'Line Scores.png',floor_plan_boundaries)
    #
    #                 else:
    #                     temp_edge_extension_penalties.append([0])
    #                 if debug_mode:
    #                     # tx1,ty1 = open_plan_text_cordinate[textID]
    #                     # cv2.circle(floor_plan_boundaries, (tx1, ty1), 10, (0, 0, 255), -1)
    #                     cv2.imwrite(score_path+'Partition '+str(textID)+' Boundary '+str(r)+ 'Line Scores.png',floor_plan_boundaries)
    #
    #             edge_extension_penalties[edge_row_count].append(temp_edge_extension_penalties)#-----------******************
    #
    #         # 4. Calculate interesting lines(ipl_lines) penalty - ipl_score
    #         ipl_penalties = []
    #         for ipl_row_count, ipl_line_row1 in enumerate(seperated_ipl_lines):
    #             line_type = 'N'
    #             temp_ipl_extension_penalties = []
    #             textID = ipl_line_row1[0]
    #             ipl_line_row = ipl_line_row1[1]
    #             ipl_penalties.append([])
    #             ipl_penalties[ipl_row_count].append(textID)
    #             for r, ipl_line_set in enumerate(ipl_line_row):
    #                 b_cord_row = []
    #                 for b_row in boundaries:
    #                     if b_row[0] == textID:
    #                         b_cord_row = b_row[1]
    #                 boundary_cordinates = b_cord_row[r]
    #
    #                 non_vor_row_lines = []
    #                 for non_voronoi_row in non_voronoi_use:
    #                     if non_voronoi_row[0] == textID:
    #                         non_vor_row_lines = non_voronoi_row[1]
    #                         break
    #                 non_voronoi_line_data = non_vor_row_lines[r]
    #
    #                 # -----used in area difference score calculation
    #                 contour_partition_lines = []
    #                 for row_set in partition_area_lines:
    #                     if textID == row_set[1]:
    #                         contour_partition_lines = row_set[0]
    #                         break
    #                 text_cordinate = open_plan_text_cordinate[textID]
    #
    #                 ####-------used in area difference score calculation
    #                 # sep_vor_row_lines = []
    #                 # for sep_voronoi_row in seperated_voronoi_lines:
    #                 #     if sep_voronoi_row[0] == textID:
    #                 #         sep_vor_row_lines = sep_voronoi_row[1]
    #                 #         break
    #                 # seperated_voronoi_for_area_difference = sep_vor_row_lines[r]
    #
    #                 temp_print_value = 250
    #                 if debug_mode:
    #                     floor_plan_boundaries = cv2.imread(
    #                         score_path + 'Partition ' + str(textID) + ' Boundary ' + str(r) + 'Line Scores.png',
    #                         cv2.IMREAD_COLOR)
    #                     # floor_plan_boundaries = floor_plan_copy.copy()
    #                 if len(ipl_line_set) > 0:
    #                     if debug_mode:
    #                         cv2.putText(floor_plan_boundaries, '------------IPL Extensions Penalties----------------',
    #                                     (5, temp_print_value), cv2.FONT_HERSHEY_PLAIN, 2,
    #                                     (255, 255, 0), 2)
    #                         temp_print_value = temp_print_value + 30
    #                         # --------calculate score
    #                     line_obj = line()
    #                     final_score, all_scores = line_obj.calculate_score(room_type, avg_door_width, weight_list,
    #                                                                        ipl_line_set, textID, boundary_cordinates,
    #                                                                        shortest_path_data, line_type,
    #                                                                        edge_extension_data, score_path,
    #                                                                        img_height, img_width,Test_path,
    #                                                                        non_voronoi_line_data, text_cordinate,open_plan_text_cordinate)
    #                     temp_ipl_extension_penalties.append(final_score)
    #                     for ip, score_row in enumerate(all_scores):
    #                         if debug_mode:
    #                             cv2.putText(floor_plan_boundaries,
    #                                         'ipl. ' + str(ip + 1) + '->T:' + str(final_score[ip]) +
    #                                         '=' + 'Len' + str(score_row[0]) + ', G' + str(score_row[1]) + ', ' + 'Txt' + str(
    #                                 score_row[2]) + ', ' + 'WL' + str(score_row[3]) + ' ,C' + str(
    #                                 score_row[4]) + ' ,Ipl' + str(score_row[5]),
    #                                         (5, temp_print_value), cv2.FONT_HERSHEY_PLAIN, 2,
    #                                         (255, 255, 0), 2)
    #                             temp_print_value = temp_print_value + 30
    #
    #                 else:
    #                     temp_ipl_extension_penalties.append([0])
    #                 if debug_mode:
    #                    cv2.imwrite(score_path + 'Partition ' + str(textID) + ' Boundary ' + str(r) + 'Line Scores.png',
    #                                 floor_plan_boundaries)
    #
    #             ipl_penalties[ipl_row_count].append(temp_ipl_extension_penalties)
    #
    #
    #
    #
    #
    #
    #                 #----------------------------------------------------------------------------------------------------------------
    #
    #
    #
    #
    #

    # ------- STEP 07 : Calculate Scores
            voronoi_scores, edge_extension_scores, improved_voronoi_scores,ipl_scores = self.penalties_scores_obj.calculate_scores(
                voronoi_penalties, edge_extension_penalties,
                improved_voronoi_penalties, ipl_penalties,
                seperated_voronoi_lines, seperated_edge_lines,
                improved_voronoi_lines, seperated_ipl_lines,
                open_plan_text_cordinate, boundaries,
                score_path, debug_mode)

            # print '----boundaries---'
            # for row in boundaries:
            #     print row
            # print '----voronoi_penalties---'
            # for row in voronoi_penalties:
            #     print row
            # print '----edge_extension_penalties---'
            # for row in edge_extension_penalties:
            #     print row
            # print '----improved_voronoi_penalties---'
            # for row in improved_voronoi_penalties:
            #     print row
            # print '----ipl_penalties---'
            # for row in ipl_penalties:
            #     print row
            # print '+++++++++++++++++++++++++++++'
            # print '----voronoi_scores---'
            # for row in voronoi_scores:
            #     print row
            # print '----edge_extension_scores---'
            # for row in edge_extension_scores:
            #     print row
            # print '----improved_voronoi_scores---'
            # for row in improved_voronoi_scores:
            #     print row
            # print '----ipl_scores---'
            # for row in ipl_scores:
            #     print row
            # # 1. Calculate voronoi score = 0 for all
            # voronoi_scores = self.line_optimization_function_obj.calculate_voronoi_score(
            #     voronoi_penalties)
            # # 2. Calculate edge_extension score = voronoi penalty - edge penalty
            # edge_extension_scores = self.line_optimization_function_obj.calculate_eel_score(
            #     voronoi_penalties,edge_extension_penalties)
            # # 3.Calculate improved_voronoi score = voronoi penalty - improved voronoi penalty
            # improved_voronoi_scores = self.line_optimization_function_obj.calculate_ivl_score(
            #     voronoi_penalties, improved_voronoi_penalties)
            # #4. Calculate ipl score = voronoi penalty - ipl penalty
            # ipl_scores = self.line_optimization_function_obj.calculate_ipl_score(
            #     voronoi_penalties, ipl_penalties)
            #
            # if debug_mode:
            #     self.line_optimization_function_obj.print_all_scores(
            #         boundaries,voronoi_scores, edge_extension_scores,
            #         improved_voronoi_scores, seperated_voronoi_lines,
            #         seperated_edge_lines, improved_voronoi_lines,
            #         seperated_ipl_lines, ipl_scores,
            #         open_plan_text_cordinate, score_path)

    # #------- STEP 08 : Get Best line from all 3 line types
            line_obj= line()
            max_line,max_line_floor_plan = line_obj.find_maximum_score(
                edge_extension_scores,improved_voronoi_scores,
                ipl_scores, voronoi_penalties,
                seperated_voronoi_lines, improved_voronoi_lines,
                seperated_edge_lines, seperated_ipl_lines,
                max_line, max_line_floor_plan)

        if debug_mode:
            cv2.imwrite(self.output_directory+'_STEP_08_MAX_LINE.png',max_line_floor_plan)
        cv2.imwrite(self.best_line_directory+'MAX_LINE.png',max_line_floor_plan)


        return max_line

    def find_closest_contour_point(self,max_line_list,open_plan_contour_lines):
        line_obj=line()
        for row_p_number, partition_row in enumerate(max_line_list):
            for line_number,partition_line in enumerate(partition_row):
                partition_line1_x,partition_line1_y = partition_line[0]
                partition_line2_x,partition_line2_y = partition_line[1]
                for point_number,partition_point in enumerate(partition_line):
                    if (line_number==0 and point_number==0)or (line_number==len(partition_row)-1 and point_number==len(partition_line)-1):
                        partition_point_x,partition_point_y = partition_point
                    else:
                        continue


                    new_line = []
                    for contour_line in open_plan_contour_lines:
                        contour_line1_x, contour_line1_y= contour_line[0]
                        contour_line2_x, contour_line2_y = contour_line[1]
                        contour_max_x = max(contour_line1_x,contour_line2_x)
                        contour_min_x = min(contour_line1_x,contour_line2_x)
                        contour_max_y = max(contour_line1_y,contour_line2_y)
                        contour_min_y = min(contour_line1_y,contour_line2_y)
                        intersect_x, intersect_y = line_obj.find_contour_intersections(partition_line1_x,partition_line1_y,partition_line2_x,partition_line2_y,contour_line1_x, contour_line1_y,contour_line2_x, contour_line2_y)

                        #-----if partition line segment and contour line intersects
                        if intersect_x != 0 and intersect_y != 0:
                            if intersect_x>=contour_min_x and intersect_x<=contour_max_x and intersect_y>=contour_min_y and intersect_y<=contour_max_y:
                                #---find distance from selected partition point to intersection point
                                distance = int(math.hypot(intersect_x - partition_point_x, intersect_y - partition_point_y))
                                #--if < threshold: replace partition line point
                                if distance<=30:
                                    new_line.append([distance,[intersect_x,intersect_y]])
                    #---Case 01
                    if len(new_line)>0:
                        sorted_new_lines = sorted(new_line,key=itemgetter(0))
                        partition_line[point_number] = [sorted_new_lines[0][1],'F']

                    else:
                        #--if C_line and P-line intersected outside C-line but C-line end point and P_line end point are very close
                        #--add C-line point as the new P_point
                        for contour_line in open_plan_contour_lines:
                            for contour_line_x, contour_line_y in contour_line:
                                distance_to_contour_points = math.hypot(contour_line_x-partition_point_x,contour_line_y-partition_point_y)
                                if distance_to_contour_points<20:
                                    new_line.append([distance_to_contour_points,[contour_line_x,contour_line_y]])
                        #---Case 02
                        if len(new_line)>0:
                            sorted_new_lines = sorted(new_line,key=itemgetter(0))
                            partition_line[point_number] = [sorted_new_lines[0][1],'F']
                        else:
                            continue
        #----Case 03
        #---to snap all P_lines to other P_lines (when 2 P_lines intersect with each other)
        #--otherwise after snapping _points to meet C_lines, the P_line ntersections with each ohter might be lost
        for row_p_number, partition_row in enumerate(max_line_list):
            for line_number,partition_line in enumerate(partition_row):
                partition_line1_p1_x, partition_line1_p1_y,partition_line1_p2_x, partition_line1_p2_y = self.find_line_cordinates(partition_line)
                for point_number,partition_point in enumerate(partition_line):
                    if (line_number==0 and point_number==0)or (line_number==len(partition_row)-1 and point_number==len(partition_line)-1):
                        if partition_point[1]!='F':
                            partition_point_x,partition_point_y = partition_point
                            new_line = []
                            for row_p_number2, partition_row2 in enumerate(max_line_list):
                                for line_number2,partition_line2 in enumerate(partition_row2):
                                    # partition_line2_p1_x, partition_line2_p1_y,partition_line2_p2_x, partition_line2_p2_y = self.find_line_cordinates(partition_line2)
                                    for point_number2,partition_point2 in enumerate(partition_line2):
                                        if (line_number2==0 and point_number2==0)or (line_number2==len(partition_row2)-1 and point_number2==len(partition_line2)-1):
                                            if point_number2 != point_number:
                                                partition_line2_p1_x, partition_line2_p1_y,partition_line2_p2_x, partition_line2_p2_y = self.find_line_cordinates(partition_line2)
                                                partition_max_x = max(partition_line2_p1_x,partition_line2_p2_x)
                                                partition_min_x = min(partition_line2_p1_x,partition_line2_p2_x)
                                                partition_max_y = max(partition_line2_p1_y,partition_line2_p2_y)
                                                partition_min_y = min(partition_line2_p1_y,partition_line2_p2_y)
                                                intersect_x, intersect_y = line_obj.find_contour_intersections(partition_line1_p1_x, partition_line1_p1_y,partition_line1_p2_x, partition_line1_p2_y,partition_line2_p1_x, partition_line2_p1_y,partition_line2_p2_x, partition_line2_p2_y)
                                                distance = 100
                                                #-----if partition line segment and contour line intersects
                                                if intersect_x>=partition_min_x and intersect_x<=partition_max_x and intersect_y>=partition_min_y and intersect_y<=partition_max_y:
                                                    #---find distance from selected partition point to intersection point
                                                     distance = int(math.hypot(intersect_x - partition_point_x, intersect_y - partition_point_y))
                                                #--if < threshold: replace partition line point
                                                if distance<20:
                                                    new_line.append([distance,[intersect_x,intersect_y]])
                            if len(new_line)>0:
                                sorted_new_lines = sorted(new_line,key=itemgetter(0))
                                partition_line[point_number] = [sorted_new_lines[0][1],'F']

        # print '----after iteration---'
        # for each_line in max_line_list:
        #     print each_line

        new_lines_array = []
        for row_number, partition_line_final in enumerate(max_line_list):
            new_lines_array.append([])
            temp_list_1 = []
            for line_number,partition_line in enumerate(partition_line_final):
                temp_list_2 = []
                for point_number,partition_point_final in enumerate(partition_line):
                    if partition_point_final[1] == 'F':
                        temp_list_2.append(partition_point_final[0])
                    else:
                        temp_list_2.append(partition_point_final)
                # print 'temp_list_2',temp_list_2
                temp_list_1.append(temp_list_2)
            # print 'temp_list_1',temp_list_1
            new_lines_array[row_number]= temp_list_1

        # print '----fixed---'
        # for each_line in new_lines_array:
        #     print each_line


        return new_lines_array


    def find_closest_contour_point_region_grow(self,image_path,room_name,max_line_list):
        all_max_lines=[]
        for each_open_plan_row in max_line_list:
            for line_collection in each_open_plan_row:
                for each_line in line_collection:
                    all_max_lines.append(each_line)

        # print '----------BEFORE all_max_lines-------'
        # for row in all_max_lines:
        #     print row


        floor_plan = cv2.imread(image_path+room_name,cv2.IMREAD_COLOR)
        for r, line in enumerate(all_max_lines):
            cv2.line(floor_plan,(tuple(line[0])),(tuple(line[1])),(0,0,255),2,cv2.cv.CV_AA)
        cv2.imwrite(image_path+room_name, floor_plan)



        color_image = Image.open(image_path+room_name)
        # print image_path+room_name
        color_width, color_height = color_image.size
        color_pixels = color_image.load()
        for img_row in range(color_height-1):
            for img_column in range(color_width-1):
                # colors go as R,G,B
                if color_pixels[img_column,img_row] == (0, 0, 0):
                    color_pixels[img_column,img_row] = (255,255,255)
        color_image.save(image_path+room_name[:-4]+'_TEMP.png')

        #--2nd iteration
        color_image = Image.open(image_path+room_name[:-4]+'_TEMP.png')
        color_width, color_height = color_image.size
        color_pixels = color_image.load()
        for img_row in range(color_height-1):
            for img_column in range(color_width-1):
                # colors go as R,G,B
                if color_pixels[img_column,img_row] != (255, 255, 255):
                    color_pixels[img_column,img_row] = (0,0,0)
        color_image.save(image_path+room_name[:-4]+'_STEP_01.png')


        #-------start process of rings
        # print image_path+room_name[:-4]+'_STEP_01.png'
        image_line_closing = Image.open(image_path+room_name[:-4]+'_STEP_01.png')
        width, height = image_line_closing.size
        pixels_line_closing = image_line_closing.load()




        # print 'pixels_line_closing[1334, 1254]',pixels_line_closing[1334, 1254]


        new_lines_array = []
        for row_number,line_row in enumerate(all_max_lines):
            new_lines_array.append([])
            # print 'line_row',line_row
            # print 'pixels_line_closing[1334, 1254]-2',pixels_line_closing[1334, 1254]
            for point_number, point in enumerate(line_row):
                # if row_number==5 and point_number==1:
                # print 'point',point
                cordinate = point
                column_start, row_start = cordinate
                # print 'column_start, row_start',column_start, row_start
                column, row = int(column_start), int(row_start)
                pixel_data = [[-1 for img_def_column in range(width)] for img_def_row in range(height)]
                end_ring, new_cordinate = False,0
                temp_array_copy = []
                temp_ring_array = []
                # print 'pixels_line_closing[1334, 1254]-3',pixels_line_closing[1334, 1254]

                # print starting_cordinate,column, row,'-1; ',pixels_line_closing[column, row]
                for ring_count in range(5000):
                    # print 'ring_count is',ring_count
                    #-------testing
                    # test_image = Image.open(image_path+room_name[:-4]+'_STEP_01.png')
                    # width, height = test_image.size
                    # test_pixels = test_image.load()
                    #------end testing
                    if ring_count == 0:
                        if pixels_line_closing[column,row] == (0, 0, 0):
                            # print column,row,pixels_line_closing[column,row]
                            new_lines_array[row_number].append(cordinate)
                            break
                        else:
                            pixel_data[row][column]= point_number
                            #-----add txt_label cordinate to temp_array
                            temp_array_copy.append(cordinate)

                    for temp_row in temp_array_copy:
                        # print 'temp_row',temp_row
                        col_coord, row_coord = temp_row
                        top_pixel = col_coord, row_coord-1
                        left_pixel = col_coord-1, row_coord
                        bottom_pixel = col_coord, row_coord+1
                        right_pixel = col_coord+1, row_coord

                        # print 'top_pixel',top_pixel
                        # print 'left_pixel',left_pixel
                        # print 'bottom_pixel',bottom_pixel
                        # print 'right_pixel',right_pixel
                        # test_pixels[col_coord, row_coord]=(0,0,0)
                        # test_pixels[top_pixel]=(0,0,255)
                        # test_pixels[left_pixel]=(255,0,0)
                        # test_pixels[bottom_pixel]=(0,255,0)
                        # test_pixels[right_pixel]=(255,255,0)

                        top_col_1,top_row_1 = top_pixel
                        top_col,top_row  = int(top_col_1),int(top_row_1)
                        # print 'top pixel', pixels_line_closing[1360, 1684]
                        # print top_pixel,pixels_line_closing[int(top_col),int(top_row)]
                        if pixels_line_closing[top_col,top_row]== (0,0,0):
                            new_cordinate = [top_col,top_row]
                            end_ring= True
                            break
                        elif pixel_data[top_row][top_col]==-1:
                            temp_ring_array, pixel_data = self.find_border(top_pixel,pixel_data,temp_ring_array,point_number)

                        left_col_1,left_row_1 = left_pixel
                        left_col,left_row = int(left_col_1),int (left_row_1)
                        if pixels_line_closing[left_col,left_row]== (0,0,0):
                            new_cordinate = [left_col,left_row]
                            end_ring= True
                            break
                        elif pixel_data[left_row][left_col]==-1:
                            temp_ring_array, pixel_data = self.find_border(left_pixel,pixel_data,temp_ring_array,point_number)

                        bottom_col_1,bottom_row_1 = bottom_pixel
                        bottom_col,bottom_row = int(bottom_col_1), int(bottom_row_1)
                        if pixels_line_closing[bottom_col,bottom_row]== (0,0,0):
                            new_cordinate = [bottom_col,bottom_row]
                            end_ring= True
                            break
                        elif pixel_data[bottom_row][bottom_col]==-1:
                            temp_ring_array, pixel_data = self.find_border(bottom_pixel,pixel_data,temp_ring_array,point_number)

                        right_col_1,right_row_1 = right_pixel
                        right_col,right_row = int(right_col_1),int(right_row_1)
                        if pixels_line_closing[right_col,right_row]== (0,0,0):
                            new_cordinate = [right_col,right_row]
                            end_ring= True
                            break
                        elif pixel_data[right_row][right_col]==-1:
                            temp_ring_array, pixel_data = self.find_border(right_pixel,pixel_data,temp_ring_array,point_number)

                        # test_image.save(image_path+'-'+str(row_number)+':'+str(point_number)+':'+str(ring_count)+'.png')
                    temp_array_copy = temp_ring_array
                    temp_ring_array = []

                    # print 'pixels_line_closing[1334, 1254]-8',pixels_line_closing[1334, 1254]
                    if end_ring:
                        new_lines_array[row_number].append(new_cordinate)
                        break

        # os.remove(image_path+room_name[:-4]+'_STEP_01.png')
        # print '----------AFTER all_max_lines-------'
        # for row in new_lines_array:
        #     print row

        return new_lines_array

    def find_border(self,cordinate, pixel_data, temp_ring_array,current_txt_label):
        image_column_1,image_row_1 = cordinate
        image_column,image_row = int(image_column_1),int(image_row_1)
        pixel_data[image_row][image_column]= current_txt_label
        temp_ring_array.append(cordinate)
        return temp_ring_array, pixel_data

    def find_line_cordinates(self, partition_line):
        if partition_line[0][-1]=='F':
            partition_line_p1_x, partition_line_p1_y= partition_line[0][0]
        else:
            partition_line_p1_x, partition_line_p1_y= partition_line[0]
        if partition_line[1][1]=='F':
            partition_line_p2_x, partition_line_p2_y = partition_line[1][0]
        else:
            partition_line_p2_x, partition_line_p2_y = partition_line[1]
        return partition_line_p1_x, partition_line_p1_y,partition_line_p2_x, partition_line_p2_y