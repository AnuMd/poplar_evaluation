__author__ = 'stefano'


from connectedcomponent import eps_circle, eps_ellipse, eps_rect
import shutil,subprocess,os,cv2,time, sys, getopt,traceback,copy
import numpy as np
from PIL import Image
from distutils.dir_util import copy_tree
from franges import frange
from franges import drange
from scipy import ndimage
from operator import itemgetter
from natsort import natsorted
from shutil import copyfile
from xlwt import Workbook


from image import Image_class
from graphCC import GraphCC
from room_object_match import room_object_match_class
import imageOperations as imop
from copy import deepcopy
from connectedcomponent import CC
from pre_process import pre_process_class
from accessible_fp import accessible_fp_generator
from iterative_functions import iterative_functions_class
from room_functions import room_functions_class
from evaluate import evaluation_class
from cupboard_find import cupboard_class
from svg_generation import svg_generator



class MainFormLogic():
    #-------initialization method
    def __init__(self):
        self.thresholds = [0,0,0,0,0]
        self.count = 0
        self.name = ''
        self.path_floorplan = ''
        self.converted_floorplan_path = ''
        self.path_output = ''
        self.path_output_d = ''
        self.path_results = ''
        self.path_final_lines = ''
        self.path_gravvitas = ''
        self.path_gravvitas_svg = ''
        self.object_path = ''
        self.object_output_path = ''
        self.current_directory =''
        self.name = ''
        #--make 'debug_mode' True to see intermediate results-------
        debug_mode = False
        iterative_obj = iterative_functions_class()

        #----get path
        self.current_directory, self.input_path, self.output_path = self.get_path()

        # #--Add new objects method
        # self.add_new_objects()


        #--create folders
        self.create_output_folders(self.current_directory,debug_mode)


        #--get list of input floorplans
        list_images_floorplan = self.pre_process_images(self.current_directory)

        #--define weight_lists
        criteria_weight_list = [['line_length', 0.6], ['Gradient', 0.75], ['TxtDistance', 1.0],
                                ['EELengthRatio', 0.4], ['ConvexityDefect', 0.5], ['IplWeight', 0.5]]

        wb = Workbook()

        sheet_name = 'Evaluation results'
        ws = wb.add_sheet(sheet_name)
        #--print header values
        ws.write(0, 0, 'Image Name')
        ws.write(0, 1, 'Avg_JI')
        ws.write(0, 2, 'Avg_CR')
        ws.write(0, 3, 'Avg_LPR')
        # ws.write(0, 4, 'Wgt_JI')
        # ws.write(0, 5, 'Wgt_CR')
        # ws.write(0, 6, 'Image_S')

        weight_list = [row[1] for row in criteria_weight_list]

        row_count = 1
        ##---take each floorplan name from list of images
        for file_name in list_images_floorplan:
            try:
                ws.write(row_count, 0, str(file_name))
                print file_name
                # total_image_start_time = time.time()
                img = cv2.imread(self.path_floorplan + file_name, cv2.IMREAD_GRAYSCALE)
                orginal_img_height, orginal_img_width = img.shape


                # ------floorplan partition process----
                # print weight_list
                # start_image_process_time = time.time()
                self.floorplan_component_ID(file_name,img,weight_list,self.path_floorplan,orginal_img_height, orginal_img_width,debug_mode)
                # image_process_end_time = time.time()
                # print image_process_end_time,start_image_process_time
                # print 'Process floorplan Time : ', image_process_end_time-start_image_process_time




                # self.path_gravvitas = self.current_directory+'/GraVVITAS/61/TEXT/'
                path_gt = self.current_directory+'/OP_GT/'+str(file_name[0:-4])+'/'
                #--create output path
                evaluation_output_path = self.eval_output_path + str(file_name[0:-4]) + '/'
                iterative_obj.make_directory(evaluation_output_path, True)

                #run evaluation
                evaluation_obj = evaluation_class()
                average_op_JI, average_op_CR, average_op_pixel_ratio, weighted_JI, weighted_CR, image_score = evaluation_obj.conduct_evaluation(orginal_img_height, orginal_img_width,self.path_gravvitas,path_gt,evaluation_output_path,file_name)

                ws.write(row_count, 1, average_op_JI)
                ws.write(row_count, 2, average_op_CR)
                ws.write(row_count, 3, average_op_pixel_ratio)
                # ws.write(row_count, 4, weighted_JI)
                # ws.write(row_count, 5, weighted_CR)
                # ws.write(row_count, 6, image_score)
                print average_op_JI, average_op_CR, average_op_pixel_ratio
                wb.save(self.eval_output_path + 'Evaluation_Results.xls')


            except(KeyboardInterrupt,SystemExit):
                print "User interrupted!"
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)

            except:
                print "---------------------Error occured with-------------------------", str(file_name)
                traceback.print_exc()
            row_count = row_count + 1

        wb.save(self.eval_output_path + 'Evaluation_Results.xls')


    #shows the total execution time for the function
    def contatempo(fn):
        def interna(*args,**kw):
            start=time.time()
            result=fn(*args,**kw)
            tempo=time.time()-start
            print "Function: %s - Execution time: %4.2f" % (fn.__name__,tempo)
            return result
        return interna

    def create_output_folders(self,current_directory,debug_mode):
        #create folders to store output - OUTPUT/OUTPUT_DEBUG/RESULTS_TEST/EDGES folders
        self.path_output=current_directory+'/OUTPUT/'
        if not(os.path.exists(self.path_output)):
                os.mkdir(self.path_output)
        self.path_output_d=current_directory+'/OUTPUT_DEBUG/'
        if not(os.path.exists(self.path_output_d)):
                os.mkdir(self.path_output_d)
        self.path_results = current_directory+'/RESULTS_TEST/'
        if not(os.path.exists(self.path_results)):
                os.mkdir(self.path_results)
        self.path_final_lines = current_directory+'/Final Lines/'
        if (os.path.exists(self.path_final_lines)):
            shutil.rmtree(self.path_final_lines)
        os.mkdir(self.path_final_lines)
        #--create path for OBJECTS results
        self.object_path = current_directory+'/OBJECTS/'
        if not os.path.exists(self.object_path):
            os.mkdir(self.object_path)
        #--create place to write text for GraVVITTAS
        self.path_gravvitas=current_directory+'/GraVVITAS/'
        if not(os.path.exists(self.path_gravvitas)):
            os.mkdir(self.path_gravvitas)
        #--create place to store final SVG to pass to JS
        self.svg_path=current_directory+'/SVG_results/'
        if not(os.path.exists(self.svg_path)):
            os.mkdir(self.svg_path)
        #--create place to store tactile results
        self.tactile_path=current_directory+'/Tactile_FP/'
        if not(os.path.exists(self.tactile_path)):
            os.mkdir(self.tactile_path)
        # --create place to store evaluation results
        self.evaluation_path = current_directory + '/EVALUATION/'
        if not (os.path.exists(self.evaluation_path)):
            os.mkdir(self.evaluation_path)
        # --create place to store Weight Test results
        self.path_weight_results = current_directory + '/Weight Test/'
        if (os.path.exists(self.path_weight_results)):
            shutil.rmtree(self.path_weight_results)
        os.mkdir(self.path_weight_results)

        #--incremental folder creation--------------------------------------
        #OUTPUT folder incremental values for folder name
        list_dir_output = os.listdir(self.path_output)
        if len(list_dir_output)>0:
            t = [int(x) for x in list_dir_output]
            t.sort()
            name = str(t[len(t)-1]+1)
            self.path_output = self.path_output+name+'/'
            os.mkdir(self.path_output)
        else:
            self.path_output = self.path_output+str(1)+'/'
            name="1"
            os.mkdir(self.path_output)

        #OUTPUT_DEBUG folder incremental values for folder name
        if not(os.path.exists(self.path_output_d+ name +"/")):
            os.mkdir(self.path_output_d+ name +"/")
            self.path_output_d = self.path_output_d+ name +"/"
        else:
            shutil.rmtree(self.path_output_d+ name +"/")
            os.mkdir(self.path_output_d+ name +"/")
            self.path_output_d = self.path_output_d+ name +"/"

        #GRAVVITAS folder incremental values for folder name
        if not(os.path.exists(self.path_gravvitas+ name +"/")):
            os.mkdir(self.path_gravvitas+ name +"/")
            self.path_gravvitas = self.path_gravvitas+ name +"/"
        else:
            shutil.rmtree(self.path_gravvitas+ name +"/")
            os.mkdir(self.path_gravvitas+ name +"/")
            self.path_gravvitas = self.path_gravvitas+ name +"/"
        self.path_gravvitas_svg=self.path_gravvitas+'/SVG/'
        if not(os.path.exists(self.path_gravvitas_svg)):
            os.mkdir(self.path_gravvitas_svg)
        self.path_gravvitas=self.path_gravvitas+'/TEXT/'
        if not(os.path.exists(self.path_gravvitas)):
            os.mkdir(self.path_gravvitas)

        #tactile folder incremental values for folder name
        if not(os.path.exists(self.tactile_path+ name +"/")):
            os.mkdir(self.tactile_path+ name +"/")
            self.tactile_path = self.tactile_path+ name +"/"
        else:
            shutil.rmtree(self.tactile_path+ name +"/")
            os.mkdir(self.tactile_path+ name +"/")
            self.tactile_path = self.tactile_path+ name +"/"

        #OBJECTS folder incremental values for folder name
        list_dir_output = os.listdir(self.object_path)
        if len(list_dir_output)>0:
            t = [int(x) for x in list_dir_output]
            t.sort()
            self.name = str(t[len(t)-1]+1)+'/'
        else:
            self.name = '1/'
        self.object_output_path = self.object_path
        self.object_path = self.object_path+self.name
        if not os.path.exists(self.object_path):
            os.mkdir(self.object_path)

        # EVALUATION folder incremental values for folder name
        list_dir_output = os.listdir(self.evaluation_path)
        if len(list_dir_output) > 0:
            t = [int(x) for x in list_dir_output]
            t.sort()
            self.name = str(t[len(t) - 1] + 1) + '/'
        else:
            self.name = '1/'
        # self.eval_output_path = self.evaluation_path
        self.eval_output_path = self.evaluation_path + self.name
        if not os.path.exists(self.eval_output_path):
            os.mkdir(self.eval_output_path)



        #create subfolders inside main OUTPUT and OUTPUT_DEBUG folders
        self.create_path_extract_text()
        self.create_path_wall()
        if debug_mode:
            self.create_path_find_door()
        self.create_path_find_windows()
        self.create_path_preprocessing()
        self.create_path_merge_wwd()
        self.create_path_scale()
        self.create_path_merge_all()
        self.create_path_output()



    def pre_process_images(self,current_directory):
        input_path_floorplan = self.input_path

        #--check if input folder exists, carry out rest of the program if it does
        if not(os.path.exists(input_path_floorplan)):
            print 'Directory Not Found.'
            sys.exit()
        else:
            # self.path_floorplan = input_path_floorplan
            # print 'input_path_floorplan',input_path_floorplan
            original_fp_path_unsorted = os.listdir(input_path_floorplan)
            original_fp_path = natsorted(original_fp_path_unsorted)
            # for each_image in original_fp_path:


            self.converted_floorplan_path = self.current_directory+'/input_fps/converted_fps/'
            if (os.path.exists(self.converted_floorplan_path)):
                shutil.rmtree(self.converted_floorplan_path)
            os.makedirs(self.converted_floorplan_path)


            for floor in original_fp_path:
                # print 'str(floor)',str(floor)
                if str(floor)=='.DS_Store':
                    os.remove(input_path_floorplan+str(floor))
                else:
                    if floor[-4:]=='.png':
                        shutil.copy(input_path_floorplan+str(floor),self.converted_floorplan_path+str(floor))
                    elif floor[-4:]=='.svg' or floor[-4:]=='.SVG':
                        im_name = str(floor)
                        only_im_name = im_name[:-4]
                        # os.system('convert -density 40 '+input_path_floorplan+str(floor)+' -set density 40 '+self.converted_floorplan_path+only_im_name+'_40.png')
                        # os.system('convert -density 50 '+input_path_floorplan+str(floor)+' -set density 50 '+self.converted_floorplan_path+only_im_name+'_50.png')
                        # os.system('convert -density 60 '+input_path_floorplan+str(floor)+' -set density 60 '+self.converted_floorplan_path+only_im_name+'_60.png')
                        os.system('convert -density 70 '+input_path_floorplan+str(floor)+' -set density 70 '+self.converted_floorplan_path+only_im_name+'_70.png')
                        # os.system('convert -density 100 '+input_path_floorplan+str(floor)+' -set density 100 '+self.converted_floorplan_path+only_im_name+'_100.png')
                        # os.system('convert -density 150 '+input_path_floorplan+str(floor)+' -set density 150 '+self.converted_floorplan_path+only_im_name+'_150.png')
                        # os.system('convert -density 200 '+input_path_floorplan+str(floor)+' -set density 200 '+self.converted_floorplan_path+only_im_name+'_200.png')
                        # os.system('convert -density 250 '+input_path_floorplan+str(floor)+' -set density 250 '+self.converted_floorplan_path+only_im_name+'_250.png')

                    else:
                        im_conv = Image.open(input_path_floorplan+str(floor))
                        im_name = str(floor)
                        only_im_name = im_name[:-4]
                        im_conv.save(self.converted_floorplan_path+only_im_name+'.png')

            self.path_floorplan = self.converted_floorplan_path
            list_images_floorplan_unsorted = os.listdir(self.converted_floorplan_path)
            list_images_floorplan = natsorted(list_images_floorplan_unsorted)

            self.create_path_open_plan(list_images_floorplan)
            return list_images_floorplan

    def floorplan_component_ID(self,file_name,img,weight_list, orginal_img_path,orginal_img_height, orginal_img_width,debug_mode):
        #---extract only image name without extensions
        only_image_name=file_name[0:-4]
        path = self.path_output_d+only_image_name+'/'
        if not(os.path.exists(path)):
            os.mkdir(path)

        image_color =  cv2.imread(orginal_img_path+file_name,cv2.IMREAD_COLOR )
        ret2,otsu = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        cv2.imwrite(self.path_output_d+'preprocessing/'+only_image_name+'_otsu.png', otsu)
        image = Image_class(self.current_directory,file_name,self.path_output,self.path_output_d, otsu,image_color)
        ##### image.test()


        # # start_find_connected_componet= time.time()
        # image.find_connected_componet()
        # # end_find_connected_componet = time.time()
        # # print 'Finding connected components Time : ', end_find_connected_componet-start_find_connected_componet

        # start_Methods_Text_Recognition_Based_Analysis_cc= time.time()
        # input_org_image_path = self.path_output_d+'preprocessing/'+only_image_name+'_otsu.png'
        text_cordinates = image.Methods_Text_Recognition_Based_Analysis_cc(self.path_gravvitas, self.current_directory,
                                                                           otsu, debug_mode)
        # end_Methods_Text_Recognition_Based_Analysis_cc = time.time()
        # print 'Text Recognition : ', end_Methods_Text_Recognition_Based_Analysis_cc-start_Methods_Text_Recognition_Based_Analysis_cc


        # start_find_room_functions = time.time()
        avg_door_width = 100
        room_obj = room_functions_class()
        room_obj.all_room_functions(self.current_directory, self.path_output_d, self.path_output, self.path_gravvitas, self.path_final_lines, avg_door_width,weight_list, only_image_name, otsu, text_cordinates,debug_mode)
        # end_find_room_functions  = time.time()
        # print 'Find_room_functions  : ', end_find_room_functions - start_find_room_functions


    def get_path(self):
        current_directory, input_path, output_path = self.get_path_from_local_machine()
        # current_directory, input_path, output_path = self.get_path_from_js()
        # print 'current_directory',current_directory
        # print 'input_path',input_path
        # print 'output_path',output_path
        return current_directory, input_path, output_path

    def get_path_from_local_machine(self):
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code
        current_directory = os.path.abspath(os.path.join(os.getcwd(), '..'))
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/input_fps/anu/
        input_path = current_directory+ '/input_fps/anu/'
        # input_path = current_directory + '/input_fps/current_working_fps/'
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/SVG_results/final_floorplan.svg
        output_path = current_directory+'/SVG_results/final_floorplan.png'
        return current_directory, input_path, output_path

    def get_path_from_js(self):
        js_message = sys.stdin.readlines()
        input_path, current_directory,output_path = '','',''
        for row_num,each in enumerate(js_message):
            if row_num==0:
                current_directory = each.rstrip()
            elif row_num==1:
                input_path = each.rstrip()
            else:
                output_path = each.rstrip()
        return current_directory, input_path, output_path

    def add_new_objects(self):
        # self.usage=raw_input('Do you want to create new dictionaries? y for Yes, any digit for no: ')
        a = self.current_directory
        self.usage = 'y'
        if self.usage == 'y':
            self.__buildDictionary(a+'/dictionary/')
            self.__buildDictionary(a+'/secondary dictionary/')
            self.__buildDictionary(a+'/noise/')
        self.calculate_gt_features()
        print '--- finding objects in floorplans ----'
        self.colors_label = []
        self.gt_path = a+'/GT_Files/'
        self.read_color()
        self.num_class = len(self.colors_label)
        self.true_pos = np.zeros((self.num_class, 1))
        self.false_pos = np.zeros((self.num_class, self.num_class))
        self.objects = np.zeros((self.num_class, 1))
        self.avg = []
        for i in range(0, self.num_class):
            self.avg.append([])
            self.avg[i].append([])
            self.avg[i].append([])

        # self.usage=raw_input('Do you want to create new dictionaries? y for Yes, any digit for no: ')
        # self.usage = 'y'
        # if self.usage == 'y':
        #     self.__buildDictionary(self.current_directory+'/dictionary/')
        #     self.__buildDictionary(self.current_directory+'/secondary dictionary/')
        #     self.__buildDictionary(self.current_directory+'/noise/')
        # self.colors_label = []
        # self.gt_path = self.current_directory+'/GT_Files/'
        # #--read "self.current_directory+'/GT_Files/colors.txt'"
        # #-- get its each line "'crossed', '000040'"
        # #-- add to self.colors_label list
        # self.read_color()
        # for row in self.colors_label:
        #     print row
        # #--10 lines from "colors.txt" file + 'nothing', '000000' = len(self.colors_label) is 11
        # self.num_class = len(self.colors_label)
        # print self.num_class
        # self.true_pos = np.zeros((self.num_class, 1))
        # print 'true_pos'
        # for row in self.true_pos:
        #     print row
        # self.false_pos = np.zeros((self.num_class, self.num_class))
        # self.objects = np.zeros((self.num_class, 1))
        # self.avg = []
        # for i in range(0, self.num_class):
        #     self.avg.append([])
        #     self.avg[i].append([])
        #     self.avg[i].append([])
        # self.calculate_gt_features()
        # print '--- finding objects in floorplans ----'
        # self.colors_label = []
        # self.gt_path = self.current_directory+'/GT_Files/'
        # self.read_color()
        # self.num_class = len(self.colors_label)
        # self.true_pos = np.zeros((self.num_class, 1))
        # self.false_pos = np.zeros((self.num_class, self.num_class))
        # self.objects = np.zeros((self.num_class, 1))
        # self.avg = []
        # for i in range(0, self.num_class):
        #     self.avg.append([])
        #     self.avg[i].append([])
        #     self.avg[i].append([])

    def create_path_find_oblique_walls(self):
        path = self.path_output_d+'oblique_walls/'
        if not(os.path.exists(path)):
            os.mkdir(path)

    def create_path_scale(self):
        path = self.path_output_d+'scale/'
        if not(os.path.exists(path)):
            os.mkdir(path)

    def create_path_merge_all(self):
        path = self.path_output_d+'merge_all/'
        if not(os.path.exists(path)):
            os.mkdir(path)

    def create_path_merge_wwd(self):
        path_merge_wall=self.path_output_d+'merge_walls_doors_windows/'
        if not(os.path.exists(path_merge_wall)):
            os.mkdir(path_merge_wall)
       # os.mkdir(path_merge_wall+'step1/')

    def create_path_find_windows(self):
        path_window=self.path_output_d+'windows/'
        if not(os.path.exists(path_window)):
            os.mkdir(path_window)
       # os.mkdir(path_window+'step1/')

    def create_path_output(self):
        path_window=self.path_output_d+'output/'
        if not(os.path.exists(path_window)):
            os.mkdir(path_window)

    def create_path_wall(self):
        path_external_wall=self.path_output_d+'wall/'
        if not(os.path.exists(path_external_wall)):
            os.mkdir(path_external_wall)
            os.mkdir(self.path_output_d+'oblique_walls/')

    def create_path_find_door(self):
        path_external_wall=self.path_output_d+'door/'
        if not(os.path.exists(path_external_wall)):
            os.mkdir(path_external_wall)
            os.mkdir(path_external_wall+'ante/')


    def create_path_extract_text(self):
        extract_text_path = self.path_output_d+'Extract_text/'
        if not(os.path.exists(extract_text_path)):
            os.mkdir(extract_text_path)
            os.mkdir(extract_text_path+'area_dim/')
            os.mkdir(extract_text_path+'area_dim_2/')
            os.mkdir(extract_text_path+'asp_ratio/')
            os.mkdir(extract_text_path+'density/')
            os.mkdir(extract_text_path+'final_images/')
            os.mkdir(extract_text_path+'text_identification/')

         #in the path "path_clean_image", I save the output of the function clean_image (image.py)
        path_clean_image=extract_text_path+'clean_image/'
        if os.path.exists(path_clean_image):
            shutil.rmtree(path_clean_image)
        else:
            os.mkdir(path_clean_image)
            os.mkdir(path_clean_image+'Canny/')
            os.mkdir(path_clean_image+'Hough/')
            os.mkdir(path_clean_image+'Clean/')
            os.mkdir(path_clean_image+'Open/')# in this path there are the final image

    def create_path_preprocessing(self):
        path_preprocessing=self.path_output_d+'preprocessing/'
        if not(os.path.exists(path_preprocessing)):
            os.mkdir(path_preprocessing)


    def create_path_open_plan(self,list_images_floorplan):
        path_open_plan=self.path_output_d+'Rooms/'
        if not(os.path.exists(path_open_plan)):
            os.mkdir(path_open_plan)
        for each_image in list_images_floorplan:
            image_name = each_image[0:-4]
            image_folder_path = path_open_plan+image_name+'/'
            if not(os.path.exists(image_folder_path)):
                os.mkdir(image_folder_path)
                os.mkdir(image_folder_path+'All_Rooms/')
                os.mkdir(image_folder_path+'OpenPlans/')





    # Pierpaolo's:
    def __buildDictionary(self, path):
        # a=os.path.dirname(os.getcwd())
        a = self.current_directory
        # dictionary_path = raw_input('Insert dictionary input directory: ')
        dictionary_path = path
        if not(os.path.exists(dictionary_path)):
            print 'Directory Not Found.'
            sys.exit()
        else:
            list_object_folder = os.listdir(dictionary_path)
            for folder in list_object_folder:
                if '.' not in folder:
                    object_path = dictionary_path+folder+'/'
                    #--object_path=dictionary/001 bath/
                    output_path = object_path+'output/'
                    debug_path = object_path+'preprocessing/'
                    #--creates 'dictionary/output/'
                    #--creates 'dictionary/preprocessing/'
                    if not(os.path.exists(output_path)):
                        os.mkdir(output_path)
                    if not(os.path.exists(debug_path)):
                        os.mkdir(debug_path)
                    list_images_o = os.listdir(object_path)
                    # find only image .png in directory
                    list_object_floorplan=[]
                    i = 1
                    for object in list_images_o:
                        #get image extension
                        #--object_path=dictionary/001 bath/bath001.png or 'dictionary/001 bath/bath002.png'
                        typefile=object[len(object)-4:len(object)]
                        if typefile=='.png':
                            list_object_floorplan.append(object)
                    list_object_floorplan.sort()
                    for f in list_object_floorplan:
                        #--for each bath001.png and bath002.png create a folder in 'dictionary/output/'
                        out_path = output_path+str(i)+'/'
                        if not(os.path.exists(out_path)):
                            os.mkdir(out_path)
                        # print '\n'+str(f)
                        s=f[0:-4]
                        img = cv2.imread(object_path+f,cv2.IMREAD_GRAYSCALE)
                        image_color =  cv2.imread(object_path+f,cv2.IMREAD_COLOR )
                        # Otsu's thresholding to clean image and remove smoothing effects
                        ret2,otsu = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
                        image = Image_class(self.current_directory,f,out_path, debug_path, otsu,image_color)
                        #--create objects
                        image.find_connected_componet_dictionary()
                        i+=1




    def save_objects_recognition(self, path):
        file = open(path+'results.csv', 'w')
        file.write(' ;Objects found;TP:\n')
        for i in range(0, len(self.objects)):
            label = self.colors_label[i][0]
            file.write(label+'; '+str(int(self.objects[i][0]))+';'+str(int(self.true_pos[i][0]))+';\n')
        file.write('Total = '+str(int(self.objects.sum()))+';'+str(int(self.true_pos.sum()))+';\n')
        file.write('\nConfusion table:\n;')
        for i in range(0, len(self.false_pos)):
            label = self.colors_label[i][0]
            file.write(label+';')

        for i in range(0, len(self.false_pos)):
            file.write('\n')
            label = self.colors_label[i][0]
            file.write(label+';')
            for j in range(0, len(self.false_pos[i])):
                file.write(str(int(int(self.false_pos[i][j])))+';') #object j confused with object i
        file.write('\n')
        file.write('Total;')
        for i in range(0, len(self.objects)):
            file.write(str(int(self.false_pos[i].sum()))+';')
        file.write('\nTotal FP;'+str(int(self.false_pos.sum()))+'\n')
        # file.write('Similiar = '+str(self.similiar)+'\n')
        file.close()

    def read_color(self):
        file = open(self.gt_path+'colors.txt', 'r')
        line = file.readline()
        while len(line)>2:
            color = []
            color.append(line[0:-1])
            line = file.readline()
            color.append(line[0:-1])
            self.colors_label.append(color)
            line = file.readline()
        self.colors_label.append(['nothing', '000000'])


    def get_label_from_color(self, col):
        for color in self.colors_label:
            if color[1] == col:
                return color



    def calculate_gt_features(self):
        a = self.current_directory
        # a=os.path.dirname(os.getcwd())
        gt_path = a+'/GT_Files/'
        list_gt = os.listdir(gt_path)
        list_gt.sort()
        total_door = []
        for gt_file in list_gt:
            if gt_file[-3:] == 'svg':
                name = gt_file[0:-4]
                file = open(gt_path+'doors_width.txt', 'r')
                line = file.readline()
                end = line.find(':')
                count = 0
                while name != line[0:end] and count < 100:
                    line = file.readline()
                    end = line.find(':')
                    count+=1
                file.close()
                avg_door = float(line[end+2:])
                if avg_door > 40:
                    # print line[0:end]+': '+str(avg_door)
                    self.read_gt_and_weight_size(gt_path+name, avg_door)
                    total_door.append(avg_door)
        file = open(gt_path+'objects_proportions.csv', 'w')
        file.write(' ;min ;dev ;max ; dev\n')
        for i in range(0, len(self.avg)):
            label = self.colors_label[i][0]
            file.write(label+';')
            path_class = gt_path+label
            if not os.path.exists(path_class):
                os.mkdir(path_class)
            prop = open(path_class+'/proportions.txt', 'w')
            for k in range(0, 2):
                # print str(self.avg[i][k])
                avg = sum(self.avg[i][k])/len(self.avg[i][k])
                var = 0
                for j in self.avg[i][k]:
                    var+=(avg-j)**2
                var = (var/(len(self.avg[i][k])-1))
                dev = np.sqrt(var)
                file.write(str(avg)+';'+str(dev)+';')
                thresh1 = avg-dev
                thresh2 = avg+dev
                prop.write(str(thresh1)+'\n')
                prop.write(str(thresh2)+'\n')
            prop.close()
            file.write('\n')
        file.write('\n')
        bound = int(round(len(total_door)*0.05))
        for i in range(0, bound):
            total_door.remove(max(total_door))
            total_door.remove(min(total_door))
        avg = sum(total_door)/len(total_door)
        var = 0
        file.write('doors;')
        for j in total_door:
            var+=(avg-j)**2
        var = (var/(len(self.avg[i][k])-1))
        dev = np.sqrt(var)
        file.write(str(avg)+';'+str(dev)+';')
        file.close()

    def get_id_color(self, name):
        # print 'get id color for '+name[0:3]
        for i in range(0, len(self.colors_label)):
            # print 'self.colors_label[i][0][0:3] = '+self.colors_label[i][0][0:3]
            if self.colors_label[i][0][0:3] == name[0:3]:
                return i
        return i

    def match_gt(self, obj, gt):
        id = self.get_id_color(obj.label)
        cc = CC(id)
        xmin, xmax, ymin, ymax = obj.get_extremes()
        cc.x_min = xmin
        cc.y_min = ymin
        # print 'match object '+obj.label
        # print 'coordinates = '+str(obj.get_extremes())
        img = np.ones((ymax-ymin-2, xmax-xmin-2))
        cc.image = imop.add_pad_single_pixel(img)
        cc.set_coordinates()
        hit = False
        for cc_gt in gt.get_indices():
            found = imop.logic_match(cc, cc_gt)
            if found:
                if cc.id == cc_gt.id:
                    self.true_pos[cc_gt.id]+=1
                else:
                    self.false_pos[cc.id][cc_gt.id]+=1
                hit = True
        if not hit:
            self.false_pos[cc.id][-1]+=1

    def read_gt_and_weight_size(self, path, avg_door):
        file = open(path+'.svg')
        line = file.readline()
        while line.find('</svg>') == -1:
            #print line
            if '<rect' in line:
                line = file.readline()
                # print line
                stroke = line.find('stroke:#')
                color = line[stroke+8:stroke+14]
                i = len(self.colors_label)-1
                for j in range(0, len(self.colors_label)):
                    if self.colors_label[j] == color:
                        i = j
                cc = CC(i)
                self.objects[i]+=1
                # print 'color is '+color
                line = file.readline()
                # print line
                line = file.readline()
                # print line
                eq = line.find('=')
                w = float(line[eq+2:-2])
                line = file.readline()
                eq = line.find('=')
                h = float(line[eq+2:-2])
                max_axis = float(max(h,w))
                min_axis = float(min(h,w))
                self.avg[i][0].append(min_axis/avg_door)
                self.avg[i][1].append(max_axis/avg_door)
            line = file.readline()
        file.close()


    def read_gt(self, path):
        file = open(path+'.svg')
        line = file.readline()
        graph = GraphCC()
        while line.find('</svg>') == -1:
            #print line
            if '<rect' in line:
                line = file.readline()
                # print line
                stroke = line.find('stroke:#')
                color = line[stroke+8:stroke+14]
                i = len(self.colors_label)-1
                # print 'color = '+color
                for j in range(0, len(self.colors_label)):
                    if self.colors_label[j][1] == color:
                        i = j
                cc = CC(i)
                self.objects[i]+=1
                # print 'color is '+color
                line = file.readline()
                # print line
                line = file.readline()
                # print line
                eq = line.find('=')
                w = float(line[eq+2:-2])
                line = file.readline()
                eq = line.find('=')
                h = float(line[eq+2:-2])
                line = file.readline()
                eq = line.find('=')
                x = float(line[eq+2:-2])
                line = file.readline()
                eq = line.find('=')
                eq2 = line[eq+2:].find('"')
                y = float(line[eq+2:eq+2+eq2])
                cc.x_min = int(x)
                cc.y_min = int(y)
                img = np.ones((int(h-2), int(w-2)))
                cc.image = imop.add_pad_single_pixel(img)
                cc.set_coordinates()
                # print 'w = '+str(int(w)) +', h = '+str(h)+', x = '+str(x)+', y = '+str(y)
                graph.add_node(cc)
            line = file.readline()
        file.close()
        return graph

    def increase_thresholding(self):
        if self.count%5 == 0:
            self.thresholds[0] += 0.002
        if self.count%5 == 1:
            self.thresholds[1] += 0.002
        if self.count%5 == 2:
            self.thresholds[2] += 0.01
        if self.count%5 == 3:
            self.thresholds[3] += 0.01
        if self.count%5 == 4:
            self.thresholds[4] += 0.05
        self.count+=1
        self.true_pos = np.zeros((self.num_class, 1))
        self.false_pos = np.zeros((self.num_class, self.num_class))
        self.objects = np.zeros((self.num_class, 1))
        return self.thresholds






if __name__ == '__main__':
    start = time.time()
    hwl1 = MainFormLogic()
    end = time.time()
    print end - start
    # subprocess.call(['spd-say', '"Program Execution is Completed"'])
