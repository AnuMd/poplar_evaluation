__author__ = 'pierpaolo'

from graphCC import GraphCC
import imageOperations as imop
import cv2
from copy import deepcopy
import os
import numpy as np
from connectedcomponent import CC, bins1, bins2
from label import Label

class Room(object):
    def __init__(self, id, contour_cc, path):
        self.label = []
        self.type = 0
        if id == 0:
            label = Label()
            label.set_label('external', 5, 5, 1, 1, 11, 11)
            self.label.append(label)
        self.path=path
        # print 'inititalizaton path',self.path
        self.contour_cc = contour_cc
        self.graph = GraphCC() # graph with all the cc inside the room
        self.name = ''
        self.window = GraphCC()
        self.id = id
        self.width = 0
        self.contour_from_cc = None
        self.floor = None #floor cc
        self.floor_graph = GraphCC()

    def set_floor(self, door_width):
        self.floor = self.graph.getMaxNode()
        self.floor_graph.add_node(self.floor)
        for cc in self.graph.get_indices():
            if cc.area > 9*(door_width**2) and cc.id != 0:
                self.floor_graph.add_node(cc)
        return self.floor_graph
        # self.graph.remove_node_and_edge(self.floor)
        # #print 'il pavimento della stanza '+self.name+' is la cc '+str(self.floor.id)

    def linked_to_floor(self, cc):
        for cc_f in self.floor_graph.get_indices():
            # #print 'floor cc = '+str(cc_f.id)
            linked = self.graph.exist_link(cc, cc_f)
            if linked:
                return True
        return False

    def get_num_shapes(self):
        return self.graph.get_num_shapes()

    def set_contour(self):
        s = self.graph.get_extremes()
        self.contour_cc.x_min = s[0]
        self.contour_cc.y_min = s[2]
        self.contour_cc.set_coordinates()

    def add_wall_node(self):
        #add wall node with id = 0
        cc = CC(0)
        cc.image = cv2.inRange(self.graph.img_red_wall, (0,0,255), (0,0,255))
        cc.image=imop.add_pad_single_pixel(cc.image)
        cc.set_coordinates()
        cc.numPixel=(cc.image.sum()/255)
        cc.check_shape()
        cc.create_histogram(bins1, bins2)
        self.graph.add_node(cc)
        # self.graph.check_wall_adjacence(self.width)
        # if self.graph.width == 0:
        #     self.graph.width = self.width
        # self.graph.save_graph(self.path, 'graph.txt',dictionary=False)

    def draw_edges(self, path_edges):
        self.floor = self.graph.getMaxNode()
        img, img2 = self.graph.save_edges(self.floor)
        cc = self.contour_cc
        if cc.x_max == 0:
            cc.x_min, cc.x_max, cc.y_min, cc.y_max = self.graph.get_extremes()
        cc.find_contour()
        cnt = cc.contourSimple[0]
        for xA in cnt:
            xA[0][0]+=cc.x_min
            xA[0][1]+=cc.y_min
        cv2.drawContours(img, [cnt], 0, (0,255,255), 2)
        wall = self.graph.get_node_from_id(0)
        for cnt in wall.contourSimple:
            cv2.drawContours(img, [cnt], 0, (0,0,255), 2)
        cv2.circle(img,(self.floor.center_x, self.floor.center_y), 3, (255,0,255), -1)
        cv2.circle(img2,(self.floor.center_x, self.floor.center_y), 3, (255,0,255), -1)
        img = img[cc.y_min:cc.y_max, cc.x_min:cc.x_max]
        img2 = img2[cc.y_min:cc.y_max, cc.x_min:cc.x_max]
        cv2.imwrite(path_edges+self.name+'_edges.png', img)
        cv2.imwrite(path_edges+self.name+'edges_graph.png', img2)

    def draw_graph(self, img):
        # #print 'adding edges at the room '+self.label[0].name
        if self.graph.size()>0:
            img = self.graph.save_edges2(self.floor, img)
            wall = self.graph.get_node_from_id(0)
            for cnt in wall.contourSimple:
                cv2.drawContours(img, [cnt], 0, (0,0,255), 2)
            cc = self.contour_cc
            if cc.x_max == 0:
                cc.x_min, cc.x_max, cc.y_min, cc.y_max = self.graph.get_extremes()
            cc.find_contour()
            cnt = cc.contourSimple[0]
            for xA in cnt:
                xA[0][0]+=cc.x_min
                xA[0][1]+=cc.y_min
            cv2.drawContours(img, [cnt], 0, (0,255,255), 2)
            cv2.circle(img,(self.floor.center_x, self.floor.center_y), 3, (255,0,255), -1)
        return img

    def get_wall(self):
        wall = self.graph.get_node_from_id(0)
        return wall

    def check_wall_link(self, cc):
        wall = self.get_wall()
        if wall.id != cc.id:
            adj, dst = imop.touching_dilation(cc, wall, 3*self.width)
            if adj:
                return True
        return False

    def _draw_contour_from_cc_inside(self):
        if self.graph.size() > 0:
            cc = CC(0)
            cc.x_min, cc.x_max, cc.y_min, cc.y_max = self.graph.get_extremes()
            cc.width = cc.x_max-cc.x_min+1
            cc.height = cc.y_max-cc.y_min+1
            cc.image = np.zeros((cc.height, cc.width))
            cc.set_coordinates()

            for cc2 in self.graph.get_indices():
                if cc2.id != 0:
                    img1, img2 = imop.same_size(cc, cc2)
                    cc.image = img1+img2
                    cc.set_coordinates()

            width = self.width
            width = int(2*width)+3
            kernel = np.ones((width,width),np.uint8)
            closure = cv2.morphologyEx(cc.image, cv2.MORPH_CLOSE, kernel)
            cv2.imwrite(self.path+'closed_contour_from_cc.png', closure)
            cc.image = closure
            self.contour_from_cc = cc


    def add_label(self, text):
        label = Label()
        cx, cy = text[1]
        x1, y1, x2, y2 = text[2]
        label.set_label(text[0], cx, cy, x1, y1, x2, y2)
        self.label.append(label)

    def save_windows(self,debug_mode):
        if self.window.size()>0:
            path = self.path+'windows/'
            if not(os.path.exists(path)):
                os.mkdir(path)
            # self.window.save_graph(path,'graph.txt',dictionary=False)
            self.window.save_images(path)
            ccWhole = CC(0)
            img = np.array(self.window.img_red_wall, dtype=np.uint8)
            ccWhole.image=cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            ccWhole.x_min = 0
            ccWhole.y_min = 0
            ccWhole.set_coordinates()
            img = self.window.img_red_wall
            for cc in self.window.get_indices():
                imgWin, image = imop.same_size(cc,ccWhole)
                imgWin = np.array(imgWin,dtype=np.uint8)
                imgWin = cv2.cvtColor(imgWin, cv2.COLOR_GRAY2BGR)
                img = np.where(imgWin!=(0,0,0), (0,255,0), img)
            cc = self.contour_cc
            # if debug_mode:
            #     cv2.imwrite(path+'room_with_new_windows.png', img[cc.y_min:cc.y_max, cc.x_min:cc.x_max])



        # self.find_objects()
        # self.graph_cc.create_edges(self.width, dictionary=False)
        # self.add_object(self.graph_cc)

    def save_images(self):
        cc = self.contour_cc
        if len(self.label) == 0:
            self.add_none()

        # #print 'saving room '+self.label[0].name
        cv2.imwrite(self.path+'contour.png', cc.image)
        cv2.imwrite(self.path+'no_text.png', self.graph.img_no_text) #[cc.y_min:cc.y_max, cc.x_min:cc.x_max])
        if self.label[0].name != 'external' and cc!=None and self.graph.size() != 0:
            self._draw_contour_from_cc_inside()
            cv2.imwrite(self.path+'red_wall.png', self.graph.img_red_wall[cc.y_min:cc.y_max, cc.x_min:cc.x_max])


    def set_images(self, red_wall, no_wall, no_text):
        self.graph.img_red_wall = red_wall
        self.graph.img_no_wall = no_wall
        self.graph.img_no_text = no_text
        self.window.img_red_wall = red_wall
        self.window.img_no_text = no_text

    def add_none(self):
        cc = self.contour_cc
        label = Label()
        label.set_label('None', cc.center_x, cc.center_y, cc.center_x-20, cc.center_y-20, cc.center_x+20, cc.center_x+20)
        self.label.append(label)

    def update_graph(self, avg_door):
        self.add_wall_node()
        self.graph.create_edges(self.width,dictionary=False)
        self.set_floor(avg_door)
        i = 0
        for cc in self.floor_graph.get_indices():
            cv2.imwrite(self.path+'floor_'+str(i)+'.png', cc.image)
            self.graph.floor_adjacence(cc, dictionary=False)
            i+=1
            self.graph.save_graph(self.path,'graph.txt',dictionary=False)
            self.draw_edges(self.path)

    def calculate_graph(self, avg_door):
        # print 'inside door function',self.path
        if self.graph.size() > 0:
            if len(self.label)==0:
                self.add_none()
            name = ''
            for label in self.label:
                name+=label.name+'-'
            # print '--name',name, ';',len(name)
            if len(name)>0:
                name = name[0:-1]
            path = self.path+name+'/'
            # print 'new path', path
            i=1
            while (os.path.exists(path)):
                i+=1
                name2 = name+' '+str(i)
                path = self.path+name2+'/'
                # print 'loop path',path
            self.path = path
            # print 'exit loop path', self.path
            # self.label[0].name = label
            if not(os.path.exists(path)):
                os.mkdir(path)
            # #print 'calculate graph of the room '+name

            if self.id != 0:
                    if self.width==0:
                        self.width = imop.calculate_avg_width(self.graph.img_no_wall)
                    self.add_wall_node()
                    self.graph.create_edges(self.width,dictionary=False)
                    self.set_floor(avg_door)
                    i = 0
                    for cc in self.floor_graph.get_indices():
                        cv2.imwrite(self.path+'floor_'+str(i)+'.png', cc.image)
                        self.graph.floor_adjacence(cc,dictionary=False)
                        i+=1
                    self.graph.save_graph(self.path,'graph.txt',dictionary=False)
                    self.draw_edges(self.path)
            self.graph.save_images(self.path)

        # print 'before exiting path',self.path
        # imop.crop_image(self.contour_cc, self.graph.img_no_text[ymin:ymax, xmin:xmax])
        #     self.graph.save_crop_image(self.path)


    def add_node(self, cc):
        self.graph.add_node(cc)

    def add_window(self, cc):
        self.window.add_node(cc)

    # def find_objects(self):
    #     if self.graph_cc.size() is not 0:
    #         self.graph_cc.create_edges(self.width, dictionary=False)
    #         size = 0
    #         graph_cc = deepcopy(self.graph_cc)
    #         i = 1
    #         while graph_cc.size() != 0:
    #             graph = GraphCC()
    #             max = graph_cc.getMaxNode()
    #             graph.add_node(max, hist=True)
    #             graph = self.explore(max,graph_cc)
    #             graph2 = deepcopy(graph)
    #             self.add_object(graph2)
    #             graph2.img_red_wall = self.image
    #             graph2.save_crop_image(self.path+'object_'+str(i)+'.png')
    #             graph2.save_images(self.path+'object_'+str(i)+'_cc_')
    #             # #print 'graph.size = '+str(graph2.size())
    #             graph_cc.purge(graph)
    #             # #print 'graph.size = '+str(graph2.size())
    #             i+=1


#not used!
    def explore(self, node, graph):
        for cc in node.get_linked():
            if cc.id != node.id:
                search = graph.add_node2(cc)

                if search is not 0:
                    graph = self.explore(cc, graph)
        return graph

    def get_object(self, obj):
        return self._obj_graphs[self._obj_index[obj]]

    def add_object(self, obj): #obj must be a object graph!
        if obj not in self._obj_index:
            self._obj_graphs.append(obj)
            self._obj_index[obj] = len(self._obj_graphs) - 1
        return self.get_object(obj)

    def add_object2(self, obj): #check if the object can be inside the room!
        if obj not in self._obj_index and self.check_isIn(obj):
            self._obj_graphs.append(obj)
            self._obj_index[obj] = len(self._obj_graphs) - 1

    def remove_object(self,obj):
        if obj in self._obj_index:
            self._obj_graphs.remove(obj)
            for n in self._obj_graphs[self._obj_index[obj]:len(self._obj_graphs)]:
                self._obj_index[n] = self._obj_index[n] - 1
            del self._obj_index[obj]

    def get_indices(self):
        return self._obj_index

    def size(self):
        return len(self._obj_graphs)

    def isIn(self,obj):
        if obj in self._obj_index:
            return True
        else:
            return False

    def check_isIn(self, obj):
        count = 0
        purgandi = GraphCC()
        for cc in obj.get_indices:
            isin = self.contour_cc.is_inside(cc)
            if isin:
                count+=1
            else:
                purgandi.add_node(cc)
        if count == 0:
            #print 'obj not found in the room '+self.label+' with id = '+str(self.id)
            self.remove_object(obj)
            return False
        elif count == len(obj.size()):
            #print 'obj found in the room '+self.label+' with id = '+str(self.id)
            self.add_object(obj)
            return True
        else:
            obj.purge(purgandi)
            #print 'obj found only partially in the room '+self.label+' with id = '+str(self.id)
            self.add_object(obj)
            return True
